/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dal;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Ads;
import model.Advertiser;

/**
 *
 * @author FPTSHOP
 */
public class AdsDBContext extends DBContext {

    public Ads addAds(String AdsID, String name, InputStream img) {
        try {

            String sql = "INSERT INTO [Ads]\n"
                    + "           ([AdsID]\n"
                    + "           ,[Name]\n"
                    + "           ,[img])\n"
                    + "     VALUES\n"
                    + "           (? "
                    + "           ,? "
                    + "           ,?) ";
            PreparedStatement stm = connection.prepareStatement(sql);

            stm.setString(1, AdsID);
            stm.setString(2, name);
            stm.setBlob(3, img);
            stm.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(AdsDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public ArrayList<Ads> getAds() throws IOException {
        ArrayList<Ads> ads = new ArrayList<>();
        try {
            String sql = "SELECT [AdsID]\n"
                    + "      ,[Name]\n"
                    + "      ,[img]\n"
                    + "  FROM [Ads]";

            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Ads a = new Ads();
                Advertiser Ad = new Advertiser();
                Ad.setAdsid(rs.getString("AdsID"));
                a.setName(rs.getString("Name"));
                Blob blob = rs.getBlob("img");
                InputStream inputStream = blob.getBinaryStream();
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                byte[] buffer = new byte[4096];
                int bytesRead = -1;

                while ((bytesRead = inputStream.read(buffer)) != -1) {
                    outputStream.write(buffer, 0, bytesRead);
                }

                byte[] imageBytes = outputStream.toByteArray();
                String base64Image = Base64.getEncoder().encodeToString(imageBytes);

                inputStream.close();
                outputStream.close();
                a.setBase64Image(base64Image);
                a.setAdvers(Ad);
                ads.add(a);
            }
        } catch (SQLException ex) {
            Logger.getLogger(AdsDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ads;
    }
    
    
    
    
    
}
