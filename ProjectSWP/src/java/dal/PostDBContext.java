/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dal;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.Part;
import model.AccountPost;
import model.Post;
import java.util.Base64;

import java.util.UUID;
import model.Account;
import model.Comment;
import model.MutedTag;
import model.MutedUser;
import model.PinPost;
import model.PostHistory;
import model.PostLike;
import model.PostTag;
import model.Tag;
import model.Reply;

/**
 *
 * @author BK
 */
public class PostDBContext extends DBContext {

    ResultSet rs = null;
    PreparedStatement stm = null;

    public ArrayList<Post> GetWorks() throws IOException {
        ArrayList<Post> pro = new ArrayList<>();
        try {
            String sql = "SELECT workid, [name], [description], img, publishdate, [like] FROM Work";

            PreparedStatement stm = connection.prepareStatement(sql);
            rs = stm.executeQuery();
            while (rs.next()) {
                Post p = new Post();
                AccountPost Ap = new AccountPost();
                Ap.setWorkid(rs.getString("workid"));
                p.setName(rs.getString("name"));
                p.setDescription(rs.getString("description"));

                Blob blob = rs.getBlob("img");

                InputStream inputStream = blob.getBinaryStream();
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                byte[] buffer = new byte[4096];
                int bytesRead = -1;

                while ((bytesRead = inputStream.read(buffer)) != -1) {
                    outputStream.write(buffer, 0, bytesRead);
                }

                byte[] imageBytes = outputStream.toByteArray();
                String base64Image = Base64.getEncoder().encodeToString(imageBytes);

                inputStream.close();
                outputStream.close();
                p.setLike(rs.getInt("like"));
                p.setBase64Image(base64Image);

                p.setDate(rs.getDate("publishdate"));
                p.setAp(Ap);
                pro.add(p);
            }
        } catch (SQLException ex) {
            Logger.getLogger(PostDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return pro;
    }

    public ArrayList<Post> GetWork(int pageIndex) throws IOException {
        ArrayList<Post> pro = new ArrayList<>();
        try {
            String sql = "SELECT A.workid, username, [name], [description], img, publishdate, [like] FROM Work A JOIN AccountWork B\n"
                    + "ON A.workid = B.workid\n"
                    + "                    ORDER BY publishdate asc\n"
                    + "                    OFFSET 4*(? - 1) ROWS \n"
                    + "                    FETCH FIRST 4 ROWS ONLY";

            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, pageIndex);
            ResultSet rs = stm.executeQuery();

            while (rs.next()) {
                Account a = new Account();
                Post p = new Post();
                AccountPost Ap = new AccountPost();
                a.setUsername(rs.getString("username"));
                Ap.setA(a);
                Ap.setWorkid(rs.getString("workid"));
                p.setName(rs.getString("name"));
                p.setDescription(rs.getString("description"));

                Blob blob = rs.getBlob("img");

                InputStream inputStream = blob.getBinaryStream();
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                byte[] buffer = new byte[4096];
                int bytesRead = -1;

                while ((bytesRead = inputStream.read(buffer)) != -1) {
                    outputStream.write(buffer, 0, bytesRead);
                }

                byte[] imageBytes = outputStream.toByteArray();
                String base64Image = Base64.getEncoder().encodeToString(imageBytes);

                inputStream.close();
                outputStream.close();
                p.setLike(rs.getInt("like"));
                p.setBase64Image(base64Image);

                p.setDate(rs.getDate("publishdate"));
                p.setAp(Ap);
                pro.add(p);
            }
        } catch (SQLException ex) {
            Logger.getLogger(PostDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return pro;
    }

    public ArrayList<Post> GetUserPinned(int page, String username) throws IOException {
        ArrayList<Post> pro = new ArrayList<>();
        try {
            String sql = "SELECT c.workid 'workid', [name], [description], img, publishdate, [like] FROM Work A JOIN AccountWork B\n"
                    + "ON A.workid = B.workid\n"
                    + "JOIN SavedPost C\n"
                    + "ON A.workid = C.workid\n"
                    + "WHERE c.username = ?\n"
                    + "ORDER BY publishdate DESC\n"
                    + "OFFSET 4*(? - 1) ROWS \n"
                    + "FETCH FIRST 4 ROWS ONLY;";

            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, username);
            stm.setInt(2, page);
            rs = stm.executeQuery();
            while (rs.next()) {
                Post p = new Post();
                AccountPost Ap = new AccountPost();
                Ap.setWorkid(rs.getString("workid"));
                p.setName(rs.getString("name"));
                p.setDescription(rs.getString("description"));

                Blob blob = rs.getBlob("img");

                InputStream inputStream = blob.getBinaryStream();
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                byte[] buffer = new byte[4096];
                int bytesRead = -1;

                while ((bytesRead = inputStream.read(buffer)) != -1) {
                    outputStream.write(buffer, 0, bytesRead);
                }

                byte[] imageBytes = outputStream.toByteArray();
                String base64Image = Base64.getEncoder().encodeToString(imageBytes);

                inputStream.close();
                outputStream.close();
                p.setLike(rs.getInt("like"));
                p.setBase64Image(base64Image);

                p.setDate(rs.getDate("publishdate"));
                p.setAp(Ap);
                pro.add(p);
            }
        } catch (SQLException ex) {
            Logger.getLogger(PostDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return pro;
    }

    public ArrayList<Post> GetProfileWork(int pageIndex, String username) throws IOException {
        ArrayList<Post> pro = new ArrayList<>();
        try {
            String sql = "SELECT workid, [name], [description], img, publishdate, [like] FROM Work\n"
                    + "ORDER BY publishdate DESC \n"
                    + "OFFSET 4*(? - 1) ROWS \n"
                    + "FETCH FIRST 4 ROWS ONLY;";

            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, pageIndex);
            ResultSet rs = stm.executeQuery();

            while (rs.next()) {
                Post p = new Post();
                AccountPost Ap = new AccountPost();
                Ap.setWorkid(rs.getString("workid"));
                p.setName(rs.getString("name"));
                p.setDescription(rs.getString("description"));

                Blob blob = rs.getBlob("img");

                InputStream inputStream = blob.getBinaryStream();
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                byte[] buffer = new byte[4096];
                int bytesRead = -1;

                while ((bytesRead = inputStream.read(buffer)) != -1) {
                    outputStream.write(buffer, 0, bytesRead);
                }

                byte[] imageBytes = outputStream.toByteArray();
                String base64Image = Base64.getEncoder().encodeToString(imageBytes);

                inputStream.close();
                outputStream.close();
                p.setLike(rs.getInt("like"));
                p.setBase64Image(base64Image);

                p.setDate(rs.getDate("publishdate"));
                p.setAp(Ap);
                pro.add(p);
            }
        } catch (SQLException ex) {
            Logger.getLogger(PostDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return pro;
    }

    public ArrayList<Post> GetProfileWorkPage(int pageIndex, String username) throws IOException {
        ArrayList<Post> pro = new ArrayList<>();
        try {
            String sql = "SELECT A.workid, [name], [description], img, publishdate, [like] FROM Work A JOIN AccountWork B\n"
                    + "ON A.workid = B.workid\n"
                    + "WHERE username = ?\n"
                    + "ORDER BY publishdate DESC\n"
                    + "OFFSET 4*(? - 1) ROWS\n"
                    + "FETCH FIRST 4 ROWS ONLY";

            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, username);
            stm.setInt(2, pageIndex);
            ResultSet rs = stm.executeQuery();

            while (rs.next()) {
                Post p = new Post();
                AccountPost Ap = new AccountPost();
                Ap.setWorkid(rs.getString("workid"));
                p.setName(rs.getString("name"));
                p.setDescription(rs.getString("description"));

                Blob blob = rs.getBlob("img");

                InputStream inputStream = blob.getBinaryStream();
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                byte[] buffer = new byte[4096];
                int bytesRead = -1;

                while ((bytesRead = inputStream.read(buffer)) != -1) {
                    outputStream.write(buffer, 0, bytesRead);
                }

                byte[] imageBytes = outputStream.toByteArray();
                String base64Image = Base64.getEncoder().encodeToString(imageBytes);

                inputStream.close();
                outputStream.close();
                p.setLike(rs.getInt("like"));
                p.setBase64Image(base64Image);

                p.setDate(rs.getDate("publishdate"));
                p.setAp(Ap);
                pro.add(p);
            }
        } catch (SQLException ex) {
            Logger.getLogger(PostDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return pro;
    }

    public ArrayList<PinPost> GetPin(String username) {
        ArrayList<PinPost> pin = new ArrayList<>();
        try {
            String sql = "SELECT username, workid FROM SavedPost\n"
                    + "WHERE username = ?";

            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, username);
            rs = stm.executeQuery();
            while (rs.next()) {
                Account a = new Account();
                AccountPost AP = new AccountPost();
                PinPost SP = new PinPost();
                a.setUsername(rs.getString("username"));
                AP.setWorkid(rs.getString("workid"));
                SP.setA(a);
                SP.setAp(AP);
                pin.add(SP);
            }
        } catch (SQLException ex) {
            Logger.getLogger(PostDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return pin;
    }

    public Post AddPin(String username, String WorkID) {
        try {

            String sql = "INSERT INTO SavedPost (username, workid)\n"
                    + "VALUES (?, ?)";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, username);
            stm.setString(2, WorkID);
            stm.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(PostDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public ArrayList<MutedTag> GetMutedTag(String username, String muteduser) {
        ArrayList<MutedTag> mts = new ArrayList<>();
        try {
            String sql = "SELECT username, tagid FROM UserMutedTag\n"
                    + "WHERE username = ? AND tagid = ?";

            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, username);
            stm.setString(2, muteduser);
            rs = stm.executeQuery();
            while (rs.next()) {
                Account a = new Account();
                Tag t = new Tag();
                MutedTag mt = new MutedTag();
                a.setUsername(rs.getString("username"));
                t.setTagid(rs.getString("tagid"));
                mt.setT(t);
                mt.setA(a);
                mts.add(mt);
            }
        } catch (SQLException ex) {
            Logger.getLogger(PostDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return mts;
    }

    public ArrayList<PostLike> GetLike(String username) {
        ArrayList<PostLike> like = new ArrayList<>();
        try {
            String sql = "SELECT username, workid FROM PostLike\n"
                    + "WHERE username = ?";

            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, username);
            rs = stm.executeQuery();
            while (rs.next()) {
                Account a = new Account();
                AccountPost AP = new AccountPost();
                PostLike SP = new PostLike();
                a.setUsername(rs.getString("username"));
                AP.setWorkid(rs.getString("workid"));
                SP.setA(a);
                SP.setAp(AP);
                like.add(SP);
            }
        } catch (SQLException ex) {
            Logger.getLogger(PostDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return like;
    }

    public Post AddLike(String username, String WorkID) {
        try {

            String sql = "INSERT INTO PostLike (username, workid)\n"
                    + "VALUES (?, ?)";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, username);
            stm.setString(2, WorkID);
            stm.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(PostDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public Post DeleteLike(String username, String WorkID) {
        try {

            String sql = "DELETE FROM PostLike\n"
                    + "WHERE username = ? AND workid = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, username);
            stm.setString(2, WorkID);
            stm.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(PostDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public Post DeletePin(String username, String WorkID) {
        try {

            String sql = "DELETE FROM SavedPost\n"
                    + "WHERE username = ? AND workid = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, username);
            stm.setString(2, WorkID);
            stm.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(PostDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public ArrayList<Post> GetWorkWname(int page, String username) throws IOException {
        ArrayList<Post> pro = new ArrayList<>();
        try {
            String sql = "SELECT A.workid, username, [name], [description], img, publishdate, [like] FROM Work A JOIN AccountWork B\n"
                    + "ON A.workid = B.workid\n"
                    + "WHERE [name] LIKE ?"
                    + "                    ORDER BY publishdate asc\n"
                    + "                    OFFSET 4*(? - 1) ROWS \n"
                    + "                    FETCH FIRST 4 ROWS ONLY";

            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, "%" + username + "%");
            stm.setInt(2, page);
            rs = stm.executeQuery();
            while (rs.next()) {
                Post p = new Post();
                AccountPost Ap = new AccountPost();
                Account a = new Account();
                a.setUsername(rs.getString("username"));
                Ap.setWorkid(rs.getString("workid"));
                Ap.setA(a);
                p.setName(rs.getString("name"));
                p.setDescription(rs.getString("description"));

                Blob blob = rs.getBlob("img");

                InputStream inputStream = blob.getBinaryStream();
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                byte[] buffer = new byte[4096];
                int bytesRead = -1;

                while ((bytesRead = inputStream.read(buffer)) != -1) {
                    outputStream.write(buffer, 0, bytesRead);
                }

                byte[] imageBytes = outputStream.toByteArray();
                String base64Image = Base64.getEncoder().encodeToString(imageBytes);

                inputStream.close();
                outputStream.close();
                p.setLike(rs.getInt("like"));
                p.setBase64Image(base64Image);

                p.setDate(rs.getDate("publishdate"));
                p.setAp(Ap);
                pro.add(p);
            }
        } catch (SQLException ex) {
            Logger.getLogger(PostDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return pro;
    }

    public ArrayList<Post> GetWorkOption(int page, String workname, int tag, int savednum, String order) throws IOException {
        ArrayList<Post> pro = new ArrayList<>();
        try {
            String sql = "SELECT DISTINCT A.workid 'workid',E.username 'username', displayname 'displayname', [name], [description], img, publishdate, [like], [view], saved, private, comments FROM Work A JOIN AccountWork B\n"
                    + "ON A.workid = B.workid\n"
                    + "JOIN WorkTag C\n"
                    + "ON A.workid = C.workid\n"
                    + "JOIN Tag D\n"
                    + "ON C.tagid = D.tagid\n"
                    + "JOIN Account E\n"
                    + "ON B.username = E.username\n";
            if (tag == 0) {
                sql += "WHERE [name] LIKE ? AND saved >= ?\n";
            } else {
                sql += "WHERE [name] LIKE ? AND C.tagid = ? AND saved >= ?\n";
            }

            if (order.equalsIgnoreCase("asc") || order.equalsIgnoreCase("desc")) {
                sql += "ORDER BY publishdate " + order + "\n";
            } else if (order.equalsIgnoreCase("pop")) {
                sql += "ORDER BY [view] DESC \n";
            } else {
                sql += "ORDER BY publishdate DESC \n";
            }
            sql += "OFFSET 4*(? - 1) ROWS\n"
                    + "FETCH FIRST 4 ROWS ONLY";

            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, "%" + workname + "%");
            if (tag == 0) {
                stm.setInt(2, savednum);
                stm.setInt(3, page);
            } else {
                stm.setInt(2, tag);
                stm.setInt(3, savednum);
                stm.setInt(4, page);
            }

            rs = stm.executeQuery();
            while (rs.next()) {
                Post p = new Post();
                AccountPost Ap = new AccountPost();
                Account a = new Account();
                a.setUsername(rs.getString("username"));
                Ap.setWorkid(rs.getString("workid"));
                Ap.setA(a);
                a.setDisplayname(rs.getString("displayname"));
                p.setName(rs.getString("name"));
                p.setDescription(rs.getString("description"));

                Blob blob = rs.getBlob("img");

                InputStream inputStream = blob.getBinaryStream();
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                byte[] buffer = new byte[4096];
                int bytesRead = -1;

                while ((bytesRead = inputStream.read(buffer)) != -1) {
                    outputStream.write(buffer, 0, bytesRead);
                }

                byte[] imageBytes = outputStream.toByteArray();
                String base64Image = Base64.getEncoder().encodeToString(imageBytes);

                inputStream.close();
                outputStream.close();
                p.setLike(rs.getInt("like"));
                p.setSaved(rs.getInt("saved"));
                p.setView(rs.getInt("view"));
                p.setBase64Image(base64Image);
                p.setPrivacy(rs.getInt("private"));
                p.setComments(rs.getInt("comments"));
                p.setDate(rs.getDate("publishdate"));
                p.setAp(Ap);
                pro.add(p);
            }
        } catch (SQLException ex) {
            Logger.getLogger(PostDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return pro;
    }

    public ArrayList<PostHistory> GetPostHistoryDate(String username, int subcription) throws IOException {
        ArrayList<PostHistory> history = new ArrayList<>();
        try {
            String sql = "SELECT DISTINCT [date] FROM PostView\n";
            if (subcription == 0) {
                sql += "WHERE username = ? AND [date] > dateadd(wk, datediff(wk, 0, getdate()), 0)\n";
            } else {
                sql += "WHERE username = ?\n";
            }
            sql += "ORDER BY [date] DESC";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, username);

            rs = stm.executeQuery();
            while (rs.next()) {
                PostHistory hp = new PostHistory();
                hp.setDate(rs.getDate("date"));
                history.add(hp);
            }
        } catch (SQLException ex) {
            Logger.getLogger(PostDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return history;
    }

    public ArrayList<PostTag> GetTags(String workname, int savednum, String order) throws IOException {
        ArrayList<PostTag> Tags = new ArrayList<>();
        try {
            String sql = "SELECT A.workid 'workid', C.tagid 'tagid', D.tagname 'tagname', saved FROM Work A JOIN AccountWork B\n"
                    + "ON A.workid = B.workid\n"
                    + "JOIN WorkTag C\n"
                    + "ON A.workid = C.workid\n"
                    + "JOIN Tag D\n"
                    + "ON C.tagid = D.tagid\n"
                    + "WHERE [name] LIKE ? AND saved >= ?\n";

            if (order.equalsIgnoreCase("asc") || order.equalsIgnoreCase("desc")) {
                sql += "ORDER BY publishdate " + order + "\n";
            } else {
                sql += "ORDER BY view " + order + "\n";
            }

            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, "%" + workname + "%");
            stm.setInt(2, savednum);

            rs = stm.executeQuery();
            while (rs.next()) {
                Post p = new Post();
                PostTag pt = new PostTag();
                AccountPost ap = new AccountPost();
                Tag t = new Tag();
                t.setTagid(rs.getString("tagid"));
                ap.setWorkid(rs.getString("workid"));
                p.setAp(ap);
                pt.setTag(t);
                pt.setPost(p);

                Tags.add(pt);
            }
        } catch (SQLException ex) {
            Logger.getLogger(PostDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Tags;
    }

    public ArrayList<PostTag> GetWorkTag(String workid) throws IOException {
        ArrayList<PostTag> Tags = new ArrayList<>();
        try {
            String sql = "SELECT A.workid 'workid', C.tagid 'tagid', D.tagname 'tagname', saved FROM Work A JOIN AccountWork B\n"
                    + "ON A.workid = B.workid\n"
                    + "JOIN WorkTag C\n"
                    + "ON A.workid = C.workid\n"
                    + "JOIN Tag D\n"
                    + "ON C.tagid = D.tagid\n"
                    + "WHERE A.workid = ?\n";

            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, workid);

            rs = stm.executeQuery();
            while (rs.next()) {
                Post p = new Post();
                PostTag pt = new PostTag();
                AccountPost ap = new AccountPost();
                Tag t = new Tag();
                t.setTagid(rs.getString("tagid"));
                ap.setWorkid(rs.getString("workid"));
                p.setAp(ap);
                pt.setTag(t);
                pt.setPost(p);

                Tags.add(pt);
            }
        } catch (SQLException ex) {
            Logger.getLogger(PostDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Tags;
    }

    public ArrayList<Post> LoggedWorkOption(int page, String workname, int tag, int savednum, String order, ArrayList<MutedUser> mw, ArrayList<MutedTag> mt) throws IOException {
        ArrayList<Post> pro = new ArrayList<>();
        String mutedwork = "", mutedtag = "";

        for (int i = 0; i < mw.size(); i++) {
            mutedwork += " AND B.username != " + mw.get(i).getA().getUsername() + " ";
        }

        for (int i = 0; i < mt.size(); i++) {
            mutedtag += " AND C.tagid != " + mt.get(i).getT().getTagid() + " ";
        }

        try {
            String sql = "SELECT DISTINCT A.workid 'workid', username, [name], [description], img, publishdate, [like], [view], saved FROM Work A JOIN AccountWork B\n"
                    + "ON A.workid = B.workid\n"
                    + "JOIN WorkTag C\n"
                    + "ON B.workid = C.workid\n";
            if (tag == 0) {
                sql += "WHERE [name] LIKE ? AND saved >= ?" + mutedwork + mutedtag + "\n";
            } else {
                sql += "WHERE [name] LIKE ? AND C.tagid = ? AND saved >= ?" + mutedwork + mutedtag + "\n";
            }

            if (order.equalsIgnoreCase("asc") || order.equalsIgnoreCase("desc")) {
                sql += "ORDER BY publishdate " + order + "\n";
            } else {
                sql += "ORDER BY view " + order + "\n";
            }
            sql += "OFFSET 4*(? - 1) ROWS\n"
                    + "FETCH FIRST 4 ROWS ONLY";

            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, "%" + workname + "%");
            if (tag == 0) {
                stm.setInt(2, savednum);
                stm.setInt(3, page);
            } else {
                stm.setInt(2, tag);
                stm.setInt(3, savednum);
                stm.setInt(4, page);
            }

            rs = stm.executeQuery();
            while (rs.next()) {
                Post p = new Post();
                AccountPost Ap = new AccountPost();
                Account a = new Account();
                a.setUsername(rs.getString("username"));
                Ap.setWorkid(rs.getString("workid"));
                Ap.setA(a);
                p.setName(rs.getString("name"));
                p.setDescription(rs.getString("description"));

                Blob blob = rs.getBlob("img");

                InputStream inputStream = blob.getBinaryStream();
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                byte[] buffer = new byte[4096];
                int bytesRead = -1;

                while ((bytesRead = inputStream.read(buffer)) != -1) {
                    outputStream.write(buffer, 0, bytesRead);
                }

                byte[] imageBytes = outputStream.toByteArray();
                String base64Image = Base64.getEncoder().encodeToString(imageBytes);

                inputStream.close();
                outputStream.close();
                p.setLike(rs.getInt("like"));
                p.setSaved(rs.getInt("saved"));
                p.setView(rs.getInt("view"));
                p.setBase64Image(base64Image);

                p.setDate(rs.getDate("publishdate"));
                p.setAp(Ap);
                pro.add(p);
            }
        } catch (SQLException ex) {
            Logger.getLogger(PostDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return pro;
    }

    public ArrayList<Post> GetUserWorks(int page, String username) throws IOException {
        ArrayList<Post> pro = new ArrayList<>();
        try {
            String sql = "SELECT A.workid, C.username 'username', displayname, [name], [description], img, publishdate, [like] FROM Work A JOIN AccountWork B\n"
                    + "ON A.workid = B.workid\n"
                    + "JOIN Account C\n"
                    + "ON B.username = C.username\n"
                    + "WHERE C.username LIKE ?\n";
            if (page != 0) {
                sql += "                    ORDER BY publishdate asc\n"
                        + "                    OFFSET 4*(? - 1) ROWS \n"
                        + "                    FETCH FIRST 4 ROWS ONLY";
            }
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, "%" + username + "%");
            if (page != 0) {
                stm.setInt(2, page);
            }
            rs = stm.executeQuery();
            while (rs.next()) {
                Post p = new Post();
                AccountPost Ap = new AccountPost();
                Account a = new Account();
                a.setUsername(rs.getString("username"));
                a.setDisplayname(rs.getString("displayname"));
                Ap.setWorkid(rs.getString("workid"));
                Ap.setA(a);
                p.setName(rs.getString("name"));
                p.setDescription(rs.getString("description"));

                Blob blob = rs.getBlob("img");

                InputStream inputStream = blob.getBinaryStream();
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                byte[] buffer = new byte[4096];
                int bytesRead = -1;

                while ((bytesRead = inputStream.read(buffer)) != -1) {
                    outputStream.write(buffer, 0, bytesRead);
                }

                byte[] imageBytes = outputStream.toByteArray();
                String base64Image = Base64.getEncoder().encodeToString(imageBytes);

                inputStream.close();
                outputStream.close();
                p.setLike(rs.getInt("like"));
                p.setBase64Image(base64Image);

                p.setDate(rs.getDate("publishdate"));
                p.setAp(Ap);
                pro.add(p);
            }
        } catch (SQLException ex) {
            Logger.getLogger(PostDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return pro;
    }

    public ArrayList<Post> GetUserViewByDate(String username, String date) throws IOException {
        ArrayList<Post> ps = new ArrayList<>();
        try {
            String sql = "SELECT DISTINCT A.workid 'workid', B.username 'username', [name], [description], img, publishdate, [like], [private], [comments] FROM Work A JOIN AccountWork B\n"
                    + "ON A.workid = B.workid\n"
                    + "join PostView C\n"
                    + "ON A.workid = C.workid\n"
                    + "WHERE c.username = ? AND [date] = ?\n";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, username);
            stm.setString(2, date);
            rs = stm.executeQuery();
            while (rs.next()) {
                Post p = new Post();
                AccountPost Ap = new AccountPost();
                Account a = new Account();
                a.setUsername(rs.getString("username"));
                Ap.setWorkid(rs.getString("workid"));
                Ap.setA(a);
                p.setName(rs.getString("name"));
                p.setDescription(rs.getString("description"));
                p.setPrivacy(rs.getInt("private"));
                p.setComments(rs.getInt("comments"));
                Blob blob = rs.getBlob("img");

                InputStream inputStream = blob.getBinaryStream();
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                byte[] buffer = new byte[4096];
                int bytesRead = -1;

                while ((bytesRead = inputStream.read(buffer)) != -1) {
                    outputStream.write(buffer, 0, bytesRead);
                }

                byte[] imageBytes = outputStream.toByteArray();
                String base64Image = Base64.getEncoder().encodeToString(imageBytes);

                inputStream.close();
                outputStream.close();
                p.setLike(rs.getInt("like"));
                p.setBase64Image(base64Image);

                p.setDate(rs.getDate("publishdate"));
                p.setAp(Ap);
                ps.add(p);
            }
        } catch (SQLException ex) {
            Logger.getLogger(PostDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ps;
    }

    public Post GetUserWork(String workid) throws IOException {
        try {
            String sql = "SELECT A.workid, username, [name], [description], img, publishdate, [like], [private], [comments] FROM Work A JOIN AccountWork B\n"
                    + "ON A.workid = B.workid\n"
                    + "WHERE A.workid = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, workid);
            rs = stm.executeQuery();
            if (rs.next()) {
                Post p = new Post();
                AccountPost Ap = new AccountPost();
                Account a = new Account();
                a.setUsername(rs.getString("username"));
                Ap.setWorkid(rs.getString("workid"));
                Ap.setA(a);
                p.setName(rs.getString("name"));
                p.setDescription(rs.getString("description"));
                p.setPrivacy(rs.getInt("private"));
                p.setComments(rs.getInt("comments"));
                Blob blob = rs.getBlob("img");

                InputStream inputStream = blob.getBinaryStream();
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                byte[] buffer = new byte[4096];
                int bytesRead = -1;

                while ((bytesRead = inputStream.read(buffer)) != -1) {
                    outputStream.write(buffer, 0, bytesRead);
                }

                byte[] imageBytes = outputStream.toByteArray();
                String base64Image = Base64.getEncoder().encodeToString(imageBytes);

                inputStream.close();
                outputStream.close();
                p.setLike(rs.getInt("like"));
                p.setBase64Image(base64Image);

                p.setDate(rs.getDate("publishdate"));
                p.setAp(Ap);
                return p;
            }
        } catch (SQLException ex) {
            Logger.getLogger(PostDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public Post AddPost(String WorkID, String title, String description, InputStream img, int privacy, int comments) {
        try {

            String sql = "INSERT INTO Work (workid, [name], [description], img, publishdate, [like], saved, [view], [private], comments)\n"
                    + "VALUES (?, ?, ?, ?, GETDATE(), 0, 0, 0, ?, ?)";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, WorkID);
            stm.setString(2, title);
            stm.setString(3, description);
            stm.setBlob(4, img);
            stm.setInt(5, privacy);
            stm.setInt(6, comments);
            stm.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(PostDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public Post AddView(String username, String workid) {
        try {

            String sql = "INSERT INTO PostView (username, workid, [date])\n"
                    + "VALUES (?, ?, GETDATE())";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, username);
            stm.setString(2, workid);
            stm.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(PostDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public int GetNoOfRecord() {
        int total = 0;
        try {
            String sql = "SELECT COUNT(*) 'Total' FROM [Work]";
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();

            if (rs.next()) {
                total = rs.getInt("Total");
            }
        } catch (SQLException ex) {
            Logger.getLogger(PostDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return total;
    }

    public int GetNoOfUserRecord(String username) {
        int total = 0;
        try {
            String sql = "SELECT COUNT(*) 'Total' FROM [Work] A JOIN AccountWork B\n"
                    + "ON A.workid = B.workid\n"
                    + "WHERE username = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, username);
            ResultSet rs = stm.executeQuery();

            if (rs.next()) {
                total = rs.getInt("Total");
            }
        } catch (SQLException ex) {
            Logger.getLogger(PostDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return total;
    }

    public void UpdateWork(String workid, String name, String description, InputStream img, int privates, int comment) {
        String sql = "UPDATE Work SET [name] = ?, [img] = ?, [description] = ?, [private] = ?, [comments] = ?\n"
                + "WHERE workid = ?";
        PreparedStatement stm = null;
        try {
            stm = connection.prepareStatement(sql);
            stm.setString(1, name);
            stm.setBlob(2, img);
            stm.setString(3, description);
            stm.setInt(4, privates);
            stm.setInt(5, comment);
            stm.setString(6, workid);

            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(PostDBContext.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (stm != null) {
                try {
                    stm.close();
                } catch (SQLException ex) {
                    Logger.getLogger(PostDBContext.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(PostDBContext.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

    }

    public void deletePostTag(String WorkID) {
        String sql = "DELETE FROM WorkTag\n"
                + "WHHERE workid = ?";
        PreparedStatement stm = null;
        try {
            stm = connection.prepareStatement(sql);
            stm.setString(1, WorkID);

            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(PostDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void deletePost(String WorkID) {
        String sql = "DELETE FROM Work WHERE workid = ?";
        PreparedStatement stm = null;
        try {
            stm = connection.prepareStatement(sql);
            stm.setString(1, WorkID);

            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(PostDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void deleteAccountPost(String WorkID) {
        String sql = "DELETE FROM AccountWork WHERE workid = ?";
        PreparedStatement stm = null;
        try {
            stm = connection.prepareStatement(sql);
            stm.setString(1, WorkID);

            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(PostDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public ArrayList<Post> GetWorks(String workid) throws IOException {
        ArrayList<Post> pro = new ArrayList<>();
        try {
            String sql = "SELECT workid, [name], [description], img, publishdate FROM Work\n"
                    + "WHERE workid = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, workid);

            ResultSet rs = stm.executeQuery();

            rs = stm.executeQuery();

            while (rs.next()) {
                Post p = new Post();
                AccountPost Ap = new AccountPost();
                Ap.setWorkid(rs.getString("workid"));
                p.setName(rs.getString("name"));
                p.setDescription(rs.getString("description"));

                Blob blob = rs.getBlob("img");

                InputStream inputStream = blob.getBinaryStream();
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                byte[] buffer = new byte[4096];
                int bytesRead = -1;

                while ((bytesRead = inputStream.read(buffer)) != -1) {
                    outputStream.write(buffer, 0, bytesRead);
                }

                byte[] imageBytes = outputStream.toByteArray();
                String base64Image = Base64.getEncoder().encodeToString(imageBytes);

                inputStream.close();
                outputStream.close();
                p.setLike(rs.getInt("like"));
                p.setBase64Image(base64Image);

                p.setDate(rs.getDate("publishdate"));
                p.setAp(Ap);
                pro.add(p);
            }
        } catch (SQLException ex) {
            Logger.getLogger(PostDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return pro;
    }

    public ArrayList<Tag> GetPostTag(String workid) throws SQLException {
        ArrayList<Tag> tags = new ArrayList<>();
        try {
            String sql = "SELECT C.tagid 'tagid', tagname  FROM Work A JOIN AccountWork B\n"
                    + "ON A.workid = B.workid\n"
                    + "JOIN WorkTag C\n"
                    + "ON B.workid = C.workid\n"
                    + "JOIN Tag D\n"
                    + "ON C.tagid = D.tagid\n"
                    + "WHERE A.workid = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, workid);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Tag t = new Tag();
                t.setTagid(rs.getString("tagid"));
                t.setTagname(rs.getString("tagname"));
                tags.add(t);

            }
        } catch (SQLException ex) {
            Logger.getLogger(PostDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return tags;
    }

    public Post GetWork(String workid) throws IOException {
        try {

            String sql = "SELECT A.workid, username, [name], [description], img, publishdate, [like], saved, [view], private, comments  FROM Work A JOIN AccountWork B\n"
                    + "ON A.workid = B.workid\n"
                    + "WHERE A.workid = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, workid);
            ResultSet rs = stm.executeQuery();
            if (rs.next()) {
                Post p = new Post();
                AccountPost Ap = new AccountPost();
                Account a = new Account();
                a.setUsername(rs.getString("username"));
                Ap.setWorkid(rs.getString("workid"));
                Ap.setA(a);
                p.setName(rs.getString("name"));
                p.setDescription(rs.getString("description"));

                Blob blob = rs.getBlob("img");

                InputStream inputStream = blob.getBinaryStream();
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                byte[] buffer = new byte[4096];
                int bytesRead = -1;

                while ((bytesRead = inputStream.read(buffer)) != -1) {
                    outputStream.write(buffer, 0, bytesRead);
                }

                byte[] imageBytes = outputStream.toByteArray();
                String base64Image = Base64.getEncoder().encodeToString(imageBytes);

                inputStream.close();
                outputStream.close();
                p.setLike(rs.getInt("like"));
                p.setBase64Image(base64Image);
                p.setView(rs.getInt("view"));
                p.setSaved(rs.getInt("saved"));
                p.setDate(rs.getDate("publishdate"));
                p.setPrivacy(rs.getInt("private"));
                p.setComments(rs.getInt("comments"));
                p.setAp(Ap);
                return p;
            }
        } catch (SQLException ex) {
            Logger.getLogger(PostDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public String getWorkUsername(String workid) {
        try {
            String sql = "select username from AccountWork \n"
                    + "where workid=?";
            PreparedStatement stm = connection.prepareCall(sql);
            stm.setString(1, workid);
            ResultSet rs = stm.executeQuery();

            while (rs.next()) {
                String username = rs.getString("username");
                return username;
            }

        } catch (SQLException ex) {
            Logger.getLogger(PostDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public ArrayList<Comment> getWorkComments(String workid) throws IOException {
        ArrayList<Comment> comments = new ArrayList<>();
        try {
            String sql = "SELECT cid, workid ,username, content FROM WorkComment \n"
                    + "WHERE workid = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, workid);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Comment c = new Comment();
                AccountPost ap = new AccountPost();
                Account acc = new Account();
                ap.setWorkid(rs.getString("workid"));
                acc.setUsername(rs.getString("username"));
                c.setCid(rs.getString("cid"));
                c.setContent(rs.getString("content"));
                c.setAp(ap);
                c.setAcc(acc);
                comments.add(c);
            }
        } catch (SQLException ex) {
            Logger.getLogger(PostDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return comments;
    }

    public String getCommentUsername(String cid) {
        try {
            String sql = "select username from WorkComment \n"
                    + "where cid=?";
            PreparedStatement stm = connection.prepareCall(sql);
            stm.setString(1, cid);
            ResultSet rs = stm.executeQuery();

            while (rs.next()) {
                String username = rs.getString("username");
                return username;
            }

        } catch (SQLException ex) {
            Logger.getLogger(PostDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public ArrayList<Reply> getReplies(String cid) throws IOException {
        ArrayList<Reply> replies = new ArrayList<>();
        try {
            String sql = "SELECT rid, cid ,username, content FROM ReplyComment \n"
                    + "WHERE cid = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, cid);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Reply r = new Reply();
                Comment c = new Comment();
                Account acc = new Account();
                acc.setUsername(rs.getString("username"));
                c.setCid(rs.getString("cid"));
                r.setRid(rs.getString("rid"));
                r.setCmt(c);
                r.setContent(rs.getString("content"));
                r.setAcc(acc);
                replies.add(r);
            }
        } catch (SQLException ex) {
            Logger.getLogger(PostDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return replies;
    }

    public void addComment(String cid, String workid, String username, String content) throws IOException {
        try {
            String sql = "INSERT INTO WorkComment (cid, workid, [username], [content])\n"
                    + "VALUES (?, ?, ?, ?)";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, cid);
            stm.setString(2, workid);
            stm.setString(3, username);
            stm.setString(4, content);
            stm.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(PostDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void addReply(String cid, String username, String content) throws IOException {
        try {
            String sql = "INSERT INTO ReplyComment (rid, cid, [username], [content])\n"
                    + "VALUES (?, ?, ?, ?)";
            String uniqueID = UUID.randomUUID().toString();
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, uniqueID);
            stm.setString(2, cid);
            stm.setString(3, username);
            stm.setString(4, content);
            stm.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(PostDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public ArrayList<Post> SortWorkbyOrder(String order) throws IOException {
        ArrayList<Post> pro = new ArrayList<>();
        try {
            String sql = "SELECT workid, [name], [description], img, publishdate, [like] FROM Work\n"
                    + "order by publishdate " + order;
            PreparedStatement stm = connection.prepareStatement(sql);
            rs = stm.executeQuery();
            while (rs.next()) {
                Post p = new Post();
                AccountPost Ap = new AccountPost();
                Ap.setWorkid(rs.getString("workid"));
                p.setName(rs.getString("name"));
                p.setDescription(rs.getString("description"));

                Blob blob = rs.getBlob("img");

                InputStream inputStream = blob.getBinaryStream();
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                byte[] buffer = new byte[4096];
                int bytesRead = -1;

                while ((bytesRead = inputStream.read(buffer)) != -1) {
                    outputStream.write(buffer, 0, bytesRead);
                }

                byte[] imageBytes = outputStream.toByteArray();
                String base64Image = Base64.getEncoder().encodeToString(imageBytes);

                inputStream.close();
                outputStream.close();
                p.setLike(rs.getInt("like"));
                p.setBase64Image(base64Image);

                p.setDate(rs.getDate("publishdate"));
                p.setAp(Ap);
                pro.add(p);
            }
        } catch (SQLException ex) {
            Logger.getLogger(PostDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return pro;
    }
  public ArrayList<Post> GetAdminWork(int pageIndex) throws IOException {
        ArrayList<Post> pro = new ArrayList<>();
        try {
            String sql = "SELECT A.workid, username, [name], [description], img, publishdate, [like] FROM Work A JOIN AccountWork B\n"
                    + "ON A.workid = B.workid\n"
                    + "                    ORDER BY publishdate asc\n"
                    + "                    OFFSET 4*(? - 1) ROWS \n"
                    + "                    FETCH FIRST 3 ROWS ONLY";

            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, pageIndex);
            ResultSet rs = stm.executeQuery();

            while (rs.next()) {
                Account a = new Account();
                Post p = new Post();
                AccountPost Ap = new AccountPost();
                a.setUsername(rs.getString("username"));
                Ap.setA(a);
                Ap.setWorkid(rs.getString("workid"));
                p.setName(rs.getString("name"));
                p.setDescription(rs.getString("description"));

                Blob blob = rs.getBlob("img");

                InputStream inputStream = blob.getBinaryStream();
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                byte[] buffer = new byte[4096];
                int bytesRead = -1;

                while ((bytesRead = inputStream.read(buffer)) != -1) {
                    outputStream.write(buffer, 0, bytesRead);
                }

                byte[] imageBytes = outputStream.toByteArray();
                String base64Image = Base64.getEncoder().encodeToString(imageBytes);

                inputStream.close();
                outputStream.close();
                p.setLike(rs.getInt("like"));
                p.setBase64Image(base64Image);

                p.setDate(rs.getDate("publishdate"));
                p.setAp(Ap);
                pro.add(p);
            }
        } catch (SQLException ex) {
            Logger.getLogger(PostDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return pro;
    }
}
