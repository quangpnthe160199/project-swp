/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dal;

import com.sun.org.apache.bcel.internal.generic.AALOAD;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import java.util.Base64;

import java.util.UUID;

import java.util.logging.Level;
import java.util.logging.Logger;
import model.Account;
import model.Advertiser;
import model.AccountDetail;
import model.AccountSubcription;
import model.Fanbox;
import model.Follow;
import model.MutedTag;
import model.MutedUser;
import model.Tag;
import model.FollowUser;
import model.ReportUser;

/**
 *
 * @author BK
 */
public class AccountDBContext extends DBContext {

    public ArrayList<Account> GetAccounts() {
        ArrayList<Account> acc = new ArrayList<>();
        try {
            String sql = "SELECT username, [password], displayname FROM Account";

            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Account a = new Account();
                a.setUsername(rs.getString("username"));
                a.setPassword(rs.getString("password"));
                a.setDisplayname(rs.getString("displayname"));
                acc.add(a);
            }
        } catch (SQLException ex) {
            Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return acc;
    }

    public AccountSubcription getSubcription(String username) {
        try {
            String sql = "SELECT username, MIN(FromDate) 'FromDate', MAX(EndDate) 'EndDate' FROM UserSubcription \n"
                    + "WHERE username = ? \n"
                    + "GROUP BY username";

            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, username);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                AccountSubcription as = new AccountSubcription();
                Account a = new Account();
                a.setUsername(rs.getString("username"));
                as.setFromdate(rs.getDate("FromDate"));
                as.setTodate(rs.getDate("EndDate"));
                as.setA(a);
                return as;
            }
        } catch (SQLException ex) {
            Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public ArrayList<MutedUser> GetMutedUsers(String username, int page) {
        ArrayList<MutedUser> muted = new ArrayList<>();
        try {
            String sql = "SELECT username, muteduser FROM UserMutedUser\n"
                    + "WHERE username = ?\n";
            if (page != 0) {
                sql += "ORDER BY username ASC\n"
                        + "OFFSET 10*(? - 1) ROWS\n"
                        + "FETCH FIRST 10 ROWS ONLY";
            }
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, username);
            if (page != 0) {
                stm.setInt(2, page);
            }
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Account a = new Account();
                MutedUser mt = new MutedUser();
                a.setUsername(rs.getString("username"));
                mt.setMuteduser(rs.getString("muteduser"));
                mt.setA(a);
                muted.add(mt);
            }
        } catch (SQLException ex) {
            Logger.getLogger(PostDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return muted;
    }

    public int GetTotalMutedTags(String username) {
        int total = 0;
        try {
            String sql = "SELECT COUNT(*) 'Total' FROM [UserMutedTag]\n"
                    + "WHERE username = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, username);
            ResultSet rs = stm.executeQuery();

            if (rs.next()) {
                total = rs.getInt("Total");
            }
        } catch (SQLException ex) {
            Logger.getLogger(PostDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return total;
    }
    
    public ArrayList<Fanbox> getFanboxList(String username){
        ArrayList<Fanbox> fanbox = new ArrayList<>();
        try {
            String sql = "select A.username, A.fid, [Name] from FanboxSubscriber A join Fanbox B\n" +
                         "on A.fid = B.fid\n" +
                         "join FanboxDetails C\n" +
                         "on A.fid = C.fid\n" +
                         "where A.username = ?";

            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, username);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Fanbox a = new Fanbox();
                a.setFname(rs.getString("Name"));
                a.setFid(rs.getString("fid"));
                fanbox.add(a);
            }
        } catch (SQLException ex) {
            Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return fanbox;
    }
    
    public ArrayList<Follow> getFollowList(String username){
        ArrayList<Follow> follow = new ArrayList<>();
        try {
            String sql = "select A.username, B.displayname, follower_username from Follow A join Account B\n" +
                         "on A.username = B.username\n" +
                         "where follower_username = ?";

            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, username);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Account x = new Account();
                Follow a = new Follow();
                x.setUsername(rs.getString("username"));
                x.setDisplayname(rs.getString("displayname"));
                a.setFollowed(x);
                a.setFollower(rs.getString("follower_username"));
                follow.add(a);
            }
        } catch (SQLException ex) {
            Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return follow;
    }

    public int GetTotalMutedUser(String username) {
        int total = 0;
        try {
            String sql = "SELECT COUNT(*) 'Total' FROM [UserMutedUser]"
                    + "WHERE username = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, username);
            ResultSet rs = stm.executeQuery();

            if (rs.next()) {
                total = rs.getInt("Total");
            }
        } catch (SQLException ex) {
            Logger.getLogger(PostDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return total;
    }

    public ArrayList<MutedTag> GetMutedTags(String username, int page) {
        ArrayList<MutedTag> muted = new ArrayList<>();
        try {
            String sql = "SELECT username, A.tagid 'tagid', tagname FROM UserMutedTag A JOIN Tag B\n"
                    + "ON A.tagid = B.tagid\n"
                    + "WHERE username = ?\n";
            if (page != 0) {
                sql += "ORDER BY username ASC\n"
                        + "OFFSET 10*(? - 1) ROWS\n"
                        + "FETCH FIRST 10 ROWS ONLY";
            }
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, username);
            if (page != 0) {
                stm.setInt(2, page);
            }
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Account a = new Account();
                Tag t = new Tag();
                MutedTag mt = new MutedTag();
                a.setUsername(rs.getString("username"));
                t.setTagid(rs.getString("tagid"));
                t.setTagname(rs.getString("tagname"));
                mt.setA(a);
                mt.setT(t);
                muted.add(mt);
            }
        } catch (SQLException ex) {
            Logger.getLogger(PostDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return muted;
    }

    public void addMutedUser(String username, String muteduser) throws IOException {
        try {
            String sql = "INSERT INTO UserMutedUser (username, muteduser)\n"
                    + "VALUES (?, ?)";
            String uniqueID = UUID.randomUUID().toString();
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, username);
            stm.setString(2, muteduser);
            stm.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(PostDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void DeleteMutedUsers(String username) {
        try {

            String sql = "DELETE FROM UserMutedUser\n"
                    + "WHERE username = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, username);
            stm.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(PostDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void DeleteMutedUser(String username, String muteduser) {
        try {

            String sql = "DELETE FROM UserMutedUser\n"
                    + "WHERE username = ? AND muteduser = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, username);
            stm.setString(2, muteduser);
            stm.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(PostDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public MutedUser GetMutedUser(String username, String muteduser) {
        try {
            String sql = "SELECT username, muteduser FROM UserMutedUser\n"
                    + "WHERE username = ? AND muteduser = ?";

            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, username);
            stm.setString(2, muteduser);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Account a = new Account();
                MutedUser mt = new MutedUser();
                a.setUsername(rs.getString("username"));
                mt.setMuteduser(rs.getString("muteduser"));
                mt.setA(a);
                return mt;
            }
        } catch (SQLException ex) {
            Logger.getLogger(PostDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public void addMutedTags(String username, String tag) throws IOException {
        try {
            String sql = "INSERT INTO UserMutedTag (username, tagid)\n"
                    + "VALUES (?, ?)";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, username);
            stm.setString(2, tag);
            stm.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(PostDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void addMutedTag(String username, String tag) throws IOException {
        try {
            String sql = "INSERT INTO UserMutedTag (username, tagid)\n"
                    + "VALUES (?, ?)";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, username);
            stm.setString(2, tag);
            stm.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(PostDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void DeleteMutedTags(String username) {
        try {

            String sql = "DELETE FROM UserMutedTag\n"
                    + "WHERE username = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, username);
            stm.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(PostDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void DeleteMutedTag(String username) {
        try {

            String sql = "DELETE FROM UserMutedTag\n"
                    + "WHERE username = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, username);
            stm.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(PostDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void DeleteMutedTag(String username, String tagid) {
        try {

            String sql = "DELETE FROM UserMutedTag\n"
                    + "WHERE username = ? AND tagid = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, username);
            stm.setString(2, tagid);
            stm.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(PostDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Account GetAccount(String username, String password) {
        try {
            String sql = "SELECT username, [password],displayname FROM Account\n"
                    + "WHERE username = ? AND [password] = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, username);
            stm.setString(2, password);
            ResultSet rs = stm.executeQuery();
            if (rs.next()) {
                Account a = new Account();
                a.setUsername(rs.getString("username"));
                a.setPassword(rs.getString("password"));
                a.setDisplayname(rs.getString("displayname"));
                return a;
            }
        } catch (SQLException ex) {
            Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public int checkUsernameExist(String username) {
        try {
            String sql = "select count(*) as Exist from Account where username = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, username);
            ResultSet rs = stm.executeQuery();
            if (rs.next()) {
                return rs.getInt("Exist");
            }
        } catch (SQLException ex) {
            Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    public Account addAccount(String username, String password, String displayname) {
        try {
            String sql = "INSERT INTO [Account]\n"
                    + "           ([username]\n"
                    + "           ,[password]\n"
                    + "           ,[displayname])\n"
                    + "     VALUES\n"
                    + "           (?\n"
                    + "           ,?\n"
                    + "           ,?)";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, username);
            stm.setString(2, password);
            stm.setString(3, displayname);

            stm.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public void addAccountDetail(String username, String fullname, int gender, String mobile, String email, String address) {
        try {
            String sql = "INSERT INTO [dbo].[AccountDetail]\n"
                    + "           ([username]\n"
                    + "           ,[fullname]\n"
                    + "           ,[gender]\n"
                    + "           ,[mobile]\n"
                    + "           ,[email]\n"
                    + "           ,[address]\n"
                    + "            )\n"
                    + "     VALUES\n"
                    + "           (?\n"
                    + "           ,?\n"
                    + "           ,?\n"
                    + "           ,?\n"
                    + "           ,?\n"
                    + "           ,?)";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, username);
            stm.setString(2, fullname);
            stm.setInt(3, gender);
            stm.setString(4, mobile);
            stm.setString(5, email);
            stm.setString(6, address);

            stm.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public AccountDetail getAccountDetail(String username) {
        try {
            String sql = "select a.username,b.password ,a.fullname, a.gender, a.mobile, a.[address], a.email  from AccountDetail a join Account b\n"
                    + "on a.username = b.username\n"
                    + "where a.username = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, username);
            ResultSet rs = stm.executeQuery();
            if (rs.next()) {
                Account acc = new Account();
                AccountDetail ad = new AccountDetail();
                acc.setUsername(rs.getString("username"));
                acc.setPassword(rs.getString("password"));
                ad.setAcc(acc);
                ad.setAddress(rs.getString("address"));
                ad.setFullname(rs.getString("fullname"));
                ad.setGender(rs.getByte("gender"));
                ad.setMobile(rs.getString("mobile"));
                ad.setEmail(rs.getString("email"));
                return ad;
            }
        } catch (SQLException ex) {
            Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public Account addAccountWork(String username, String WorkID) {
        try {
            String sql = "INSERT INTO AccountWork (username, workid)\n"
                    + " VALUES (?, ?)";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, username);
            stm.setString(2, WorkID);
            stm.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public int getAdminPermission(String username, String url) {
        try {
            String sql = "select count(*) as Total from (\n"
                    + "select b.username,a.url from\n"
                    + "(select a.gid,b.url from GroupPermission a join Permission b on a.pid=b.pid) a join\n"
                    + "(select a.username,b.gid from Account a join GroupAccount b on a.username=b.username) b\n"
                    + "on a.gid=b.gid\n"
                    + "where b.username=? and a.url=?\n"
                    + ") a";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, username);
            stm.setString(2, url);
            ResultSet rs = stm.executeQuery();
            if (rs.next()) {
                return rs.getInt("Total");
            }
        } catch (SQLException ex) {
            Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }

    public void createAccount(String username, String password) {
        String sql = "insert into Account([username],[password]) values (?,?)";
        PreparedStatement stm = null;
        try {
            stm = connection.prepareStatement(sql);
            stm.setString(1, username);
            stm.setString(2, password);
            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (stm != null) {
                try {
                    stm.close();
                } catch (SQLException ex) {
                    Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    public void changePassword(String username, String password) {
        String sql = "UPDATE [Account]\n"
                + "   SET [password] = ?"
                + " WHERE [username] = ?";
        PreparedStatement stm = null;
        try {
            stm = connection.prepareStatement(sql);
            stm.setString(2, username);
            stm.setString(1, password);
            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (stm != null) {
                try {
                    stm.close();
                } catch (SQLException ex) {
                    Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

    }

    public void updateAccountDetail(String username, String fullname, String mobile, String email, String address, int gender) {
        String sql = "update AccountDetail\n"
                + "set fullname = ?, gender = ?, mobile = ?, [address] = ?, email = ?\n"
                + "where username = ?";
        PreparedStatement stm = null;
        try {
            stm = connection.prepareStatement(sql);
            stm.setString(1, fullname);
            stm.setInt(2, gender);
            stm.setString(3, mobile);
            stm.setString(4, address);
            stm.setString(5, email);
            stm.setString(6, username);
            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (stm != null) {
                try {
                    stm.close();
                } catch (SQLException ex) {
                    Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    public Account getAccount(String username) {
        try {
            String sql = "SELECT username,password FROM Account\n"
                    + "WHERE username = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, username);
            ResultSet rs = stm.executeQuery();
            if (rs.next()) {
                Account account = new Account();
                account.setUsername(rs.getString("username"));
                account.setPassword(rs.getString("password"));
                return account;
            }
        } catch (SQLException ex) {
            Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public void updateAccount(String username, String newPassword, String newDisplayname) {
        String sql = "UPDATE [Account]\n"
                + "   SET [displayname] = ?\n"
                + ",[password] = ?\n"
                + " WHERE [username] = ?";
        PreparedStatement stm = null;
        try {
            stm = connection.prepareStatement(sql);
            stm.setString(1, newDisplayname);
            stm.setString(2, newPassword);
            stm.setString(3, username);
            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (stm != null) {
                try {
                    stm.close();
                } catch (SQLException ex) {
                    Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

        }

    }

    public void addSercurityQuestion(String username, String password) {
        String sql = "insert into Account([username],[password]) values (?,?)";
        PreparedStatement stm = null;
        try {
            stm = connection.prepareStatement(sql);
            stm.setString(1, username);
            stm.setString(2, password);
            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (stm != null) {
                try {
                    stm.close();
                } catch (SQLException ex) {
                    Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    public ArrayList<Account> searchUserByName(String username) {

        ArrayList<Account> acc = new ArrayList<>();
        try {
            String sql = "SELECT username \n"
                    + "FROM Account \n"
                    + "WHERE username LIKE ? ";

            PreparedStatement stm = connection.prepareStatement(sql);
            String temp = "%" + username + "%";
            stm.setString(1, temp);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Account a = new Account();
                a.setUsername(rs.getString("username"));
                acc.add(a);
            }
        } catch (SQLException ex) {
            Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return acc;
    }

    public Account addAccountAds(String username, String AdsID) {
        try {
            String sql = "INSERT INTO Advertiser (username, AdsID)\n"
                    + " VALUES (?, ?)";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, username);
            stm.setString(2, AdsID);
            stm.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public int isFlagged(String username) {
        try {
            String sql = "select count(*) as flag from FlaggedAccount\n"
                    + "where username = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, username);
            ResultSet rs = stm.executeQuery();
            if (rs.next()) {
                return rs.getInt("flag");
            }
        } catch (SQLException ex) {
            Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;

    }

    public int isBanned(String username) {
        try {
            String sql = "select count(*) as ban from BannedAccount\n"
                    + "                    where username =?  ";
            PreparedStatement stm = connection.prepareStatement(sql);

            stm.setString(1, username);

            ResultSet rs = stm.executeQuery();
            if (rs.next()) {
                return rs.getInt("ban");
            }
        } catch (SQLException ex) {
            Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    public void flagAccount(String username) {
        String sql = "insert into FlaggedAccount(username) values (?)";
        PreparedStatement stm = null;
        try {
            stm = connection.prepareStatement(sql);
            stm.setString(1, username);
            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (stm != null) {
                try {
                    stm.close();
                } catch (SQLException ex) {
                    Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    public void removeFlagAccount(String username) {
        String sql = "delete from FlaggedAccount where username = ?";
        PreparedStatement stm = null;
        try {
            stm = connection.prepareStatement(sql);
            stm.setString(1, username);
            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (stm != null) {
                try {
                    stm.close();
                } catch (SQLException ex) {
                    Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    public boolean checkEmailExist(String email) {
        String sql = "select count(email) as Total from AccountDetail\n"
                + "where email = ?";
        try {
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, email);
            ResultSet rs = stm.executeQuery();
            if (rs.next()) {
                return rs.getInt("Total") > 0;
            }

        } catch (SQLException ex) {
            Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public Account getAccountByEmail(String email) {
        try {
            String sql = "select a.username,b.password ,b.displayname from AccountDetail a join Account b\n"
                    + "on a.username = b.username\n"
                    + "where a.email = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, email);
            ResultSet rs = stm.executeQuery();
            if (rs.next()) {
                Account acc = new Account();
                acc.setUsername(rs.getString("username"));
                acc.setPassword(rs.getString("password"));
                acc.setDisplayname(rs.getString("displayname"));
                return acc;
            }
        } catch (SQLException ex) {
            Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public void AccountPurchase(String purchaseID, String username, int amount, String type, String bankcode) {
        String sql = "INSERT INTO UserPurchase (PurchaseID, username, Amount, [Type], BankCode)\n"
                + "VALUES (?, ?, ?, ?, ?)";
        PreparedStatement stm = null;
        try {
            stm = connection.prepareStatement(sql);
            stm.setString(1, purchaseID);
            stm.setString(2, username);
            stm.setInt(3, amount);
            stm.setString(4, type);
            stm.setString(5, bankcode);
            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void AccountSubcription(String PurchaseID, String username, int months) {
        String sql = "INSERT INTO UserSubcription (PurchaseID, username, FromDate, EndDate)\n"
                + "VALUES (?, ?, GETDATE(), dateadd(m, ?, GETDATE()))";
        PreparedStatement stm = null;
        try {
            stm = connection.prepareStatement(sql);
            stm.setString(1, PurchaseID);
            stm.setString(2, username);
            stm.setInt(3, months);
            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (stm != null) {
                try {
                    stm.close();
                } catch (SQLException ex) {
                    Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    public int IsPremium(String username) {
        try {
            String sql = "SELECT CAST(\n"
                    + "             CASE\n"
                    + "                  WHEN  GETDATE() <= MAX(EndDate)\n"
                    + "                     THEN 1\n"
                    + "                  ELSE 0\n"
                    + "             END AS bit) as IsValid, username\n"
                    + "FROM UserSubcription\n"
                    + "WHERE username = ?\n"
                    + "GROUP BY username";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, username);
            ResultSet rs = stm.executeQuery();
            if (rs.next()) {
                return rs.getInt("IsValid");
            }
        } catch (SQLException ex) {
            Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;

    }

    public void AddAccountRole(String role, String username) {
        String sql = "INSERT INTO GroupAccount (username, gid)\n"
                + "VALUES (?, ?)";
        PreparedStatement stm = null;
        try {
            stm = connection.prepareStatement(sql);
            stm.setString(1, username);
            stm.setString(2, role);
            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (stm != null) {
                try {
                    stm.close();
                } catch (SQLException ex) {
                    Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    public void RemoveAccountRole(String role, String username) {
        String sql = "DELETE FROM GroupAccount WHERE username = ? AND gid = ?";
        PreparedStatement stm = null;
        try {
            stm = connection.prepareStatement(sql);
            stm.setString(1, username);
            stm.setString(2, role);
            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (stm != null) {
                try {
                    stm.close();
                } catch (SQLException ex) {
                    Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    public boolean haveFanbox(String username) {
        int a = 0;
        try {
            String sql = "select count(username)as Total from Fanbox where username = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, username);
            ResultSet rs = stm.executeQuery();
            if (rs.next()) {
                a = rs.getInt("Total");
            }
        } catch (SQLException ex) {
            Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return a > 0;
    }

    public ArrayList<FollowUser> GetFollow(String username) {
        ResultSet rs = null;
        PreparedStatement stm = null;
        ArrayList<FollowUser> followers = new ArrayList<>();
        try {
            String sql = "SELECT follower_username  FROM  Follow\n"
                    + "                    WHERE username = ?";
            stm = connection.prepareStatement(sql);
            stm.setString(1, username);
            rs = stm.executeQuery();
            while (rs.next()) {
                Account a = new Account();
                FollowUser fu = new FollowUser();

                fu.setFollowername(rs.getString("follower_username"));
                fu.setA(a);
                followers.add(fu);
            }
        } catch (SQLException ex) {
            Logger.getLogger(PostDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return followers;
    }

    public AccountDetail getAccountDetailByName(String username) {
        try {
            String sql = "select a.username,b.password ,a.fullname, a.gender, a.mobile, a.[address], a.email ,a.avatar , a.background from AccountDetail a join Account b\n"
                    + "on a.username = b.username\n"
                    + "where a.username = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, username);
            ResultSet rs = stm.executeQuery();
            if (rs.next()) {
                Account acc = new Account();
                AccountDetail ad = new AccountDetail();
                acc.setUsername(rs.getString("username"));
                acc.setPassword(rs.getString("password"));
                ad.setAcc(acc);
                ad.setAddress(rs.getString("address"));
                ad.setFullname(rs.getString("fullname"));
                ad.setGender(rs.getByte("gender"));
                ad.setMobile(rs.getString("mobile"));
                ad.setEmail(rs.getString("email"));
                ad.setAva(rs.getString("avatar"));
                ad.setBackground(rs.getString("background"));
                return ad;
            }
        } catch (SQLException ex) {
            Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public int getEdtiorInsertAva(String username) {
        try {
            String sql = "select count(username) as Total from (\n"
                    + "				select username from AccountDetail \n"
                    + "				where username =? and AvaImg is NULL\n"
                    + "				)a";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, username);

            ResultSet rs = stm.executeQuery();
            if (rs.next()) {
                return rs.getInt("Total");
            }
        } catch (SQLException ex) {
            Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    public int getEdtiorInsertBG(String username) {
        try {
            String sql = "select count(username) as Total from (\n"
                    + "				select username from AccountDetail \n"
                    + "				where username = ? and BackGroundImg is NULL\n"
                    + "				)a";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, username);

            ResultSet rs = stm.executeQuery();
            if (rs.next()) {
                return rs.getInt("Total");
            }
        } catch (SQLException ex) {
            Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    public void editProfilePic(String username, InputStream inputStreamAva, InputStream inputStreambackground) {
        String sql = "UPDATE [dbo].[AccountDetail]\n"
                + "   SET \n"
                + "      [BackGroundImg] = ?\n"
                + "      ,[AvaImg] = ?\n"
                + " WHERE username= ? ";
        PreparedStatement stm = null;
        try {
            stm = connection.prepareStatement(sql);
            stm.setString(3, username);
            stm.setBlob(2, inputStreamAva);
            stm.setBlob(1, inputStreambackground);

            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (stm != null) {
                try {
                    stm.close();
                } catch (SQLException ex) {
                    Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    public void deleteDefaultAvaAndBG(String username) {
        String sql = "";
        PreparedStatement stm = null;

        try {
            stm = connection.prepareStatement(sql);
            stm.setString(1, username);
            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (stm != null) {
                try {
                    stm.close();
                } catch (SQLException ex) {
                    Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    public String getbase64Ava(String username) throws IOException {

        try {
            String sql = "SELECT \n"
                    + "      [avatar]\n"
                    + "  FROM  [AccountDetail]"
                    + "where username = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, username);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Blob avaBlob = rs.getBlob("avatar");
                if (avaBlob == null) {
                    return null;

                } else {

                    InputStream inputStreamAva = avaBlob.getBinaryStream();
                    ByteArrayOutputStream outputStreamAva = new ByteArrayOutputStream();

                    byte[] buffer = new byte[4096];
                    int bytesRead = -1;

                    while ((bytesRead = inputStreamAva.read(buffer)) != -1) {
                        outputStreamAva.write(buffer, 0, bytesRead);
                    }

                    byte[] imageBytesAva = outputStreamAva.toByteArray();
                    String base64AvaImage = Base64.getEncoder().encodeToString(imageBytesAva);

                    inputStreamAva.close();
                    outputStreamAva.close();
                    return base64AvaImage;
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public String getbase64BG(String username) throws IOException {

        try {
            String sql = "SELECT \n"
                    + "      [background]\n"
                    + "  FROM  [AccountDetail]"
                    + "where username = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, username);

            ResultSet rs = stm.executeQuery();
            if (rs.next()) {
                Blob BGBlob = rs.getBlob("background");
                if (BGBlob == null) {
                    return null;

                } else {
                    InputStream inputStreamBG = BGBlob.getBinaryStream();
                    ByteArrayOutputStream outputStreamBG = new ByteArrayOutputStream();

                    byte[] buffer = new byte[4096];
                    int bytesRead = -1;

                    while ((bytesRead = inputStreamBG.read(buffer)) != -1) {
                        outputStreamBG.write(buffer, 0, bytesRead);
                    }

                    byte[] imageBytesBG = outputStreamBG.toByteArray();
                    String base64BGImage = Base64.getEncoder().encodeToString(imageBytesBG);

                    inputStreamBG.close();
                    outputStreamBG.close();
                    return base64BGImage;
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public void banAccount(String username) {
        String sql = "insert into BannedAccount(username) values (?)";
        PreparedStatement stm = null;
        try {
            stm = connection.prepareStatement(sql);
            stm.setString(1, username);
            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (stm != null) {
                try {
                    stm.close();
                } catch (SQLException ex) {
                    Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    public void removeBanAccount(String username) {
        String sql = "delete from BannedAccount  where username = ?";
        PreparedStatement stm = null;
        try {
            stm = connection.prepareStatement(sql);
            stm.setString(1, username);
            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (stm != null) {
                try {
                    stm.close();
                } catch (SQLException ex) {
                    Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    public static void main(String[] args) {
        AccountDBContext adb = new AccountDBContext();
        int i = adb.isBanned("c");
        System.out.println(i);
    }

    public ArrayList<Account> getUserForPaging(int pageIndex) {
        ArrayList<Account> acc = new ArrayList<>();
        try {
            String sql = "SELECT a.username , a.password , a.displayname From Account a\n"
                    + "inner join Report r on a.username=r.username\n"
                    + "                    order by a.username\n"
                    + "                    OFFSET 4*(? - 1) ROWS \n"
                    + "                     FETCH FIRST 4 ROWS ONLY";

            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, pageIndex);
            ResultSet rs = stm.executeQuery();

            while (rs.next()) {
                Account a = new Account();
                a.setUsername(rs.getString("username"));
                a.setPassword(rs.getString("password"));
                a.setDisplayname(rs.getString("displayname"));
                acc.add(a);
            }
        } catch (SQLException ex) {
            Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return acc;
    }

    public int GetNoOfAccount() {
        int total = 0;
        try {
            String sql = "SELECT COUNT(*) 'Total' FROM [Account]";
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();

            if (rs.next()) {
                total = rs.getInt("Total");
            }
        } catch (SQLException ex) {
            Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return total;
    }

//    public void updateUserRole(String username) {
//        String sql = "INSERT INTO [dbo].[GroupAccount]\n"
//                + "           ([username]\n"
//                + "           ,[gid])\n"
//                + "     VALUES\n"
//                + "           (? \n"
//                + "           ,2)";
//        PreparedStatement stm = null;
//        try {
//            stm = connection.prepareStatement(sql);
//            stm.setString(1, username);
//
//            stm.executeUpdate();
//        } catch (SQLException ex) {
//            Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
//        } finally {
//            if (stm != null) {
//                try {
//                    stm.close();
//                } catch (SQLException ex) {
//                    Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
//                }
//            }
//            if (connection != null) {
//                try {
//                    connection.close();
//                } catch (SQLException ex) {
//                    Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
//                }
//            }
//        }
//    }

    public FollowUser CounterFollower(String username) {
        ResultSet rs = null;
        PreparedStatement stm = null;
        try {
            String sql = "select count (follower_username) 'follow' from Follow\n"
                    + "where username= ?";

            stm = connection.prepareStatement(sql);
            stm.setString(1, username);
            rs = stm.executeQuery();
            if (rs.next()) {
                FollowUser fu = new FollowUser();
                fu.setFollower(rs.getInt("follow"));
                return fu;

            }
        } catch (SQLException ex) {
            Logger.getLogger(PostDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public Account AddFollow(String username, String followerName) {
        try {

            String sql = "INSERT INTO [dbo].[Follow]\n"
                    + "           ([username]\n"
                    + "           ,[follower_username])\n"
                    + "     VALUES\n"
                    + "           (?\n"
                    + "           ,?)";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, username);
            stm.setString(2, followerName);
            stm.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(PostDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public Account UnFollow(String username, String followerName) {
        try {

            String sql = "DELETE FROM [dbo].[Follow]\n"
                    + "      WHERE username= ? AND follower_username=? ";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, username);
            stm.setString(2, followerName);
            stm.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(PostDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public void addFirstEdit(String username) {
        try {
            String sql = "INSERT INTO [dbo].[Check Edit]\n"
                    + "           ([username]\n"
                    + "           ,[isFirstEdit])\n"
                    + "     VALUES\n"
                    + "           (?\n"
                    + "           ,0)";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, username);

            stm.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void UpdateFirstEditTrue(String username) {
        try {
            String sql = "UPDATE [dbo].[Check Edit]\n"
                    + "   SET [username] = ?\n"
                    + "      ,[isFirstEdit] = 1\n"
                    + " WHERE username= ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, username);
            stm.setString(2, username);
            stm.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public boolean getFirstEdit(String username) {
        boolean check = false;
        try {
            String sql = "select isFirstEdit from [Check Edit]\n"
                    + "where username=?";

            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, username);
            ResultSet rs = stm.executeQuery();

            if (rs.next()) {
                check = rs.getBoolean("isFirstEdit");

            }
        } catch (SQLException ex) {
            Logger.getLogger(PostDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return check;
    }

    public ReportUser CounterReporter(String username) {
        ResultSet rs = null;
        PreparedStatement stm = null;
        try {
            String sql = "select count (reporter_username) 'report' from Report\n"
                    + "where username= ?";

            stm = connection.prepareStatement(sql);
            stm.setString(1, username);
            rs = stm.executeQuery();
            if (rs.next()) {
                ReportUser ru = new ReportUser();
                ru.setReporter(rs.getInt("report"));
                return ru;

            }
        } catch (SQLException ex) {
            Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public Account UnReport(String username, String reportername) {
        try {

            String sql = "DELETE FROM [dbo].[Report]\n"
                    + "      WHERE username= ? AND reporter_username=? ";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, username);
            stm.setString(2, reportername);
            stm.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public Account Report(String username, String reporterName) {
        try {

            String sql = "INSERT INTO [dbo].[Report]\n"
                    + "           ([username]\n"
                    + "           ,[reporter_username])\n"
                    + "     VALUES\n"
                    + "           (?\n"
                    + "           ,?)";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, username);
            stm.setString(2, reporterName);
            stm.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public ArrayList<ReportUser> getReport(String username) {
        ResultSet rs = null;
        PreparedStatement stm = null;
        ArrayList<ReportUser> reporters = new ArrayList<>();
        try {
            String sql = "SELECT reporter_username  FROM  Report\n"
                    + "                    WHERE username = ?";
            stm = connection.prepareStatement(sql);
            stm.setString(1, username);
            rs = stm.executeQuery();
            while (rs.next()) {
                Account a = new Account();
                ReportUser ru = new ReportUser();

                ru.setReportname(rs.getString("reporter_username"));
                ru.setA(a);
                reporters.add(ru);
            }
        } catch (SQLException ex) {
            Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return reporters;
    }

    public void updatebase64AvaAndDB(String username, InputStream inputStreamAva, InputStream inputStream64BG) {
        String sql = "UPDATE [dbo].[AccountDetail]\n"
                + "   SET \n"
                + "      [background] = ?\n"
                + "      ,[avatar] = ?\n"
                + " WHERE username= ? ";
        PreparedStatement stm = null;
        try {
            stm = connection.prepareStatement(sql);
            stm.setString(3, username);
            stm.setBlob(2, inputStreamAva);
            stm.setBlob(1, inputStream64BG);

            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (stm != null) {
                try {
                    stm.close();
                } catch (SQLException ex) {
                    Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    public void updatebase64DB(String username, InputStream inputStream64BG) {
        String sql = "UPDATE [dbo].[AccountDetail]\n"
                + "   SET \n"
                + "      [background] = ?\n"
                + " WHERE username= ? ";
        PreparedStatement stm = null;
        try {
            stm = connection.prepareStatement(sql);
            stm.setString(1, username);
            stm.setBlob(2, inputStream64BG);
            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (stm != null) {
                try {
                    stm.close();
                } catch (SQLException ex) {
                    Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    public void updatebase64Ava(String username, InputStream inputStreamAva) {
         String sql = "UPDATE AccountDetail set [avatar] = ? WHERE username = ?";
        PreparedStatement stm = null;
        try {
            stm = connection.prepareStatement(sql);
            stm.setString(1, username);
            stm.setBlob(2, inputStreamAva);
            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (stm != null) {
                try {
                    stm.close();
                } catch (SQLException ex) {
                    Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
     }


    
        public boolean isAdmin(String username) {
 boolean check= false;
        try {
            String sql = "select gid from GroupAccount\n" +
                      "where username = ?";

            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, username);
            ResultSet rs = stm.executeQuery();
           
            if (rs.next()) {
              rs.getBoolean("isFirstEdit");
              
            }
        } catch (SQLException ex) {
            Logger.getLogger(PostDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
    return check;
    }
    
      public void UpdateUserRoleToAdmin(String username) {
        try {
            String sql = "UPDATE [dbo].[GroupAccount]\n"
                    + "   SET [username] = ?\n"
                    + "      ,[gid] = '1'\n"
                    + " WHERE username = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, username);
            stm.setString(2, username);
            stm.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
      
        public void UpdateAdminRoleToUser(String username) {
        try {
            String sql = "UPDATE [dbo].[GroupAccount]\n"
                    + "   SET [username] = ?\n"
                    + "      ,[gid] = '3'\n"
                    + " WHERE username = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, username);
            stm.setString(2, username);
            stm.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
