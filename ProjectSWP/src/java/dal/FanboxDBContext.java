/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dal;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Account;
import model.AccountPost;
import model.FanboxPost;
import model.FanboxPostLike;
import model.Post;
import model.PostLike;
import model.Tag;

/**
 *
 * @author admin
 */
public class FanboxDBContext extends DBContext {

    ResultSet rs = null;
    PreparedStatement stm = null;

    public void createFanbox(String fid, String username) {
        String sql = "INSERT INTO Fanbox (fid, username)\n"
                + "VALUES (?, ?)";
        PreparedStatement stm = null;
        try {
            stm = connection.prepareStatement(sql);
            stm.setString(1, fid);
            stm.setString(2, username);
            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(FanboxDBContext.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (stm != null) {
                try {
                    stm.close();
                } catch (SQLException ex) {
                    Logger.getLogger(FanboxDBContext.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(FanboxDBContext.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    public void addFanboxDetails(String fid, String fname) {
        String sql = "INSERT INTO FanboxDetails (fid, Name)\n"
                + "VALUES (?, ?)";
        PreparedStatement stm = null;
        try {
            stm = connection.prepareStatement(sql);
            stm.setString(1, fid);
            stm.setString(2, fname);
            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(FanboxDBContext.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (stm != null) {
                try {
                    stm.close();
                } catch (SQLException ex) {
                    Logger.getLogger(FanboxDBContext.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(FanboxDBContext.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    public Post AddPost(String pid, String fid, String title, String description, InputStream img, int comments) {
        try {

            String sql = "INSERT INTO FanboxPost (pid,fid ,[Name], [description], img, publishdate, [like], saved, [view], comments)\n"
                    + "VALUES (?, ?, ?, ?, ?, GETDATE(), 0, 0, 0, ?)";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, pid);
            stm.setString(2, fid);
            stm.setString(3, title);
            stm.setString(4, description);
            stm.setBlob(5, img);
            stm.setInt(6, comments);
            stm.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(FanboxDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public String getFanboxName(String fid) {
        String id = null;
        try {
            String sql = "SELECT [Name] FROM FanboxDetails where fid = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, fid);
            ResultSet rs = stm.executeQuery();

            while (rs.next()) {
                id = rs.getString("Name");
            }
        } catch (SQLException ex) {
            Logger.getLogger(FanboxDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return id;
    }
    
    public String getFanboxNameOwner(String fid) {
        String username = null;
        try {
            String sql = "SELECT [username] FROM Fanbox where fid = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, fid);
            ResultSet rs = stm.executeQuery();

            while (rs.next()) {
                username = rs.getString("username");
            }
        } catch (SQLException ex) {
            Logger.getLogger(FanboxDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return username;
    }

    public String getFanboxID(String username) {
        String id = null;
        try {
            String sql = "SELECT fid FROM Fanbox where username = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, username);
            ResultSet rs = stm.executeQuery();

            while (rs.next()) {
                id = rs.getString("fid");
            }
        } catch (SQLException ex) {
            Logger.getLogger(FanboxDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return id;
    }

    public ArrayList<Post> getFanboxPosts(int page, String fid) throws IOException {
        ArrayList<Post> pro = new ArrayList<>();
        try {
            String sql = "SELECT pid, [Name], [description], img, publishdate, [like] FROM FanboxPost\n"
                    + "WHERE fid = ?\n"
                    + "ORDER BY publishdate DESC\n"
                    + "OFFSET 4*(? - 1) ROWS \n"
                    + "FETCH FIRST 4 ROWS ONLY;";

            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, fid);
            stm.setInt(2, page);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Post p = new Post();
                AccountPost Ap = new AccountPost();
                Ap.setWorkid(rs.getString("pid"));
                p.setName(rs.getString("Name"));
                p.setDescription(rs.getString("description"));

                Blob blob = rs.getBlob("img");

                InputStream inputStream = blob.getBinaryStream();
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                byte[] buffer = new byte[4096];
                int bytesRead = -1;

                while ((bytesRead = inputStream.read(buffer)) != -1) {
                    outputStream.write(buffer, 0, bytesRead);
                }

                byte[] imageBytes = outputStream.toByteArray();
                String base64Image = Base64.getEncoder().encodeToString(imageBytes);

                inputStream.close();
                outputStream.close();
                p.setLike(rs.getInt("like"));
                p.setBase64Image(base64Image);

                p.setDate(rs.getDate("publishdate"));
                p.setAp(Ap);
                pro.add(p);
            }
        } catch (SQLException ex) {
            Logger.getLogger(FanboxDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return pro;
    }

    public FanboxPost GetWork(String pid) throws IOException {
        FanboxPost p = new FanboxPost();
        try {

            String sql = "SELECT pid, A.[Name] as fname, B.[Name] as [Name], [description], img, publishdate, [like], saved, [view], comments  FROM FanboxPost B JOIN FanboxDetails A\n"
                    + "ON A.fid=B.fid\n"
                    + "WHERE pid = ?";
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setString(1, pid);
            ResultSet rst = stmt.executeQuery();
            if (rst.next()) {
                p.setPid(rst.getString("pid"));
                p.setFname(rst.getString("fname"));
                p.setName(rst.getString("Name"));
                p.setDescription(rst.getString("description"));

                Blob blob = rst.getBlob("img");

                InputStream inputStream = blob.getBinaryStream();
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                byte[] buffer = new byte[4096];
                int bytesRead = -1;

                while ((bytesRead = inputStream.read(buffer)) != -1) {
                    outputStream.write(buffer, 0, bytesRead);
                }

                byte[] imageBytes = outputStream.toByteArray();
                String base64Image = Base64.getEncoder().encodeToString(imageBytes);

                inputStream.close();
                outputStream.close();
                p.setLike(rst.getInt("like"));
                p.setBase64Image(base64Image);
                p.setView(rst.getInt("view"));
                p.setDate(rst.getDate("publishdate"));
                p.setComments(rst.getInt("comments"));
                return p;
            }
        } catch (SQLException ex) {
            Logger.getLogger(FanboxDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return p;
    }

    public void AddView(String username, String pid) {
        try {

            String sql = "INSERT INTO FanboxPostView (username, pid, [date])\n"
                    + "VALUES (?, ?, GETDATE())";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, username);
            stm.setString(2, pid);
            stm.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(FanboxDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public ArrayList<FanboxPostLike> getLikes(String pid) {
        ArrayList<FanboxPostLike> like = new ArrayList<>();
        try {
            String sql = "SELECT username, pid FROM FanboxPostLike\n"
                    + "WHERE pid = ?";

            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, pid);
            rs = stm.executeQuery();
            while (rs.next()) {
                FanboxPostLike fpl = new FanboxPostLike();
                fpl.setUsername(rs.getString("username"));
                fpl.setPid(rs.getString("pid"));
                like.add(fpl);
            }
        } catch (SQLException ex) {
            Logger.getLogger(FanboxDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return like;
    }

    public void DeleteLike(String username, String pid) {
        try {

            String sql = "DELETE FROM FanboxPostLike\n"
                    + "WHERE username = ? AND pid = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, username);
            stm.setString(2, pid);
            stm.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(FanboxDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void AddLike(String username, String pid) {
        try {

            String sql = "INSERT INTO FanboxPostLike (username, pid)\n"
                    + "VALUES (?, ?)";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, username);
            stm.setString(2, pid);
            stm.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(FanboxDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    

    public ArrayList<Post> getFanboxPreViewPost(String fid) throws IOException {
        ArrayList<Post> pro = new ArrayList<>();
        try {
            String sql = "SELECT top 2 pid, [Name], [description], img, publishdate, [like] FROM FanboxPost\n"
                    + "WHERE fid = ?\n"
                    + "ORDER BY publishdate DESC\n";

            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, fid);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Post p = new Post();
                AccountPost Ap = new AccountPost();
                Ap.setWorkid(rs.getString("pid"));
                p.setName(rs.getString("Name"));
                p.setDescription(rs.getString("description"));

                Blob blob = rs.getBlob("img");

                InputStream inputStream = blob.getBinaryStream();
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                byte[] buffer = new byte[4096];
                int bytesRead = -1;

                while ((bytesRead = inputStream.read(buffer)) != -1) {
                    outputStream.write(buffer, 0, bytesRead);
                }

                byte[] imageBytes = outputStream.toByteArray();
                String base64Image = Base64.getEncoder().encodeToString(imageBytes);

                inputStream.close();
                outputStream.close();
                p.setLike(rs.getInt("like"));
                p.setBase64Image(base64Image);

                p.setDate(rs.getDate("publishdate"));
                p.setAp(Ap);
                pro.add(p);
            }
        } catch (SQLException ex) {
            Logger.getLogger(FanboxDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return pro;
    }

    public boolean IsSubscribed(String username, String fid) {
        int result = 0;
        try {

            String sql = "SELECT count(username) as IsValid\n"
                    + "FROM FanboxSubscriber\n"
                    + "WHERE username = ? AND fid = ?  \n"
                    + "GROUP BY username\n"
                    + "HAVING GETDATE() <= MAX(EndDate)";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, username);
            stm.setString(2, fid);
            ResultSet rs = stm.executeQuery();
            if (rs.next()) {
                result = rs.getInt("IsValid");
            }
        } catch (SQLException ex) {
            Logger.getLogger(FanboxDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result > 0;

    }

    public void FanboxSubcription(String PurchaseID, String username, String fid, int months) {
        String sql = "INSERT INTO FanboxSubscriber (PurchaseID, username, fid, FromDate, EndDate)\n"
                + "VALUES (?, ?, ?, GETDATE(), dateadd(m, ?, GETDATE()))";
        PreparedStatement stm = null;
        try {
            stm = connection.prepareStatement(sql);
            stm.setString(1, PurchaseID);
            stm.setString(2, username);
            stm.setString(3, fid);
            stm.setInt(4, months);
            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(FanboxDBContext.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (stm != null) {
                try {
                    stm.close();
                } catch (SQLException ex) {
                    Logger.getLogger(FanboxDBContext.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(FanboxDBContext.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    public int GetNoOfFanboxRecord(String fid) {
        int total = 0;
        try {
            String sql = "SELECT COUNT(*) 'Total' FROM FanboxPost\n"
                    + "WHERE fid = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, fid);
            ResultSet rs = stm.executeQuery();

            if (rs.next()) {
                total = rs.getInt("Total");
            }
        } catch (SQLException ex) {
            Logger.getLogger(FanboxDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return total;
    }
    public ArrayList<Tag> GetPostTag(String pid) throws SQLException {
        ArrayList<Tag> tags = new ArrayList<>();
        try {
            String sql = "select A.tagid, tagname from FanboxPostTag A join Tag B\n" +
                         "on A.tagid = B.tagid\n" +
                         "where pid = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, pid);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Tag t = new Tag();
                t.setTagid(rs.getString("tagid"));
                t.setTagname(rs.getString("tagname"));
                tags.add(t);
                
            }
        } catch (SQLException ex) {
            Logger.getLogger(PostDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return tags;
    }
    
    public ArrayList<Account> getFanboxSubscribers(String username){
        ArrayList<Account> accs = new ArrayList<>();
        try {
            String sql = "select A.username, displayname from FanboxSubscriber A join Account B\n" +
                         "on A.username = B.username\n" +
                         "where fid = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, username);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Account t = new Account();
                t.setUsername(rs.getString("username"));
                t.setDisplayname(rs.getString("displayname"));
                accs.add(t);
                
            }
        } catch (SQLException ex) {
            Logger.getLogger(PostDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return accs;
    }

}
