/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Question;
import model.UserAnswer;

/**
 *
 * @author Dell
 */
public class QuestionDBContext extends DBContext {

    public ArrayList<Question> getQuestions() {
        ArrayList<Question> questions = new ArrayList<>();
        try {
            String sql = "select qid, question from Questions";
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Question q = new Question();
                q.setQid(rs.getString("qid"));
                q.setQuestion(rs.getString("question"));
                questions.add(q);
            }
        } catch (SQLException ex) {
            Logger.getLogger(String.class.getName()).log(Level.SEVERE, null, ex);

        }

        return questions;
    }

    public Question addQuestion(String qid, String qcontent) {
        String sql = "INSERT INTO [dbo].[Questions]\n"
                + "           ([qid]\n"
                + "           ,[question])\n"
                + "     VALUES\n"
                + "           (?\n"
                + "           ,?)";
        PreparedStatement stm = null;
        try {
            stm = connection.prepareStatement(sql);

            stm.setString(1, qid);
            stm.setString(2, qcontent);
            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(String.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (stm != null) {
                try {
                    stm.close();
                } catch (SQLException ex) {
                    Logger.getLogger(String.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(String.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return null;
    }

    public UserAnswer addAnswer(String username, String qid, String answer) {
        String sql = "INSERT INTO [dbo].[Security Question]\n"
                + "           ([username]\n"
                + "           ,[qid]\n"
                + "           ,[answer])\n"
                + "     VALUES\n"
                + "           ( ?\n"
                + "           , ?\n"
                + "           , ?)";
        PreparedStatement stm = null;
        try {
            stm = connection.prepareStatement(sql);
            stm.setString(1, username);
            stm.setString(2, qid);
            stm.setString(3, answer);
            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(String.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (stm != null) {
                try {
                    stm.close();
                } catch (SQLException ex) {
                    Logger.getLogger(String.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(String.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return null;
    }

    public ArrayList<Question> getSercurityQuestionbyUsername(String username) {
        ArrayList<Question> sercuriyquestions = new ArrayList<>();
        try {
            String sql = "select sq.qid'qid',question from Questions q join [Security Question] sq on q.qid=sq.qid\n"
                    + "                    where username= ? ";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, username);
            ResultSet rs = stm.executeQuery();
            if (rs.next()) {

                Question question = new Question();
                question.setQid(rs.getString("qid"));
                question.setQuestion(rs.getString("question"));

                sercuriyquestions.add(question);
            }
        } catch (SQLException ex) {
            Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return sercuriyquestions;
    }

    public ArrayList<UserAnswer> getAnswerbyUsername(String username) {
        ArrayList<UserAnswer> answers = new ArrayList<>();
        try {
            String sql = "select qid, answer from [Security Question]\n"
                    + "where username= ? ";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, username);

            ResultSet rs = stm.executeQuery();
            if (rs.next()) {
                Question q = new Question();
                UserAnswer ua = new UserAnswer();
                ua.setAnswer(rs.getString("answer"));
                q.setQid(rs.getString("qid"));
                ua.setQ(q);
                answers.add(ua);
            }
        } catch (SQLException ex) {
            Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return answers;
    }

    public void DeleteQuestion(String qid) {
        String sql = "DELETE FROM [dbo].[Questions]\n"
                + "      WHERE qid = ?";
        PreparedStatement stm = null;
        try {
            stm = connection.prepareStatement(sql);

            stm.setString(1, qid);

            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(String.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (stm != null) {
                try {
                    stm.close();
                } catch (SQLException ex) {
                    Logger.getLogger(String.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(String.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
     
    }

}
