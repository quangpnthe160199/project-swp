/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dal;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Account;
import model.AccountPost;
import model.Notification;

/**
 *
 * @author haiph
 */
public class NotificationDBContext extends DBContext {

    ResultSet rs = null;
    PreparedStatement stm = null;

    public ArrayList<Notification> getNotifications(String username) throws IOException {
        ArrayList<Notification> notifications = new ArrayList<>();
        try {
            String sql = "SELECT TOP 10 nid, sendername, receivername, [content], timesent, workid FROM Notification\n"
                    + "WHERE receivername = ?\n"
                    +"ORDER BY timesent DESC";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, username);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Notification notification = new Notification();
                Account senderAcc = new Account();
                Account receiverAcc = new Account();
                AccountPost ap = new AccountPost();
                ap.setWorkid(rs.getString("workid"));
                senderAcc.setUsername(rs.getString("sendername"));
                receiverAcc.setUsername(rs.getString("receivername"));

                notification.setNid(rs.getString("nid"));
                notification.setTimesent(rs.getTimestamp("timesent"));
                notification.setContent(rs.getString("content"));
                notification.setSenderAcc(senderAcc);
                notification.setReceiverAcc(receiverAcc);
                notification.setAp(ap);
                notifications.add(notification);

            }
        } catch (SQLException ex) {
            Logger.getLogger(NotificationDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return notifications;
    }
    
    public ArrayList<Notification> getAllNotifications(String username) throws IOException {
        ArrayList<Notification> notifications = new ArrayList<>();
        try {
            String sql = "SELECT nid, sendername, receivername, [content], timesent, workid FROM Notification\n"
                    + "WHERE receivername = ?\n"
                    +"ORDER BY timesent DESC";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, username);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Notification notification = new Notification();
                Account senderAcc = new Account();
                Account receiverAcc = new Account();
                AccountPost ap = new AccountPost();
                ap.setWorkid(rs.getString("workid"));
                senderAcc.setUsername(rs.getString("sendername"));
                receiverAcc.setUsername(rs.getString("receivername"));

                notification.setNid(rs.getString("nid"));
                notification.setTimesent(rs.getTimestamp("timesent"));
                notification.setContent(rs.getString("content"));
                notification.setSenderAcc(senderAcc);
                notification.setReceiverAcc(receiverAcc);
                notification.setAp(ap);
                notifications.add(notification);

            }
        } catch (SQLException ex) {
            Logger.getLogger(NotificationDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return notifications;
    }
    public void addNotification(String sendername, String receivername, String content, Timestamp timesent, String workid) throws IOException {
        try {
            String sql = "INSERT INTO Notification (nid, sendername, receivername,[content], timesent, workid)\n"
                    + "VALUES (?, ?, ?, ?, ?, ?)";
            String uniqueID = UUID.randomUUID().toString();
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, uniqueID);
            stm.setString(2, sendername);
            stm.setString(3, receivername);
            stm.setString(4, content);
            stm.setTimestamp(5, timesent);
            stm.setString(6, workid);
            stm.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(MessageDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
