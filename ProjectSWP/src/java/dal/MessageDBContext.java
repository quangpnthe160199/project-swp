/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dal;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Account;
import model.Message;

/**
 *
 * @author haiph
 */
public class MessageDBContext extends DBContext {

    ResultSet rs = null;
    PreparedStatement stm = null;

    public ArrayList<Message> getPersonalMessages(String username, String username2) throws IOException {
        ArrayList<Message> messages = new ArrayList<>();
        try {
            String sql = "SELECT mid, sendername, receivername, [content], timesent FROM Message \n"
                    + "WHERE receivername = ? AND sendername = ? OR receivername = ? AND sendername = ?\n"
                    + "ORDER BY timesent ASC";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, username);
            stm.setString(2, username2);
            stm.setString(3, username2);
            stm.setString(4, username);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Message message = new Message();
                Account senderAcc = new Account();
                Account receiverAcc = new Account();
                senderAcc.setUsername(rs.getString("sendername"));
                receiverAcc.setUsername(rs.getString("receivername"));
                message.setMid(rs.getString("mid"));
                message.setContent(rs.getString("content"));
                message.setTimesent(rs.getTimestamp("timesent"));
                message.setSenderAcc(senderAcc);
                message.setReceiverAcc(receiverAcc);

                messages.add(message);

            }
        } catch (SQLException ex) {
            Logger.getLogger(MessageDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return messages;
    }

    public ArrayList<String> getPassiveContacts(String username) {
        ArrayList<String> passivecontacts = new ArrayList<>();
        try {
            String sql = "SELECT DISTINCT sendername FROM [Message]\n"
                    + "WHERE receivername = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, username);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                String contact = rs.getString("sendername");
                passivecontacts.add(contact);

            }
        } catch (SQLException ex) {
            Logger.getLogger(MessageDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return passivecontacts;
    }

    public ArrayList<String> getActiveContacts(String username) {
        ArrayList<String> activecontacts = new ArrayList<>();
        try {
            String sql = "SELECT DISTINCT receivername FROM [Message]\n"
                    + "WHERE sendername = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, username);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                String contact = rs.getString("receivername");
                activecontacts.add(contact);

            }
        } catch (SQLException ex) {
            Logger.getLogger(MessageDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return activecontacts;
    }
    public void addMessage(String sendername, String receivername, String content, Timestamp timesent) throws IOException {
        try {
            String sql = "INSERT INTO Message(mid, sendername, receivername , [content], timesent)\n"
                    + "VALUES (?, ?, ?, ?, ?)";
            String uniqueID = UUID.randomUUID().toString();
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, uniqueID);
            stm.setString(2, sendername);
            stm.setString(3, receivername);
            stm.setString(4, content);
            stm.setTimestamp(5, timesent);
            stm.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(MessageDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
/*
    public int GetNoOfRecord() {
        int total = 0;
        try {
            String sql = "SELECT COUNT(*) 'Total' FROM [Message]";
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();

            if (rs.next()) {
                total = rs.getInt("Total");
            }
        } catch (SQLException ex) {
            Logger.getLogger(MessageDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return total;
    }
    */
}
