/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dal;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.AccountPost;
import model.Post;
import model.Tag;

/**
 *
 * @author Dell
 */
public class TagDBContext extends DBContext {
      ResultSet rs = null;
    PreparedStatement stm = null;

    public ArrayList<Tag> getTags() {
        ArrayList<Tag> tags = new ArrayList<>();
        try {
            String sql = "select tagid,tagname from Tag";

            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Tag t = new Tag();
                t.setTagid(rs.getString("tagid"));
                t.setTagname(rs.getString("tagname"));
                tags.add(t);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TagDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return tags;
    }

    public void addWorkTag(String WorkId, String TagId) {
        try {
            String sql = "INSERT INTO [dbo].[WorkTag]\n"
                    + "           ([workid]\n"
                    + "           ,[tagid])\n"
                    + "     VALUES\n"
                    + "           (?\n"
                    + "           ,?)";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, WorkId);
            stm.setString(2, TagId);
            stm.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(TagDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void addFWorkTag(String pid, String TagId) {
        try {
            String sql = "INSERT INTO [dbo].[FanboxPostTag]\n"
                    + "           ([pid]\n"
                    + "           ,[tagid])\n"
                    + "     VALUES\n"
                    + "           (?\n"
                    + "           ,?)";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, pid);
            stm.setString(2, TagId);
            stm.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(TagDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public ArrayList<Post> GetWorkbyTagId(String TagId) throws IOException {
        ArrayList<Post> pro = new ArrayList<>();
        try {
            String sql = "SELECT w.workid'workid', w.description,w.name, w.publishdate, w.img, w.[like], wt.tagid FROM Work w INNER JOIN WorkTag wt\n"
                    + "ON w.workid = wt.workid\n"
                    + "WHERE tagid = ?";

            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, TagId);
            rs = stm.executeQuery();
            while (rs.next()) {
                Post p = new Post();
                AccountPost Ap = new AccountPost();
                Ap.setWorkid(rs.getString("workid"));
                p.setName(rs.getString("name"));
                p.setDescription(rs.getString("description"));

                Blob blob = rs.getBlob("img");

                InputStream inputStream = blob.getBinaryStream();
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                byte[] buffer = new byte[4096];
                int bytesRead = -1;

                while ((bytesRead = inputStream.read(buffer)) != -1) {
                    outputStream.write(buffer, 0, bytesRead);
                }

                byte[] imageBytes = outputStream.toByteArray();
                String base64Image = Base64.getEncoder().encodeToString(imageBytes);

                inputStream.close();
                outputStream.close();
                p.setLike(rs.getInt("like"));
                p.setBase64Image(base64Image);

                p.setDate(rs.getDate("publishdate"));
                p.setAp(Ap);
                pro.add(p);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TagDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return pro;
    }

}
