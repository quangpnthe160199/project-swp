
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package validator;

import dal.AccountDBContext;

/**
 *
 * @author admin
 */
public class Validator {
    
    public void PremiumRoleChecker(String username){
        AccountDBContext adb = new AccountDBContext();
        int IsValid = adb.IsPremium(username);
        if (IsValid == 1){
            adb.AddAccountRole("1", username);
        } else if (IsValid == 0){
            adb.RemoveAccountRole("1", username);
        }
    }
    public boolean IsPremium(String username){
        AccountDBContext adb = new AccountDBContext();
        int IsValid = adb.IsPremium(username);
        if (IsValid == 1){
            return true;
        }else {
            return false;
        }
    }

    public String checkMobile(String mobile){
        String validMobile = "[0-9]+";
        if (!mobile.matches(validMobile)) return "Mobile format is incorrect!";
        else return null;
    }
    public String checkUsername(String username){
        AccountDBContext db = new AccountDBContext();
        if (db.checkUsernameExist(username)!=0) return "This username already exists!";
        else return null;
    }
    public String checkEmail(String email){
        AccountDBContext db = new AccountDBContext();
        String validEmail = "^[a-zA-Z0-9_!#$%&'*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$";
        //Regular Expression by RFC 5322 for Email Validation
        if (!email.matches(validEmail)) return "Email formant is incorrect!";
        else if (db.checkEmailExist(email)) return "Email already exists!";
        else return null;
    }
    
    public String checkLogin(String username, String password){
        AccountDBContext db = new AccountDBContext();
        if (db.checkUsernameExist(username)==0) return "This username doesn't exist!";
        else return "Password is incorrect!";
    }
    
    
}

