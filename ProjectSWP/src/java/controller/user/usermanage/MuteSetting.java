/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.user.usermanage;

import dal.AccountDBContext;
import dal.AdsDBContext;
import dal.PostDBContext;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Account;
import model.AccountSubcription;
import model.Ads;
import model.MutedTag;
import model.MutedUser;
import model.PinPost;
import model.Post;
import model.PostLike;
import model.PostTag;
import validator.Validator;

/**
 *
 * @author BK
 */
public class MuteSetting extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet MuteControlelr</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet MuteControlelr at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Account account = (Account) request.getSession().getAttribute("account");
        AccountDBContext adb = new AccountDBContext();
        AccountSubcription as = new AccountSubcription();
        Validator validate = new Validator();
        String username = account.getUsername();
        int recordsPerPage = 10;
        // Get user current liked work
        ArrayList<MutedUser> UserMutedWork = new ArrayList<>();
        ArrayList<MutedTag> UserMutedTag = new ArrayList<>();

        int ppage = 1;
        int onoOfRecords = 0;
        int pageIndex;
        // Check user current page index
        try {
            pageIndex = Integer.parseInt(request.getParameter("page"));
        } catch (NumberFormatException e) {
            pageIndex = 1;
        }

        if (validate.IsPremium(username)) {
            validate.PremiumRoleChecker(username);
            as = adb.getSubcription(username);
            request.getSession().setAttribute("subcription", as);

        }

        if (request.getParameter("type") == null || request.getParameter("type").equalsIgnoreCase("tags")) {
            UserMutedTag = adb.GetMutedTags(username, pageIndex);
            onoOfRecords = adb.GetTotalMutedTags(username);
        } else if (request.getParameter("type").equalsIgnoreCase("user")) {
            UserMutedWork = adb.GetMutedUsers(username, pageIndex);
            onoOfRecords = adb.GetTotalMutedUser(username);
        }

        // Set the current post list and the paging system accordingly
        int onoOfPages = (int) Math.ceil(onoOfRecords * 1.0 / recordsPerPage);
        request.setAttribute("mutedPost", UserMutedWork);
        request.setAttribute("mutedTag", UserMutedTag);
        request.setAttribute("pageIndex", pageIndex);
        request.setAttribute("onoOfRecords", onoOfRecords);
        request.setAttribute("onoOfPages", onoOfPages);
        request.getRequestDispatcher("/view/affiliates/mutedsetting.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Account acc = (Account) request.getSession().getAttribute("account");
        String Type = request.getParameter("type");
        AccountSubcription as = new AccountSubcription();
        AccountDBContext adb = new AccountDBContext();
        Validator validate = new Validator();
        String MutedChoice = request.getParameter("mute");
        PostDBContext pdb = new PostDBContext();
        String Duplicate = "";

        if (Type.equalsIgnoreCase("user")) {
            String username = acc.getUsername();
            adb.DeleteMutedUsers(username);
            adb.DeleteMutedTags(username);
            adb.addMutedUser(username, MutedChoice);
        } else if (Type.equalsIgnoreCase("tags")) {
            String username = acc.getUsername();
            adb.DeleteMutedTags(username);
            adb.DeleteMutedUsers(username);
            adb.addMutedTags(username, MutedChoice);
        }

        response.getWriter().println(Type);
        response.getWriter().println(MutedChoice);
        response.getWriter().println(acc.getUsername());
        request.getRequestDispatcher("/view/affiliates/mutedsetting.jsp?type=" + request.getParameter("type")).forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
