/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.user.usermanage;

import dal.AccountDBContext;
import dal.PostDBContext;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Account;

import model.AccountDetail;

import model.FollowUser;

import model.Post;
import model.ReportUser;

/**
 *
 * @author admin
 */
public class ProfileController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//        if (request.getSession().getAttribute("account") == null){
//            response.sendRedirect( request.getContextPath() + "/home");
//        }

        AccountDBContext adb = new AccountDBContext();
        String jsp = "viewprofile.jsp";
        String username = "";
        String proname = request.getParameter("proname");
        ArrayList<FollowUser> FollowedUser = adb.GetFollow("");
        ArrayList<ReportUser> ReportedUser = adb.getReport("");
        
        if (request.getSession().getAttribute("account") != null) {
            response.getWriter().println(proname);
            Account acc = (Account) request.getSession().getAttribute("account");
            username = acc.getUsername();
            FollowedUser = adb.GetFollow(proname);
            ReportedUser = adb.getReport(proname);
            request.setAttribute("followedUser", FollowedUser);
            request.setAttribute("reportedUser", ReportedUser);
            if (username.equalsIgnoreCase(proname) || proname == null) {
                jsp = "userprofile.jsp";
            }
            
            String base64Ava = adb.getbase64Ava(username);
            String base64BG = adb.getbase64BG(username);
            AccountDetail accPic = adb.getAccountDetailByName(username);
            accPic.setAva(base64Ava);
            accPic.setBackground(base64BG);
            request.setAttribute("accPic", accPic);
        }
        request.setAttribute("followedUser", FollowedUser);
        request.setAttribute("reportedUser", ReportedUser);
        FollowUser Follower = adb.CounterFollower(proname);
        ReportUser Reporter =adb.CounterReporter(username);
        request.setAttribute("follower", Follower);
         request.setAttribute("reporter", Reporter);
        PostDBContext pdb = new PostDBContext();
        int pageIndex;
        try {
            pageIndex = Integer.parseInt(request.getParameter("page"));
        } catch (NumberFormatException e) {
            pageIndex = 1;
        }

        int recordsPerPage = 4;
        if (proname != null) {
            username = proname;
        }

        ArrayList<Post> Posts = pdb.GetUserWorks(pageIndex, username);

        if (request.getParameter("work") != null) {
            String workname = request.getParameter("work");
            Posts = pdb.GetWorkWname(pageIndex, workname);
        }

        String base64Ava = adb.getbase64Ava(username);
        String base64BG = adb.getbase64BG(username);

        AccountDetail acc = adb.getAccountDetailByName(username);
        acc.setAva(base64Ava);
        acc.setBackground(base64BG);
        request.setAttribute("acc", acc);

        request.setAttribute("post", Posts);
        int onoOfRecords = pdb.GetNoOfUserRecord(username);
        int onoOfPages = (int) Math.ceil(onoOfRecords * 1.0 / recordsPerPage);
        request.setAttribute("pageIndex", pageIndex);
        request.setAttribute("onoOfRecords", onoOfRecords);
        request.setAttribute("onoOfPages", onoOfPages);

        response.getWriter().print(FollowedUser.size());
        request.getRequestDispatcher("/view/affiliates/editprofile/" + jsp).forward(request, response);

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public static void main(String[] args) throws IOException {
        AccountDBContext ad = new AccountDBContext();
        String ava = ad.getbase64Ava("mmm");

        if (ava == null) {
            System.out.println("yes");
            System.out.println(ava);
        } else {
            System.out.println(ava);
        }

    }
}
