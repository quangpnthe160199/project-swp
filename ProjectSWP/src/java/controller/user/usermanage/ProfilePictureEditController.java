/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.user.usermanage;

import dal.AccountDBContext;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import static java.lang.System.out;
import java.util.Base64;
import java.util.UUID;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import model.Account;
import model.AccountDetail;

/**
 *
 * @author FPTSHOP
 */
@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 10, // 10 MB 
        maxFileSize = 1024 * 1024 * 50, // 50 MB
        maxRequestSize = 1024 * 1024 * 100)
public class ProfilePictureEditController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
//    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
//            throws ServletException, IOException {
//        response.setContentType("text/html;charset=UTF-8");
//        try (PrintWriter out = response.getWriter()) {
//            /* TODO output your page here. You may use following sample code. */
//            out.println("<!DOCTYPE html>");
//            out.println("<html>");
//            out.println("<head>");
//            out.println("<title>Servlet ProfilePictureEditController</title>");            
//            out.println("</head>");
//            out.println("<body>");
//            out.println("<h1>Servlet ProfilePictureEditController at " + request.getContextPath() + "</h1>");
//            out.println("</body>");
//            out.println("</html>");
//        }
//    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Account account = (Account) request.getSession().getAttribute("account");
        String username = account.getUsername();
        AccountDBContext adb = new AccountDBContext();
        String base64Ava = adb.getbase64Ava(username);
        String base64BG = adb.getbase64BG(username);
        AccountDetail accPic = adb.getAccountDetailByName(username);
        accPic.setAva(base64Ava);
        accPic.setBackground(base64BG);
        request.setAttribute("accPic", accPic);
        request.getRequestDispatcher("/view/affiliates/editprofile/editPicProfile.jsp").forward(request, response);

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        AccountDBContext accDB = new AccountDBContext();
        Account account = (Account) request.getSession().getAttribute("account");
        String username = account.getUsername();

        ServletOutputStream output = response.getOutputStream();

        InputStream inputStreamAva = null;
        InputStream inputStreamBG = null;
        Part filePartAva = request.getPart("avatar");
        Part filePartBG = request.getPart("background");
        if (filePartAva != null) {
            // prints out some information for debugging
//            output.println(filePartAva.getName());
//            output.println(filePartAva.getSize());
//            output.println(filePartAva.getContentType());

            // obtains input stream of the upload file
            inputStreamAva = filePartAva.getInputStream();
        }
        if (filePartBG != null) {
            // prints out some information for debugging
//            output.println(filePartBG.getName());
//            output.println(filePartBG.getSize());
//            output.println(filePartBG.getContentType());

            // obtains input stream of the upload file
            inputStreamBG = filePartBG.getInputStream();
        }
        if (inputStreamAva == null  && inputStreamBG !=null) {
            accDB.updatebase64DB(username, inputStreamBG);
        } else if (inputStreamBG == null && inputStreamAva != null) {
            accDB.updatebase64Ava(username, inputStreamAva);
        } else {

            accDB.updatebase64AvaAndDB(username, inputStreamAva, inputStreamBG);
        }
        response.sendRedirect(request.getContextPath() + "/home");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    public static void main(String[] args) throws IOException {
        AccountDBContext accDB = new AccountDBContext();
        String username = "mra";
        String base64Ava = accDB.getbase64Ava(username);
        if (base64Ava == null) {
            System.out.println("no");
        } else {
            System.out.println("ava: " + base64Ava);
        }

    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
