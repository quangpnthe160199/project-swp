/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.user.usermanage;

import dal.AccountDBContext;
import dal.AdsDBContext;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.UUID;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import model.Account;
import model.AccountDetail;

/**
 *
 * @author FPTSHOP
 */
@MultipartConfig(fileSizeThreshold=1024*1024*10, 	// 10 MB 
                 maxFileSize=1024*1024*50,      	// 50 MB
                 maxRequestSize=1024*1024*100)   
public class UserRegisterAdsController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet UserRegisterAdsController</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet UserRegisterAdsController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
          Account account = (Account) request.getSession().getAttribute("account");
        //String username = account.getUsername();
        AccountDBContext adb = new AccountDBContext();
        String username = account.getUsername();
          String base64Ava = adb.getbase64Ava(username);
        String base64BG = adb.getbase64BG(username);
        AccountDetail accPic = adb.getAccountDetailByName(username);
        accPic.setAva(base64Ava);
        accPic.setBackground(base64BG);
        request.setAttribute("accPic", accPic);
                request.getRequestDispatcher("/view/affiliates/userRegisterAds.jsp").forward(request, response);

     }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
         AdsDBContext adsDB = new AdsDBContext();
        AccountDBContext adb = new AccountDBContext();
        ServletOutputStream output = response.getOutputStream();

        String name = request.getParameter("name");
      
        
        InputStream inputStream = null; // input stream of the upload file
         
        // obtains the upload file part in this multipart request
        Part filePart = request.getPart("img");
        if (filePart != null) {
            // prints out some information for debugging
            output.println(filePart.getName());
            output.println(filePart.getSize());
          output.println(filePart.getContentType());
             
            // obtains input stream of the upload file
            inputStream = filePart.getInputStream();
        }
        
        //String stast = request.getParameter("stast");
        String uniqueID = UUID.randomUUID().toString();
         
        
        Account account = (Account) request.getSession().getAttribute("account");
        //String username = account.getUsername();
        String username = account.getUsername();
           
        adsDB.addAds(uniqueID, name,  inputStream);
        adb.addAccountAds(username, uniqueID);
         
        //pdb.addProduct(productid, productname);
       
        response.sendRedirect( request.getContextPath() + "/home");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
