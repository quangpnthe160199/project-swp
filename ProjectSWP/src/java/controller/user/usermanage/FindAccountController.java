/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.user.usermanage;

import dal.AccountDBContext;
import dal.QuestionDBContext;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Random;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Account;
import model.Question;

/**
 *
 * @author admin
 */
public class FindAccountController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    public String generateSecurityCode() {
        String code = "";
        Random charType = new Random();
        //loop to create random captcha with length = 5
        for (int i = 0; i < 5; i++) {
            int randomCharType = charType.nextInt(3);
            //randomizing between number, uppercase letter and lowercase letter
            switch (randomCharType) {
                case 0://number
                    Random number = new Random();
                    int randomNumber = number.nextInt(10) + 48;
                    code = code + (char) randomNumber;
                    break;
                case 1://uppercase letter
                    Random upperLetter = new Random();
                    int randomUpperLetter = upperLetter.nextInt(26) + 65;
                    code = code + (char) randomUpperLetter;
                    break;
                case 2://lowercase letter
                    Random lowerLetter = new Random();
                    int randomLowerLetter = lowerLetter.nextInt(26) + 97;
                    code = code + (char) randomLowerLetter;
                    break;
            }
        }
        return code;
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("/view/affiliates/resetpsw/findacc.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String username = request.getParameter("username");
        int type = Integer.parseInt(request.getParameter("resettype"));
        AccountDBContext db = new AccountDBContext();
        Account acc = db.getAccount(username);
        
        if (type == 1){
        QuestionDBContext qdb = new QuestionDBContext();
        ArrayList<Question> questions = qdb.getSercurityQuestionbyUsername(username);
        request.setAttribute("questions", questions);
        } else {
            String code = generateSecurityCode();
            request.getSession().setAttribute("code", code);
            request.getSession().setAttribute("username", username);
        }
        if(acc!=null)
        {
          request.setAttribute("username", username);
          if (type == 1)
          request.getRequestDispatcher("/view/affiliates/resetpsw/sercurity.jsp").forward(request, response);
          else response.sendRedirect("../email");
            
        }
        else
        {
            response.sendRedirect("/ProjectSWP/view/affiliates/resetpsw/findacc.jsp");
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
