/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.user.workmanage;

import dal.AccountDBContext;
import dal.PostDBContext;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Account;
import model.AccountSubcription;
import validator.Validator;

/**
 *
 * @author BK
 */
public class MuteController extends HttpServlet {

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.sendRedirect(request.getContextPath() + "/home");
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Account acc = (Account) request.getSession().getAttribute("account");
        String Type = request.getParameter("type");
        AccountSubcription as = new AccountSubcription();
        AccountDBContext adb = new AccountDBContext();
        Validator validate = new Validator();
        String MutedChoice = request.getParameter("mute");
        PostDBContext pdb = new PostDBContext();
        String Duplicate = "";
        
        
        if (validate.IsPremium(acc.getUsername())) {
            validate.PremiumRoleChecker(acc.getUsername());
            as = adb.getSubcription(acc.getUsername());
            request.getSession().setAttribute("subcription", as);
            if (request.getParameter("duplicate") == null) {
                if (Type.equalsIgnoreCase("user")) {
                    String username = acc.getUsername();
                    adb.addMutedUser(username, MutedChoice);
                } else if (Type.equalsIgnoreCase("tag")) {
                    String username = acc.getUsername();
                    adb.addMutedTags(username, MutedChoice);
                }
            } else {
                Duplicate = request.getParameter("duplicate");
                if (Duplicate.equalsIgnoreCase("user")) {
                    String username = acc.getUsername();
                    adb.DeleteMutedUsers(username);
                } else if (Duplicate.equalsIgnoreCase("tag")) {
                    String username = acc.getUsername();
                    adb.DeleteMutedTags(username);
                }

            }
        } else {
            if (request.getParameter("duplicate") == null) {
                if (Type.equalsIgnoreCase("user")) {
                    String username = acc.getUsername();
                    adb.DeleteMutedUsers(username);
                    adb.DeleteMutedTags(username);
                    adb.addMutedUser(username, MutedChoice);
                } else if (Type.equalsIgnoreCase("tag")) {
                    String username = acc.getUsername();
                    adb.DeleteMutedTags(username);
                    adb.DeleteMutedUsers(username);
                    adb.addMutedTags(username, MutedChoice);
                }
            } else {
                Duplicate = request.getParameter("duplicate");
                if (Duplicate.equalsIgnoreCase("user")) {
                    String username = acc.getUsername();
                    adb.DeleteMutedTags(username);
                    adb.DeleteMutedUsers(username);
                } else if (Duplicate.equalsIgnoreCase("tag")) {
                    String username = acc.getUsername();
                    adb.DeleteMutedTags(username);
                    adb.DeleteMutedUsers(username);
                }

            }
        }
        response.getWriter().println(Type);
        response.getWriter().println(MutedChoice);
        response.getWriter().println(acc.getUsername());
        response.sendRedirect(request.getContextPath() + "/home/work?workid=" + request.getParameter("workid"));
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
