/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.user.workmanage;

import dal.AccountDBContext;
import dal.NotificationDBContext;
import dal.PostDBContext;
import dal.TagDBContext;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.UUID;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import model.Account;
import model.Notification;
import model.Tag;

/**
 *
 * @author BK
 */
@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 10, // 10 MB 
        maxFileSize = 1024 * 1024 * 50, // 50 MB
        maxRequestSize = 1024 * 1024 * 100)

public class UploadWork extends HttpServlet {

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        TagDBContext tdb = new TagDBContext();
        NotificationDBContext ndb = new NotificationDBContext();
        Account account = new Account();
        account = (Account) request.getSession().getAttribute("account");
        
        ArrayList<Notification> notifications = ndb.getNotifications(account.getUsername());
        ArrayList<Tag> tags = tdb.getTags();
        request.setAttribute("tags", tags);
        request.setAttribute("notifications", notifications);
        request.getRequestDispatcher("/view/affiliates/IlluPost.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PostDBContext pdb = new PostDBContext();
        AccountDBContext adb = new AccountDBContext();
        TagDBContext tdb = new TagDBContext();
        ServletOutputStream output = response.getOutputStream();

        String name = request.getParameter("title");
        String description = request.getParameter("desc");
        int opento = 0;
        if (request.getParameter("open").equalsIgnoreCase("public")) {
            opento = 1;
        } else if (request.getParameter("open").equalsIgnoreCase("private")) {
            opento = 0;
        }
        int comments = 0;
        if (request.getParameter("comment").equalsIgnoreCase("on")) {
            comments = 1;
        } else if (request.getParameter("comment").equalsIgnoreCase("off")) {
            comments = 0;
        }
        InputStream inputStream = null; // input stream of the upload file

        // obtains the upload file part in this multipart request
        Part filePart = request.getPart("img");
        if (filePart != null) {
            // prints out some information for debugging
            //output.println(filePart.getName());
            //output.println(filePart.getSize());
            ///output.println(filePart.getContentType());

            // obtains input stream of the upload file
            inputStream = filePart.getInputStream();
        }
        String uniqueID = UUID.randomUUID().toString();

        Account account = (Account) request.getSession().getAttribute("account");
        String username = account.getUsername();

        String[] tagChoice = request.getParameterValues("tagChoice");

        adb.addAccountWork(username, uniqueID);
        pdb.AddPost(uniqueID, name, description, inputStream, opento, comments);
        for (int i = 0; i < tagChoice.length; i++) {
            tdb.addWorkTag(uniqueID, tagChoice[i]);
        }

        response.sendRedirect(request.getContextPath() + "/home");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
