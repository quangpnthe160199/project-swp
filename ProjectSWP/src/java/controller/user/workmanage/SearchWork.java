/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.user.workmanage;

import dal.PostDBContext;
import java.io.IOException;

import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Account;
import model.Ads;
import model.Post;
import model.PinPost;

/**
 *
 * @author BK
 */
public class SearchWork extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int recordsPerPage = 4;
        int ppage = 1;
        int pageIndex;
        int tag = Integer.parseInt(request.getParameter("tags"));
        int savednum = Integer.parseInt(request.getParameter("savednum"));
        

        PostDBContext pdb = new PostDBContext();
        Account account = (Account) request.getSession().getAttribute("account");
        try {
            pageIndex = Integer.parseInt(request.getParameter("page"));
        } catch (NumberFormatException e) {
            pageIndex = 1;
        }

        ArrayList<Post> Posts = pdb.GetWork(pageIndex);

        //response.getWriter().write(request.getParameter("work"));
        if (request.getParameter(
                "work") != null) {
            String workname = request.getParameter("work");
            Posts = pdb.GetWorkWname(pageIndex, workname);
        }

        if (request.getParameter(
                "order") != null) {
            String order = request.getParameter("order");
            Posts = pdb.SortWorkbyOrder(order);
        }

        request.setAttribute("post", Posts);
        int onoOfRecords = pdb.GetNoOfRecord();
        int onoOfPages = (int) Math.ceil(onoOfRecords * 1.0 / recordsPerPage);

        request.setAttribute("pageIndex", pageIndex);
        request.setAttribute("onoOfRecords", onoOfRecords);
        request.setAttribute("onoOfPages", onoOfPages);

        request.setAttribute("onoOfRecords", onoOfRecords);
        request.setAttribute("onoOfPages", onoOfPages);
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
