/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.user.workmanage;

import dal.AccountDBContext;
import dal.PostDBContext;
import dal.TagDBContext;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.UUID;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import model.Account;
import model.Post;
import model.Tag;
import validator.Validator;

/**
 *
 * @author BK
 */

@MultipartConfig(fileSizeThreshold=1024*1024*10, 	// 10 MB 
                 maxFileSize=1024*1024*50,      	// 50 MB
                 maxRequestSize=1024*1024*100)   
public class EditWork extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet EditWork</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet EditWork at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Account account = (Account) request.getSession().getAttribute("account");
        PostDBContext pdb = new PostDBContext();
        Post post = new Post();
        if (request.getParameter("id") != null) {
            String workid = request.getParameter("id");
            post = pdb.GetUserWork(workid);
        }
        TagDBContext tdb = new TagDBContext();
        ArrayList<Tag> tags = tdb.getTags();
        request.setAttribute("tags", tags);
        request.setAttribute("post", post);
        request.getRequestDispatcher("/view/affiliates/IlluEdit.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PostDBContext pdb = new PostDBContext();
        AccountDBContext adb = new AccountDBContext();
        TagDBContext tdb = new TagDBContext();
        ServletOutputStream output = response.getOutputStream();
        int opento = 0;
        if (request.getParameter("open").equalsIgnoreCase("public")) {
            opento = 1;
        } else if (request.getParameter("open").equalsIgnoreCase("private")) {
            opento = 0;
        }
        int comments = 0;
        if (request.getParameter("comment").equalsIgnoreCase("on")) {
            comments = 1;
        } else if (request.getParameter("comment").equalsIgnoreCase("off")) {
            comments = 0;
        }
        String name = request.getParameter("title");
        String description = request.getParameter("desc");

        InputStream inputStream = null; // input stream of the upload file
        // obtains the upload file part in this multipart request
        Part filePart = request.getPart("img");
        if (filePart != null) {
            // prints out some information for debugging
            output.println(filePart.getName());
            output.println(filePart.getSize());
            output.println(filePart.getContentType());

            // obtains input stream of the upload file
            inputStream = filePart.getInputStream();
        }
        String workid = request.getParameter("workid");

        Account account = (Account) request.getSession().getAttribute("account");
        String username = account.getUsername();

        String[] tagChoice = request.getParameterValues("tagChoice");
        pdb.deletePostTag(workid);
        pdb.UpdateWork(workid, name, description, inputStream, opento, comments);
        for (int i = 0; i < tagChoice.length; i++) {
            tdb.addWorkTag(workid, tagChoice[i]);
        }
        
        response.sendRedirect(request.getContextPath() + "/home/delete");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
