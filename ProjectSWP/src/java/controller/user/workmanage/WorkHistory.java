/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.user.workmanage;

import dal.AccountDBContext;
import dal.PostDBContext;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Account;
import model.MutedTag;
import model.MutedUser;
import model.Post;
import model.PostHistory;
import validator.Validator;

/**
 *
 * @author BK
 */
public class WorkHistory extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Account a = (Account) request.getSession().getAttribute("account");
        PostDBContext pdb = new PostDBContext();
        AccountDBContext adb = new AccountDBContext();
        Validator validate = new Validator();
        model.AccountSubcription as = new model.AccountSubcription();

        if (validate.IsPremium(a.getUsername())) {
            validate.PremiumRoleChecker(a.getUsername());
            as = adb.getSubcription(a.getUsername());
            request.getSession().setAttribute("subcription", as);
        }
        ArrayList<PostHistory> hp = new ArrayList<>();
        if (as == null){
            hp = pdb.GetPostHistoryDate(a.getUsername(), 0);
        } else{
            hp = pdb.GetPostHistoryDate(a.getUsername(), 1);
        }
        
        request.setAttribute("history", hp);

        ArrayList<MutedUser> UserMutedWork = adb.GetMutedUsers(a.getUsername(), 0);
        ArrayList<MutedTag> UserMutedTag = adb.GetMutedTags(a.getUsername(), 0);
        request.setAttribute("mutedPost", UserMutedWork);
        request.setAttribute("mutedTag", UserMutedTag);
        request.getRequestDispatcher("/view/affiliates/history.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
