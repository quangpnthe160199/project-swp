/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.user.workmanage;

import dal.AccountDBContext;
import dal.PostDBContext;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Account;
import model.AccountDetail;
import model.Post;
import validator.Validator;

/**
 *
 * @author BK
 */
public class DeleteWork extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Account account = (Account) request.getSession().getAttribute("account");
        PostDBContext pdb = new PostDBContext();
        String username = "";
        AccountDBContext adb = new AccountDBContext();
        model.AccountSubcription as = new model.AccountSubcription();
        if (account == null) {
            username = "";
        } else {
            username = account.getUsername();
        }
        String base64Ava = adb.getbase64Ava(username);
        String base64BG = adb.getbase64BG(username);
        AccountDetail accPic = adb.getAccountDetailByName(username);
        accPic.setAva(base64Ava);
        accPic.setBackground(base64BG);
        request.setAttribute("accPic", accPic);
        if (request.getParameter("id") != null) {
            String workid = request.getParameter("id");
            pdb.deletePost(workid);
            pdb.deleteAccountPost(workid);
            
        }
        Validator validate = new Validator();
        if (validate.IsPremium(username)) {
            validate.PremiumRoleChecker(username);
            as = adb.getSubcription(username);
            
        }

        int pageIndex;
        try {
            pageIndex = Integer.parseInt(request.getParameter("page"));
        } catch (NumberFormatException e) {
            pageIndex = 1;
        }
        int recordsPerPage = 4;

        ArrayList<Post> Posts = pdb.GetUserWorks(pageIndex, account.getUsername());
        request.setAttribute("post", Posts);
        int onoOfRecords = pdb.GetNoOfRecord();
        int onoOfPages = (int) Math.ceil(onoOfRecords * 1.0 / recordsPerPage);
        request.setAttribute("pageIndex", pageIndex);
        request.setAttribute("onoOfRecords", onoOfRecords);
        request.setAttribute("onoOfPages", onoOfPages);
        request.setAttribute("subcription", as);

        request.getRequestDispatcher("/view/affiliates/IlluDelete.jsp").forward(request, response);

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        /*String company = request.getParameter("cid");
        String product = request.getParameter("pid");
        OrderDBContext odb = new OrderDBContext();
        Product p = new Product();
        String invoiceid = request.getParameter("ivd");
        Order o = new Order();
        o.setInvoice_id(invoiceid);
        p.setProductid(product);
        o.setP(p);
        odb.deleteOrder(o);
        response.sendRedirect(request.getContextPath() + "/search");*/
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
