/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.user.workmanage;

import dal.AccountDBContext;
import dal.PostDBContext;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Account;
import model.AccountDetail;
import model.Post;

/**
 *
 * @author BK
 */
public class PinWork extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet PinWork</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet PinWork at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Account account = (Account) request.getSession().getAttribute("account");

        String username = account.getUsername();
        AccountDBContext adb = new AccountDBContext();
        String base64Ava = adb.getbase64Ava(username);
        String base64BG = adb.getbase64BG(username);
        AccountDetail accPic = adb.getAccountDetailByName(username);
        accPic.setAva(base64Ava);
        accPic.setBackground(base64BG);
        request.setAttribute("accPic", accPic);
 
        processRequest(request, response);

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Account account = (Account) request.getSession().getAttribute("account");
        PostDBContext pdb = new PostDBContext();
        String workid = "";
        PrintWriter out = response.getWriter();

        if (account != null) {

            workid = request.getParameter("workid");
            StringBuffer sb = new StringBuffer(workid);
            sb.deleteCharAt(sb.length() - 1);
            workid = sb.toString();
            if (request.getParameter("type").equalsIgnoreCase("delete")) {
                pdb.DeletePin(account.getUsername(), workid);
            } else if (request.getParameter("type").equalsIgnoreCase("insert")) {
                pdb.AddPin(account.getUsername(), workid);
            }
        }
        Post PostLike = pdb.GetWork(workid);
        out.print(PostLike.getSaved());
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
