/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.homepage;

import dal.AccountDBContext;
import dal.AdsDBContext;
import dal.NotificationDBContext;
import dal.PostDBContext;
import dal.TagDBContext;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Account;
import model.AccountSubcription;
import model.Ads;
import model.AccountDetail;
import model.Notification;

import model.MutedTag;
import model.MutedUser;

import model.Post;
import model.PinPost;
import model.PostLike;
import model.PostTag;
import model.Tag;
import validator.Validator;

/**
 *
 * @author BK
 */
public class DisplayWorks extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Account account = (Account) request.getSession().getAttribute("account");
        PostDBContext pdb = new PostDBContext();
        AccountDBContext adb = new AccountDBContext();
        AccountSubcription as = new AccountSubcription();
        Validator validate = new Validator();
        AdsDBContext adsDB = new AdsDBContext();
        AccountDetail acc = new AccountDetail();
        NotificationDBContext ndb = new NotificationDBContext();
        String username = "";
        int tag = 0;
        int savednum = 0;
        String workname = "";
        int recordsPerPage = 4;
        // Get user current liked work
        ArrayList<PinPost> UserPinnedPost = pdb.GetPin("");
        ArrayList<PostLike> UserLikedPost = pdb.GetLike("");
        ArrayList<MutedUser> UserMutedWork = adb.GetMutedUsers("", 0);
        ArrayList<MutedTag> UserMutedTag = adb.GetMutedTags("", 0);
        ArrayList<Notification> notifications = ndb.getNotifications("");

        int ppage = 1;

        int pageIndex;
        // Check user current page index
        try {
            pageIndex = Integer.parseInt(request.getParameter("page"));
        } catch (NumberFormatException e) {
            pageIndex = 1;
        }

        if (request.getParameter("tags") != null || request.getParameter("savenum") != null || request.getParameter("work") != null) {
            tag = Integer.parseInt(request.getParameter("tags"));
            savednum = Integer.parseInt(request.getParameter("savednum"));
            workname = request.getParameter("work");
        }

        ArrayList<Post> Posts = new ArrayList<>();
        if (account == null) {

            Posts = pdb.GetWorkOption(pageIndex, workname, tag, savednum, "asc");
            username = "";

            // If user used search work option
            if (request.getParameter("search") != null) {
                request.getSession().setAttribute("tag", tag);
                request.getSession().setAttribute("savednum", savednum);
                request.getSession().setAttribute("work", workname);
                Posts = pdb.GetWorkOption(pageIndex, workname, tag, savednum, "ASC");
            }

            if (request.getParameter("order") != null) {

                String order = request.getParameter("order");
                Posts = pdb.GetWorkOption(pageIndex, workname, tag, savednum, order);
            }
        } else {
            username = account.getUsername();
            acc = adb.getAccountDetailByName(username);
            UserMutedWork = adb.GetMutedUsers(username, 0);
            UserMutedTag = adb.GetMutedTags(username, 0);
            username = account.getUsername();
            UserPinnedPost = pdb.GetPin(username);
            UserLikedPost = pdb.GetLike(username);
            Posts = pdb.GetWorkOption(pageIndex, workname, tag, savednum, "asc");

            //Get user's notifications
            notifications = ndb.getNotifications(username);

            if (validate.IsPremium(username)) {
                validate.PremiumRoleChecker(username);
                as = adb.getSubcription(username);
                request.getSession().setAttribute("subcription", as);
            }

            if (request.getParameter("search") != null) {
                request.getSession().setAttribute("tag", tag);
                request.getSession().setAttribute("savednum", savednum);
                request.getSession().setAttribute("work", workname);
                Posts = pdb.GetWorkOption(pageIndex, workname, tag, savednum, "ASC");
            }

            if (request.getParameter("order") != null) {

                String order = request.getParameter("order");
                Posts = pdb.GetWorkOption(pageIndex, workname, tag, savednum, order);
            }

        String base64Ava = adb.getbase64Ava(username);
        String base64BG = adb.getbase64BG(username);
        AccountDetail accPic = adb.getAccountDetailByName(username);
        accPic.setAva(base64Ava);
        accPic.setBackground(base64BG);
        request.getSession().setAttribute("accPic", accPic);
        }
        TagDBContext tdb = new TagDBContext();
        ArrayList<Tag> tags = tdb.getTags();
        request.setAttribute("tags", tags);

        // Get ads in the system
        ArrayList<Ads> ads = adsDB.getAds();
        // Set the current post list and the paging system accordingly
        int onoOfRecords = pdb.GetNoOfRecord();
        int onoOfPages = (int) Math.ceil(onoOfRecords * 1.0 / recordsPerPage);
        String message = (String) request.getSession().getAttribute("Message");
        request.setAttribute("post", Posts);
        request.setAttribute("ads", ads);
        request.setAttribute("mutedPost", UserMutedWork);
        request.setAttribute("mutedTag", UserMutedTag);
        request.setAttribute("pageIndex", pageIndex);
        request.setAttribute("onoOfRecords", onoOfRecords);
        request.setAttribute("onoOfPages", onoOfPages);
        request.getSession().setAttribute("notifications", notifications);
        request.setAttribute("likedPost", UserLikedPost);
        request.setAttribute("savedPost", UserPinnedPost);
        request.setAttribute("acc", acc);
        request.getSession().setAttribute("Message", null);
        request.setAttribute("message", message);

        request.getRequestDispatcher("/view/affiliates/home.jsp").forward(request, response);

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}