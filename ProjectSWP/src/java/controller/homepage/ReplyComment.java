/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.homepage;

import dal.NotificationDBContext;
import dal.PostDBContext;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Comment;

/**
 *
 * @author haiph
 */
public class ReplyComment extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String workid = request.getParameter("workid");
        String cid = request.getParameter("cid");
        String content = request.getParameter("content");
        String username = request.getParameter("username");
        PostDBContext pdb = new PostDBContext();        
        pdb.addReply(cid, username, content);
        /*
        String receivername = pdb.getCommentUsername(cid);
        if (!username.equals(receivername)) {
            NotificationDBContext ndb = new NotificationDBContext();
            ndb.addNotification(username, receivername, "replied to your comment", new Timestamp(System.currentTimeMillis()), workid);
        }*/ 
        /*
        ArrayList<Comment> comments = pdb.getWorkComments(workid);
        for (Comment c : comments) {
            c.setReplies(pdb.getReplies(c.getCid()));
        }
        request.setAttribute("comments", comments);
        request.setAttribute("workid", workid);
        request.getRequestDispatcher("/view/affiliates/displaycomments.jsp").forward(request, response);
        */
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
