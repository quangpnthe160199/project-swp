/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.homepage;

import dal.AccountDBContext;
import dal.PostDBContext;
import dal.TagDBContext;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Account;
import model.AccountDetail;
import model.PinPost;
import model.Post;
import model.PostLike;
import model.Tag;

/**
 *
 * @author Dell
 */
public class DisplayWorkByTag extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
      
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Account account = (Account) request.getSession().getAttribute("account");
        String username;
        PostDBContext pdb = new PostDBContext();
        TagDBContext tdb= new TagDBContext();

        int pageIndex;
        try{
            pageIndex = Integer.parseInt(request.getParameter("page"));
        }catch(NumberFormatException e){
            pageIndex = 1;
        }
        int recordsPerPage = 4;
        ArrayList<Post> Posts = pdb.GetWork(pageIndex);
        
 if (request.getParameter("work") != null){
        String workname = request.getParameter("work");
        Posts = pdb.GetWorkWname(pageIndex, workname);
        }
     
      String tagid = request.getParameter("tagid");
        Posts = tdb.GetWorkbyTagId(tagid);
     


        request.setAttribute("post", Posts);
        int onoOfRecords = pdb.GetNoOfRecord();
        int onoOfPages = (int) Math.ceil(onoOfRecords * 1.0 / recordsPerPage);
        request.setAttribute("pageIndex", pageIndex);
        request.setAttribute("onoOfRecords", onoOfRecords);
        request.setAttribute("onoOfPages", onoOfPages);

        
        ArrayList<PinPost> UserPinnedPost = pdb.GetPin("");
        ArrayList<PostLike> UserLikedPost = pdb.GetLike("");
         
        
        if (account!=null){
          username = account.getUsername();
          UserPinnedPost = pdb.GetPin(username);
          UserLikedPost = pdb.GetLike(username);
          request.setAttribute("savedPost", UserPinnedPost);
          request.setAttribute("likedPost", UserLikedPost);
          
         AccountDBContext adb = new AccountDBContext();
         String base64Ava = adb.getbase64Ava(username);
        String base64BG = adb.getbase64BG(username);
        AccountDetail accPic = adb.getAccountDetailByName(username);
        accPic.setAva(base64Ava);
        accPic.setBackground(base64BG);
        request.setAttribute("accPic", accPic);
        }
        else {username ="";}
        
     
        
        request.setAttribute("likedPost", UserLikedPost);      
        request.setAttribute("savedPost", UserPinnedPost);

        request.getRequestDispatcher("/view/affiliates/tag.jsp").forward(request, response); 
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
