/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.homepage;


import dal.NotificationDBContext;
import dal.PostDBContext;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;

import dal.AccountDBContext;

import java.sql.SQLException;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Account;
import model.AccountDetail;
import model.Comment;
import model.MutedTag;
import model.MutedUser;
import model.PinPost;
import model.Post;
import model.PostLike;
import model.Tag;
import validator.Validator;

/**
 *
 * @author BK
 */
@WebServlet(name = "ViewWork", urlPatterns = {"/home/works"})
public class ViewWork extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        response.setContentType("text/html;charset=UTF-8");
        PostDBContext pdb = new PostDBContext();
        Post post = new Post();
        ArrayList<PinPost> UserPinnedPost = pdb.GetPin("");
        ArrayList<PostLike> UserLikedPost = pdb.GetLike("");
        MutedUser mu = new MutedUser();
        ArrayList<MutedTag> mt = new ArrayList<>();
        Account account = (Account) request.getSession().getAttribute("account");
        AccountDBContext adb = new AccountDBContext();
        ArrayList<Tag> tag = new ArrayList<>();
        ArrayList<Comment> comments = new ArrayList<>();

        if (account != null) {
            if (request.getParameter("workid") != null) {

                String workid = request.getParameter("workid");
                tag = pdb.GetPostTag(workid);
                pdb.AddView(account.getUsername(), workid);
                post = pdb.GetWork(workid);
                comments = pdb.getWorkComments(workid);
                mt = adb.GetMutedTags(account.getUsername(), 0);
                mu = adb.GetMutedUser(account.getUsername(), post.getAp().getA().getUsername());
                UserPinnedPost = pdb.GetPin(account.getUsername());
                UserLikedPost = pdb.GetLike(account.getUsername());
                for (Comment c : comments) {
                    c.setReplies(pdb.getReplies(c.getCid()));
                }
                request.setAttribute("workid", workid);
            }
        } else {
            String workid = request.getParameter("workid");
            tag = pdb.GetPostTag(workid);
            post = pdb.GetWork(workid);
            comments = pdb.getWorkComments(workid);
            for (Comment c : comments) {
                c.setReplies(pdb.getReplies(c.getCid()));
            }
            request.setAttribute("workid", workid);

        }
        String base64Ava = adb.getbase64Ava(post.getAp().getA().getUsername());
        AccountDetail accPic = new AccountDetail();
        accPic.setAva(base64Ava);
        request.setAttribute("userpic", accPic);
        request.setAttribute("mutedtag", mt);
        request.setAttribute("muteduser", mu);

        request.setAttribute("comments", comments);
        request.setAttribute("tags", tag);
        request.setAttribute("post", post);
        request.setAttribute("likedPost", UserLikedPost);
        request.setAttribute("savedPost", UserPinnedPost);
        request.getRequestDispatcher("/view/affiliates/viewwork.jsp").forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(ViewWork.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String cid = request.getParameter("cid");
        String workid = request.getParameter("workid");
        String content = request.getParameter("content");
        String username = request.getParameter("username");
        PostDBContext pdb = new PostDBContext();
        pdb.addComment(cid, workid, username, content);
        /*
        String receivername = pdb.getWorkUsername(workid);
        ArrayList<Comment> comments = pdb.getWorkComments(workid);
        for (Comment c : comments) {
            c.setReplies(pdb.getReplies(c.getCid()));
        }
        request.setAttribute("comments", comments);
        request.setAttribute("workid", workid);
        request.getRequestDispatcher("/view/affiliates/displaycomments.jsp").forward(request, response);
        */
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
