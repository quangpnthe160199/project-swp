/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.homepage;

import java.util.Collections;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

/**
 *
 * @author haiph
 */
@ServerEndpoint("/comment")
public class CommentEndpoint {
private static Set<Session> userSessions = Collections.newSetFromMap(new ConcurrentHashMap<Session, Boolean>());
    @OnOpen
    public void onOpen(Session session) {
        userSessions.add(session);
    }

    @OnClose
    public void onClose(Session session) {
        userSessions.remove(session);
    }

    @OnMessage
    public void onMessage(String comment, Session userSession) {
        for(Session sus : userSessions){
            sus.getAsyncRemote().sendText(comment);
        }
    }
}
