/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.login;

import dal.AccountDBContext;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Account;
import validator.Validator;
import validator.VerifyRecaptcha;

/**
 *
 * @author admin
 */
public class LoginController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet LoginController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet LoginController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("/view/affiliates/login.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");

        AccountDBContext db = new AccountDBContext();
        VerifyRecaptcha verify = new VerifyRecaptcha();
        Account account = db.GetAccount(username, password);
        boolean checkFirstEdit = db.getFirstEdit(username);

        //trung duc start
        int adminpermission=db.getAdminPermission(username,"/home/dashboard");        
        int permission = db.isBanned(username);
        if (account == null) {
            Validator x = new Validator();
            String msg = x.checkLogin(username, password);
            request.setAttribute("msg", msg);
            request.getRequestDispatcher("/view/affiliates/login.jsp").forward(request, response);
            
        } else if (permission == 1) {
            request.getRequestDispatcher("/view/affiliates/banUser.jsp").forward(request, response);
        } else if(adminpermission==1){
             request.getSession().setAttribute("account", account);
                      response.sendRedirect(request.getContextPath() + "/home/dashboard");
        } else {

            account.setIsFirstEdit(checkFirstEdit);
            
            if (!verify.verify(request.getParameter("g-recaptcha-response"))){
               String msg = "Please enter captcha!"; 
               request.setAttribute("msg", msg);
            request.getRequestDispatcher("/view/affiliates/login.jsp").forward(request, response);
            } else{
                request.getSession().setAttribute("account", account);
            response.sendRedirect(request.getContextPath() + "/home");
            }
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public static void main(String[] args) {
        AccountDBContext db = new AccountDBContext();
        int i = db.isBanned("mre");
        if (i == 1) {

            System.out.println(i);
        }
        else System.out.println("no");
    }
}
