/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.email;

import dal.AccountDBContext;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Account;
import model.AccountDetail;
import utility.EmailUtility;

/**
 *
 * @author admin
 */
public class EmailSendingController extends HttpServlet {

    private String host;
    private String port;
    private String user;
    private String pass;
    private String recipient;
    public void init() {
        // reads SMTP server setting from web.xml file
        ServletContext context = getServletContext();
        host = context.getInitParameter("host");
        port = context.getInitParameter("port");
        user = context.getInitParameter("user");
        pass = context.getInitParameter("pass");
        recipient = context.getInitParameter("recipient");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String subject = "Password Reset";
        String username = (String)request.getSession().getAttribute("username");
        String code = (String)request.getSession().getAttribute("code");
        AccountDBContext db = new AccountDBContext();
        AccountDetail ad = db.getAccountDetail(username);
        String content = "<a>Security Code: "+ code+"</a>";
        String recepient = ad.getEmail();
        try {
            EmailUtility.sendEmail(host, port, user, pass, recepient, subject,
                    content);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            request.getSession().setAttribute("username", username);
            request.setAttribute("code", code);
            request.getRequestDispatcher("/view/affiliates/resetpsw/code.jsp").forward(request, response);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String page =  request.getParameter("page");
        String subject = request.getParameter("subject");
        String email = request.getParameter("email");
        String content = request.getParameter("content");
        Account acc = (Account) request.getSession().getAttribute("account");
        AccountDBContext db = new AccountDBContext();
        if (acc == null) {
            content = "<a> Reply Email: "+email+"</a><br/> <a> Content:<br/> " + content +"</a>";
        } else {
            AccountDetail ad = db.getAccountDetail(acc.getUsername());
            content = "<a>Username: "+acc.getUsername()+"</a><br/><a>User Email: "+
                    ad.getEmail()+"</a><br/><a> Reply Email: "+email+"</a><br/> <a> Content:<br/> " + content +"</a>";
                    
        }
        
        
 
        String resultMessage = "";
 
        try {
            EmailUtility.sendEmail(host, port, user, pass, recipient, subject,
                    content);
            resultMessage = "The e-mail was sent successfully";
        } catch (Exception ex) {
            ex.printStackTrace();
            resultMessage = "There were an error, please try again";
        } finally {
            request.getSession().setAttribute("Message", resultMessage);
            if (page.equals("home")) response.sendRedirect("/ProjectSWP/home");
            else response.sendRedirect("/ProjectSWP/home/contact");
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
