/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.fanbox;

import dal.AccountDBContext;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Account;
import model.AccountDetail;

/**
 *
 * @author admin
 */
public class FanboxSubscribePurchaseController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet FanboxSubscribePurchaseController</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet FanboxSubscribePurchaseController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Account acc = (Account) request.getSession().getAttribute("account");
        String username = acc.getUsername();
        String password = acc.getPassword();
        String displayname = acc.getDisplayname();
        String price = request.getParameter("price");
        String type = request.getParameter("type");
        
        request.setAttribute("price", price);
        request.setAttribute("username", username);
        request.setAttribute("password", password);
        request.setAttribute("displayname", displayname);
        request.setAttribute("type",type);
        AccountDBContext db = new AccountDBContext();
        AccountDetail ad = db.getAccountDetail(username);
        request.setAttribute("address", ad.getAddress());
        request.setAttribute("email", ad.getEmail());
        request.setAttribute("fullname", ad.getFullname());
        request.setAttribute("gender", ad.getGender());
        request.setAttribute("mobile", ad.getMobile());
        response.getWriter().print(price);
        request.getRequestDispatcher("/view/affiliates/VNPay/fanboxsub.jsp").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
