/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.fanbox;

import controller.homepage.ViewWork;
import dal.AccountDBContext;
import dal.FanboxDBContext;
import dal.PostDBContext;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Account;
import model.Comment;
import model.FanboxPost;
import model.FanboxPostLike;
import model.MutedTag;
import model.MutedUser;
import model.PinPost;
import model.Post;
import model.PostLike;
import model.Tag;
import validator.Validator;

/**
 *
 * @author admin
 */
public class FanboxPostViewController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        response.setContentType("text/html;charset=UTF-8");
        FanboxDBContext fdb = new FanboxDBContext();
        ArrayList<Tag> tag = new ArrayList<>();
        if (request.getParameter("id") != null) {

                String id = request.getParameter("id");
                FanboxPost fpost = fdb.GetWork(id);
                Account acc = (Account)request.getSession().getAttribute("account");
                fdb.AddView(acc.getUsername(),id);
                tag = fdb.GetPostTag(id);
                request.setAttribute("id", id);
                request.setAttribute("tags", tag);
                request.setAttribute("post", fpost);
                ArrayList<FanboxPostLike> fpl = fdb.getLikes(id);
                request.setAttribute("likedPost", fpl);
                request.getRequestDispatcher("/view/affiliates/fanboxpostview.jsp").forward(request, response);
            }
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(FanboxPostViewController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
