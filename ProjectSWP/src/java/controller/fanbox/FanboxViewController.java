/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.fanbox;

import dal.AccountDBContext;
import dal.FanboxDBContext;
import dal.NotificationDBContext;
import dal.PostDBContext;
import dal.TagDBContext;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.UUID;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import model.Account;
import model.AccountDetail;
import model.Notification;
import model.Post;
import model.Tag;

/**
 *
 * @author admin
 */
@MultipartConfig(fileSizeThreshold=1024*1024*10, 	// 10 MB 
                 maxFileSize=1024*1024*50,      	// 50 MB
                 maxRequestSize=1024*1024*100)  
public class FanboxViewController extends HttpServlet {



    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //Posts
        String username = "";
        if (request.getSession().getAttribute("account") != null) {
            Account acc = (Account) request.getSession().getAttribute("account");
            username = acc.getUsername();
        }
        FanboxDBContext db = new FanboxDBContext();
        AccountDBContext adb = new AccountDBContext();
        int pageIndex;
        try {
            pageIndex = Integer.parseInt(request.getParameter("page"));
        } catch (NumberFormatException e) {
            pageIndex = 1;
        }
        int recordsPerPage = 4;
        String fid = db.getFanboxID(username);
        ArrayList<Post> Posts = db.getFanboxPosts(pageIndex, fid);


        request.setAttribute("post", Posts);
        int onoOfRecords = db.GetNoOfFanboxRecord(fid);
        int onoOfPages = (int) Math.ceil(onoOfRecords * 1.0 / recordsPerPage);
        request.setAttribute("pageIndex", pageIndex);
        request.setAttribute("onoOfRecords", onoOfRecords);
        request.setAttribute("onoOfPages", onoOfPages);
        //End posts
        
        String fname = db.getFanboxName(fid);
        request.setAttribute("FanboxName", fname);
        TagDBContext tdb = new TagDBContext();
        NotificationDBContext ndb = new NotificationDBContext();
        ArrayList<Tag> tags = tdb.getTags();
        ArrayList<Notification> notifications = ndb.getNotifications(username);
        ArrayList<Account> subs = db.getFanboxSubscribers(fid);
        String base64Ava = adb.getbase64Ava(username);
        AccountDetail accPic = new AccountDetail();
        accPic.setAva(base64Ava);
        request.setAttribute("userpic", accPic);
        request.setAttribute("tags", tags);
        request.setAttribute("subs", subs);
        request.setAttribute("notifications", notifications);
        request.getRequestDispatcher("/view/affiliates/fanbox.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        FanboxDBContext db = new FanboxDBContext();
        TagDBContext tdb = new TagDBContext();
        ArrayList<Tag> tags = tdb.getTags();
        ServletOutputStream output = response.getOutputStream();

        String name = request.getParameter("title");
        String description = request.getParameter("desc");
        int opento = 0;
        if (request.getParameter("open").equalsIgnoreCase("public")) {
            opento = 1;
        } else if (request.getParameter("open").equalsIgnoreCase("private")) {
            opento = 0;
        }
        int comments = 0;
        if (request.getParameter("comment").equalsIgnoreCase("on")) {
            comments = 1;
        } else if (request.getParameter("comment").equalsIgnoreCase("off")) {
            comments = 0;
        }
        InputStream inputStream = null; // input stream of the upload file

        // obtains the upload file part in this multipart request
        Part filePart = request.getPart("img");
        if (filePart != null) {
            // prints out some information for debugging
            //output.println(filePart.getName());
            //output.println(filePart.getSize());
            ///output.println(filePart.getContentType());

            // obtains input stream of the upload file
            inputStream = filePart.getInputStream();
        }
        String uniqueID = UUID.randomUUID().toString();

        Account account = (Account) request.getSession().getAttribute("account");
        String username = account.getUsername();

        String[] tagChoice = request.getParameterValues("tagChoice");
        String fid = db.getFanboxID(account.getUsername());
        
        db.AddPost(uniqueID,fid ,name, description, inputStream, comments);
        for (int i = 0; i < tagChoice.length; i++) {
            tdb.addFWorkTag(uniqueID, tagChoice[i]);
        }
        

       
        response.sendRedirect( request.getContextPath() + "/home/viewfanbox");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
