/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.fanbox;

import com.vnpay.common.Config;
import dal.AccountDBContext;
import dal.FanboxDBContext;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author admin
 */
public class FanboxSubscribeReturnController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet FanboxSubscribeReturnController</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet FanboxSubscribeReturnController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Map fields = new HashMap();
        AccountDBContext adb = new AccountDBContext();
        FanboxDBContext fdb = new FanboxDBContext();
        for (Enumeration params = request.getParameterNames(); params.hasMoreElements();) {
            String fieldName = (String) params.nextElement();
            String fieldValue = request.getParameter(fieldName);
            if ((fieldValue != null) && (fieldValue.length() > 0)) {
                fields.put(fieldName, fieldValue);
            }
        }
        String username = request.getParameter("username");
        //String fid = (String) request.getSession().getAttribute("fid");
        String vnp_SecureHash = request.getParameter("vnp_SecureHash");
        if (fields.containsKey("vnp_SecureHashType")) {
            fields.remove("vnp_SecureHashType");
        }
        if (fields.containsKey("vnp_SecureHash")) {
            fields.remove("vnp_SecureHash");
        }
        String signValue = Config.hashAllFields(fields);
        String PurchaseID = request.getParameter("vnp_TxnRef");
        int Amount = Integer.parseInt(request.getParameter("vnp_Amount"));
        String Info = request.getParameter("vnp_OrderInfo");
        String[] AccountOrder = Info.split("_", 5);
        username = AccountOrder[0];
        String type = AccountOrder[1];
        String fid = AccountOrder[2];
        request.getParameter("vnp_ResponseCode");
        request.getParameter("vnp_TransactionNo");
        String bankcode = request.getParameter("vnp_BankCode");
        request.getParameter("vnp_PayDate");
        if (signValue.equals(vnp_SecureHash)) {
            if ("00".equals(request.getParameter("vnp_ResponseCode"))) {
                response.getWriter().print("GD Thanh cong");
                //response.getWriter().write(PurchaseID + " " + username + " " + Amount + " " + type + " " + bankcode + " " + type.equalsIgnoreCase("1month") + " " + type.equalsIgnoreCase("6month") + " " + type.equalsIgnoreCase("12month"));
                adb.AccountPurchase(PurchaseID, username, Amount, type, bankcode);
                if (type.equalsIgnoreCase("1monthfanbox")){
                    fdb.FanboxSubcription(PurchaseID, username, fid, 1);
                } else if (type.equalsIgnoreCase("6monthfanbox")){
                    fdb.FanboxSubcription(PurchaseID, username, fid, 6);
                } else if (type.equalsIgnoreCase("12monthfanbox")){
                    fdb.FanboxSubcription(PurchaseID, username, fid, 12);
                }

            } else {
                response.getWriter().print("GD Khong thanh cong");
            }

        } else {
            response.getWriter().print("Chu ky khong hop le");
        }

        response.sendRedirect( request.getContextPath() + "/home/fanbox/view?id="+fid);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
