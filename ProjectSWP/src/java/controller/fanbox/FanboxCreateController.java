/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.fanbox;

import dal.AccountDBContext;
import dal.FanboxDBContext;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.UUID;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Account;
import model.AccountDetail;

/**
 *
 * @author admin
 */
public class FanboxCreateController extends HttpServlet {

    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Account account = (Account) request.getSession().getAttribute("account");
         String  username = account.getUsername();
         AccountDBContext adb= new  AccountDBContext();
         String base64Ava = adb.getbase64Ava(username);
        String base64BG = adb.getbase64BG(username);
        AccountDetail accPic = adb.getAccountDetailByName(username);
        accPic.setAva(base64Ava);
        accPic.setBackground(base64BG);
        request.setAttribute("accPic", accPic);
        request.getRequestDispatcher("/view/affiliates/fanboxname.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String fname = (String) request.getParameter("fname");
        String fid = UUID.randomUUID().toString();
        Account acc = (Account) request.getSession().getAttribute("account");
        FanboxDBContext db = new FanboxDBContext();
        db.createFanbox(fid, acc.getUsername());
        FanboxDBContext db1 = new FanboxDBContext();
        db1.addFanboxDetails(fid, fname);   
        response.sendRedirect( request.getContextPath() + "/home/viewfanbox");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
