/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.fanbox;

import dal.AccountDBContext;
import dal.FanboxDBContext;
import dal.NotificationDBContext;
import dal.TagDBContext;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Account;
import model.AccountDetail;
import model.Notification;
import model.Post;
import model.Tag;

/**
 *
 * @author admin
 */
public class FanboxNonSubViewController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String id = request.getParameter("id");
        FanboxDBContext fdb = new FanboxDBContext();
        AccountDBContext adb = new AccountDBContext();
        Account acc = (Account) request.getSession().getAttribute("account");
        boolean check = fdb.IsSubscribed(acc.getUsername(), id);

        String name = fdb.getFanboxName(id);
        String username = fdb.getFanboxNameOwner(id);
        if (!check) {
            ArrayList<Post> Posts = fdb.getFanboxPreViewPost(id);
            request.setAttribute("id", id);
            request.setAttribute("name", name);
            request.setAttribute("post", Posts);
            request.getRequestDispatcher("/view/affiliates/fanboxpreview.jsp").forward(request, response);
        } else {
            int pageIndex;
            try {
                pageIndex = Integer.parseInt(request.getParameter("page"));
            } catch (NumberFormatException e) {
                pageIndex = 1;
            }
            int recordsPerPage = 4;
            ArrayList<Post> Posts = fdb.getFanboxPosts(pageIndex, id);
            int onoOfRecords = fdb.GetNoOfFanboxRecord(id);
            int onoOfPages = (int) Math.ceil(onoOfRecords * 1.0 / recordsPerPage);
            String base64Ava = adb.getbase64Ava(username);
            AccountDetail accPic = new AccountDetail();
            accPic.setAva(base64Ava);
            request.setAttribute("userpic", accPic);
            request.setAttribute("pageIndex", pageIndex);
            request.setAttribute("onoOfRecords", onoOfRecords);
            request.setAttribute("onoOfPages", onoOfPages);
            request.setAttribute("id", id);
            request.setAttribute("name", name);
            request.setAttribute("post", Posts);
            TagDBContext tdb = new TagDBContext();
            NotificationDBContext ndb = new NotificationDBContext();
            ArrayList<Tag> tags = tdb.getTags();
            ArrayList<Notification> notifications = ndb.getNotifications(acc.getUsername());
            request.setAttribute("tags", tags);
            request.setAttribute("notifications", notifications);
            request.getRequestDispatcher("/view/affiliates/fanboxview.jsp").forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
