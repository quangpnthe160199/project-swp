/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.message;

import dal.AccountDBContext;
import dal.MessageDBContext;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import model.Account;
import model.AccountDetail;
import model.Message;

/**
 *
 * @author haiph
 */
public class DisplayMessages extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        MessageDBContext mdb = new MessageDBContext();
        AccountDBContext adb = new AccountDBContext();
        Account acc = (Account) request.getSession().getAttribute("account");
        if (acc != null) {
            ArrayList<Message> messages = new ArrayList<>();
            request.setAttribute("messages", messages);
            ArrayList<String> contacts = mdb.getActiveContacts(acc.getUsername());
            contacts.addAll(mdb.getPassiveContacts(acc.getUsername()));
            Set<String> set = new HashSet<>(contacts);
            contacts.clear();
            contacts.addAll(set);
            request.setAttribute("contacts", contacts);
            String username = acc.getUsername();
            String base64Ava = adb.getbase64Ava(username);
            String base64BG = adb.getbase64BG(username);
            AccountDetail accPic = adb.getAccountDetailByName(username);
            accPic.setAva(base64Ava);
            accPic.setBackground(base64BG);
            request.setAttribute("accPic", accPic);
            request.getRequestDispatcher("../view/affiliates/messages.jsp").forward(request, response);
          
        } else {
            response.sendRedirect("/ProjectSWP/home");
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        MessageDBContext mdb = new MessageDBContext();
        Account acc = (Account) request.getSession().getAttribute("account");
        String contactname = request.getParameter("contactname");
        if (acc != null) {
            ArrayList<Message> messages = mdb.getPersonalMessages(acc.getUsername(), contactname);
            request.setAttribute("messages", messages);
            request.setAttribute("contactname", contactname);
        } else {
            ArrayList<Message> messages = new ArrayList<>();
            request.setAttribute("messages", messages);
            request.setAttribute("contactname", contactname);
        }
        request.getRequestDispatcher("../view/affiliates/displaymessages.jsp").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
