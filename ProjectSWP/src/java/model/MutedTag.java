/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author BK
 */
public class MutedTag {
    private Account a;
    private Tag t;

    public Account getA() {
        return a;
    }

    public void setA(Account a) {
        this.a = a;
    }

    public Tag getT() {
        return t;
    }

    public void setT(Tag t) {
        this.t = t;
    }
    
    
}
