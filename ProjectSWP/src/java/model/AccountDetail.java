/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author admin
 */
public class AccountDetail {

    private Account acc;
    private String fullname;
    private int gender;
    private String mobile;
    private String email;
    private String address;
    private byte[] avaImg;
//    private String base64Ava;
    private byte[] backgroundImg;
//    private String base64backGround;
    private String ava ;
    private String background ;

    public byte[] getAvaImg() {
        return avaImg;
    }

    public void setAvaImg(byte[] avaImg) {
        this.avaImg = avaImg;
    }

    public byte[] getBackgroundImg() {
        return backgroundImg;
    }

    public void setBackgroundImg(byte[] backgroundImg) {
        this.backgroundImg = backgroundImg;
    }

//    

    public String getAva() {
        return ava;
    }

    public void setAva(String ava) {
        this.ava = ava;
    }

    public String getBackground() {
        return background;
    }

    public void setBackground(String background) {
        this.background = background;
    }
    

    public Account getAcc() {
        return acc;
    }

    public void setAcc(Account acc) {
        this.acc = acc;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

}
