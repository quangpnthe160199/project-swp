/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Timestamp;

/**
 *
 * @author haiph
 */
public class Message {

    private String mid;
    private Account senderAcc;
    private Account receiverAcc;
    private String content;
    private Timestamp timesent;

    public Timestamp getTimesent() {
        return timesent;
    }

    public void setTimesent(Timestamp timesent) {
        this.timesent = timesent;
    }

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    public Account getSenderAcc() {
        return senderAcc;
    }

    public void setSenderAcc(Account senderAcc) {
        this.senderAcc = senderAcc;
    }

    public Account getReceiverAcc() {
        return receiverAcc;
    }

    public void setReceiverAcc(Account receiverAcc) {
        this.receiverAcc = receiverAcc;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
