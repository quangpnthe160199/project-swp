/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import dal.AccountDBContext;

/**
 *
 * @author BK
 */
public class Account {
    private String username;
    private String password;
    private String displayname;
private boolean IsFirstEdit;
AccountDetail ad ;

    public AccountDetail getAd() {
        return ad;
    }

    public void setAd(AccountDetail ad) {
        this.ad = ad;
    }
    public boolean isIsFirstEdit() {
        return IsFirstEdit;
    }

    public void setIsFirstEdit(boolean IsFirstEdit) {
        this.IsFirstEdit = IsFirstEdit;
    }
    public String getDisplayname() {
        return displayname;
    }

    public void setDisplayname(String displayname) {
        this.displayname = displayname;
    }
    
    
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isFlagged(){
        AccountDBContext db = new AccountDBContext();
        return db.isFlagged(username)==1;
        
    }
     public boolean isBanned(){
        AccountDBContext db = new AccountDBContext();
        return db.isBanned(username)==1;
        
    }
  
  
}
