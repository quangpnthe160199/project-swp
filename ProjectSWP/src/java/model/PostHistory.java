/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Date;

/**
 *
 * @author BK
 */
public class PostHistory {
    private Account a;
    private AccountPost ap;
    private Date date;

    public Account getA() {
        return a;
    }

    public void setA(Account a) {
        this.a = a;
    }

    public AccountPost getAp() {
        return ap;
    }

    public void setAp(AccountPost ap) {
        this.ap = ap;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
    
    
}
