/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author admin
 */
public class Follow {
    private Account followed;
    private String follower;

    public Account getFollowed() {
        return followed;
    }

    public void setFollowed(Account followed) {
        this.followed = followed;
    }

    

    public String getFollower() {
        return follower;
    }

    public void setFollower(String follower) {
        this.follower = follower;
    }
    
}
