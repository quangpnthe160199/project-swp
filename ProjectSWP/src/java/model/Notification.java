/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Timestamp;

/**
 *
 * @author haiph
 */
public class Notification {

    private String nid;
    private AccountPost ap;
    private Account senderAcc;
    private Account receiverAcc;
    private String content;
    private Timestamp timesent;

    public String getNid() {
        return nid;
    }

    public void setNid(String nid) {
        this.nid = nid;
    }

    public AccountPost getAp() {
        return ap;
    }

    public void setAp(AccountPost ap) {
        this.ap = ap;
    }

    public Account getSenderAcc() {
        return senderAcc;
    }

    public void setSenderAcc(Account senderAcc) {
        this.senderAcc = senderAcc;
    }

    public Account getReceiverAcc() {
        return receiverAcc;
    }

    public void setReceiverAcc(Account receiverAcc) {
        this.receiverAcc = receiverAcc;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Timestamp getTimesent() {
        return timesent;
    }

    public void setTimesent(Timestamp timesent) {
        this.timesent = timesent;
    }

}
