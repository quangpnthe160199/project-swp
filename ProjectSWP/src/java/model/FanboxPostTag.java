/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author admin
 */
public class FanboxPostTag {
    private FanboxPost fpost;
    private Tag tag;

    public FanboxPost getFpost() {
        return fpost;
    }

    public void setFpost(FanboxPost fpost) {
        this.fpost = fpost;
    }

    public Tag getTag() {
        return tag;
    }

    public void setTag(Tag tag) {
        this.tag = tag;
    }
    
}
