
<%@page import="model.Post"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<!--
=========================================================
* Material Dashboard Dark Edition - v2.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/material-dashboard-dark
* Copyright 2019 Creative Tim (http://www.creative-tim.com)

* Coded by www.creative-tim.com

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
-->
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8" />
        <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
        <link rel="icon" type="image/png" href="../assets/img/favicon.png">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <title>
            Admin page
        </title>
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
        <!--     Fonts and icons     -->
        <link rel="icon" href="assets/img/faces/logo-footer.png">
        <!--<img src="assets/img/faces/logo-footer.png" alt=""/>-->
        <!--<img src="view/affiliates/assets/images/logo-footer.png" alt=""/>-->
        <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
        <%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
        <!-- CSS Files -->
        <link href="../assets/css/material-dashboard.css?v=2.1.0" rel="stylesheet" />
        <!-- CSS Just for demo purpose, don't include it in your project -->
        <link href="../assets/demo/demo.css" rel="stylesheet" />
        <%  ArrayList<Post> works = (ArrayList<Post>) request.getAttribute("works");
            ArrayList<String> usernames = (ArrayList<String>) request.getAttribute("usernames");
        %>
        <script>


            function banUser(username)
            {
                func
                var result = confirm("Are you sure?");
                if (result)
                {
                    window.location.href = "banUser?username=" + username;
                }
            }
            function paging(id, pageindex, totalpage, gap)
            {
                var container = document.getElementById(id);
                var result = '';
                if (pageindex - gap > 1)
                    result += '<a href="adminmanagework?page=1">' + '<button>' + 'First' + '</button>' + '</a>';

                for (var i = pageindex - gap; i < pageindex; i++)
                    if (i > 0)
                        result += '<a href="adminmanagework?page=' + i + '">' + '<button>' + i + '</button>' + '</a>';

                result += '<button>' + '<span>' + pageindex + '</span>' + '</button>';

                for (var i = pageindex + 1; i <= pageindex + gap; i++)
                    if (i <= totalpage)
                        result += '<a href="adminmanagework?page=' + i + '">' + '<button>' + i + '</button>' + '</a>';

                if (pageindex + gap < totalpage)
                    result += '<a href="adminmanagework?page=' + totalpage + '">' + '<button>' + 'Last' + '</button>' + '</a>';

                container.innerHTML = result;
            }
        </script>
    </head>

    <body class="dark-edition">
        <div class="wrapper ">
            <div class="sidebar" data-color="purple" data-background-color="black" data-image="../assets/img/sidebar-2.jpg">
                <!--
                  Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"
          
                  Tip 2: you can also add an image using data-image tag
                -->
                <div class="logo">
                    <a class="navbar-brand" href="../home">
                        <img src="../assets/img/logo-footer.png" alt="Affiliates" style="position: relative; width: 170px; left: 15px; background-size: contain;">
                    </a>
                </div>
                <div class="sidebar-wrapper">
                    <ul class="nav">
                        <li class="nav-item  ">
                            <a class="nav-link" href="../home/dashboard">
                                <i class="material-icons">dashboard</i>
                                <p>Dashboard</p>
                            </a>
                        </li>



                        <li class="nav-item ">
                            <a class="nav-link" href="../home/adminusersearch">
                                <i class="material-icons">content_paste</i>
                                <p>User Management</p>
                            </a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link" href="../home/addquestion">
                                <i class="material-icons">library_books</i>
                                <p>Sercurity Question List</p>
                            </a>
                        </li>
                        <li class="nav-item active ">
                            <a class="nav-link" href="../home/adminmanagework">
                                <i class="material-icons">plagiarism</i>
                                <p>Management Work</p>
                            </a>
                        </li>
                        ________________________________________________
                        ________________________________________________
                        <li class="nav-item ">
                            <a class="nav-link" href="../home/login">
                                <i class="material-icons">supervisor_account</i>
                                <p>Change Account</p>
                            </a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link" href="../home/logout">
                                <i class="material-icons">logout</i>
                                <p>Log Out</p>
                            </a>
                        </li>

                        <!-- <li class="nav-item active-pro ">
                              <a class="nav-link" href="./upgrade.html">
                                  <i class="material-icons">unarchive</i>
                                  <p>Upgrade to PRO</p>
                              </a>
                          </li> -->
                    </ul>
                </div>
            </div>
            <div class="main-panel">
                <!-- Navbar -->
                <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top " id="navigation-example">
                    <div class="container-fluid">
                        <div class="navbar-wrapper">
                            <a class="navbar-brand" href="javascript:void(0)">Dashboard</a>
                        </div>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation" data-target="#navigation-example">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="navbar-toggler-icon icon-bar"></span>
                            <span class="navbar-toggler-icon icon-bar"></span>
                            <span class="navbar-toggler-icon icon-bar"></span>
                        </button>
                        <div class="collapse navbar-collapse justify-content-end">
                            <form class="navbar-form">
                                <div class="input-group no-border">
                                    <input type="text" value="" class="form-control" placeholder="Search...">
                                    <button type="submit" class="btn btn-default btn-round btn-just-icon">
                                        <i class="material-icons">search</i>
                                        <div class="ripple-container"></div>
                                    </button>
                                </div>
                            </form>
                            <ul class="navbar-nav">

                                <li class="nav-item dropdown">
                                    <a class="nav-link" href="javscript:void(0)" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">dashboard</i>

                                        <p class="d-lg-none d-md-block">
                                            Some Actions
                                        </p>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                                        <a class="dropdown-item" href="../home/login">Change Account</a>
                                        <a class="dropdown-item" href="../home/logout">Logout</a>

                                    </div>
                                </li>

                            </ul>

                        </div>
                    </div>
                </nav>
                <!-- End Navbar -->
                <div class="content">
                    <div class="container-fluid">
                        Recently reported work 
                        <div class="row">
                            <c:forEach items="${requestScope.works}" var="o">

                                <div class="col-xl-4 col-lg-12">

                                    <div class="card card-chart">
                                        <div class="card-header">

                                            <span><a href="../home/work?id=${o.ap.workid}"><img class="img-fluid" src="data:image/jpg;base64,${o.base64Image}" style="height: 250px; width: 500px"></a></span>


                                        </div>
                                        <div class="card-body">
                                            <h4 class="card-title"><a href="single.html"  style="color:black;">${o.name}</a></h4>
                                            
                                            <c:set var = "desc" value = "${o.description}"/>
                                            <c:if test="${fn:length(desc) < 70 }">
                                                <h4  class="card-text">${o.description}</h4>
                                            </c:if>
                                            <c:if test="${fn:length(desc) > 70 }">
                                                <c:set var = "shortdesc" value = "${fn:substring(desc, 0, 69)}"/>
                                                <h4  class="card-text">${shortdesc}...</h4>
                                            </c:if>
                                        </div>
                                        <div class="card-footer">
                                            <div class="stats">
                                                <span class="author-meta">

                                                    <span class="post-date">${o.date}</span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </c:forEach>
                        </div>
                        <div class="bottompagination">
                            <div class="navigation">
                                <nav class="pagination">
                                    <div id="container" class="paging1"> </div>
                                    <% Integer totalPage = (Integer) request.getAttribute("onoOfPages");
                                        Integer pageIndex = (Integer) request.getAttribute("pageIndex");
                                    %>
                                    <script>
                                        paging("container",<%=pageIndex%>,<%=totalPage%>, 1);
                                    </script>
                                    <span class="page-number"> &nbsp; &nbsp; Page <%=pageIndex%> of <%=totalPage%> &nbsp; &nbsp; </span>
                                </nav>
                            </div>
                        </div>

                        <footer class="footer">
                            <div class="container-fluid">
                                <nav class="float-left">
                                    <ul>
                                        <li>
                                            <a href="https://www.creative-tim.com">
                                                Creative Tim
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://creative-tim.com/presentation">
                                                About Us
                                            </a>
                                        </li>
                                        <li>
                                            <a href="http://blog.creative-tim.com">
                                                Blog
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://www.creative-tim.com/license">
                                                Licenses
                                            </a>
                                        </li>
                                    </ul>
                                </nav>
                                <div class="copyright float-right" id="date">
                                    , made with <i class="material-icons">favorite</i> by
                                    <a href="https://www.creative-tim.com" target="_blank">Creative Tim</a> for a better web.
                                </div>
                            </div>
                        </footer>
                        <script>
                            const x = new Date().getFullYear();
                            let date = document.getElementById('date');
                            date.innerHTML = '&copy; ' + x + date.innerHTML;
                        </script>
                    </div>
                </div>
                <div class="fixed-plugin">
                    <div class="dropdown show-dropdown">
                        <a href="#" data-toggle="dropdown">
                            <i class="fa fa-cog fa-2x"> </i>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header-title"> Sidebar Filters</li>
                            <li class="adjustments-line">
                                <a href="javascript:void(0)" class="switch-trigger active-color">
                                    <div class="badge-colors ml-auto mr-auto">
                                        <span class="badge filter badge-purple active" data-color="purple"></span>
                                        <span class="badge filter badge-azure" data-color="azure"></span>
                                        <span class="badge filter badge-green" data-color="green"></span>
                                        <span class="badge filter badge-warning" data-color="orange"></span>
                                        <span class="badge filter badge-danger" data-color="danger"></span>
                                    </div>
                                    <div class="clearfix"></div>
                                </a>
                            </li>
                            <li class="header-title">Images</li>
                            <li>
                                <a class="img-holder switch-trigger" href="javascript:void(0)">
                                    <img src="../assets/img/sidebar-1.jpg" alt="">
                                </a>
                            </li>
                            <li class="active">
                                <a class="img-holder switch-trigger" href="javascript:void(0)">
                                    <img src="../assets/img/sidebar-2.jpg" alt="">
                                </a>
                            </li>
                            <li>
                                <a class="img-holder switch-trigger" href="javascript:void(0)">
                                    <img src="../assets/img/sidebar-3.jpg" alt="">
                                </a>
                            </li>
                            <li>
                                <a class="img-holder switch-trigger" href="javascript:void(0)">
                                    <img src="../assets/img/sidebar-4.jpg" alt="">
                                </a>
                            </li>
                            <li class="button-container">
                                <a href="https://www.creative-tim.com/product/material-dashboard-dark" target="_blank" class="btn btn-primary btn-block">Free Download</a>
                            </li>
                            <!-- <li class="header-title">Want more components?</li>
                                <li class="button-container">
                                    <a href="https://www.creative-tim.com/product/material-dashboard-pro" target="_blank" class="btn btn-warning btn-block">
                                      Get the pro version
                                    </a>
                                </li> -->
                            <li class="button-container">
                                <a href="https://demos.creative-tim.com/material-dashboard-dark/docs/2.0/getting-started/introduction.html" target="_blank" class="btn btn-default btn-block">
                                    View Documentation
                                </a>
                            </li>
                            <li class="button-container github-star">
                                <a class="github-button" href="https://github.com/creativetimofficial/material-dashboard/tree/dark-edition" data-icon="octicon-star" data-size="large" data-show-count="true" aria-label="Star ntkme/github-buttons on GitHub">Star</a>
                            </li>
                            <li class="header-title">Thank you for 95 shares!</li>
                            <li class="button-container text-center">
                                <button id="twitter" class="btn btn-round btn-twitter"><i class="fa fa-twitter"></i> &middot; 45</button>
                                <button id="facebook" class="btn btn-round btn-facebook"><i class="fa fa-facebook-f"></i> &middot; 50</button>
                                <br>
                                <br>
                            </li>
                        </ul>
                    </div>
                </div>
                <!--   Core JS Files   -->
                <script src="../assets/js/core/jquery.min.js"></script>
                <script src="../assets/js/core/popper.min.js"></script>
                <script src="../assets/js/core/bootstrap-material-design.min.js"></script>
                <script src="https://unpkg.com/default-passive-events"></script>
                <script src="../assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
                <!-- Place this tag in your head or just before your close body tag. -->
                <script async defer src="https://buttons.github.io/buttons.js"></script>
                <!--  Google Maps Plugin    -->
                <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
                <!-- Chartist JS -->
                <script src="../assets/js/plugins/chartist.min.js"></script>
                <!--  Notifications Plugin    -->
                <script src="../assets/js/plugins/bootstrap-notify.js"></script>
                <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
                <script src="../assets/js/material-dashboard.js?v=2.1.0"></script>
                <!-- Material Dashboard DEMO methods, don't include it in your project! -->
                <script src="../assets/demo/demo.js"></script>
                <script>
                        $(document).ready(function () {
                            $().ready(function () {
                                $sidebar = $('.sidebar');

                                $sidebar_img_container = $sidebar.find('.sidebar-background');

                                $full_page = $('.full-page');

                                $sidebar_responsive = $('body > .navbar-collapse');

                                window_width = $(window).width();

                                $('.fixed-plugin a').click(function (event) {
                                    // Alex if we click on switch, stop propagation of the event, so the dropdown will not be hide, otherwise we set the  section active
                                    if ($(this).hasClass('switch-trigger')) {
                                        if (event.stopPropagation) {
                                            event.stopPropagation();
                                        } else if (window.event) {
                                            window.event.cancelBubble = true;
                                        }
                                    }
                                });

                                $('.fixed-plugin .active-color span').click(function () {
                                    $full_page_background = $('.full-page-background');

                                    $(this).siblings().removeClass('active');
                                    $(this).addClass('active');

                                    var new_color = $(this).data('color');

                                    if ($sidebar.length != 0) {
                                        $sidebar.attr('data-color', new_color);
                                    }

                                    if ($full_page.length != 0) {
                                        $full_page.attr('filter-color', new_color);
                                    }

                                    if ($sidebar_responsive.length != 0) {
                                        $sidebar_responsive.attr('data-color', new_color);
                                    }
                                });

                                $('.fixed-plugin .background-color .badge').click(function () {
                                    $(this).siblings().removeClass('active');
                                    $(this).addClass('active');

                                    var new_color = $(this).data('background-color');

                                    if ($sidebar.length != 0) {
                                        $sidebar.attr('data-background-color', new_color);
                                    }
                                });

                                $('.fixed-plugin .img-holder').click(function () {
                                    $full_page_background = $('.full-page-background');

                                    $(this).parent('li').siblings().removeClass('active');
                                    $(this).parent('li').addClass('active');


                                    var new_image = $(this).find("img").attr('src');

                                    if ($sidebar_img_container.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
                                        $sidebar_img_container.fadeOut('fast', function () {
                                            $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
                                            $sidebar_img_container.fadeIn('fast');
                                        });
                                    }

                                    if ($full_page_background.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
                                        var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

                                        $full_page_background.fadeOut('fast', function () {
                                            $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
                                            $full_page_background.fadeIn('fast');
                                        });
                                    }

                                    if ($('.switch-sidebar-image input:checked').length == 0) {
                                        var new_image = $('.fixed-plugin li.active .img-holder').find("img").attr('src');
                                        var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

                                        $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
                                        $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
                                    }

                                    if ($sidebar_responsive.length != 0) {
                                        $sidebar_responsive.css('background-image', 'url("' + new_image + '")');
                                    }
                                });

                                $('.switch-sidebar-image input').change(function () {
                                    $full_page_background = $('.full-page-background');

                                    $input = $(this);

                                    if ($input.is(':checked')) {
                                        if ($sidebar_img_container.length != 0) {
                                            $sidebar_img_container.fadeIn('fast');
                                            $sidebar.attr('data-image', '#');
                                        }

                                        if ($full_page_background.length != 0) {
                                            $full_page_background.fadeIn('fast');
                                            $full_page.attr('data-image', '#');
                                        }

                                        background_image = true;
                                    } else {
                                        if ($sidebar_img_container.length != 0) {
                                            $sidebar.removeAttr('data-image');
                                            $sidebar_img_container.fadeOut('fast');
                                        }

                                        if ($full_page_background.length != 0) {
                                            $full_page.removeAttr('data-image', '#');
                                            $full_page_background.fadeOut('fast');
                                        }

                                        background_image = false;
                                    }
                                });

                                $('.switch-sidebar-mini input').change(function () {
                                    $body = $('body');

                                    $input = $(this);

                                    if (md.misc.sidebar_mini_active == true) {
                                        $('body').removeClass('sidebar-mini');
                                        md.misc.sidebar_mini_active = false;

                                        $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar();

                                    } else {

                                        $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar('destroy');

                                        setTimeout(function () {
                                            $('body').addClass('sidebar-mini');

                                            md.misc.sidebar_mini_active = true;
                                        }, 300);
                                    }

                                    // we simulate the window Resize so the charts will get updated in realtime.
                                    var simulateWindowResize = setInterval(function () {
                                        window.dispatchEvent(new Event('resize'));
                                    }, 180);

                                    // we stop the simulation of Window Resize after the animations are completed
                                    setTimeout(function () {
                                        clearInterval(simulateWindowResize);
                                    }, 1000);

                                });
                            });
                        });
                </script>
                <script>
                    $(document).ready(function () {
                        // Javascript method's body can be found in assets/js/demos.js
                        md.initDashboardPageCharts();

                    });
                </script>
                </body>

                </html>