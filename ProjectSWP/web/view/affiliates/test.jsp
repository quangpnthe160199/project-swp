<%-- 
    Document   : test
    Created on : Jun 17, 2022, 11:04:00 PM
    Author     : FPTSHOP
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <img src = "https://www.gravatar.com/avatar/b1cc14991db7a456fcd761680bbc8f81?s=250&amp;d=mm&amp;r=x"/>
        <div class="mainheading">
                                            <h1 class="posttitle">Hello${requestScope.post.fname}</h1>
                                        </div>
                                        <div class="d-flex flex-row">
                                            <c:forEach items="${requestScope.tags}" var="t"> 
                                                <a class="p-2" href="../home?tag=${t.tagid}">#${t.tagname}</a>
                                            </c:forEach>
                                        </div>
                                        <!-- Post Featured Image -->
                                        <img class="featured-image img-fluid" src="data:image/jpg;base64,${requestScope.post.base64Image}" alt="">
                                        <!-- End Featured Image -->
                                        <!-- Post Content -->
                                        <div class="article-post">
                                            <p>${requestScope.post.description}</p>
                                            <div class="clearfix">
                                            </div>
                                        </div>
    </body>
</html>
