<%-- 
    Document   : fanbox
    Created on : Jul 2, 2022, 1:38:40 PM
    Author     : admin
--%>

<%@page import="model.Notification"%>
<%@page import="java.util.ArrayList"%>
<%@page import="model.Tag"%>
<%@page import="dal.AccountDBContext"%>
<%@page import="model.Account"%>
<%@page import="model.Post"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="icon" href="assets/images/favicon.ico">
        <title>Beaucoup | Edit Profile</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Rubik:400,400i,500,500i,700,700i" rel="stylesheet">
        <link href="../view/affiliates/assets/css/theme.css" rel="stylesheet">
        <link href="../view/Test JSP/test.css" rel="stylesheet">
        <%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
        <script src="../view/affiliates/assets/js/profilepaging.js" type="text/javascript"></script>
        <link href="../view/affiliates/assets/css/paging.css" rel="stylesheet" type="text/css"/>
        <!-- base css -->
        <link rel="stylesheet" media="screen, print" href="../view/affiliates/assets/inteltemplate/css/vendors.bundle.css">
        <link rel="stylesheet" media="screen, print" href="../view/affiliates/assets/inteltemplate/css/app.bundle.css">

        <link rel="stylesheet" media="screen, print" href="../view/affiliates/assets/inteltemplate/css/formplugins/select2/select2.bundle.css">
        <!-- Place favicon.ico in the root directory -->
        <link rel="apple-touch-icon" sizes="180x180" href="../view/affiliates/assets/inteltemplate/img/favicon/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="../view/affiliates/assets/inteltemplate/img/favicon/favicon-32x32.png">
        <!-- Begin tracking codes here, including ShareThis/Analytics -->
        
        <!-- End tracking codes here, including ShareThis/Analytics -->
         <style>
            body {
                font-family: sans-serif;
                background-color: #eeeeee;
            }

            .file-upload {
                background-color: #ffffff;
                width: 600px;
                margin: 0 auto;
                padding: 20px;
            }

            .file-upload-btn {
                width: 100%;
                margin: 0;
                color: #fff;
                background: #1FB264;
                border: none;
                padding: 10px;
                border-radius: 4px;
                border-bottom: 4px solid #15824B;
                transition: all .2s ease;
                outline: none;
                text-transform: uppercase;
                font-weight: 700;
            }

            .file-upload-btn:hover {
                background: #1AA059;
                color: #ffffff;
                transition: all .2s ease;
                cursor: pointer;
            }

            .file-upload-btn:active {
                border: 0;
                transition: all .2s ease;
            }

            .file-upload-content {
                display: none;
                text-align: center;
            }

            .file-upload-input {
                position: absolute;
                margin: 0;
                padding: 0;
                width: 100%;
                height: 100%;
                outline: none;
                opacity: 0;
                cursor: pointer;
            }

            .image-upload-wrap {
                margin-top: 20px;
                border: 4px dashed #1FB264;
                position: relative;
            }

            .image-dropping,
            .image-upload-wrap:hover {
                background-color: #1FB264;
                border: 4px dashed #ffffff;
            }

            .image-title-wrap {
                padding: 0 15px 15px 15px;
                color: #222;
            }

            .drag-text {
                text-align: center;
            }

            .drag-text h3 {
                font-weight: 100;
                text-transform: uppercase;
                color: #15824B;
                padding: 60px 0;
            }

            .file-upload-image {
                max-height: 200px;
                max-width: 200px;
                margin: auto;
                padding: 20px;
            }

            .remove-image {
                width: 200px;
                margin: 0;
                color: #fff;
                background: #cd4535;
                border: none;
                padding: 10px;
                border-radius: 4px;
                border-bottom: 4px solid #b02818;
                transition: all .2s ease;
                outline: none;
                text-transform: uppercase;
                font-weight: 700;
            }

            .remove-image:hover {
                background: #c13b2a;
                color: #ffffff;
                transition: all .2s ease;
                cursor: pointer;
            }

            .remove-image:active {
                border: 0;
                transition: all .2s ease;
            }
        </style>
       <%
            ArrayList<Tag> tags = (ArrayList<Tag>) request.getAttribute("tags");
        %>
    </head>
    <body style="background-color: #000"class="layout-page">
        <!-- Begin Menu Navigation
        ================================================== -->
        <header class="navbar navbar-expand-lg navbar-light bg-light fixed-top mediumnavigation">   
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarsWow" aria-controls="navbarsWow" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="container">
                <!-- Begin Logo -->
                <a class="navbar-brand" href="../home">
                    <img src="../view/affiliates/assets/images/logo.png" alt="Affiliates - Free Bootstrap Template" style="position: relative; width: 170px; left: 15px; background-size: contain;">
                </a>
                <!-- End Logo -->
                <!-- Begin Menu -->
                <div class="collapse navbar-collapse" id="navbarsWow">
                    <!-- Begin Menu -->
                    <%
                        Account account = (Account) request.getSession().getAttribute("account");
                    %>
                    <ul class="navbar-nav ml-auto">
                        <%
                            if (account != null) {
                        %>
                        <c:choose>
                            <c:when test="${not empty sessionScope.subcription}">
                                <li class="nav-item">
                                    <a class="nav-link" style="color:#Fcbc19" href="../home/subcription">BeauCoup Blog Premium</a>
                                </li>
                            </c:when>
                            <c:when test="${empty sessionScope.subcription}">
                                <li class="nav-item">
                                    <a class="nav-link" style="color:#Fcbc19" href="../home/subcription">BeauCoup Blog Premium Trial</a>
                                </li>
                            </c:when>
                        </c:choose>


                        <%}%>
                        <li class="nav-item">
                            <a class="nav-link" href="../home">Home</a>
                        </li>
                        <c:if test="${not empty account}">
                                <a class="dropdown-item" href="../home/upload">Publish</a>
                        </c:if>
                        <li class="nav-item">
                            <a class="nav-link" href="../home/contact">Contact</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Tags</a>

                            <div class="dropdown-menu" aria-labelledby="dropdown01">
                                <a class="dropdown-item" href="/ProjectSWP/home/tag?tagid=1" >Action</a>
                                <a class="dropdown-item" href="/ProjectSWP/home/tag?tagid=2">Humor</a>
                                <a class="dropdown-item" href="/ProjectSWP/home/tag?tagid=3">Adventure</a>
                            </div>

                        </li>
                        <%
                            if (account == null) {
                        %>

                        <li class="nav-item">
                            <a class="nav-link highlight" href="../home/login">Login</a>
                        </li>
                        <%} else {%>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Register Advertisement</a>
                            <div class="dropdown-menu" aria-labelledby="dropdown01">
                                <a class="dropdown-item" href="../home/userRegisterAds">Register Advertisement</a>

                            </div>
                        </li>
                        <%ArrayList<Notification> notifications = (ArrayList<Notification>) request.getSession().getAttribute("notifications");%>
                        <li class="nav-item">
                            <a class="nav-link" href="../home/messages"><i class="fa fa-envelope" style="font-size:24px"></i></a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bell" style="font-size:24px"></i></a>

                            <div class="dropdown-menu" aria-labelledby="dropdown01" style="background-color: lightblue" >
                                <%for (Notification notif : notifications) {%>
                                <div style="background-color: lightblue; border:solid 1px" id="notif<%=notif.getNid()%>" onclick="setNotifColor()">
                                    <img class="img-fluid img-responsive rounded-circle mr-2" src="https://wompampsupport.azureedge.net/fetchimage?siteId=7575&v=2&jpgQuality=100&width=700&url=https%3A%2F%2Fi.kym-cdn.com%2Fphotos%2Fimages%2Fnewsfeed%2F002%2F111%2F316%2Fc57.gif" width="38">
                                    <a class="dropdown-item" href="/ProjectSWP/home/work?workid=<%=notif.getAp().getWorkid()%>"><%=notif.getSenderAcc().getUsername()%> <%=notif.getContent()%> at <%=notif.getTimesent().getHours()%> : <%=notif.getTimesent().getMinutes()%> | <%=notif.getTimesent().getDate()%>/<%=notif.getTimesent().getMonth() + 1%>/<%=notif.getTimesent().getYear() + 1900%></a> 
                                </div>
                                <%}%>
                                <a class="dropdown-item" href="/ProjectSWP/notifications" style="text-align: center"> View all notifications </a>
                            </div>

                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              
                                <img class="author-thumb" src="data:image/jpg;base64,${sessionScope.accPic.ava}"   />
                                <c:if test="${not empty sessionScope.subcription}">
                                    <img src="../view/affiliates/assets/images/logo-footer.png" style="width: 50px;length: 50px; color: #Fcbc19" title="Your Subcription Last Until ${sessionScope.subcription.todate}">
                                </c:if>

                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                <a class="dropdown-item" href="../home/profile">View Profile</a>
                                <a class="dropdown-item" href="../home/pin">Pinned Work</a>
                                <a class="dropdown-item" href ="../home/delete">My Work</a> 
                                <a class="dropdown-item" href="../mute">Muted Work</a>
                                <a class="dropdown-item" href="../history">View History</a>
                                <a class="dropdown-item" href="../home/logout">Log Out</a>
                            </div>
                        </li>
                        <%}%>
                    </ul>
                    <!-- End Menu -->
                </div>
            </div>
        </header>
        <!-- End Menu Navigation
        ================================================== -->
        <div class="site-content">
            <div class="container">
                <!-- Content (replace with your e-mail address below)
    ================================================== -->
                <div class="main-content">
                    <div class="row">
      <div class="col-md-12">
         <div id="content" class="content content-full-width">
            <!-- begin profile -->
            <div class="profile">
               <div class="profile-header">
                  <!-- BEGIN profile-header-cover -->
                  <div class="profile-header-cover"></div>
                  <!-- END profile-header-cover -->
                  <!-- BEGIN profile-header-content -->
                  <div class="profile-header-content">
                     <!-- BEGIN profile-header-img -->
                     <div class="profile-header-img">
                         <img class="author-thumb" src="data:image/jpg;base64,${requestScope.userpic.ava}" style="width: 250px; height: 100px" />
                     </div>
                     <!-- END profile-header-img -->
                     <!-- BEGIN profile-header-info -->
                     <div class="profile-header-info">
                         <h4 class="m-t-10 m-b-5">${requestScope.FanboxName}</h4>
                        <p class="m-b-10">UXUI + Frontend Developer</p>                                            
                     </div>
                     <!-- END profile-header-info -->
                  </div>
                  <!-- END profile-header-content -->
                  <!-- BEGIN profile-header-tab -->
                  <ul style="background-color: #D9138A"class="profile-header-tab nav nav-tabs">
                     <li class="nav-item"><a href="#profile-post" class="nav-link active show" data-toggle="tab">POSTS</a></li>
                     <li class="nav-item"><a href="#profile-upload" class="nav-link" data-toggle="tab">UPLOAD</a></li>
                     <li class="nav-item"><a href="#profile-about" class="nav-link" data-toggle="tab">ABOUT</a></li>
                     <li class="nav-item"><a href="#profile-subscriber" class="nav-link" data-toggle="tab">SUBSCRIBERS</a></li>
                     <li class="nav-item"><a href="#profile-edit" class="nav-link" data-toggle="tab">EDIT</a></li>
                  </ul>
                  <!-- END profile-header-tab -->
               </div>
            </div>
            <!-- end profile -->
            <!-- begin profile-content -->
            <div class="profile-content" >
               <!-- begin tab-content -->
               <div class="tab-content p-0">
                  <!-- begin #profile-post tab -->
                  <div class="tab-pane fade active show" id="profile-post">
                     <!-- begin timeline -->
                     <%
                            Post p = (Post) request.getSession().getAttribute("post");
                        %>
                        <c:set var="date1" value="" />
                     <ul class="timeline">
                         <li class="post" id="content"
                       <c:forEach items="${requestScope.post}" var="o">
                           <c:set var="date2" value="${o.date}" />                         
                         
                           <!-- begin timeline-time -->
                           <c:if test="${date1 != date2}">
                           <div class="timeline-time">
                                  <span class="date">${o.date}</span>
<!--                              <span class="time">04:20</span>-->
                           </div
                           </c:if>
                           <!-- end timeline-time -->
                           <!-- begin timeline-icon -->
                           <div class="timeline-icon">
                              <a href="../home/work?id=${o.ap.workid}">&nbsp;</a>
                           </div>
                           <!-- end timeline-icon -->
                           <!-- begin timeline-body -->
                           
                           <div style="background-color: #E2D810"class="timeline-body">
                              <div class="timeline-header">
                                  <img class="author-thumb" src="data:image/jpg;base64,${requestScope.userpic.ava}"   />
                                 
                                 <span class="username"><a href="javascript:;">${sessionScope.account.username}</a> <small></small></span>
                                 <span><a href="../home/fanbox/work?id=${o.ap.workid}"><img class="img-fluid" src="data:image/jpg;base64,${o.base64Image}" style="height: 250px; width: 500px"></a></span>
<!--                                 <span class="pull-right text-muted">18 Views</span>-->
                              </div>
                              <div class="timeline-content">
                                  <span class="post-name"><a target="_blank" href="../home/fanbox/work?id=${o.ap.workid}">${o.name}</a></span><br/>
                                 <c:set var = "desc" value = "${o.description}"/>
                                                <c:if test="${fn:length(desc) < 70 }">
                                                    <p class="card-text">${o.description}</p>
                                                </c:if>
                                                <c:if test="${fn:length(desc) > 250 }">
                                                    <c:set var = "shortdesc" value = "${fn:substring(desc, 0, 249)}"/>
                                                    <p class="card-text">${shortdesc}...</p>
                                                </c:if>
                              </div>
<!--                              <div class="timeline-likes">
                                 <div class="stats-right">
                                    <span class="stats-text">259 Shares</span>
                                    <span class="stats-text">21 Comments</span>
                                 </div>
                                 <div class="stats">
                                    <span class="fa-stack fa-fw stats-icon">
                                    <i class="fa fa-circle fa-stack-2x text-danger"></i>
                                    <i class="fa fa-heart fa-stack-1x fa-inverse t-plus-1"></i>
                                    </span>
                                    <span class="fa-stack fa-fw stats-icon">
                                    <i class="fa fa-circle fa-stack-2x text-primary"></i>
                                    <i class="fa fa-thumbs-up fa-stack-1x fa-inverse"></i>
                                    </span>
                                    <span class="stats-total">4.3k</span>
                                 </div>
                              </div>-->
<!--                              <div class="timeline-footer">
                                 <a href="javascript:;" class="m-r-15 text-inverse-lighter"><i class="fa fa-thumbs-up fa-fw fa-lg m-r-3"></i> Like</a>
                                 <a href="javascript:;" class="m-r-15 text-inverse-lighter"><i class="fa fa-comments fa-fw fa-lg m-r-3"></i> Comment</a> 
                                 <a href="javascript:;" class="m-r-15 text-inverse-lighter"><i class="fa fa-share fa-fw fa-lg m-r-3"></i> Share</a>
                              </div>-->
<!--                              <div class="timeline-comment-box">
                                 <div class="user"><img src="https://bootdey.com/img/Content/avatar/avatar3.png"></div>
                                 <div class="input">
                                    <form action="">
                                       <div class="input-group">
                                          <input type="text" class="form-control rounded-corner" placeholder="Write a comment...">
                                          <span class="input-group-btn p-l-10">
                                          <button class="btn btn-primary f-s-12 rounded-corner" type="button">Comment</button>
                                          </span>
                                       </div>
                                    </form>
                                 </div>
                              </div>-->
                           </div>
                            <c:set var="date1" value="${o.date}" />
                           </c:forEach>
                           <!-- end timeline-body -->
                        </li>
                        
                        
                        
                        <li>
                           <!-- begin timeline-icon -->
                           <div class="timeline-icon">
                              <a href="javascript:;">&nbsp;</a>
                           </div>
                           <!-- end timeline-icon -->
                           <!-- begin timeline-body -->
                           <div style="background-color: #E2D810"class="timeline-body">
                               <div style="background-color: #E2D810"class="bottompagination">
                                <div style="background-color: #E2D810"class="navigation">
                                    <nav style="background-color: #E2D810"class="pagination">
                                        <div style="background-color: #E2D810"id="container" class="paging"> </div>
                                        <% Integer totalPage = (Integer) request.getAttribute("onoOfPages");
                                           Integer pageIndex = (Integer) request.getAttribute("pageIndex");
                                        %>
                                        <script>
                                            paging("container",<%=pageIndex%>,<%=totalPage%>, 1);
                                        </script>
                                        <span style="background-color:#E2D810 "class="page-number"> &nbsp; &nbsp; Page <%=pageIndex%> of <%=totalPage%> &nbsp; &nbsp; </span>
                                    </nav>
                                </div>
                            </div>
                           </div>
                           <!-- begin timeline-body -->
                        </li>
                     </ul>
                     <!-- end timeline -->
                  </div>
                  <!-- end #profile-post tab -->
                  <div class="tab-pane fade " id="profile-upload">
                      
                    <form action="viewfanbox" method="POST" enctype="multipart/form-data">
                        <div class="file-upload form-group">
                            <button class="file-upload-btn" type="button" onclick="$('.file-upload-input').trigger('click')">Add Image</button>
                            <div class="image-upload-wrap">
                                <input class="file-upload-input" type='file' id="image" name="img" onchange="readURL(this);" accept="image/*" required/>
                                <div class="drag-text">
                                    <h3>Drag and drop a file or select add Image</h3>
                                </div>
                            </div>
                            <div class="file-upload-content">
                                <img class="file-upload-image img-fluid" style="width:1250px;height:250x" src="#" alt="your image" />
                                <div class="image-title-wrap">
                                    <button type="button" onclick="removeUpload()" class="remove-image">Remove <span class="image-title">Uploaded Image</span></button>
                                </div>
                            </div>
                        </div>
                        <div class="p-2">
                            <div class="form-group">
                                <label class="form-label" for="multiple-basic">
                                    Basic Multi Select
                                </label>
                                <select class="select2 form-control" multiple="multiple" name="tagChoice" id="multiple-basic" required>
                                    <optgroup>
                                        <% for (Tag t : tags) {%>
                                        <option value="<%=t.getTagid()%>"><%=t.getTagname()%></option>
                                        <% }%>
                                    </optgroup>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Title</label>
                            <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter Title" name="title" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Description</label>
                            <textarea type="text" class="form-control" rows="5" placeholder="Enter Description" name="desc" required></textarea>
                        </div>
                        <div class="form-group" >
                            <label for="exampleInputPassword1">Open To:</label>
                            <label class="radio-inline" style="margin-right: 20px;">
                                <input type="radio" name="open" value="public" checked>Public
                            </label>
                            <label class="radio-inline" style="margin-right: 20px;">
                                <input type="radio" name="open" value="private">Private
                            </label>
                        </div>
                        <div class="form-group" id="comments">
                            <label for="exampleInputPassword1">Comments:</label>
                            <label class="radio-inline" style="margin-right: 20px;">
                                <input type="radio" name="comment" value="on" checked>ON
                            </label>
                            <label class="radio-inline" style="margin-right: 20px;">
                                <input type="radio" name="comment" value="off">OFF
                            </label>
                        </div>
                        <br>
                        <div style="font-size: 15px">
                            <p>Please note that submitting the following contents is against our guidelines:</p>
                            <p>- Contents produced by others, reproductions of products that are on sale and any kind of work protected by a third-party copyright</p>
                            <p>These include screenshots of video games and illustrations.</p>
                            <p>- Contents that make use of materials belonging to the categories listed above and that you didn't make 100% yourself.</p>
                            <p>- Photography, except those which portray an artwork or illustration.</p>
                            <p>Posting contents that violate the Terms of Use will result in a suspension from posting or possibly a termination of your account</p>
                        </div>
                        <div class="container">
                            <div class="row">
                                <div class="col text-center">
                                    <button type="submit" class="btn btn-primary btn-lg">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                  </div>
                  <div class="tab-pane fade " id="profile-subscriber">
                      <table class="table table-bordered" >
                        <thead class="thead-dark">
                            <tr>

                                <th scope="col">Subscribers</th>


                            </tr>
                        </thead>
                        <tbody>

                            <c:forEach items="${requestScope.subs}" var="p">
                                <tr>

                                    <td><a target="_blank" href="/ProjectSWP/home/profile?proname=${p.username}">${p.displayname}</a></td>
                                    
                                </tr>
                                </c:forEach>
                        </tbody>
                    </table>
                    
                  </div>
                  
                  
                  
               </div>
               <!-- end tab-content -->
            </div>
            <!-- end profile-content -->
         </div>
      </div>
   </div>
                </div>
            </div>
            <!-- /.container -->
            <!-- Begin Footer
        ================================================== -->
            <footer class="footer">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="footer-widget">
                                <a href="contact.jsp">
                                    <img src="assets/images/logo-footer.png" alt="logo footer">
                                </a>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="footer-widget">
                                <h5 class="title">Author</h5>
                                <ul>
                                    <li><a href="#">About Us</a></li>
                                    <li><a target="_blank" href="https://www.wowthemes.net/affiliate-area/">Beaucoup</a></li>
                                    <li><a href="https://www.wowthemes.net/terms-and-conditions/">License</a></li>
                                    <li><a href="https://www.wowthemes.net/blog/">Blog</a></li>
                                    <li><a href="contact.jsp">Contact</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="footer-widget textwidget">
                                <h5 class="title">Email us</h5>
                                <form action="https://formspree.io/wowthemesnet@gmail.com" method="POST">
                                    <input type="text" name="name" placeholder="Name">
                                    <input type="email" name="_replyto" placeholder="E-mail Address">
                                    <textarea rows="8" name="message" placeholder="Message"></textarea><br/>
                                    <input class="btn btn-success" type="submit" value="Send">
                                </form>                             
                            </div>
                        </div>
                    </div>
                    <div class="copyright">
                        <p class="pull-left">
                            Copyright © 2018 Affiliates HTML Template
                        </p>
                        <p class="pull-right">
                            <!-- Leave credit to author unless you own a commercial license: https://www.wowthemes.net/freebies-license/ -->
                            <a target="_blank" href="https://www.wowthemes.net/affiliates-free-bootstrap-template/">"Affiliates Template"</a> - Design & Code by WowThemes.net
                        </p>
                        <div class="clearfix">
                        </div>
                    </div>
                </div>
            </footer>
            <!-- End Footer
        ================================================== -->
        </div>
        <!-- JavaScript
        ================================================== -->
        <script src="../view/affiliates/assets/js/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
        <script src="../view/affiliates/assets/js/ie10-viewport-bug-workaround.js"></script>
        <script src="../view/affiliates/assets/js/masonry.pkgd.min.js"></script>
        <script src="../view/affiliates/assets/js/theme.js"></script>
        <script src="../view/affiliates/assets/js/pageScript.js"></script>
        <script src="../view/affiliates/assets/inteltemplate/js/vendors.bundle.js"></script>
        <script src="../view/affiliates/assets/inteltemplate/js/app.bundle.js"></script>
        <script src="../view/affiliates/assets/inteltemplate/js/formplugins/select2/select2.bundle.js"></script>
        <script>
                                        $("#image").change(function ()
                                        {
                                            var Data = document.getElementById('image');
                                            var FileUploadPath = Data.value;

                                            if (FileUploadPath != '') {
                                                var Extension = FileUploadPath.substring(FileUploadPath.lastIndexOf('.') + 1).toLowerCase();
                                                //The file uploaded is an image

                                                if (Extension == "gif" || Extension == "png" || Extension == "bmp"
                                                        || Extension == "jpeg" || Extension == "jpg") {
                                                    return true;
                                                } else if (Extension != 'gif' || Extension != 'png' || Extension != 'bmp' || Extension != 'jpeg' || Extension != 'jpg') {

                                                    alert("Photo only allows file types of GIF, PNG, JPG, JPEG and BMP. ");
                                                    Data.type = '';
                                                    Data.type = 'file';
                                                    return false;
                                                }
                                            }
                                        });

                                        $("input[type=file]").change(function () {
                                            $(this).siblings("div").eq(0).html($(this).val());
                                        });

                                        function readURL(input) {
                                            if (input.files && input.files[0]) {

                                                var reader = new FileReader();

                                                reader.onload = function (e) {
                                                    $('.image-upload-wrap').hide();

                                                    $('.file-upload-image').attr('src', e.target.result);
                                                    $('.file-upload-content').show();

                                                    $('.image-title').html(input.files[0].name);
                                                };

                                                reader.readAsDataURL(input.files[0]);

                                            } else {
                                                removeUpload();
                                            }
                                        }

                                        function removeUpload() {
                                            $('.file-upload-input').replaceWith($('.file-upload-input').clone());
                                            $('.file-upload-content').hide();
                                            $('.image-upload-wrap').show();
                                        }
                                        $('.image-upload-wrap').bind('dragover', function () {
                                            $('.image-upload-wrap').addClass('image-dropping');
                                        });
                                        $('.image-upload-wrap').bind('dragleave', function () {
                                            $('.image-upload-wrap').removeClass('image-dropping');
                                        });

        </script>
        
        <script>
            $(document).ready(function()
            {
                $(function()
                {
                    $('.select2').select2();

                    $(".select2-placeholder-multiple").select2(
                    {
                        placeholder: "Select State"
                    });
                    $(".js-hide-search").select2(
                    {
                        minimumResultsForSearch: 1 / 0
                    });
                    $(".js-max-length").select2(
                    {
                        maximumSelectionLength: 2,
                        placeholder: "Select maximum 2 items"
                    });
                    $(".select2-placeholder").select2(
                    {
                        placeholder: "Select a state",
                        allowClear: true
                    });



                    $(".js-select2-icons").select2(
                    {
                        minimumResultsForSearch: 1 / 0,
                        templateResult: icon,
                        templateSelection: icon,
                        escapeMarkup: function(elm)
                        {
                            return elm
                        }
                    });

                    function icon(elm)
                    {
                        elm.element;
                        return elm.id ? "<i class='" + $(elm.element).data("icon") + " mr-2'></i>" + elm.text : elm.text
                    }

                    $(".js-data-example-ajax").select2(
                    {
                        ajax:
                        {
                            url: "https://api.github.com/search/repositories",
                            dataType: 'json',
                            delay: 250,
                            data: function(params)
                            {
                                return {
                                    q: params.term, // search term
                                    page: params.page
                                };
                            },
                            processResults: function(data, params)
                            {
                                // parse the results into the format expected by Select2
                                // since we are using custom formatting functions we do not need to
                                // alter the remote JSON data, except to indicate that infinite
                                // scrolling can be used
                                params.page = params.page || 1;

                                return {
                                    results: data.items,
                                    pagination:
                                    {
                                        more: (params.page * 30) < data.total_count
                                    }
                                };
                            },
                            cache: true
                        },
                        placeholder: 'Search for a repository',
                        escapeMarkup: function(markup)
                        {
                            return markup;
                        }, // let our custom formatter work
                        minimumInputLength: 1,
                        templateResult: formatRepo,
                        templateSelection: formatRepoSelection
                    });

                    function formatRepo(repo)
                    {
                        if (repo.loading)
                        {
                            return repo.text;
                        }

                        var markup = "<div class='select2-result-repository clearfix d-flex'>" +
                            "<div class='select2-result-repository__avatar mr-2'><img src='" + repo.owner.avatar_url + "' class='width-2 height-2 mt-1 rounded' /></div>" +
                            "<div class='select2-result-repository__meta'>" +
                            "<div class='select2-result-repository__title fs-lg fw-500'>" + repo.full_name + "</div>";

                        if (repo.description)
                        {
                            markup += "<div class='select2-result-repository__description fs-xs opacity-80 mb-1'>" + repo.description + "</div>";
                        }

                        markup += "<div class='select2-result-repository__statistics d-flex fs-sm'>" +
                            "<div class='select2-result-repository__forks mr-2'><i class='fal fa-lightbulb'></i> " + repo.forks_count + " Forks</div>" +
                            "<div class='select2-result-repository__stargazers mr-2'><i class='fal fa-star'></i> " + repo.stargazers_count + " Stars</div>" +
                            "<div class='select2-result-repository__watchers mr-2'><i class='fal fa-eye'></i> " + repo.watchers_count + " Watchers</div>" +
                            "</div>" +
                            "</div></div>";

                        return markup;
                    }

                    function formatRepoSelection(repo)
                    {
                        return repo.full_name || repo.text;
                    }

                });
            });

        </script>
    </body>
</html>
