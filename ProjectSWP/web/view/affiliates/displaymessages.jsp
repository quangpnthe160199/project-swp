<%-- 
    Document   : displaymessages
    Created on : Jun 26, 2022, 10:22:00 PM
    Author     : haiph
--%>
<%@page import="model.Message"%>
<%@page import="java.util.ArrayList"%>
<%@page import="model.Account"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Beaucoup | An All-you-can-post page</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Rubik:400,400i,500,500i,700,700i" rel="stylesheet">
        <link href="../view/affiliates/assets/css/theme.css" rel="stylesheet">
        <script src="../view/affiliates/assets/js/pageScript.js"></script>
        <!-- base css -->
        <link rel="stylesheet" media="screen, print" href="../view/affiliates/assets/inteltemplate/css/vendors.bundle.css">
        <link rel="stylesheet" media="screen, print" href="../view/affiliates/assets/inteltemplate/css/app.bundle.css">
        <%
            ArrayList<Message> messages = (ArrayList<Message>) request.getAttribute("messages");
            Account acc = (Account) request.getSession().getAttribute("account");
            String contactname = request.getAttribute("contactname").toString();
        %>
    </head>
    <body>
        <div class="container mt-5 mb-5">
            <div>
                <img class="img-fluid img-responsive rounded-circle mr-2" src="https://wompampsupport.azureedge.net/fetchimage?siteId=7575&v=2&jpgQuality=100&width=700&url=https%3A%2F%2Fi.kym-cdn.com%2Fphotos%2Fimages%2Fnewsfeed%2F002%2F111%2F316%2Fc57.gif" width="38">
                <b><%=contactname%></b>
            </div>
            <div class="d-flex justify-content-center row">
                <div class="d-flex flex-column col-md-8"> 
                    <div class="coment-bottom bg-white p-2 px-4">
                        <div id="messageDisplay">
                        <%for (Message message : messages) {%>
                        <div class="commented-section mt-2">
                            <div class="d-flex flex-row align-items-center commented-user">
                                <h5 class="mr-2"><%=message.getSenderAcc().getUsername()%></h5><span class="dot mb-1"></span><span class="mb-1 ml-2">- At <%=message.getTimesent()%></span>
                            </div>
                            <%if (message.getSenderAcc().getUsername().equals(acc.getUsername())) {%>
                            <div class="comment-text-sm" style="background-color: lightskyblue">
                                <span><%=message.getContent()%></span>
                            </div>        
                            <%} else {%>
                            <div class="comment-text-sm" style="background-color: gainsboro">
                                <span><%=message.getContent()%></span>
                            </div>  
                            <%}%>
                        </div>
                        <%}%>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="navigation">
                <nav class="pagination">
                    <form name="messageForm" id="messageForm" onsubmit="sendMsg()">
                        <input type="text" name="content" id="content" placeholder="Send message here..." style="height: 50px; width: 650px;">
                        <input type="hidden" name="sendername" id="sendername" value=<%=acc.getUsername()%>>
                        <input type="hidden" name="receivername" id="receivername" value=<%=contactname%>>
                        <input type="button" value="Send" onclick="sendMsg()"/>
                    </form>
                    <script>
                        document.getElementById("messageForm").onkeypress = function (event) {
                            var key = event.charCode || event.keyCode || 0;
                            if (key === 13) {
                                //alert("No Enter!");
                                event.preventDefault();
                            }
                        };
                    </script>
                </nav>
            </div>
        </div>
        <script src="../view/affiliates/assets/js/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
        <script src="../view/affiliates/assets/js/ie10-viewport-bug-workaround.js"></script>
        <script src="../view/affiliates/assets/js/masonry.pkgd.min.js"></script>
        <script src="../view/affiliates/assets/js/theme.js"></script>
        <script src="../view/affiliates/assets/js/pageScript.js"></script>
       <script src="../view/affiliates/assets/inteltemplate/js/vendors.bundle.js"></script>
        <script src="../view/affiliates/assets/inteltemplate/js/app.bundle.js"></script>
    </body>
</html>
