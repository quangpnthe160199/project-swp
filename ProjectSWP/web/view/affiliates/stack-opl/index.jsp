<%-- 
    Document   : demohome
    Created on : May 13, 2022, 3:15:39 PM
    Author     : haiph
--%>

<%@page import="model.Account"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="icon" href="../view/affiliates/assets/images/logo-footer.png">
        <title>Beaucoup | An All-you-can-post page</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Rubik:400,400i,500,500i,700,700i" rel="stylesheet">
        <link href="../view/affiliates/assets/css/theme.css" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
        <%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
        <%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
        <link rel="stylesheet" href="../view/affiliates/assets/css/bootstrap.min.css" >
        <!-- Icon -->
        <link rel="stylesheet" href="../view/affiliates/assets/fonts/line-icons.css">
        <!-- Slicknav -->
        <link rel="stylesheet" href="../view/affiliates/assets/css/slicknav.css">
        <!-- Owl carousel -->
        <link rel="stylesheet" href="../view/affiliates/assets/css/owl.carousel.min.css">
        <link rel="stylesheet" href="../view/affiliates/assets/css/owl.theme.css">

        <link rel="stylesheet" href="../view/affiliates/assets/css/magnific-popup.css">
        <link rel="stylesheet" href="../view/affiliates/assets/css/nivo-lightbox.css">
        <!-- Animate -->
        <link rel="stylesheet" href="../view/affiliates/assets/css/animate.css">
        <!-- Main Style -->
        <link rel="stylesheet" href="../view/affiliates/assets/css/main.css">
        <link href="../view/affiliates/assets/css/theme.css" rel="stylesheet">
        <!-- Responsive Style -->
        <link rel="stylesheet" href="../view/affiliates/assets/css/responsive.css">
        <style>
            table {
                border-collapse: collapse;
                border-spacing: 0;
                width: 100%;
                border: 1px solid #ddd;
            }

            /* Style table headers and table data */
            th, td {
                text-align: center;
                padding: 16px;
            }

            th:first-child, td:first-child {
                text-align: left;
            }

            /* Zebra-striped table rows */
            tr:nth-child(even) {
                background-color: #f2f2f2
            }

            .fa-check {
                color: green;
            }

            .fa-remove {
                color: red;
            }
        </style>
        <!-- Begin tracking codes here, including ShareThis/Analytics -->

        <!-- End tracking codes here, including ShareThis/Analytics -->
    </head>
    <body class="layout-default">
        <!-- Begin Menu Navigation
        ================================================== -->
        <header class="navbar navbar-toggleable-md navbar-light bg-white fixed-top mediumnavigation">
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarsWow" aria-controls="navbarsWow" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="container">
                <!-- Begin Logo -->

                <!-- End Logo -->
                <!-- Begin Menu -->
                <div class="collapse navbar-collapse" id="navbarsWow">
                    <a class="navbar-brand" href="../home">
                        <img src="../view/affiliates/assets/images/logo.png" alt="Affiliates - Free Bootstrap Template" style="position: relative; width: 170px; left: 15px; background-size: contain;">
                    </a>
                    <!-- Begin Menu -->
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="#services">Services</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#compare">Comparasion</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="../home/contact">Contact</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link highlight" href="#pricing">Buy Now</a>
                        </li>
                    </ul>
                    <!-- End Menu -->
                </div>
            </div>
        </header>
        <!-- End Menu Navigation
        ================================================== -->
        <div class="site-content">
            <!-- Home Jumbotron
        ================================================== -->
            <section class="intro">
            </section>
            <!-- Container
        ================================================== -->
            <div class="container">
                <div class="main-content">
                    <div id="hero-area" class="hero-area-bg">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12 col-sm-12">
                                    <div class="contents text-center">
                                        <h2 class="head-title wow fadeInUp"> BeauCoup Premium <br> From 5$/Month</h2>
                                        <div class="header-button wow fadeInUp" data-wow-delay="0.3s">
                                            <a href="#pricing" class="btn btn-common">Start Now</a>
                                        </div>
                                    </div>
                                    <div class="img-thumb text-center wow fadeInUp" data-wow-delay="0.6s">
                                        <img class="img-fluid" src="assets/img/hero-1.png" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Hero Area End -->

                    </header>
                    <!-- Header Area wrapper End -->

                    <section id="services" class="section-padding bg-gray">
                        <div class="container">
                            <div class="section-header text-center">
                                <h2 class="section-title wow fadeInDown" data-wow-delay="0.3s">Our Services</h2>
                                <p>A desire to help and empower others between community contributors in technology <br> began to grow in 2020.</p>
                            </div>
                            <div class="row">
                                <!-- Services item -->
                                <div class="col-md-6 col-lg-4 col-xs-12">
                                    <div class="services-item wow fadeInRight" data-wow-delay="0.3s">
                                        <div class="icon">
                                            <i><img src="../view/affiliates/assets/img/feature/search_by_popularity.svg"></i>
                                        </div>
                                        <div class="services-content">
                                            <h3><a href="#">Sort by popularity</a></h3>
                                            <p>Find your work by seeing what's popular among other users</p>
                                        </div>
                                    </div>
                                </div>
                                <!-- Services item -->
                                <div class="col-md-6 col-lg-4 col-xs-12">
                                    <div class="services-item wow fadeInRight" data-wow-delay="0.6s">
                                        <div class="icon">
                                            <i><img src="../view/affiliates/assets/img/feature/bookmark.svg"></i>
                                        </div>
                                        <div class="services-content">
                                            <h3><a href="#">Filter by Bookmarks</a></h3>
                                            <p>Make the job of finding your favorite work easier</p>
                                        </div>
                                    </div>
                                </div>
                                <!-- Services item -->
                                <div class="col-md-6 col-lg-4 col-xs-12">
                                    <div class="services-item wow fadeInRight" data-wow-delay="0.9s">
                                        <div class="icon">
                                            <i><img src="../view/affiliates/assets/img/feature/hide_ad.svg"></i>
                                        </div>
                                        <div class="services-content">
                                            <h3><a href="#">Hide ads</a></h3>
                                            <p>Hide annoying ads, making page load even faster</p>
                                        </div>
                                    </div>
                                </div>
                                <!-- Services item -->
                                <div class="col-md-6 col-lg-4 col-xs-12">
                                    <div class="services-item wow fadeInRight" data-wow-delay="1.2s">
                                        <div class="icon">
                                            <i><img src="../view/affiliates/assets/img/feature/history.svg"></i>
                                        </div>
                                        <div class="services-content">
                                            <h3><a href="#">Browsing history</a></h3>
                                            <p>Keep a list of work you've viewed in the last month</p>
                                        </div>
                                    </div>
                                </div>
                                <!-- Services item -->
                                <div class="col-md-6 col-lg-4 col-xs-12">
                                    <div class="services-item wow fadeInRight" data-wow-delay="1.5s">
                                        <div class="icon">
                                            <i><img src="../view/affiliates/assets/img/feature/mute.svg"></i>
                                        </div>
                                        <div class="services-content">
                                            <h3><a href="#">Mute function</a></h3>
                                            <p>Hide certain work by tags and user for up to 180 days</p>
                                        </div>
                                    </div>
                                </div>
                                <!-- Services item -->
                                <div class="col-md-6 col-lg-4 col-xs-12">
                                    <div class="services-item wow fadeInRight" data-wow-delay="1.8s">
                                        <div class="icon">
                                            <i><img src="../view/affiliates/assets/img/feature/change_img.svg"></i>
                                        </div>
                                        <div class="services-content">
                                            <h3><a href="#">Edit a work</a></h3>
                                            <p>Fix your mistake without losing likes or comment</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>

                    <!-- Feature Section Start -->

                    <section id="compare" class="section-padding bg-gray">
                        <div class="container">
                            <div class="section-header text-center">
                                <h2 class="section-title wow fadeInDown" data-wow-delay="0.3s">Comparison</h2>
                                <p>A comparison between the two different services our website provide.</p>
                            </div>
                            <div class="row">
                                <div class="table-responsive">
                                    <table class="table table-striped fa-check text-successtable-border border-light" data-wow-delay="0.6s">
                                        <thead class="border-light">
                                            <tr>
                                                <th scope="col"></th>
                                                <th scope="col"><strong>Free</strong></th>
                                                <th scope="col"><strong>Premium</strong></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <th scope="row">Search Work</th>
                                                <td><i>By post time only</i></td>
                                                <td><i>By popularity</i></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Browsing History</th>
                                                <td><i>1 week limit of browsing history</i></td>
                                                <td><i>Browsing history within the last month</i></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Mute Item</th>
                                                <td><i>Mute only 1 item</i></td>
                                                <td><i>Mute up to 500 items</i></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Change ID</th>
                                                <td><i>Change ID only once</i></td>
                                                <td><i>Change ID unlimitedly</i></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">High-speed loading with no ads</th>
                                                <td><i class="fas fa-times text-danger"></i></td>
                                                <td><i class="fas fa-check text-success"></i></td>
                                            </tr
                                            <tr>
                                                <th scope="row">Search novels with custom range of character count</th>
                                                <td><i class="fas fa-times text-danger"></i></td>
                                                <td><i class="fas fa-check text-success"></i></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Edit a work you've uploaded</th>
                                                <td><i class="fas fa-times text-danger"></i></td>
                                                <td><i class="fas fa-check text-success"></i></td>
                                            </tr>

                                        </tbody>
                                    </table>
                                </div>  
                            </div>
                        </div>
                    </section>
                    <!-- Feature Section End -->

                    <!-- Services Section Start -->

                    <!-- Services Section End -->

                    <!--                     Start Video promo Section 
                                        <section class="video-promo section-padding">
                                            <div class="overlay"></div>
                                            <div class="container">
                                                <div class="row">
                                                    <div class="col-md-12 col-sm-12">
                                                        <div class="video-promo-content text-center wow fadeInUp" data-wow-delay="0.3s">
                                                            <a href="https://www.youtube.com/watch?v=yP6kdOZHids" class="video-popup"><i class="lni-film-play"></i></a>
                                                            <h2 class="mt-3 wow zoomIn" data-wow-duration="1000ms" data-wow-delay="100ms">Watch Video</h2>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                         End Video Promo Section 
                    
                                         Team Section Start 
                                        <section id="team" class="section-padding text-center">
                                            <div class="container">
                                                <div class="section-header text-center">
                                                    <h2 class="section-title wow fadeInDown" data-wow-delay="0.3s">Meet our team</h2>
                                                    <p>A desire to help and empower others between community contributors in technology <br> began to grow in 2020.</p>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-6 col-md-6 col-lg-3">
                                                         Team Item Starts 
                                                        <div class="team-item text-center wow fadeInRight" data-wow-delay="0.3s">
                                                            <div class="team-img">
                                                                <img class="img-fluid" src="assets/img/team/team-01.png" alt="">
                                                                <div class="team-overlay">
                                                                    <div class="overlay-social-icon text-center">
                                                                        <ul class="social-icons">
                                                                            <li><a href="#"><i class="lni-facebook-filled" aria-hidden="true"></i></a></li>
                                                                            <li><a href="#"><i class="lni-twitter-filled" aria-hidden="true"></i></a></li>
                                                                            <li><a href="#"><i class="lni-instagram-filled" aria-hidden="true"></i></a></li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="info-text">
                                                                <h3><a href="#">David Smith</a></h3>
                                                                <p>Chief Operating Officer</p>
                                                            </div>
                                                        </div>
                                                         Team Item Ends 
                                                    </div>
                                                    <div class="col-sm-6 col-md-6 col-lg-3">
                                                         Team Item Starts 
                                                        <div class="team-item text-center wow fadeInRight" data-wow-delay="0.6s">
                                                            <div class="team-img">
                                                                <img class="img-fluid" src="assets/img/team/team-02.png" alt="">
                                                                <div class="team-overlay">
                                                                    <div class="overlay-social-icon text-center">
                                                                        <ul class="social-icons">
                                                                            <li><a href="#"><i class="lni-facebook-filled" aria-hidden="true"></i></a></li>
                                                                            <li><a href="#"><i class="lni-twitter-filled" aria-hidden="true"></i></a></li>
                                                                            <li><a href="#"><i class="lni-instagram-filled" aria-hidden="true"></i></a></li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="info-text">
                                                                <h3><a href="#">Eric Peterson</a></h3>
                                                                <p>Product Designer</p>
                                                            </div>
                                                        </div>
                                                         Team Item Ends 
                                                    </div>
                    
                                                    <div class="col-sm-6 col-md-6 col-lg-3">
                                                         Team Item Starts 
                                                        <div class="team-item text-center wow fadeInRight" data-wow-delay="0.9s">
                                                            <div class="team-img">
                                                                <img class="img-fluid" src="assets/img/team/team-03.png" alt="">
                                                                <div class="team-overlay">
                                                                    <div class="overlay-social-icon text-center">
                                                                        <ul class="social-icons">
                                                                            <li><a href="#"><i class="lni-facebook-filled" aria-hidden="true"></i></a></li>
                                                                            <li><a href="#"><i class="lni-twitter-filled" aria-hidden="true"></i></a></li>
                                                                            <li><a href="#"><i class="lni-instagram-filled" aria-hidden="true"></i></a></li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="info-text">
                                                                <h3><a href="#">Durwin Babb</a></h3>
                                                                <p>Lead Designer</p>
                                                            </div>
                                                        </div>
                                                         Team Item Ends 
                                                    </div>
                    
                                                    <div class="col-sm-6 col-md-6 col-lg-3">
                                                         Team Item Starts 
                                                        <div class="team-item text-center wow fadeInRight" data-wow-delay="1.2s">
                                                            <div class="team-img">
                                                                <img class="img-fluid" src="assets/img/team/team-04.png" alt="">
                                                                <div class="team-overlay">
                                                                    <div class="overlay-social-icon text-center">
                                                                        <ul class="social-icons">
                                                                            <li><a href="#"><i class="lni-facebook-filled" aria-hidden="true"></i></a></li>
                                                                            <li><a href="#"><i class="lni-twitter-filled" aria-hidden="true"></i></a></li>
                                                                            <li><a href="#"><i class="lni-instagram-filled" aria-hidden="true"></i></a></li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="info-text">
                                                                <h3><a href="#">Marijn Otte</a></h3>
                                                                <p>Front-end Developer</p>
                                                            </div>
                                                        </div>
                                                         Team Item Ends 
                                                    </div>
                    
                                                </div>
                                            </div>
                                        </section>
                                         Team Section End -->

                    <!-- Counter Section Start -->
<!--                    <section id="counter" class="section-padding">
                        <div class="overlay"></div>
                        <div class="container">
                            <div class="row justify-content-between">
                                <div class="col-lg-12 col-md-12 col-xs-12">
                                    <div class="row">
                                         Start counter 
                                        <div class="col-lg-3 col-md-6 col-xs-12">
                                            <div class="counter-box wow fadeInUp" data-wow-delay="0.2s">
                                                <div class="icon-o"><i class="lni-users"></i></div>
                                                <div class="fact-count">
                                                    <h3><span class="counter">23576</span></h3>
                                                    <p>Users</p>
                                                </div>
                                            </div>
                                        </div>
                                         End counter 
                                         Start counter 
                                        <div class="col-lg-3 col-md-6 col-xs-12">
                                            <div class="counter-box wow fadeInUp" data-wow-delay="0.4s">
                                                <div class="icon-o"><i class="lni-emoji-smile"></i></div>
                                                <div class="fact-count">
                                                    <h3><span class="counter">2124</span></h3>
                                                    <p>Positive Reviews</p>
                                                </div>
                                            </div>
                                        </div>
                                         End counter 
                                         Start counter 
                                        <div class="col-lg-3 col-md-6 col-xs-12">
                                            <div class="counter-box wow fadeInUp" data-wow-delay="0.6s">
                                                <div class="icon-o"><i class="lni-download"></i></div>
                                                <div class="fact-count">
                                                    <h3><span class="counter">54598</span></h3>
                                                    <p>Downloads</p>
                                                </div>
                                            </div>
                                        </div>
                                         End counter 
                                         Start counter 
                                        <div class="col-lg-3 col-md-6 col-xs-12">
                                            <div class="counter-box wow fadeInUp" data-wow-delay="0.8s">
                                                <div class="icon-o"><i class="lni-thumbs-up"></i></div>
                                                <div class="fact-count">
                                                    <h3><span class="counter">3212</span></h3>
                                                    <p>Followers</p>
                                                </div>
                                            </div>
                                        </div>
                                         End counter 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                     Counter Section End -->

                    <section id="pricing" class="section-padding bg-gray">
                        <div class="container">
                            <div class="section-header text-center">
                                <h2 class="section-title wow fadeInDown" data-wow-delay="0.3s">Best Pricing</h2>
                                <p>A desire to help and empower others between community contributors in technology <br> began to grow in 2020.</p>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 col-md-6 col-xs-12">
                                    <div class="table wow fadeInLeft" data-wow-delay="1.2s">
                                        <div class="title">
                                            <h3>1 Month</h3>
                                        </div>
                                        <div class="pricing-header">
                                            <p class="price-value">116,000VND<span>/ Month</span></p>
                                        </div>
                                        <ul class="description">
                                        </ul>
                                        <form action="subcription/purchase" method="POST">
                                            <img class="mt-4 mb-4" src="">
                                            <input type="hidden" name="type" value="1month">
                                            <input type="hidden" name="price" value="116,000" >
                                            <input class="btn btn-common" type="submit" value="Get it" />
                                        </form>
                                    </div> 
                                </div>
                                <div class="col-lg-4 col-md-6 col-xs-12 active">
                                    <div class="table wow fadeInUp" id="active-tb" data-wow-delay="1.2s">
                                        <div class="title">
                                            <h3>6 Months</h3>
                                        </div>
                                        <div class="pricing-header">
                                            <p class="price-value">580,000VND<span>/ 6 Month</span></p>
                                        </div>
                                        <ul class="description">
                                        </ul>
                                        <form action="subcription/purchase" method="POST">
                                            <img class="mt-4 mb-4" src="">
                                            <input type="hidden" name="type" value="6month">
                                            <input type="hidden" name="price" value="580,000" >
                                            <input class="btn btn-common" type="submit" value="Get it" />
                                        </form>
                                    </div> 
                                </div>

                                <div class="col-lg-4 col-md-6 col-xs-12">
                                    <div class="table wow fadeInRight" data-wow-delay="1.2s">
                                        <div class="title">
                                            <h3>12 Months</h3>
                                        </div>
                                        <div class="pricing-header">
                                            <p class="price-value">1,044,000VND<span>/ 12 Month</span></p>
                                        </div>
                                        <ul class="description">
                                        </ul>
                                        <form action="subcription/purchase" method="POST">
                                            <img class="mt-4 mb-4" src="">
                                            <input type="hidden" name="type" value="12month">
                                            <input type="hidden" name="price" value="1,044,000" >
                                            <input class="btn btn-common" type="submit" value="Get it" />
                                        </form>
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </section>
                    Pricing Table Section End <!--

                   <div class="skill-area section-padding">
                       <div class="container">
                           <div class="row">
                               <div class="col-lg-6 col-md-12 col-xs-12 wow fadeInLeft" data-wow-delay="0.3s">
                                   <img class="img-fluid" src="assets/img/about/img-1.jpg" alt="" >
                               </div>
                               <div class="col-lg-6 col-md-12 col-xs-12 info wow fadeInRight" data-wow-delay="0.3s">
                                   <div class="site-heading">
                                       <h2 class="section-title">Our <span>Skill</span></h2>
                                       <p>
                                           Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus architecto laudantium dolorem, aut aspernatur modi minima alias provident obcaecati! Minima odio porro nemo magnam dolore minus asperiores veniam dolorum est!
                                       </p>
                                       <p>
                                           Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas, nesciunt possimus quaerat ipsam, corporis architecto aspernatur non aut! Dolorum consectetur placeat excepturi, perspiciatis sunt.
                                       </p>
                                   </div>
                                   <div class="skills-section">
                                        Progress Bar Start 
                                       <div class="progress-box">
                                           <h5>Strategy &amp; Analysis <span class="pull-right">88%</span></h5>
                                           <div class="progress" style="opacity: 1; left: 0px;">
                                               <div class="progress-bar" role="progressbar" data-width="87" style="width: 87%;"></div>
                                           </div>
                                       </div>
                                       <div class="progress-box">
                                           <h5>Eeconomic growth <span class="pull-right">95%</span></h5>
                                           <div class="progress" style="opacity: 1; left: 0px;">
                                               <div class="progress-bar" role="progressbar" data-width="96" style="width: 96%;"></div>
                                           </div>
                                       </div>
                                       <div class="progress-box">
                                           <h5>Achieves goals <span class="pull-right">70%</span></h5>
                                           <div class="progress" style="opacity: 1; left: 0px;">
                                               <div class="progress-bar" role="progressbar" data-width="52" style="width: 52%;"></div>
                                           </div>
                                       </div>
                                        End Progressbar 
                                   </div>
                               </div>
                           </div>
                       </div>
                   </div>

                    Portfolio Section 
                   <section id="portfolios" class="section-padding">
                        Container Starts 
                       <div class="container">
                           <div class="section-header text-center">
                               <h2 class="section-title wow fadeInDown" data-wow-delay="0.3s">Our Works</h2>
                               <p>A desire to help and empower others between community contributors in technology <br> began to grow in 2020.</p>
                           </div>
                           <div class="row">          
                               <div class="col-md-12">
                                    Portfolio Controller/Buttons 
                                   <div class="controls text-center">
                                       <a class="filter active btn btn-common btn-effect" data-filter="all">
                                           All 
                                       </a>
                                       <a class="filter btn btn-common btn-effect" data-filter=".design">
                                           Design 
                                       </a>
                                       <a class="filter btn btn-common btn-effect" data-filter=".development">
                                           Development
                                       </a>
                                       <a class="filter btn btn-common btn-effect" data-filter=".print">
                                           Print 
                                       </a>
                                   </div>
                                    Portfolio Controller/Buttons Ends
                               </div>
                           </div>

                            Portfolio Recent Projects 
                           <div id="portfolio" class="row">
                               <div class="col-lg-4 col-md-6 col-xs-12 mix development print">
                                   <div class="portfolio-item">
                                       <div class="shot-item">
                                           <img src="assets/img/portfolio/img-1.jpg" alt="" />  
                                           <div class="single-content">
                                               <div class="fancy-table">
                                                   <div class="table-cell">
                                                       <div class="zoom-icon">
                                                           <a class="lightbox" href="assets/img/portfolio/img-1.jpg"><i class="lni-eye item-icon"></i></a>
                                                       </div>
                                                       <a href="#">Creative Design</a>
                                                   </div>
                                               </div>
                                           </div>
                                       </div>               
                                   </div>
                               </div>
                               <div class="col-lg-4 col-md-6 col-xs-12 mix design print">
                                   <div class="portfolio-item">
                                       <div class="shot-item">
                                           <img src="assets/img/portfolio/img-2.jpg" alt="" /> 
                                           <div class="single-content">
                                               <div class="fancy-table">
                                                   <div class="table-cell">
                                                       <div class="zoom-icon">
                                                           <a class="lightbox" href="assets/img/portfolio/img-2.jpg"><i class="lni-eye item-icon"></i></a>
                                                       </div>
                                                       <a href="#">Retina Ready</a>
                                                   </div>
                                               </div>
                                           </div>
                                       </div>               
                                   </div>
                               </div>
                               <div class="col-lg-4 col-md-6 col-xs-12 mix development">
                                   <div class="portfolio-item">
                                       <div class="shot-item">
                                           <img src="assets/img/portfolio/img-3.jpg" alt="" />  
                                           <div class="single-content">
                                               <div class="fancy-table">
                                                   <div class="table-cell">
                                                       <div class="zoom-icon">
                                                           <a class="lightbox" href="assets/img/portfolio/img-3.jpg"><i class="lni-eye item-icon"></i></a>
                                                       </div>
                                                       <a href="#">Responsive</a>
                                                   </div>
                                               </div>
                                           </div>
                                       </div>               
                                   </div>
                               </div>
                               <div class="col-lg-4 col-md-6 col-xs-12 mix development design">
                                   <div class="portfolio-item">
                                       <div class="shot-item">
                                           <img src="assets/img/portfolio/img-4.jpg" alt="" /> 
                                           <div class="single-content">
                                               <div class="fancy-table">
                                                   <div class="table-cell">
                                                       <div class="zoom-icon">
                                                           <a class="lightbox" href="assets/img/portfolio/img-4.jpg"><i class="lni-eye item-icon"></i></a>
                                                       </div>
                                                       <a href="#">Well Documented</a>
                                                   </div>
                                               </div>
                                           </div>
                                       </div>               
                                   </div>
                               </div>
                               <div class="col-lg-4 col-md-6 col-xs-12 mix development">
                                   <div class="portfolio-item">
                                       <div class="shot-item">
                                           <img src="assets/img/portfolio/img-5.jpg" alt="" />  
                                           <div class="single-content">
                                               <div class="fancy-table">
                                                   <div class="table-cell">
                                                       <div class="zoom-icon">
                                                           <a class="lightbox" href="assets/img/portfolio/img-5.jpg"><i class="lni-eye item-icon"></i></a>
                                                       </div>
                                                       <a href="#">Customer Support</a>
                                                   </div>
                                               </div>
                                           </div>
                                       </div>               
                                   </div>
                               </div>
                               <div class="col-lg-4 col-md-6 col-xs-12 mix print design">
                                   <div class="portfolio-item">
                                       <div class="shot-item">
                                           <img src="assets/img/portfolio/img-6.jpg" alt="" />  
                                           <div class="single-content">
                                               <div class="fancy-table">
                                                   <div class="table-cell">
                                                       <div class="zoom-icon">
                                                           <a class="lightbox" href="assets/img/portfolio/img-6.jpg"><i class="lni-eye item-icon"></i></a>
                                                       </div>
                                                       <a href="#">User Friendly</a>
                                                   </div>
                                               </div>
                                           </div>
                                       </div>               
                                   </div>
                               </div>
                           </div>
                       </div>
                        Container Ends 
                   </section>-->
                    <!-- Portfolio Section Ends --> 

                    <!-- Testimonial Section Start -->
                    <section id="testimonial" class="testimonial section-padding">
                        <div class="overlay"></div>
                        <div class="container">
                            <div class="row justify-content-center">
                                <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12">
                                    <div id="testimonials" class="owl-carousel wow fadeInUp" data-wow-delay="1.2s">
                                        <div class="item">
                                            <div class="testimonial-item">
                                                <div class="img-thumb">
                                                    <img src="assets/img/testimonial/img1.jpg" alt="">
                                                </div>
                                                <div class="info">
                                                    <h2><a href="#">Grenchen Pearce</a></h2>
                                                    <h3><a href="#">Boston Brothers co.</a></h3>
                                                </div>
                                                <div class="content">
                                                    <p class="description">Holisticly empower leveraged ROI whereas effective web-readiness. Completely enable emerging meta-services with cross-platform web services. Quickly initiate inexpensive total linkage rather than extensible scenarios. Holisticly empower leveraged ROI whereas effective web-readiness. </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="testimonial-item">
                                                <div class="img-thumb">
                                                    <img src="assets/img/testimonial/img2.jpg" alt="">
                                                </div>
                                                <div class="info">
                                                    <h2><a href="#">Domeni GEsson</a></h2>
                                                    <h3><a href="#">Awesome Technology co.</a></h3>
                                                </div>
                                                <div class="content">
                                                    <p class="description">Holisticly empower leveraged ROI whereas effective web-readiness. Completely enable emerging meta-services with cross-platform web services. Quickly initiate inexpensive total linkage rather than extensible scenarios. Holisticly empower leveraged ROI whereas effective web-readiness. </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="testimonial-item">
                                                <div class="img-thumb">
                                                    <img src="assets/img/testimonial/img3.jpg" alt="">
                                                </div>
                                                <div class="info">
                                                    <h2><a href="#">Dommini Albert</a></h2>
                                                    <h3><a href="#">Nesnal Design co.</a></h3>
                                                </div>
                                                <div class="content">
                                                    <p class="description">Holisticly empower leveraged ROI whereas effective web-readiness. Completely enable emerging meta-services with cross-platform web services. Quickly initiate inexpensive total linkage rather than extensible scenarios. Holisticly empower leveraged ROI whereas effective web-readiness. </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="testimonial-item">
                                                <div class="img-thumb">
                                                    <img src="assets/img/testimonial/img4.png" alt="">
                                                </div>
                                                <div class="info">
                                                    <h2><a href="#">Fernanda Anaya</a></h2>
                                                    <h3><a href="#">Developer</a></h3>
                                                </div>
                                                <div class="content">
                                                    <p class="description">Holisticly empower leveraged ROI whereas effective web-readiness. Completely enable emerging meta-services with cross-platform web services. Quickly initiate inexpensive total linkage rather than extensible scenarios. Holisticly empower leveraged ROI whereas effective web-readiness. </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="testimonial-item">
                                                <div class="img-thumb">
                                                    <img src="assets/img/testimonial/img5.png" alt="">
                                                </div>
                                                <div class="info">
                                                    <h2><a href="#">Jason A.</a></h2>
                                                    <h3><a href="#">Designer</a></h3>
                                                </div>
                                                <div class="content">
                                                    <p class="description">Holisticly empower leveraged ROI whereas effective web-readiness. Completely enable emerging meta-services with cross-platform web services. Quickly initiate inexpensive total linkage rather than extensible scenarios. Holisticly empower leveraged ROI whereas effective web-readiness. </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!-- Testimonial Section End -->  

                    <!--                     Blog Section 
                                        <section id="blog" class="section-padding">
                                             Container Starts 
                                            <div class="container">
                                                <div class="section-header text-center">
                                                    <h2 class="section-title wow fadeInDown" data-wow-delay="0.3s">Latest blog</h2>
                                                    <p>A desire to help and empower others between community contributors in technology <br> began to grow in 2020.</p>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 blog-item">
                                                         Blog Item Starts 
                                                        <div class="blog-item-wrapper wow fadeInLeft" data-wow-delay="0.3s">
                                                            <div class="blog-item-img">
                                                                <a href="single-post.html">
                                                                    <img src="assets/img/blog/img1.jpg" alt="">
                                                                </a>                
                                                            </div>
                                                            <div class="blog-item-text"> 
                                                                <h3>
                                                                    <a href="single-post.html">Suspendisse dictum non velit</a>
                                                                </h3>
                                                                <p>
                                                                    Nunc in mauris a ante rhoncus tristique vitae et nisl. Quisque ullamcorper rutrum lacinia. Integer varius ornare egestas. 
                                                                </p>
                                                                <a href="single-post.html" class="btn btn-common btn-rm">Read More</a>
                                                            </div>
                                                        </div>
                                                         Blog Item Wrapper Ends
                                                    </div>
                    
                                                    <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 blog-item">
                                                         Blog Item Starts 
                                                        <div class="blog-item-wrapper wow fadeInUp" data-wow-delay="0.6s">
                                                            <div class="blog-item-img">
                                                                <a href="single-post.html">
                                                                    <img src="assets/img/blog/img2.jpg" alt="">
                                                                </a>                
                                                            </div>
                                                            <div class="blog-item-text"> 
                                                                <h3>
                                                                    <a href="single-post.html">Remarkably Did Increasing</a>
                                                                </h3>
                                                                <p>
                                                                    Nunc in mauris a ante rhoncus tristique vitae et nisl. Quisque ullamcorper rutrum lacinia. Integer varius ornare egestas. 
                                                                </p>
                                                                <a href="single-post.html" class="btn btn-common btn-rm">Read More</a>
                                                            </div>
                                                        </div>
                                                         Blog Item Wrapper Ends
                                                    </div>
                    
                                                    <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 blog-item">
                                                         Blog Item Starts 
                                                        <div class="blog-item-wrapper wow fadeInRight" data-wow-delay="0.3s">
                                                            <div class="blog-item-img">
                                                                <a href="single-post.html">
                                                                    <img src="assets/img/blog/img3.jpg" alt="">
                                                                </a>                
                                                            </div>
                                                            <div class="blog-item-text"> 
                                                                <h3>
                                                                    <a href="single-post.html">Changing the topic scope</a>
                                                                </h3>
                                                                <p>
                                                                    Nunc in mauris a ante rhoncus tristique vitae et nisl. Quisque ullamcorper rutrum lacinia. Integer varius ornare egestas. 
                                                                </p>
                                                                <a href="single-post.html" class="btn btn-common btn-rm">Read More</a>
                                                            </div>
                                                        </div>
                                                         Blog Item Wrapper Ends
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                         blog Section End 
                    
                                         Clients Section Start 
                                        <div id="clients" class="section-padding bg-gray">
                                            <div class="container">
                                                <div class="section-header text-center">
                                                    <h2 class="section-title wow fadeInDown" data-wow-delay="0.3s">NOTABLE CLIENTS</h2>
                                                    <p>Over the last 20 years, we have helped and guided organisations to achieve outstanding results</p>
                                                </div>
                                                <div class="row text-align-">
                                                    <div class="col-lg-3 col-md-3 col-xs-12 wow fadeInUp" data-wow-delay="0.3s">
                                                        <div class="client-item-wrapper">
                                                            <img class="img-fluid" src="assets/img/clients/img1.png" alt="">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3 col-md-3 col-xs-12 wow fadeInUp" data-wow-delay="0.6s">
                                                        <div class="client-item-wrapper">
                                                            <img class="img-fluid" src="assets/img/clients/img2.png" alt="">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3 col-md-3 col-xs-12 wow fadeInUp" data-wow-delay="0.9s">
                                                        <div class="client-item-wrapper">
                                                            <img class="img-fluid" src="assets/img/clients/img3.png" alt="">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3 col-md-3 col-xs-12 wow fadeInUp" data-wow-delay="1.2s">
                                                        <div class="client-item-wrapper">
                                                            <img class="img-fluid"  src="assets/img/clients/img4.png" alt="">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                         Clients Section End -->


                    <!-- Copyright Section End -->

                    <!-- Go to Top Link -->
                    <a href="#" class="back-to-top">
                        <i class="lni-arrow-up"></i>
                    </a>

                    <!-- Preloader -->
                    <div id="preloader">
                        <div class="loader" id="loader-1"></div>
                    </div>
                </div>
            </div>
            <!-- /.container -->
            <!-- Before Footer
        ================================================== -->

            <!-- Begin Footer
        ================================================== -->
            <footer class="footer">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="footer-widget">
                                <a href="../home/about"> 
                                    <img src="../view/affiliates/assets/images/logo-footer.png" alt="logo footer" style="position: relative; width: 170px; left: 15px; background-size: contain;">
                                </a>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="footer-widget">
                                <h5 class="title">Author</h5>
                                <ul>
                                    <li><a href="#">About Us</a></li>
                                    <li><a target="_blank" href="https://www.wowthemes.net/affiliate-area/">Affiliates</a></li>
                                    <li><a href="https://www.wowthemes.net/terms-and-conditions/">License</a></li>
                                    <li><a href="https://www.wowthemes.net/blog/">Blog</a></li>
                                    <li><a href="contact.jsp">Contact</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="footer-widget textwidget">
                                <h5 class="title">Email us</h5>
                                <form action="https://formspree.io/wowthemesnet@gmail.com" method="POST">
                                    <input type="text" name="name" placeholder="Name">
                                    <input type="email" name="_replyto" placeholder="E-mail Address">
                                    <textarea rows="8" name="message" placeholder="Message"></textarea><br/>
                                    <input class="btn btn-success" type="submit" value="Send">
                                </form>                             
                            </div>
                        </div>
                    </div>
                    <div class="copyright">
                        <p class="pull-left">
                            Copyright © 2018 Affiliates HTML Template
                        </p>
                        <p class="pull-right">
                            <!-- Leave credit to author unless you own a commercial license: https://www.wowthemes.net/freebies-license/ -->
                            <a target="_blank" href="https://www.wowthemes.net/affiliates-free-bootstrap-template/">"Affiliates Template"</a> - Design & Code by WowThemes.net
                        </p>
                        <div class="clearfix">
                        </div>
                    </div>
                </div>
            </footer>
            <!-- End Footer
        ================================================== -->
        </div>


        <!-- JavaScript
        ================================================== -->
        <script src="../view/affiliates/assets/js/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
        <script src="../view/affiliates/assets/js/ie10-viewport-bug-workaround.js"></script>
        <script src="../view/affiliates/assets/js/masonry.pkgd.min.js"></script>
        <script src="../view/affiliates/assets/js/theme.js"></script>
        <script src="../view/affiliates/assets/js/pageScript.js"></script>
        <script src="../view/affiliates/assets/js/jquery-min.js"></script>
        <script src="../view/affiliates/assets/js/popper.min.js"></script>
        <script src="../view/affiliates/assets/js/bootstrap.min.js"></script>
        <script src="../view/affiliates/assets/js/owl.carousel.min.js"></script>
        <script src="../view/affiliates/assets/js/jquery.mixitup.js"></script>
        <script src="../view/affiliates/assets/js/wow.js"></script>
        <script src="../view/affiliates/assets/js/jquery.nav.js"></script>
        <script src="../view/affiliates/assets/js/scrolling-nav.js"></script>
        <script src="../view/affiliates/assets/js/jquery.easing.min.js"></script>
        <script src="../view/affiliates/assets/js/jquery.counterup.min.js"></script>  
        <script src="../view/affiliates/assets/js/nivo-lightbox.js"></script>     
        <script src="../view/affiliates/assets/js/jquery.magnific-popup.min.js"></script>     
        <script src="../view/affiliates/assets/js/waypoints.min.js"></script>   
        <script src="../view/affiliates/assets/js/jquery.slicknav.js"></script>
        <script src="../view/affiliates/assets/js/main.js"></script>
        <script src="../view/affiliates/assets/js/form-validator.min.js"></script>
        <script src="../view/affiliates/assets/js/contact-form-script.min.js"></script>
    </body>
</html>

