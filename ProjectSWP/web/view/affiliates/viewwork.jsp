
<%@page import="model.Post"%>
<%@page import="model.Notification"%>
<%@page import="java.util.UUID"%>
<%@page import="model.Reply"%>
<%@page import="model.Comment"%>
<%@page import="java.util.ArrayList"%>
<%@page import="model.Account"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="icon" href="../view/affiliates/assets/images/logo-footer.png">
        <title>Beaucoup | An All-you-can-post page</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Rubik:400,400i,500,500i,700,700i" rel="stylesheet">
        <link href="../view/affiliates/assets/css/theme.css" rel="stylesheet">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.3/font/bootstrap-icons.css">
        <%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
        <%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
        <link rel="stylesheet" media="screen, print" href="../view/affiliates/assets/inteltemplate/css/vendors.bundle.css">
        <link rel="stylesheet" media="screen, print" href="../view/affiliates/assets/inteltemplate/css/app.bundle.css">
        <!-- base css -->
        <link rel="stylesheet" media="screen, print" href="../view/affiliates/assets/inteltemplate/css/vendors.bundle.css">
        
        <link rel="stylesheet" media="screen, print" href="../view/affiliates/assets/inteltemplate/css/app.bundle.css">
    </head>
    <body class="layout-page">
        <!-- Begin Menu Navigation
        ================================================== -->
        <header class="navbar navbar-expand-lg navbar-light bg-light fixed-top mediumnavigation">   
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarsWow" aria-controls="navbarsWow" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="container">
                <!-- Begin Logo -->
                <a class="navbar-brand" href="../home">
                    <img src="../view/affiliates/assets/images/logo.png" alt="Affiliates - Free Bootstrap Template" style="position: relative; width: 170px; left: 15px; background-size: contain;">
                </a>
                <!-- End Logo -->
                <!-- Begin Menu -->
                <div class="collapse navbar-collapse" id="navbarsWow">
                    <!-- Begin Menu -->
                    <%
                        Account account = (Account) request.getSession().getAttribute("account");
                    %>
                    <ul class="navbar-nav ml-auto">
                        <%
                            if (account != null) {
                        %>
                        <c:choose>
                            <c:when test="${not empty sessionScope.subcription}">
                                <li class="nav-item">
                                    <a class="nav-link" style="color:#Fcbc19" href="../home/subcription">BeauCoup Blog Premium</a>
                                </li>
                            </c:when>
                            <c:when test="${empty sessionScope.subcription}">
                                <li class="nav-item">
                                    <a class="nav-link" style="color:#Fcbc19" href="../home/subcription">BeauCoup Blog Premium Trial</a>
                                </li>
                            </c:when>
                        </c:choose>


                        <%}%>
                        <li class="nav-item">
                            <a class="nav-link" href="../home">Home</a>
                        </li>
                        <c:if test="${not empty account}">
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Publish</a>
                                <div class="dropdown-menu" aria-labelledby="dropdown01">
                                    <a class="dropdown-item" href="../home/upload">Illustration</a>
                                    <a class="dropdown-item" href="../home/upload">Post</a>
                                </div>
                            </li>
                        </c:if>
                        <li class="nav-item">
                            <a class="nav-link" href="../home/contact">Contact</a>
                        </li>
                        <%
                            if (account == null) {
                        %>

                        <li class="nav-item">
                            <a class="nav-link highlight" href="../home/login">Login</a>
                        </li>
                        <%} else {%>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Register Advertisement</a>
                            <div class="dropdown-menu" aria-labelledby="dropdown01">
                                <a class="dropdown-item" href="../home/userRegisterAds">Register Advertisement</a>

                            </div>
                        </li>
                        <%ArrayList<Notification> notifications = (ArrayList<Notification>) request.getSession().getAttribute("notifications");%>
                        <li class="nav-item">
                            <a class="nav-link" href="../home/messages"><i class="fa fa-envelope" style="font-size:24px"></i></a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bell" style="font-size:24px"></i></a>

                            <div class="dropdown-menu" aria-labelledby="dropdown01" style="background-color: lightblue" >
                                <%for (Notification notif : notifications) {%>
                                <div style="background-color: lightblue; border:solid 1px" id="notif<%=notif.getNid()%>" onclick="setNotifColor()">
                                    <img class="img-fluid img-responsive rounded-circle mr-2" src="https://wompampsupport.azureedge.net/fetchimage?siteId=7575&v=2&jpgQuality=100&width=700&url=https%3A%2F%2Fi.kym-cdn.com%2Fphotos%2Fimages%2Fnewsfeed%2F002%2F111%2F316%2Fc57.gif" width="38">
                                    <a class="dropdown-item" href="/ProjectSWP/home/work?workid=<%=notif.getAp().getWorkid()%>"><%=notif.getSenderAcc().getUsername()%> <%=notif.getContent()%> at <%=notif.getTimesent().getHours()%> : <%=notif.getTimesent().getMinutes()%> | <%=notif.getTimesent().getDate()%>/<%=notif.getTimesent().getMonth() + 1%>/<%=notif.getTimesent().getYear() + 1900%></a> 
                                </div>
                                <%}%>
                                <a class="dropdown-item" href="/ProjectSWP/notifications" style="text-align: center"> View all notifications </a>
                            </div>

                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              
                                <img class="author-thumb" src="data:image/jpg;base64,${sessionScope.accPic.ava}"   />
                                <c:if test="${not empty sessionScope.subcription}">
                                    <img src="../view/affiliates/assets/images/logo-footer.png" style="width: 50px;length: 50px; color: #Fcbc19" title="Your Subcription Last Until ${sessionScope.subcription.todate}">
                                </c:if>

                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                <a class="dropdown-item" href="../home/profile">View Profile</a>
                                <a class="dropdown-item" href="../home/pin">Pinned Work</a>
                                <a class="dropdown-item" href ="../home/delete">My Work</a> 
                                <a class="dropdown-item" href="../mute">Muted Work</a>
                                <a class="dropdown-item" href="../history">View History</a>
                                <a class="dropdown-item" href="../home/logout">Log Out</a>
                            </div>
                        </li>
                        <%}%>
                    </ul>
                    <!-- End Menu -->
                </div>
            </div>
        </header>
        <!-- End Menu Navigation
        ================================================== -->
        <div class="site-content">
            <div class="container">
                <!-- Content
    ================================================== -->
                <div class="main-content">
                    <!-- Begin Article
        ================================================== -->
                    <div class="row">
                        <!-- Sidebar -->
                        <div class="col-sm-4">
                            <div class="sidebar">
                                <div class="sidebar-section">                    

                                    
                                    <!--End mc_embed_signup-->
                                </div>
                                <div class="sidebar-section">
                                    <h5><span>Recommended</span></h5>
                                    <ul style="list-none">
                                        <li><a target="_blank" href="https://m.do.co/c/84c9b45d0c47">Digital Ocean</a></li>
                                        <li><a target="blank" href="https://www.cloudways.com/en/pricing.php?id=153986&amp;a_bid=005da123">Cloudways</a></li>
                                        <li><a target="blank" href="http://shareasale.com/r.cfm?b=875645&amp;u=1087935&amp;m=41388&amp;urllink=&amp;afftrack=">Page Speed Test</a></li>
                                        <li><a target="blank" href="https://elementor.com/?ref=1556">Elementor Page Builder</a></li>
                                        <li><a target="blank" href="https://www.wowthemes.net/category/jekyll-themes/">Our Jekyll Themes</a></li>
                                    </ul>
                                </div>
                                <div class="sidebar-section">
                                    <h5><span>Advertisements</span></h5>
                                    <a href="https://blogfb88.com" target="_blank" title="Nh ci chu u uy tn hng ??u Vi?t Nam"><img src="https://tylemacao.com/wp-content/uploads/2018/07/banner-fb88-tylemacao.gif"  alt="Nh ci chu u uy tn hng ??u Vi?t Nam"/></a>
                                    <a href="https://blogfb88.com" target="_blank" title="Nh ci chu u uy tn hng ??u Vi?t Nam"><img src="https://soicauxs888.com/wp-content/uploads/2021/09/fb88-banner-2-1.gif"  alt="Nh ci chu u uy tn hng ??u Vi?t Nam"/></a>
                                </div>
                            </div>
                        </div>
                        <!-- Post -->
                        <div class="col-sm-8">
                            <div class="mainheading">
                                <!-- Post Categories -->
                                <!--						<div class="after-post-tags">
                                                                                        <ul class="tags">
                                                                                                <li>
                                                                                                <a href="#">bootstrap</a>
                                                                                                </li>
                                                                                                <li>
                                                                                                <a href="#">tutorial</a>
                                                                                                </li>
                                                                                        </ul>
                                                                                </div>-->
                                <!-- End Categories -->
                                <!-- Post Title -->
                                <c:set var = "muted" value = "0"/>
                                <c:choose>
                                    <c:when test="${requestScope.muteduser.muteduser eq requestScope.post.ap.a.username}">
                                        <c:set var = "muted" value = "1"/>
                                    </c:when>
                                    <c:when test="${not empty requestScope.mutedtag}">
                                        <c:forEach items="${requestScope.tags}" var="t">
                                            <c:forEach items="${requestScope.mutedtag}" var="mt">
                                                <c:if test="${mt.t.tagid eq t.tagid}">
                                                    <c:set var = "muted" value = "1"/>
                                                </c:if>

                                            </c:forEach> 
                                        </c:forEach>
                                    </c:when>
                                </c:choose>
                                <c:choose>
                                    <c:when test="${muted eq 1}">
                                        <div class="mainheading">
                                            <h1 class="posttitle">${requestScope.post.name}</h1>
                                        </div>
                                        <div class="d-flex flex-row">
                                            <c:forEach items="${requestScope.tags}" var="t"> 
                                                <a style="color: transparent; text-shadow: 0 0 5px rgba(0,0,0,0.5);" class="p-2" href="../home?tag=${t.tagid}">#${t.tagname}</a>
                                            </c:forEach>
                                        </div>
                                        <!-- Post Featured Image -->
                                        <img style="filter: blur(8px);-webkit-filter: blur(8px);" class="featured-image img-fluid" src="data:image/jpg;base64,${requestScope.post.base64Image}" alt="">
                                        <!-- End Featured Image -->
                                        <!-- Post Content -->
                                        <div class="article-post">
                                            <p style="color: transparent; text-shadow: 0 0 5px rgba(0,0,0,0.5);">${requestScope.post.description}</p>
                                            <div class="clearfix">
                                            </div>
                                        </div>

                                    </c:when>
                                    <c:otherwise>
                                        <div class="mainheading">
                                            <h1 class="posttitle">${requestScope.post.name}</h1>
                                        </div>
                                        <div class="d-flex flex-row">
                                            <c:forEach items="${requestScope.tags}" var="t"> 
                                                <a class="p-2" href="../home?tag=${t.tagid}">#${t.tagname}</a>
                                            </c:forEach>
                                        </div>
                                        <!-- Post Featured Image -->
                                        <img class="featured-image img-fluid" src="data:image/jpg;base64,${requestScope.post.base64Image}" alt="">
                                        <!-- End Featured Image -->
                                        <!-- Post Content -->
                                        <div class="article-post">
                                            <p>${requestScope.post.description}</p>
                                            <div class="clearfix">
                                            </div>
                                        </div>

                                    </c:otherwise>
                                </c:choose>
                            </div>

                            <!-- Post Date -->
                            <%
                                if (account != null) {
                            %>
                            <div class="d-flex flex-row-reverse">
                                <div class="dropdown">

                                    <i class="bi bi-three-dots fa-2x" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></i>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton" >
                                        <a class="dropdown-item" data-toggle="modal" data-target="#default-example-modal-center" href="/ProjectSWP/home?order=desc">Mute</a>
                                        <a class="dropdown-item" href="#">Report This Work</a>
                                    </div>
                                </div>
                                <div class="p-2"><a onclick="setPinColor('${requestScope.post.ap.workid}P')"  title="Pin this work" class="ppin" id="${requestScope.post.ap.workid}P"><i class="bi bi-heart-fill fa-2x"  id="${requestScope.post.ap.workid}P"></i></a></div>
                                <div class="p-2"><a onclick="setLikeColor('${requestScope.post.ap.workid}L')"  title="Like this work" class="plike" id="${requestScope.post.ap.workid}L"><i class="fa fa-thumbs-up fa-2x"  id="${requestScope.post.ap.workid}L"></i></a></div>

                            </div>
                            <%}
                            %>
                            <div class="d-flex flex-row">
                                <div class="p-2"><a title="Like" class="like"><i class="fa fa-thumbs-up"  id="${requestScope.post.ap.workid}Li">     ${requestScope.post.like}</i></a></div>
                                <div class="p-2"><a title="Pin" class="pin"><i class="bi bi-heart-fill"  id="${requestScope.post.ap.workid}Pi">     ${requestScope.post.saved}</i></a></div>
                                <div class="p-2"><a title="View" class="view"><i class="bi bi-eye-fill"  id="${requestScope.post.ap.workid}V">     ${requestScope.post.view}</i></a></div>
                            </div>
                            <p>
                                <small>
                                    <span class="post-date"><time class="post-date" datetime="2018-01-12">${requestScope.post.date}</time></span>
                                </small>
                            </p>
                            
                            <div class="sidebar-section">

                                <div class="modal fade" id="default-example-modal-center" tabindex="-1" role="dialog" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                        <h2></h2>
                                        <div class="modal-content">
                                            <div class="modal-header text-center ">
                                                <h4 class="modal-title w-100" style="text-align: center">
                                                    Mute Settings
                                                </h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true"><i class="fal fa-times"></i></span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    <label>You can hide certain creator's works and tags</label>
                                                </div>
                                                <div class="form-group">
                                                    <div class="d-flex flex-row">
                                                        <p class="p-2">${requestScope.post.ap.a.username}</p>
                                                        <form action="work/mute" method="POST" name="${requestScope.post.ap.a.username}" class="ml-auto p-2">
                                                            <input type="hidden" name="mute" value="${requestScope.post.ap.a.username}">
                                                            <input type="hidden" name="type" value="user">
                                                            <input type="hidden" name="workid" value="${requestScope.post.ap.workid}">
                                                            <c:choose>
                                                                <c:when test="${requestScope.muteduser.muteduser eq requestScope.post.ap.a.username}">
                                                                    <input type="hidden" name="duplicate" value="user">
                                                                    <input type="submit"  style="background-color: green; margin: 0px 0px 16px" value="Mute"> 
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <input type="submit"  style="margin: 0px 0px 16px" value="Mute"> 
                                                                </c:otherwise>
                                                            </c:choose>

                                                        </form>
                                                    </div>
                                                    <c:forEach items="${requestScope.tags}" var="t">
                                                        <div class="d-flex flex-row">
                                                            <p class="p-2">#${t.tagname}</p>
                                                            <form action="work/mute" method="POST" name="${t.tagname}" class="ml-auto p-2">
                                                                <input type="hidden" name="mute" value="${t.tagid}">
                                                                <input type="hidden" name="type" value="tag">
                                                                <input type="hidden" name="workid" value="${requestScope.post.ap.workid}">
                                                                <c:choose>
                                                                    <c:when test="${not empty requestScope.mutedtag}">
                                                                        <c:forEach items="${requestScope.mutedtag}" var="mt">
                                                                            <c:choose>
                                                                                <c:when test="${mt.t.tagid eq t.tagid}">
                                                                                    <input type="hidden" name="duplicate" value="tag">
                                                                                    <input type="submit" style="background-color: green;margin: 0px 0px 16px" value="Mute"> 
                                                                                </c:when>
                                                                                <c:otherwise>
                                                                                    <input type="submit"  style="margin: 0px 0px 16px" value="Mute">
                                                                                </c:otherwise>
                                                                            </c:choose>
                                                                        </c:forEach> 
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                        <input type="submit"  style="margin: 0px 0px 16px" value="Mute">
                                                                    </c:otherwise>
                                                                </c:choose>
                                                            </form>
                                                        </div>
                                                    </c:forEach>
                                                    <c:if test="${empty sessionScope.subcription}">
                                                        <div style="margin:10px;padding:20px;border:1px;border-color: #Fcbc19;border-style:groove">
                                                            <p>Regular users can mute up to 1 element(s) between users and tags.</p>
                                                            <p style="color:#Fcbc19">By becoming a Premium user, you can mute up to 500 elements.</p>
                                                            <Button  class="text-center"style="color:#Fcbc19;border-color: #Fcbc19;margin: 10px 0px 0px; padding: 0px 8px;font-size:16px;text-align:center">Register For BeauCoup Premium</Button>
                                                        </div>
                                                    </c:if>

                                                </div>
                                                </form>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </div>

                            </div>
                            
                            <div class="row post-top-meta">
                                <div class="col-md-2">
                                    <img class="author-thumb" src="data:image/jpg;base64,${requestScope.userpic.ava}"/> 
                                </div>
                                <div class="col-md-10">
                                    <a target="_blank" class="link-dark" href="#" id="authorname">${requestScope.post.ap.a.username}</a><a target="_blank" href="https://twitter.com/wowthemesnet" class="btn follow">Follow</a><br/>
                                    <span class="author-description">Among us</span>
                                </div>
                            </div>
                            <!-- Begin Comments================================================== -->
                            <%
                                ArrayList<Comment> comments = (ArrayList<Comment>) request.getAttribute("comments");
                                String workid = request.getAttribute("workid").toString();
                                Account acc = (Account) request.getSession().getAttribute("account");
                            %>
                            
                            <div class="container mt-5 mb-5">
                                <div class="d-flex justify-content-center row">
                                    <div class="d-flex flex-column col-md-8">
                                        <div class="coment-bottom bg-white p-2 px-4">
                                            <div class="d-flex flex-row add-comment-section mt-4 mb-4">
                                                <%Post post = (Post)request.getAttribute("post"); %>
                                                <%if (acc != null && post.getComments() != 0) {%>
                                                <img class="author-thumb" src="data:image/jpg;base64,${sessionScope.accPic.ava}"   />
                                                <form name="myForm" id="myForm" onsubmit="Comment()">
                                                    <h5 class="mr-2">Comment as <%=acc.getUsername()%></h5>
                                                    <input type="text" name="content" id="content" style="height: 70px; width: 300px;">
                                                    <input type="hidden" name="workid" id="workid" value="<%=workid%>"/>
                                                    <input type="hidden" name="username" id="username" value=<%=acc.getUsername()%>>
                                                    <input type="button" value="Comment" onclick="Comment()"/>
                                                </form>
                                                <script>
                                                    document.getElementById("myForm").onkeypress = function (event) {
                                                        var key = event.charCode || event.keyCode || 0;
                                                        if (key === 13) {
                                                            //alert("No Enter!");
                                                            event.preventDefault();
                                                        }
                                                    };
                                                </script>
                                                
                                                <%}else if(post.getComments() == 0){%>
                                                <h5 class="mr-2">Commenting is not enabled for this post</h5>
                                                <%}else{%>
                                                <h5 class="mr-2">Not signed in? <a href="/ProjectSWP/home/login">Login or Register here</a>!</h5>
                                                <%}%>
                                            </div>                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="container mt-5 mb-5">
                                <div class="d-flex justify-content-center row">
                                    <div class="d-flex flex-column col-md-8">                   
                                        <div class="coment-bottom bg-white p-2 px-4">  
                                            <div id="myComment">
                                                <%for (Comment c : comments) {%>                      
                                                <div class="commented-section mt-2">
                                                    <div class="d-flex flex-row align-items-center commented-user">
                                                        <h5 class="mr-2"><%=c.getAcc().getUsername()%></h5><span class="dot mb-1"></span><span class="mb-1 ml-2">4 hours ago</span>
                                                    </div>
                                                    <div class="comment-text-sm">
                                                        <span><%=c.getContent()%></span>
                                                    </div>
                                                    <div class="reply-section">
                                                        <%if (acc != null) {%>
                                                        <div class="d-flex flex-row align-items-center voting-icons">
                                                            <a onclick="setVoteColor('<%=c.getCid()%>Up', '<%=c.getCid()%>Down')" title="Upvote" class="pin" style="float: right" id="<%=c.getCid()%>Up">
                                                                <i class="fa fa-sort-up fa-2x mt-3 hit-voting"></i></a>
                                                            <a onclick="setVoteColor('<%=c.getCid()%>Down', '<%=c.getCid()%>Up')" title="Downvote" class="pin" style="float: right" id="<%=c.getCid()%>Down">
                                                                <i class="fa fa-sort-down fa-2x mb-3 hit-voting"></i></a>
                                                            <span class="ml-2">10</span><span class="dot ml-2"></span>
                                                            <div id="rep<%=c.getCid()%>" style="display: none">
                                                                <form name="replyForm<%=c.getCid()%>" id="replyForm<%=c.getCid()%>" onsubmit="Reply()">
                                                                    <h5 class="mr-2">Replying <%=c.getAcc().getUsername()%> as <%=acc.getUsername()%></h5>
                                                                    <input type="text" name="repcontent" id="repcontent<%=c.getCid()%>" style="height: 30px; width: 200px;">
                                                                    <input type="hidden" name="btnid" id="btnid<%=c.getCid()%>" value="repbtn<%=c.getCid()%>"/>
                                                                    <input type="hidden" name="repworkid" id="repworkid<%=c.getCid()%>" value="<%=workid%>"/>
                                                                    <input type="hidden" name="repreceiver" id="repreceiver<%=c.getCid()%>" value="<%=c.getAcc().getUsername()%>"/>
                                                                    <input type="hidden" name="cid" id="cid<%=c.getCid()%>" value="<%=c.getCid()%>"/>
                                                                    <input type="hidden" name="repusername" id="repusername<%=c.getCid()%>" value="<%=acc.getUsername()%>"/>       
                                                                    <input type="button" value="Reply" onclick="Reply('repcontent<%=c.getCid()%>',
                                                                                    'repusername<%=c.getCid()%>',
                                                                                    'cid<%=c.getCid()%>',
                                                                                    'repworkid<%=c.getCid()%>',
                                                                                    'btnid<%=c.getCid()%>',
                                                                                    'repusername<%=c.getCid()%>',
                                                                                    'rep<%=c.getCid()%>')"/>
                                                                </form>
                                                                <script>
                                                                    document.getElementById("replyForm<%=c.getCid()%>").onkeypress = function (event) {
                                                                        var key = event.charCode || event.keyCode || 0;
                                                                        if (key === 13) {
                                                                            //alert("No Enter!");
                                                                            event.preventDefault();
                                                                        }
                                                                    };
                                                                </script>
                                                            </div>   
                                                            <input type="button" value="Reply" id="repbtn<%=c.getCid()%>" onclick="ShowTextBox('rep<%=c.getCid()%>', 'repbtn<%=c.getCid()%>')"/>                                                             
                                                        </div>
                                                        <%}%>
                                                        <div id="replies<%=c.getCid()%>">
                                                            <%for (Reply r : c.getReplies()) {%>
                                                            <div style="margin-left: 50px">
                                                                <div class="d-flex flex-row align-items-center commented-user">
                                                                    <h6 class="mr-2"><%=r.getAcc().getUsername()%></h6><span class="dot mb-1"></span><span class="mb-1 ml-2">4 hours ago</span></div>
                                                                <div class="comment-text-sm">
                                                                    <span><%=r.getContent()%></span>
                                                                </div>
                                                            </div>
                                                            <%}%>
                                                        </div>
                                                    </div>                                                  
                                                </div>
                                                <%}%>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!--End Comments ================================================== -->
                            <!-- End Post -->
                        </div>
                        <!-- End Article
            ================================================== -->
                    </div>
                </div>
            </div>
                <!-- /.container -->
                <!-- Begin Footer
            ================================================== -->
                <footer class="footer">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="footer-widget">
                                    <a href="../home/about"> 
                                        <img src="../view/affiliates/assets/images/logo-footer.png" alt="logo footer" style="position: relative; width: 170px; left: 15px; background-size: contain;">
                                    </a>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="footer-widget">
                                    <h5 class="title">Resources</h5>
                                    <ul>
                                        <li><a target="_blank" href="https://m.do.co/c/84c9b45d0c47">Digital Ocean</a></li>
                                        <li><a target="blank" href="https://www.cloudways.com/en/pricing.php?id=153986&amp;a_bid=005da123">Cloudways</a></li>
                                        <li><a target="blank" href="https://shareasale.com/r.cfm?b=875645&amp;u=1087935&amp;m=41388&amp;urllink=&amp;afftrack=">Page Speed Test</a></li>
                                        <li><a target="blank" href="https://elementor.com/?ref=1556">Elementor Page Builder</a></li>
                                        <li><a target="blank" href="https://www.wowthemes.net/category/freebies/">Our Free Themes</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="footer-widget">
                                    <h5 class="title">Author</h5>
                                    <ul>
                                        <li><a href="https://www.wowthemes.net/premium-themes-templates/">About Us</a></li>
                                        <li><a target="_blank" href="https://www.wowthemes.net/affiliate-area/">Affiliates</a></li>
                                        <li><a href="https://www.wowthemes.net/terms-and-conditions/">License</a></li>
                                        <li><a href="https://www.wowthemes.net/blog/">Blog</a></li>
                                        <li><a href="https://www.wowthemes.net/support/">Contact</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="footer-widget textwidget">
                                    <h5 class="title">Download</h5>
                                    <p>
                                        Download "Affiliates" template and use it for your next project. If you have a question, a bug report, or if you simply want to say hi, <a href="https://www.wowthemes.net/support/">contact us here</a>.
                                    </p>
                                    <a href="https://gum.co/affiliates-html-template" target="_blank">Download</a>
                                </div>
                            </div>
                        </div>
                        <div class="copyright">
                            <p class="pull-left">
                                Copyright  2018 Affiliates HTMT Template
                            </p>
                            <p class="pull-right">
                                <!-- Leave credit to author unless you own a commercial license: https://www.wowthemes.net/freebies-license/ -->
                                <a target="_blank" href="https://www.wowthemes.net/affiliates-free-bootstrap-template/">"Affiliates Template"</a> - Design & Code by WowThemes.net
                            </p>
                            <div class="clearfix">
                            </div>
                        </div>
                    </div>
                </footer>
                <!-- End Footer
            ================================================== -->
            </div>
            <!-- JavaScript
            ================================================== -->
            <script type='text/javascript'>(function ($) {
                                            window.fnames = new Array();
                                            window.ftypes = new Array();
                                            fnames[0] = 'EMAIL';
                                            ftypes[0] = 'email';
                                            fnames[3] = 'MMERGE3';
                                            ftypes[3] = 'text';
                                            fnames[1] = 'MMERGE1';
                                            ftypes[1] = 'text';
                                            fnames[2] = 'MMERGE2';
                                            ftypes[2] = 'text';
                                            fnames[4] = 'MMERGE4';
                                            ftypes[4] = 'text';
                                            fnames[5] = 'MMERGE5';
                                            ftypes[5] = 'text';
                                        }(jQuery));
                                        var $mcj = jQuery.noConflict(true);</script>
            <script>
                                var color = "";

                                <c:forEach items="${requestScope.savedPost}" var="s">
                                    <c:if test="${s.ap.workid ==  requestScope.post.ap.workid}">
                                var property = document.getElementById('${requestScope.post.ap.workid}P');
                                color = property.style.color;
                                property.style.color = "red";
                                    </c:if>
                                </c:forEach>

                                <c:forEach items="${requestScope.likedPost}" var="l">
                                    <c:if test="${l.ap.workid == requestScope.post.ap.workid}">
                                var property = document.getElementById('${requestScope.post.ap.workid}L');
                                color = property.style.color;
                                property.style.color = "blue";
                                    </c:if>
                                </c:forEach>
                            </script>


                            <script type="text/javascript">
                                var wsUrlComment;
                                if (window.location.protocol == 'http:') {
                                    wsUrlComment = 'ws://';
                                } else {
                                    wsUrlComment = 'wss://';
                                }
                                var wsComment = new WebSocket(wsUrlComment + window.location.host + "/ProjectSWP/comment");
                                wsComment.onmessage = function (event) {
                                    var data = JSON.parse(event.data);
                                    var commentDisp = document.getElementById("myComment");
                                   
                                    commentDisp.innerHTML = '<div class="commented-section mt-2">'
                                            + '<div class="d-flex flex-row align-items-center commented-user">'
                                            + '<h5 class="mr-2">' + data.username + '</h5>' + '<span class="dot mb-1">' + '</span>' + '<span class="mb-1 ml-2">' + '4 hours ago' + '</span>'
                                            + '</div>'
                                            + '<div class="comment-text-sm">'
                                            + '<span>' + data.content + '</span>'
                                            + '</div>'
                                            + '<div class="reply-section">'
                                            + '<div class="d-flex flex-row align-items-center voting-icons">'
                                            + '<i class="fa fa-sort-up fa-2x mt-3 hit-voting"></i></a>'
                                            + '<i class="fa fa-sort-down fa-2x mb-3 hit-voting"></i></a>'
                                            + '<span class="ml-2">X</span><span class="dot ml-2"></span>'
                                            + '<div id="rep' + data.cid + '" style="display: none">'
                                            + '<form name="replyForm' + data.cid + '" id="replyForm' + data.cid + '" onsubmit="Reply()">'
                                            + '<h5 class="mr-2">Replying ' + data.username + ' as ' + '<%=acc.getUsername()%>' + '</h5>'
                                            + '<input type="text" name="repcontent" id="repcontent' + data.cid + '" style="height: 30px; width: 200px;">'
                                            + '<input type="hidden" name="btnid" id="btnid' + data.cid + '" value="repbtn' + data.cid + '"/>'
                                            + '<input type="hidden" name="repworkid" id="repworkid' + data.cid + '" value="' + data.workid + '"/>'
                                            + '<input type="hidden" name="repreceiver" id="repreceiver' + data.cid + '" value="' + data.username + '"/>'
                                            + '<input type="hidden" name="cid" id="cid' + data.cid + '" value="' + data.cid + '"/>'
                                            + '<input type="hidden" name="repusername" id="repusername' + data.cid + '" value="' + '<%=acc.getUsername()%>' + '"/>'
                                            + '<input type="button" value="Reply" onclick="Reply('+"'repcontent" + data.cid + "',"
                                            +"'repusername" + data.cid + "',"
                                            +"'cid" + data.cid + "',"
                                            +"'repworkid" + data.cid + "',"
                                            +"'btnid" + data.cid + "',"
                                            +"'repusername" + data.cid + "',"
                                            + "'rep" + data.cid + "'" + ')"/>'
                                            + '</form>'
                                            + '<script>'
                                            + 'document.getElementById("replyForm' + data.cid + '").onkeypress = function (event) {'
                                            + 'var key = event.charCode || event.keyCode || 0;'
                                            + 'if (key === 13) {'
                                            + 'event.preventDefault();'
                                            + '}'
                                            + '};'
                                            + '</' + 'script>'
                                            + '</div>'
                                            + '<input type="button" value="Reply" id="repbtn' + data.cid + '" onclick="ShowTextBox(' + "'rep" + data.cid + "'," + "'repbtn" + data.cid + "'" + ');"/>'
                                            + '</div>'
                                            + '<div id="replies' + data.cid + '"></div>'
                                            + '</div>'
                                            + '</div>'
                                            + commentDisp.innerHTML;
                                };

                                wsComment.onerror = function (event) {
                                    console.log("Error ", event);
                                }
                                function uid() {
                                    return (performance.now().toString(36) + Math.random().toString(36)).replace(/\./g, "");
                                }
                                function Comment() {
                                    var xhttp;
                                    var cid = uid();
                                    var workid = document.myForm.workid.value;
                                    var receivername = document.getElementById("authorname").innerHTML;
                                    var username = document.myForm.username.value;
                                    var content = document.myForm.content.value;
                                    var url = "/ProjectSWP/home/work?cid=" + cid + "&workid=" + workid + "&content=" + content + "&username=" + username;
                                    if (window.XMLHttpRequest)
                                    {
                                        xhttp = new XMLHttpRequest();
                                    } else
                                    {
                                        xhttp = new ActiveXObject("Microsoft.XMLHTTP");
                                    }

                                    xhttp.open("POST", url, true);
                                    xhttp.send();
                                    if (username != receivername) {
                                        sendNotif(username, receivername, "commented on your work", workid);
                                    }
                                    var json = {
                                        "cid": cid,
                                        "username": username,
                                        "content": content,
                                        "workid": workid
                                    }
                                    wsComment.send(JSON.stringify(json));
                                    document.getElementById('content').value = '';
                                }
                                var wsUrlReply;
                                if (window.location.protocol == 'http:') {
                                    wsUrlReply = 'ws://';
                                } else {
                                    wsUrlReply = 'wss://';
                                }
                                var wsReply = new WebSocket(wsUrlReply + window.location.host + "/ProjectSWP/reply");
                                wsReply.onmessage = function (event) {
                                    var data = JSON.parse(event.data);
                                    var id = 'replies' + data.cid;
                                    var replyDisp = document.getElementById(id);
                                    replyDisp.innerHTML = '<div style="margin-left: 50px">'
                                            + '<div class="d-flex flex-row align-items-center commented-user">'
                                            + '<h6 class="mr-2">' + data.username + '</h6><span class="dot mb-1"></span><span class="mb-1 ml-2">4 hours ago</span></div>'
                                            + '<div class="comment-text-sm">'
                                            + '<span>' + data.content + '</span>'
                                            + '</div>'
                                            + '</div>'
                                            + replyDisp.innerHTML;
                                };

                                wsReply.onerror = function (event) {
                                    console.log("Error ", event);
                                }
                                function Reply(repcontent, repusername, cid, repworkid, btnid, receiverid, repdiv) {
                                    var xhttp;
                                    var content = document.getElementById(repcontent).value;
                                    var username = document.getElementById(repusername).value;
                                    var receivername = document.getElementById(receiverid).value;
                                    var repcid = document.getElementById(cid).value;
                                    var workid = document.getElementById(repworkid).value;
                                    var btnid = document.getElementById(btnid).value;
                                    var url = "/ProjectSWP/home/reply?workid=" + workid + "&cid=" + repcid + "&content=" + content + "&username=" + username;
  
                                    if (window.XMLHttpRequest)
                                    {
                                        xhttp = new XMLHttpRequest();
                                    } else
                                    {
                                        xhttp = new ActiveXObject("Microsoft.XMLHTTP");
                                    }
                                    xhttp.open("POST", url, true);
                                    xhttp.send();
                                    if (username != receivername) {
                                        sendNotif(username, receivername, "replied to your comment", workid);
                                    }
                                    var json = {
                                        "cid": repcid,
                                        "username": username,
                                        "content": content,
                                        "workid": workid
                                    }

                                    wsReply.send(JSON.stringify(json));
                                    document.getElementById(repdiv).style.display = "none";
                                    document.getElementById(repcontent).value = '';
                                    document.getElementById(btnid).style.display = "block";
                                }
                            </script>
                            <script>
                                var wsUrlNotif;
                                if (window.location.protocol == 'http:') {
                                    wsUrlNotif = 'ws://';
                                } else {
                                    wsUrlNotif = 'wss://';
                                }
                                var wsNotif = new WebSocket(wsUrlNotif + window.location.host + "/ProjectSWP/notifications");
                                function sendNotif(sendername, receivername, content, workid) {
                                    var timesent = Date.now();
                                    var xhttp;
                                    var url = "/ProjectSWP/notifications/send?sendername=" + sendername + "&receivername=" + receivername + "&content=" + content + "&timesent=" + timesent + "&workid=" + workid;
                                    if (window.XMLHttpRequest)
                                    {
                                        xhttp = new XMLHttpRequest();
                                    } else
                                    {
                                        xhttp = new ActiveXObject("Microsoft.XMLHTTP");
                                    }
                                    xhttp.open("POST", url, true);
                                    xhttp.send();
                                    var json = {
                                        "content": content,
                                        "sendername": sendername,
                                        "receivername": receivername,
                                        "timesent": new Date(Date.now()).toJSON(),
                                        "workid": workid
                                    }
                                    wsNotif.send(JSON.stringify(json));
                                }


                            </script>
            <script src="https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.slim.min.js"></script>
            <script src="../view/affiliates/assets/js/jquery.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
            <script src="../view/affiliates/assets/js/ie10-viewport-bug-workaround.js"></script>
            <script src="../view/affiliates/assets/js/masonry.pkgd.min.js"></script>
            <script src="../view/affiliates/assets/js/theme.js"></script>
            <script src="../view/affiliates/assets/js/pageScript.js"></script>
            <script src="../view/affiliates/assets/inteltemplate/js/vendors.bundle.js"></script>
            <script src="../view/affiliates/assets/inteltemplate/js/app.bundle.js"></script>
    </body>
</html>
