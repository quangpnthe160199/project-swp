<%-- 
    Document   : displaycomments
    Created on : Jun 16, 2022, 10:22:00 PM
    Author     : haiph
--%>

<%@page import="model.Reply"%>
<%@page import="model.Account"%>
<%@page import="model.Comment"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Beaucoup | An All-you-can-post page</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Rubik:400,400i,500,500i,700,700i" rel="stylesheet">
        <link href="../view/affiliates/assets/css/theme.css" rel="stylesheet">
        <script src="../view/affiliates/assets/js/pageScript.js"></script>
        <!-- base css -->
        <link rel="stylesheet" media="screen, print" href="../view/affiliates/assets/inteltemplate/css/vendors.bundle.css">
        <link rel="stylesheet" media="screen, print" href="../view/affiliates/assets/inteltemplate/css/app.bundle.css">
        <%
            ArrayList<Comment> comments = (ArrayList<Comment>) request.getAttribute("comments");
            String workid = request.getAttribute("workid").toString();
            Account acc = (Account) request.getSession().getAttribute("account");
        %>
    </head>
    <body>
        <div class="container mt-5 mb-5">
            <div class="d-flex justify-content-center row">
                <div class="d-flex flex-column col-md-8">                   
                    <div class="coment-bottom bg-white p-2 px-4">                     
                        <%for (Comment c : comments) {%>
                        <div class="commented-section mt-2">
                            <div class="d-flex flex-row align-items-center commented-user">
                                <h5 class="mr-2"><%=c.getAcc().getUsername()%></h5><span class="dot mb-1"></span><span class="mb-1 ml-2">4 hours ago</span></div>
                            <div class="comment-text-sm">
                                <span><%=c.getContent()%></span>
                            </div>
                            <div class="reply-section">
                                <%if (acc != null) {%>
                                <div class="d-flex flex-row align-items-center voting-icons">
                                    <a onclick="setVoteColor('<%=c.getCid()%>Up', '<%=c.getCid()%>Down')" title="Upvote" class="pin" style="float: right" id="<%=c.getCid()%>Up">
                                        <i class="fa fa-sort-up fa-2x mt-3 hit-voting"></i></a>
                                    <a onclick="setVoteColor('<%=c.getCid()%>Down', '<%=c.getCid()%>Up')" title="Downvote" class="pin" style="float: right" id="<%=c.getCid()%>Down">
                                        <i class="fa fa-sort-down fa-2x mb-3 hit-voting"></i></a>
                                    <span class="ml-2">10</span><span class="dot ml-2"></span>
                                    <div id="rep<%=c.getCid()%>" style="display: none">
                                       <form name="replyForm<%=c.getCid()%>" id="replyForm<%=c.getCid()%>" onsubmit="Reply()">
                                            <h5 class="mr-2">Replying <%=c.getAcc().getUsername()%> as <%=acc.getUsername()%></h5>
                                            <input type="text" name="repcontent" id="repcontent<%=c.getCid()%>" style="height: 30px; width: 200px;">
                                            <input type="hidden" name="btnid" id="btnid<%=c.getCid()%>" value="repbtn<%=c.getCid()%>"/>
                                            <input type="hidden" name="repworkid" id="repworkid<%=c.getCid()%>" value="<%=workid%>"/>
                                            <input type="hidden" name="cid" id="cid<%=c.getCid()%>" value="<%=c.getCid()%>"/>
                                            <input type="hidden" name="repusername" id="repusername<%=c.getCid()%>" value="<%=acc.getUsername()%>"/>       
                                            <input type="button" value="Reply" onclick="Reply('repcontent<%=c.getCid()%>',
                                                            'repusername<%=c.getCid()%>',
                                                            'cid<%=c.getCid()%>',
                                                            'repworkid<%=c.getCid()%>',
                                                            'btnid<%=c.getCid()%>')"/>
                                        </form>
                                        <script>
                                            document.getElementById("replyForm<%=c.getCid()%>").onkeypress = function (event) {
                                                var key = event.charCode || event.keyCode || 0;
                                                if (key === 13) {
                                                    //alert("No Enter!");
                                                    event.preventDefault();
                                                }
                                            };
                                        </script>
                                    </div>   
                                    <input type="button" value="Reply" id="repbtn<%=c.getCid()%>" onclick="ShowTextBox('rep<%=c.getCid()%>', 'repbtn<%=c.getCid()%>')"/>                                                             
                                </div>
                                <%}%>
                                <%for (Reply r : c.getReplies()) {%>
                                <div style="margin-left: 50px">
                                    <div class="d-flex flex-row align-items-center commented-user">
                                        <h6 class="mr-2"><%=r.getAcc().getUsername()%></h6><span class="dot mb-1"></span><span class="mb-1 ml-2">4 hours ago</span></div>
                                    <div class="comment-text-sm">
                                        <span><%=r.getContent()%></span>
                                    </div>
                                </div>
                                <%}%>
                            </div>                                                  
                        </div>
                        <%}%>
                    </div>
                </div>
            </div>
        </div>
        <script src="../view/affiliates/assets/js/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
        <script src="../view/affiliates/assets/js/ie10-viewport-bug-workaround.js"></script>
        <script src="../view/affiliates/assets/js/masonry.pkgd.min.js"></script>
        <script src="../view/affiliates/assets/js/theme.js"></script>
        <script src="../view/affiliates/assets/js/pageScript.js"></script>
        <script src="../view/affiliates/assets/inteltemplate/js/vendors.bundle.js"></script>
        <script src="../view/affiliates/assets/inteltemplate/js/app.bundle.js"></script>
    </body>
</html>
