/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function formSubmit() {

    $.ajax({
        url: 'localhost:8080/ProjectSWP/home/pinwork',
        data: $("#form1").serialize(),
        success: function (data) {
            $('#result').html(data);
        }
    });
}

function setPinColor(btn) {

    var property = document.getElementById(btn);
    const color = property.style.color;
    if (color === "red") {
        property.style.color = "#8F8F8F";
    } else {
        property.style.color = "red";
    }

}

function setLikeColor(btn) {

    var property = document.getElementById(btn);
    const color = property.style.color;
    if (color === "blue") {
        property.style.color = "#8F8F8F";
    } else {
        property.style.color = "blue";
    }
}

function setFollowColor(btn) {

    var property = document.getElementById(btn);
    const color = property.style.color;
    if (color === "green") {
        property.style.color = "white";
    } else {
        property.style.color = "green";
    }
}
function setReportColor(btn) {

    var property = document.getElementById(btn);
    const color = property.style.color;
    if (color === "brown") {
        property.style.color = "white";
    } else {
        property.style.color = "brown";
    }
}



$(document).ready(function () {
    $('a.pin').on('click', function () {

        var that = $(this);
        url = "home/pinwork";
        method = "POST";
        workid = that.attr("id");
        var property = document.getElementById(workid);
        const color = property.style.color;
        if (color === "red") {
            $.ajax({
                url: url,
                type: method,
                data: {workid: workid, type: "insert"},
                success: function (response) {
                    console.log(response);
                }
            });
            //console.log("added");

        } else {
            $.ajax({
                url: url,
                type: method,
                data: {workid: workid, type: "delete"},
                success: function (response) {
                    console.log(response);
                }
            });
            //console.log("removed");
        }


        return false;
    });
});

$(document).ready(function () {
    $('a.ppin').on('click', function () {

        var that = $(this);
        url = "pinwork";
        method = "POST";
        workid = that.attr("id");
        var property = document.getElementById(workid);
        const color = property.style.color;
        if (color === "red") {
            $.ajax({
                url: url,
                type: method,
                data: {workid: workid, type: "insert"},
                success: function (response) {
                   var row = document.getElementById(workid + "i");
                    row.innerHTML = response;
                    //console.log(response);
                }
            });
            //console.log("added");

        } else {
            $.ajax({
                url: url,
                type: method,
                data: {workid: workid, type: "delete"},
                success: function (response) {
                    var row = document.getElementById(workid + "i");
                    row.innerHTML = response;
                    //console.log(response);
                }
            });
            //console.log("removed");
        }


        return false;
    });
});

$(document).ready(function () {
    $('a.like').on('click', function () {

        var that = $(this);
        url = "home/like";
        method = "POST";
        workid = that.attr("id");
        var property = document.getElementById(workid);
        const color = property.style.color;
        if (color === "blue") {
            $.ajax({
                url: url,
                type: method,
                data: {workid: workid, type: "insert"},
                success: function (response) {
                    var row = document.getElementById(workid + "i");
                    row.innerHTML = response;
                    console.log(response);
                }
            });
            //console.log("added");

        } else {
            $.ajax({
                url: url,
                type: method,
                data: {workid: workid, type: "delete"},
                success: function (response) {
                    var row = document.getElementById(workid + "i");
                    row.innerHTML = response;
                    console.log(response);
                }
            });
            //console.log("removed");
        }


        return false;
    });
});

$(document).ready(function () {
    $('a.flike').on('click', function () {

        var that = $(this);
        url = "like";
        method = "POST";
        workid = that.attr("id");
        var property = document.getElementById(workid);
        const color = property.style.color;
        if (color === "blue") {
            $.ajax({
                url: url,
                type: method,
                data: {pid: workid, type: "insert"},
                success: function (response) {
                    var row = document.getElementById(workid + "i");
                    row.innerHTML = response;
                    console.log(response);
                }
            });
            //console.log("added");

        } else {
            $.ajax({
                url: url,
                type: method,
                data: {pid: workid, type: "delete"},
                success: function (response) {
                    var row = document.getElementById(workid + "i");
                    row.innerHTML = response;
                    console.log(response);
                }
            });
            //console.log("removed");
        }


        return false;
    });
});


$(document).ready(function () {
    $('a.plike').on('click', function () {

        var that = $(this);
        url = "like";
        method = "POST";
        workid = that.attr("id");
        var property = document.getElementById(workid);
        const color = property.style.color;
        if (color === "blue") {
            $.ajax({
                url: url,
                type: method,
                data: {workid: workid, type: "insert"},
                success: function (response) {
                    var row = document.getElementById(workid + "i");
                    row.innerHTML = response;
                    //console.log(response);
                }
            });
           // console.log("added");
           } else {
            $.ajax({
                url: url,
                type: method,
                data: {workid: workid, type: "delete"},
                success: function (response) {
                    var row = document.getElementById(workid + "i");
                    row.innerHTML = response;
                    //console.log(response);
                                    }
            });
            //console.log("removed");
        }


        return false;
    });
});

$(document).ready(function () {
    $('a.follow').on('click', function () {

        var that = $(this);
        url = "follow";
        method = "POST";
        proname = that.attr("id");
        var property = document.getElementById(proname);
        const color = property.style.color;
        if (color === "green") {
            $.ajax({
                url: url,
                type: method,
                data: {proname: proname, type: "insert"},
                success: function (response) {
                   console.log(response);
                }
            });
            //console.log("added");
        } else {
            $.ajax({
                url: url,
                type: method,
                data: {proname: proname, type: "delete"},
                success: function (response) {
                     console.log(response);
                }
            });
            //console.log("removed");
        }


        return false;
    });
});
$(document).ready(function () {
    $('a.report').on('click', function () {

        var that = $(this);
        url = "report";
        method = "POST";
        proname = that.attr("id");
        var property = document.getElementById(proname);
        const color = property.style.color;
        if (color === "brown") {
            $.ajax({
                url: url,
                type: method,
                data: {proname: proname, type: "insert"},
                success: function (response) {
                   console.log(response);
                }
            });
            //console.log("added");
        } else {
            $.ajax({
                url: url,
                type: method,
                data: {proname: proname, type: "delete"},
                success: function (response) {
                     console.log(response);
                }
            });
            //console.log("removed");
        }


        return false;
    });
});

$(document).ready(function () {
    $('a.upvote').on('click', function () {

        var that = $(this);
        url = "home/upvote";
        method = "POST";
        cid = that.attr("cid");
        var property = document.getElementById(workid);
        const color = property.style.color;
        if (color === "blue") {
            $.ajax({
                url: url,
                type: method,
                data: {workid: workid, type: "insert"},
                success: function (response) {
                    var row = document.getElementById(workid + "i");
                    row.innerHTML = response;
                }
            });
            //console.log("added");

        } else {
            $.ajax({
                url: url,
                type: method,
                data: {workid: workid, type: "delete"},
                success: function (response) {
                    var row = document.getElementById(workid + "i");
                    row.innerHTML = response;
                }
            });
            //console.log("removed");
        }


        return false;
    });
});
function loadMore() {
    var amount = document.getElementsByClassName("post").length;
    var that = $(this);
    url = "profile";
    method = "POST";
    PostAmount = amount;
    $.ajax({
        url: url,
        type: method,
        data: {amount: PostAmount},
        success: function (response) {
            console.log(response)
            var row = document.getElementById("content");
            row.innerHTML += response;
        }
    });
    return false;
}


// This part is pure javascript, will make it jquery later
function ShowTextBox(id, btnid) {
    var x = document.getElementById(id);
    var btn = document.getElementById(btnid);
    if (x.style.display === "none") {
        x.style.display = "block";
        btn.style.display = "none";
    } else {
        x.style.display = "none";
        btn.style.display = "block";
    }
}


function setVoteColor(btn, btn2) {

    var property = document.getElementById(btn);
    var property2 = document.getElementById(btn2);
    const color = property.style.color;
    if (color === "red") {
        property.style.color = "#8F8F8F";
        property2.style.display = "block";
    } else {
        property.style.color = "red";
        property2.style.display = "none";
    }

}

function setNotifColor(id) {

    var property = document.getElementById(id);
    const color = property.style.backgroundColor;
    if (color === "lightblue") {
        property.style.backgroundColor = "#8F8F8F";
    } else {
        property.style.backgroundColor = "lightblue";
    }

}
