<%-- 
    Document   : login
    Created on : May 14, 2022, 9:14:23 PM
    Author     : haiph
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="model.Notification"%>
<%@page import="model.Account"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="icon" href="../view/affiliates/assets/images/logo-footer.png">
        <title>Beaucoup | An All-you-can-post page</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Rubik:400,400i,500,500i,700,700i" rel="stylesheet">
        <link href="../view/affiliates/assets/css/theme.css" rel="stylesheet">
        <%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
        <%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
        <!-- base css -->
        <link rel="stylesheet" media="screen, print" href="../view/affiliates/assets/inteltemplate/css/vendors.bundle.css">
        <link rel="stylesheet" media="screen, print" href="../view/affiliates/assets/inteltemplate/css/app.bundle.css">
        <!-- Begin tracking codes here, including ShareThis/Analytics -->

        <!-- End tracking codes here, including ShareThis/Analytics -->
        <style>
            input[type="username"]{
                border-bottom-left-radius: 0px;;
                border-bottom-right-radius: 0px;;
            }

            input[type="password"]{
                border-top-left-radius: 0px;;
                border-top-right-radius: 0px;;
                border-top:0px;;
            }
        </style>
        <script>
  setTimeout(function() {
      document.location = "../home";
  }, 5000); // <-- this is the delay in milliseconds
</script>
    </head>
    <body class="layout-page">
        <!-- Begin Menu Navigation
        ================================================== -->
        <header class="navbar navbar-expand-lg navbar-light bg-light fixed-top mediumnavigation">   
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarsWow" aria-controls="navbarsWow" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="container">
                <!-- Begin Logo -->
                <a class="navbar-brand" href="home">
                    <img src="../view/affiliates/assets/images/logo.png" alt="Affiliates - Free Bootstrap Template" style="position: relative; width: 170px; left: 15px; background-size: contain;">
                </a>
                <!-- End Logo -->
                <!-- Begin Menu -->
                <div class="collapse navbar-collapse" id="navbarsWow">
                    <!-- Begin Menu -->
                    <%
                        Account account = (Account) request.getSession().getAttribute("account");
                    %>
                    <ul class="navbar-nav ml-auto">
                        <%
                            if (account != null) {
                        %>
                        <c:choose>
                            <c:when test="${not empty sessionScope.subcription}">
                                <li class="nav-item">
                                    <a class="nav-link" style="color:#Fcbc19" href="../home/subcription">BeauCoup Blog Premium</a>
                                </li>
                            </c:when>
                            <c:when test="${empty sessionScope.subcription}">
                                <li class="nav-item">
                                    <a class="nav-link" style="color:#Fcbc19" href="../home/subcription">BeauCoup Blog Premium Trial</a>
                                </li>
                            </c:when>
                        </c:choose>


                        <%}%>
                        <li class="nav-item">
                            <a class="nav-link" href="home">Home</a>
                        </li>
                        <c:if test="${not empty account}">
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Publish</a>
                                <div class="dropdown-menu" aria-labelledby="dropdown01">
                                    <a class="dropdown-item" href="../home/upload">Illustration</a>
                                    <a class="dropdown-item" href="../home/upload">Post</a>
                                </div>
                            </li>
                        </c:if>
                        <li class="nav-item">
                            <a class="nav-link" href="../home/contact">Contact</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Tags</a>

                            <div class="dropdown-menu" aria-labelledby="dropdown01">
                                <a class="dropdown-item" href="/ProjectSWP/home/tag?tagid=1" >Action</a>
                                <a class="dropdown-item" href="/ProjectSWP/home/tag?tagid=2">Humor</a>
                                <a class="dropdown-item" href="/ProjectSWP/home/tag?tagid=3">Adventure</a>
                            </div>

                        </li>
                        <%
                            if (account == null) {
                        %>

                        <li class="nav-item">
                            <a class="nav-link highlight" href="../home/login">Login</a>
                        </li>
                        <%} else {%>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Register Advertisement</a>
                            <div class="dropdown-menu" aria-labelledby="dropdown01">
                                <a class="dropdown-item" href="../home/userRegisterAds">Register Advertisement</a>

                            </div>
                        </li>
                        <%ArrayList<Notification> notifications = (ArrayList<Notification>) request.getSession().getAttribute("notifications");%>
                        <li class="nav-item">
                            <a class="nav-link" href="../home/messages"><i class="fa fa-envelope" style="font-size:24px"></i></a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bell" style="font-size:24px"></i></a>

                            <div class="dropdown-menu" aria-labelledby="dropdown01" style="background-color: lightblue" >
                                <%for (Notification notif : notifications) {%>
                                <div style="background-color: lightblue; border:solid 1px" id="notif<%=notif.getNid()%>" onclick="setNotifColor()">
                                    <img class="img-fluid img-responsive rounded-circle mr-2" src="https://wompampsupport.azureedge.net/fetchimage?siteId=7575&v=2&jpgQuality=100&width=700&url=https%3A%2F%2Fi.kym-cdn.com%2Fphotos%2Fimages%2Fnewsfeed%2F002%2F111%2F316%2Fc57.gif" width="38">
                                    <a class="dropdown-item" href="/ProjectSWP/home/work?workid=<%=notif.getAp().getWorkid()%>"><%=notif.getSenderAcc().getUsername()%> <%=notif.getContent()%> at <%=notif.getTimesent().getHours()%> : <%=notif.getTimesent().getMinutes()%> | <%=notif.getTimesent().getDate()%>/<%=notif.getTimesent().getMonth() + 1%>/<%=notif.getTimesent().getYear() + 1900%></a> 
                                </div>
                                <%}%>
                                <a class="dropdown-item" href="/ProjectSWP/notifications" style="text-align: center"> View all notifications </a>
                            </div>

                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              
                                <img class="author-thumb" src="data:image/jpg;base64,${sessionScope.accPic.ava}"   />
                                <c:if test="${not empty sessionScope.subcription}">
                                    <img src="../view/affiliates/assets/images/logo-footer.png" style="width: 50px;length: 50px; color: #Fcbc19" title="Your Subcription Last Until ${sessionScope.subcription.todate}">
                                </c:if>

                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                <a class="dropdown-item" href="../home/profile">View Profile</a>
                                <a class="dropdown-item" href="../home/pin">Pinned Work</a>
                                <a class="dropdown-item" href ="../home/delete">My Work</a> 
                                <a class="dropdown-item" href="../mute">Muted Work</a>
                                <a class="dropdown-item" href="../history">View History</a>
                                <a class="dropdown-item" href="../home/logout">Log Out</a>
                            </div>
                        </li>
                        <%}%>
                    </ul>
                    <!-- End Menu -->
                </div>
            </div>
        </header>
        <!-- End Menu Navigation
        ================================================== -->
        <div class="site-content">
            <div class="container">
                <!-- Content (replace with your e-mail address below)
    ================================================== -->
                <div class="main-content">
                    <section>
                        <div class="section-title" style="text-align: center">
                            <h2>
                                You are banned for seriously reasons .<br/>
                                Please contact the Administrator for more details.
                            </h2>
                              
                        </div>
                        <div class="article-post">
                            <div class="text-center mt-5">
                                
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            <!-- /.container -->
            <!-- Begin Footer
        ================================================== -->
            <footer class="footer">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="footer-widget">
                                <a href="../home/about"> 
                                    <img src="../view/affiliates/assets/images/logo-footer.png" alt="logo footer" style="position: relative; width: 170px; left: 15px; background-size: contain;">
                                </a>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="footer-widget">
                                <h5 class="title">Author</h5>
                                <ul>
                                    <li><a href="#">About Us</a></li>
                                    <li><a target="_blank" href="https://www.wowthemes.net/affiliate-area/">Affiliates</a></li>
                                    <li><a href="https://www.wowthemes.net/terms-and-conditions/">License</a></li>
                                    <li><a href="https://www.wowthemes.net/blog/">Blog</a></li>
                                    <li><a href="contact.jsp">Contact</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="footer-widget textwidget">
                                <h5 class="title">Email us</h5>
                                <form action="email" method="POST">
                                    <input type="hidden" name="page" value="home">
                                    <input type="text" name="subject" placeholder="Subject">
                                    <input type="email" name="email" placeholder="E-mail Address You Want To Get Reply">
                                    <textarea rows="8" name="content" placeholder="Message"></textarea><br/>
                                    <input class="btn btn-success" type="submit" value="Send">
                                    ${requestScope.message}
                                </form>                             
                            </div>
                        </div>

                    </div>
                    <div class="copyright">
                        <p class="pull-left">
                            Copyright © 2018 Affiliates HTML Template
                        </p>
                        <p class="pull-right">
                            <!-- Leave credit to author unless you own a commercial license: https://www.wowthemes.net/freebies-license/ -->
                            <a target="_blank" href="https://www.wowthemes.net/affiliates-free-bootstrap-template/">"Affiliates Template"</a> - Design & Code by WowThemes.net
                        </p>
                        <div class="clearfix">
                        </div>
                    </div>
                </div>
            </footer>
            <!-- End Footer
        ================================================== -->
        </div>
        <!-- JavaScript
        ================================================== -->
        <script src="../view/affiliates/assets/js/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
        <script src="assets/js/ie10-viewport-bug-workaround.js"></script>
        <script src="../view/affiliates/assets/js/masonry.pkgd.min.js"></script>
        <script src="../view/affiliates/assets/js/theme.js"></script>
        <script src="../view/affiliates/assets/inteltemplate/js/vendors.bundle.js"></script>
        <script src="../view/affiliates/assets/inteltemplate/js/app.bundle.js"></script>
    </body>
</html>
