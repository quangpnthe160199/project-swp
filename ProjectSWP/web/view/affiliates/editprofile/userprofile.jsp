<%-- 
    Document   : editprofile
    Created on : May 18, 2022, 11:49:48 AM
    Author     : admin
--%>

<%@page import="dal.AccountDBContext"%>
<%@page import="model.AccountDetail"%>
<%@page import="model.Account"%>
<%@page import="model.Post"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="icon" href="assets/images/favicon.ico">
        <title>Beaucoup | Edit Profile</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Rubik:400,400i,500,500i,700,700i" rel="stylesheet">
        <link href="../view/affiliates/assets/css/theme.css" rel="stylesheet">
        <link href="../view/Test JSP/test.css" rel="stylesheet">
        <%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
        <script src="../view/affiliates/assets/js/profilepaging.js" type="text/javascript"></script>
        <link href="../view/affiliates/assets/css/paging.css" rel="stylesheet" type="text/css"/>
        <!-- Begin tracking codes here, including ShareThis/Analytics -->

        <!-- End tracking codes here, including ShareThis/Analytics -->
        <style>
            input[type="username"]{
                border-bottom-left-radius: 0px;;
                border-bottom-right-radius: 0px;;
            }

            input[type="password"]{
                border-top-left-radius: 0px;;
                border-top-right-radius: 0px;;
            }
        </style>
    </head>
    <body class="layout-page">
        <!-- Begin Menu Navigation
        ================================================== -->
        <header class="navbar navbar-toggleable-md navbar-light bg-white fixed-top mediumnavigation">
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarsWow" aria-controls="navbarsWow" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="container">
                <!-- Begin Logo -->
                <a class="navbar-brand" href="../home">
                    <img src="../view/affiliates/assets/images/logo.png" alt="Affiliates - Free Bootstrap Template" style="position: relative; width: 170px; left: 15px; background-size: contain;">
                </a>
                <!-- End Logo -->
                <!-- Begin Menu -->
                <div class="collapse navbar-collapse" id="navbarsWow">
                    <!-- Begin Menu -->
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="../home">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="../home/about">About</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="../home/contact">Contact</a>
                        </li>
                        <%
                            Account account = (Account) request.getSession().getAttribute("account");
                            if (account == null) {
                        %>
                        <li class="nav-item">
                            <a class="nav-link highlight" href="home/login">Login</a>
                        </li>
                        <%} else {%>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                               
                                    <img class="author-thumb" src="data:image/jpg;base64,${sessionScope.accPic.ava}"   />
                                 
                                                 
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                <a class="dropdown-item" href="../home">Dashboard</a>
                                <a class="dropdown-item" href ="../home/delete">Delete Work</a>
                                <a class="dropdown-item" href="../home/edit">Edit Profile</a>
                                <a class="dropdown-item" href="../home/logout">Log Out</a>
                            </div>
                        </li>
                        <%}%>
                    </ul>
                    <!-- End Menu -->
                </div>
            </div>
        </header>
        <!-- End Menu Navigation
        ================================================== -->
        <div class="site-content">
            <div class="container">
                <!-- Content (replace with your e-mail address below)
    ================================================== -->
                <div class="main-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="content" class="content content-full-width">
                                <!-- begin profile -->
                                <div class="profile">
                                    <div class="profile-header">
                                        <!-- BEGIN profile-header-cover -->
                                        <div class="profile-header-cover">
                                            <c:if test = "${acc.background != null  }">
                                                <img class="img-fluid" src="data:image/jpg;base64,${acc.background}" style="width:200% " /> 
                                            </c:if>


                                        </div>
                                        <!-- END profile-header-cover -->
                                        <!-- BEGIN profile-header-content -->
                                        <div class="profile-header-content">
                                            <!-- BEGIN profile-header-img -->
                                            <div class="profile-header-img">
                                                <c:if test = "${acc.ava != null  }">
                                                    <img class="img-fluid" src="data:image/jpg;base64,${acc.ava}" style="width:100%;height:100%" /> 
                                                </c:if>
                                                <img class="img-fluid" src="https://180dc.org/wp-content/uploads/2022/04/Blank-Avatar.png" style="width:100%;height:90%" /> 
                                            </div>
                                            <!-- END profile-header-img -->
                                            <!-- BEGIN profile-header-info -->
                                            <div class="profile-header-info">
                                                <h4 class="m-t-10 m-b-5">${sessionScope.account.username}</h4>
                                                <p class="m-b-10">UXUI + Frontend Developer</p>
                                                <a href="edit" class="btn btn-sm btn-info mb-2">Edit Profile</a>
                                                <a href ="editPic" class="btn btn-sm btn-info mb-2" >  Edit Profile Picture 
                                                    <img src="https://icon-library.com/images/edit-profile-icon/edit-profile-icon-15.jpg" alt="Change Profile Picture" width=20" height="10" />
                                                </a>

                                                <%
                                                    AccountDBContext db = new AccountDBContext();
                                                    if (!db.haveFanbox(account.getUsername())) {
                                                %>
                                                <a href="fanbox" class="btn btn-sm btn-info mb-2">Create Fanbox</a>
                                                <%} else {%>
                                                <a href="viewfanbox" class="btn btn-sm btn-info mb-2">View Your Fanbox</a>                       
                                                <%}%>
                                            </div>
                                            <!-- END profile-header-info -->
                                        </div>
                                        <!-- END profile-header-content -->
                                        <!-- BEGIN profile-header-tab -->
                                        <ul class="profile-header-tab nav nav-tabs">
                                            <li class="nav-item"><a href="#profile-post" class="nav-link active show" data-toggle="tab">POSTS</a></li>
                                            <li class="nav-item"><a href="#profile-about" class="nav-link" data-toggle="tab">ABOUT</a></li>
                                        </ul>
                                        <!-- END profile-header-tab -->
                                    </div>
                                </div>
                                <!-- end profile -->
                                <!-- begin profile-content -->
                                <div class="profile-content" >
                                    <!-- begin tab-content -->
                                    <div class="tab-content p-0">
                                        <!-- begin #profile-post tab -->
                                        <div class="tab-pane fade active show" id="profile-post">
                                            <!-- begin timeline -->
                                            <%
                                                Post p = (Post) request.getSession().getAttribute("post");
                                            %>
                                            <c:set var="date1" value="" />
                                            <ul class="timeline">
                                                <li class="post" id="content"
                                                    <c:forEach items="${requestScope.post}" var="o">
                                                        <c:set var="date2" value="${o.date}" />                         

                                                        <!-- begin timeline-time -->
                                                        <c:if test="${date1 != date2}">
                                                            <div class="timeline-time">
                                                                <span class="date">${o.date}</span>
                                                                <!--                              <span class="time">04:20</span>-->
                                                            </div
                                                        </c:if>
                                                        <!-- end timeline-time -->
                                                        <!-- begin timeline-icon -->
                                                        <div class="timeline-icon">
                                                            <a href="../home/work?id=${o.ap.workid}">&nbsp;</a>
                                                        </div>
                                                        <!-- end timeline-icon -->
                                                        <!-- begin timeline-body -->

                                                        <div class="timeline-body">
                                                            <div class="timeline-header">
                                                                <span class="userimage"><img src="https://bootdey.com/img/Content/avatar/avatar3.png" alt=""></span>

                                                                <span class="username"><a href="javascript:;">${sessionScope.account.username}</a> <small></small></span>
                                                                <span><a href="../home/work?id=${o.ap.workid}"><img class="img-fluid" src="data:image/jpg;base64,${o.base64Image}" style="height: 250px; width: 500px"></a></span>
                                                                <!--                                 <span class="pull-right text-muted">18 Views</span>-->
                                                            </div>
                                                            <div class="timeline-content">
                                                                <span class="post-name"><a target="_blank" href="../home/work?id=${o.ap.workid}">${o.name}</a></span><br/>
                                                                    <c:set var = "desc" value = "${o.description}"/>
                                                                    <c:if test="${fn:length(desc) < 70 }">
                                                                    <p class="card-text">${o.description}</p>
                                                                </c:if>
                                                                <c:if test="${fn:length(desc) > 250 }">
                                                                    <c:set var = "shortdesc" value = "${fn:substring(desc, 0, 249)}"/>
                                                                    <p class="card-text">${shortdesc}...</p>
                                                                </c:if>
                                                            </div>
                                                            <!--                              <div class="timeline-likes">
                                                                                             <div class="stats-right">
                                                                                                <span class="stats-text">259 Shares</span>
                                                                                                <span class="stats-text">21 Comments</span>
                                                                                             </div>
                                                                                             <div class="stats">
                                                                                                <span class="fa-stack fa-fw stats-icon">
                                                                                                <i class="fa fa-circle fa-stack-2x text-danger"></i>
                                                                                                <i class="fa fa-heart fa-stack-1x fa-inverse t-plus-1"></i>
                                                                                                </span>
                                                                                                <span class="fa-stack fa-fw stats-icon">
                                                                                                <i class="fa fa-circle fa-stack-2x text-primary"></i>
                                                                                                <i class="fa fa-thumbs-up fa-stack-1x fa-inverse"></i>
                                                                                                </span>
                                                                                                <span class="stats-total">4.3k</span>
                                                                                             </div>
                                                                                          </div>-->
                                                            <!--                              <div class="timeline-footer">
                                                                                             <a href="javascript:;" class="m-r-15 text-inverse-lighter"><i class="fa fa-thumbs-up fa-fw fa-lg m-r-3"></i> Like</a>
                                                                                             <a href="javascript:;" class="m-r-15 text-inverse-lighter"><i class="fa fa-comments fa-fw fa-lg m-r-3"></i> Comment</a> 
                                                                                             <a href="javascript:;" class="m-r-15 text-inverse-lighter"><i class="fa fa-share fa-fw fa-lg m-r-3"></i> Share</a>
                                                                                          </div>-->
                                                            <!--                              <div class="timeline-comment-box">
                                                                                             <div class="user"><img src="https://bootdey.com/img/Content/avatar/avatar3.png"></div>
                                                                                             <div class="input">
                                                                                                <form action="">
                                                                                                   <div class="input-group">
                                                                                                      <input type="text" class="form-control rounded-corner" placeholder="Write a comment...">
                                                                                                      <span class="input-group-btn p-l-10">
                                                                                                      <button class="btn btn-primary f-s-12 rounded-corner" type="button">Comment</button>
                                                                                                      </span>
                                                                                                   </div>
                                                                                                </form>
                                                                                             </div>
                                                                                          </div>-->
                                                        </div>
                                                        <c:set var="date1" value="${o.date}" />
                                                    </c:forEach>
                                                    <!-- end timeline-body -->
                                                </li>



                                                <li>
                                                    <!-- begin timeline-icon -->
                                                    <div class="timeline-icon">
                                                        <a href="javascript:;">&nbsp;</a>
                                                    </div>
                                                    <!-- end timeline-icon -->
                                                    <!-- begin timeline-body -->
                                                    <div class="timeline-body">
                                                        <div class="bottompagination">
                                                            <div class="navigation">
                                                                <nav class="pagination">
                                                                    <div id="container" class="paging"> </div>
                                                                    <% Integer totalPage = (Integer) request.getAttribute("onoOfPages");
                                                                        Integer pageIndex = (Integer) request.getAttribute("pageIndex");
                                                                    %>
                                                                    <script>
                                                                        paging("container",<%=pageIndex%>,<%=totalPage%>, 1);
                                                                    </script>
                                                                    <span class="page-number"> &nbsp; &nbsp; Page <%=pageIndex%> of <%=totalPage%> &nbsp; &nbsp; </span>
                                                                </nav>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- begin timeline-body -->
                                                </li>
                                            </ul>
                                            <!-- end timeline -->
                                        </div>
                                        <!-- end #profile-post tab -->
                                    </div>
                                    <!-- end tab-content -->
                                </div>
                                <!-- end profile-content -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.container -->
            <!-- Begin Footer
        ================================================== -->
            <footer class="footer">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="footer-widget">
                                <a href="contact.jsp">
                                    <img src="assets/images/logo-footer.png" alt="logo footer">
                                </a>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="footer-widget">
                                <h5 class="title">Author</h5>
                                <ul>
                                    <li><a href="#">About Us</a></li>
                                    <li><a target="_blank" href="https://www.wowthemes.net/affiliate-area/">Beaucoup</a></li>
                                    <li><a href="https://www.wowthemes.net/terms-and-conditions/">License</a></li>
                                    <li><a href="https://www.wowthemes.net/blog/">Blog</a></li>
                                    <li><a href="contact.jsp">Contact</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="footer-widget textwidget">
                                <h5 class="title">Email us</h5>
                                <form action="https://formspree.io/wowthemesnet@gmail.com" method="POST">
                                    <input type="text" name="name" placeholder="Name">
                                    <input type="email" name="_replyto" placeholder="E-mail Address">
                                    <textarea rows="8" name="message" placeholder="Message"></textarea><br/>
                                    <input class="btn btn-success" type="submit" value="Send">
                                </form>                             
                            </div>
                        </div>
                    </div>
                    <div class="copyright">
                        <p class="pull-left">
                            Copyright © 2018 Affiliates HTML Template
                        </p>
                        <p class="pull-right">
                            <!-- Leave credit to author unless you own a commercial license: https://www.wowthemes.net/freebies-license/ -->
                            <a target="_blank" href="https://www.wowthemes.net/affiliates-free-bootstrap-template/">"Affiliates Template"</a> - Design & Code by WowThemes.net
                        </p>
                        <div class="clearfix">
                        </div>
                    </div>
                </div>
            </footer>
            <!-- End Footer
        ================================================== -->
        </div>
        <!-- JavaScript
        ================================================== -->
        <script src="../view/affiliates/assets/js/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
        <script src="../view/affiliates/assets/js/ie10-viewport-bug-workaround.js"></script>
        <script src="../view/affiliates/assets/js/masonry.pkgd.min.js"></script>
        <script src="../view/affiliates/assets/js/theme.js"></script>
        <script src="../view/affiliates/assets/js/pageScript.js"></script>

    </body>
</html>
