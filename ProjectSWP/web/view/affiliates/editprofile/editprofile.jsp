<%-- 
    Document   : editprofile
    Created on : Jun 5, 2022, 12:20:22 PM
    Author     : admin
--%>

<%@page import="model.Account"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="icon" href="assets/images/favicon.ico">
        <title>Beaucoup | Edit Profile</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Rubik:400,400i,500,500i,700,700i" rel="stylesheet">
        <link href="../view/affiliates/assets/css/theme.css" rel="stylesheet">
        <!-- Begin tracking codes here, including ShareThis/Analytics -->
        
        <!-- End tracking codes here, including ShareThis/Analytics -->
         <style>
            input[type="username"]{
                border-bottom-left-radius: 0px;;
                border-bottom-right-radius: 0px;;
            }
            
            input[type="password"]{
                border-top-left-radius: 0px;;
                border-top-right-radius: 0px;;
            }
        </style>
    </head>
    <body class="layout-page">
        <!-- Begin Menu Navigation
        ================================================== -->
        <header class="navbar navbar-toggleable-md navbar-light bg-white fixed-top mediumnavigation">
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarsWow" aria-controls="navbarsWow" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="container">
                <!-- Begin Logo -->
                <a class="navbar-brand" href="../home">
                    <img src="../view/affiliates/assets/images/logo.png" alt="Affiliates - Free Bootstrap Template" style="position: relative; width: 170px; left: 15px; background-size: contain;">
                </a>
                <!-- End Logo -->
                <!-- Begin Menu -->
                <div class="collapse navbar-collapse" id="navbarsWow">
                    <!-- Begin Menu -->
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="../home">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="../home/about">About</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="../home/contact">Contact</a>
                        </li>
                        <%
                            Account account = (Account) request.getSession().getAttribute("account");
                            if (account == null) {
                        %>
                        <li class="nav-item">
                            <a class="nav-link highlight" href="home/login">Login</a>
                        </li>
                        <%} else {%>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img class="author-thumb" src="data:image/jpg;base64,${sessionScope.accPic.ava}"   />
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                <a class="dropdown-item" href="../home">Dashboard</a>
                                <a class="dropdown-item" href ="../home/delete">Delete Work</a>
                                <a class="dropdown-item" href="../home/edit">Edit Profile</a>
                                <a class="dropdown-item" href="../home/logout">Log Out</a>
                            </div>
                        </li>
                        <%}%>
                    </ul>
                    <!-- End Menu -->
                </div>
            </div>
        </header>
        <!-- End Menu Navigation
        ================================================== -->
        <div class="site-content">
            <div class="container">
                <!-- Content (replace with your e-mail address below)
    ================================================== -->
                <div class="main-content">
                    <section>
                        <div class="section-title" style="text-align: center">
                            <h2><span>Edit profile</span></h2>
                        </div>
                            <h5 class="mr-2">Want to change display name unlimited time? <a href="/ProjectSWP/home/subcription">Become Premium Today</a>!</h5>
                        <div class="article-post">
                            <div class="text-center mt-5">
                                <form style="max-width:800px;margin:auto;"action="edit" method="POST" id="frmSearch" class="ajax">
                                    <img class="mt-4 mb-4" src="">
                                    <table>
                                        <tr>
                                            <td>Your Username: </td>
                                            <td>${requestScope.username}
                                                <label for="username" class='sr-only'>Username</label>
                                                <input type="hidden" class='form-control' placeholder='Username' name="username" value="${requestScope.username}" required autofocus></td>
                                            <td>Your Password: </td>
                                            <td><label for="password" class='sr-only'>Password</label>
                                                <input type="password" class='form-control' placeholder='Password' name="password" value="${requestScope.password}" required autofocus></td>
                                        </tr>

                                        <tr>
                                            <td>Your Display Name:  </td>
                                            <td> ${requestScope.displayname} <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-lock-fill" viewBox="0 0 16 16">
  <path d="M8 1a2 2 0 0 1 2 2v4H6V3a2 2 0 0 1 2-2zm3 6V3a3 3 0 0 0-6 0v4a2 2 0 0 0-2 2v5a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V9a2 2 0 0 0-2-2z"/>
</svg></td>
                                           
                                            <td>Your Full Name: </td>
                                            <td><label for="fullname" class='sr-only'>Full Name</label>
                                                <input type="text" class='form-control' placeholder='Full Name' name="fullname" value="${requestScope.fullname}" required autofocus></td>
                                        </tr>
                                        <tr>
                                            <td>Gender: </td>
                                            <td> 
                                                <c:choose>
                                                    <c:when test="${requestScope.gender == '1'}">
                                                        <input checked type="radio" name="gender" value="1" />Male
                                                        <input type="radio" name="gender" value='0'/>Female<br/>
                                                    </c:when>
                                                    <c:when test="${requestScope.gender == '0'}">
                                                        <input type="radio" name="gender" value="1" />Male
                                                        <input checked type="radio" name="gender" value='0'/>Female<br/>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <input type="radio" name="gender" value="1" />Male
                                                        <input type="radio" name="gender" value='0'/>Female<br/>
                                                    </c:otherwise>
                                                </c:choose>
                                            </td>
                                            <td>Your Mobile: </td>
                                            <td><label for="mobile" class='sr-only'>Mobile</label>
                                                <input type="text" class='form-control' placeholder='Mobile' name="mobile" value="${requestScope.mobile}"required autofocus></td>
                                        </tr>

                                        <tr>
                                            <td>Your Email: </td>
                                            <td><label for="email" class='sr-only'>Email</label>
                                                <input type="text" class='form-control' placeholder='Email' name="email" value="${requestScope.email}" required autofocus></td>
                                            <td>Your Address: </td>
                                            <td><label for="address" class='sr-only'>Address</label>
                                                <input type="text" class='form-control' placeholder='Address' name="address" value="${requestScope.address}" required autofocus></td>
                                        </tr>
                                    </table>
                                    
                                    <div class='mt-1'>
                                        <input class='btn-lg btn-primary btn-block' type="submit" value="Edit" />
                                    </div>
                                </form>
                            </div>
                        </div>

                    </section>
                </div>
            </div>
            <!-- /.container -->
            <!-- Begin Footer
        ================================================== -->
            <footer class="footer">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="footer-widget">
                                <a href="contact.jsp">
                                    <img src="assets/images/logo-footer.png" alt="logo footer">
                                </a>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="footer-widget">
                                <h5 class="title">Author</h5>
                                <ul>
                                    <li><a href="#">About Us</a></li>
                                    <li><a target="_blank" href="https://www.wowthemes.net/affiliate-area/">Beaucoup</a></li>
                                    <li><a href="https://www.wowthemes.net/terms-and-conditions/">License</a></li>
                                    <li><a href="https://www.wowthemes.net/blog/">Blog</a></li>
                                    <li><a href="contact.jsp">Contact</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="footer-widget textwidget">
                                <h5 class="title">Email us</h5>
                                <form action="https://formspree.io/wowthemesnet@gmail.com" method="POST">
                                    <input type="text" name="name" placeholder="Name">
                                    <input type="email" name="_replyto" placeholder="E-mail Address">
                                    <textarea rows="8" name="message" placeholder="Message"></textarea><br/>
                                    <input class="btn btn-success" type="submit" value="Send">
                                </form>                             
                            </div>
                        </div>
                    </div>
                    <div class="copyright">
                        <p class="pull-left">
                            Copyright © 2018 Affiliates HTML Template
                        </p>
                        <p class="pull-right">
                            <!-- Leave credit to author unless you own a commercial license: https://www.wowthemes.net/freebies-license/ -->
                            <a target="_blank" href="https://www.wowthemes.net/affiliates-free-bootstrap-template/">"Affiliates Template"</a> - Design & Code by WowThemes.net
                        </p>
                        <div class="clearfix">
                        </div>
                    </div>
                </div>
            </footer>
            <!-- End Footer
        ================================================== -->
        </div>
        <!-- JavaScript
        ================================================== -->
        <script src="../view/affiliates/assets/js/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
        <script src="../view/affiliates/assets/js/ie10-viewport-bug-workaround.js"></script>
        <script src="../view/affiliates/assets/js/masonry.pkgd.min.js"></script>
        <script src="../view/affiliates/assets/js/theme.js"></script>
    </body>
</html>




