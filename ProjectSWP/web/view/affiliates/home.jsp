<%-- 
    Document   : home
    Created on : May 13, 2022, 3:15:39 PM
    Author     : haiph
--%>


<%@page import="model.Tag"%>
<%@page import="dal.AccountDBContext"%>
<%@page import="model.AccountDetail"%>
<%@page import="model.Notification"%>

<%@page import="dal.PostDBContext"%>
<%@page import="model.PostTag"%>

<%@page import="java.util.ArrayList"%>
<%@page import="model.Account"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="icon" href="view/affiliates/assets/images/logo-footer.png">
        <title>Beaucoup | An All-you-can-post page</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Rubik:400,400i,500,500i,700,700i" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Rubik:400,400i,500,500i,700,700i" rel="stylesheet">
        <%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
        <%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
        <%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.3/font/bootstrap-icons.css">
        <link href="view/affiliates/assets/css/theme.css" rel="stylesheet">
        <script src="view/affiliates/assets/js/homepaging.js" type="text/javascript"></script>
        <link href="view/affiliates/assets/css/paging.css" rel="stylesheet" type="text/css"/>
        <!-- base css -->
        <link rel="stylesheet" media="screen, print" href="view/affiliates/assets/inteltemplate/css/vendors.bundle.css">
        <link rel="stylesheet" media="screen, print" href="view/affiliates/assets/inteltemplate/css/app.bundle.css">
        <link rel="mask-icon" href="img/favicon/safari-pinned-tab.svg" color="#5bbad5">
    </head>
    <body class="layout-default">
        <!-- Begin Menu Navigation
        ================================================== -->
        <header class="navbar navbar-expand-lg navbar-light bg-light fixed-top mediumnavigation">   
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarsWow" aria-controls="navbarsWow" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="container">
                <!-- Begin Logo -->
                <a class="navbar-brand" href="home">
                    <img src="view/affiliates/assets/images/logo.png" alt="Affiliates - Free Bootstrap Template" style="position: relative; width: 170px; left: 15px; background-size: contain;">
                </a>
                <!-- End Logo -->
                <!-- Begin Menu -->
                <div class="collapse navbar-collapse" id="navbarsWow">
                    <!-- Begin Menu -->
                    <%
                        ArrayList<Tag> tags = (ArrayList<Tag>) request.getAttribute("tags");
                        Account account = (Account) request.getSession().getAttribute("account");
                    %>
                    <ul class="navbar-nav ml-auto">
                        <%
                            if (account != null) {
                        %>
                        <c:choose>
                            <c:when test="${not empty sessionScope.subcription}">
                                <li class="nav-item">
                                    <a class="nav-link" style="color:#Fcbc19" href="home/subcription">BeauCoup Blog Premium</a>
                                </li>
                            </c:when>
                            <c:when test="${empty sessionScope.subcription}">
                                <li class="nav-item">
                                    <a class="nav-link" style="color:#Fcbc19" href="home/subcription">BeauCoup Blog Premium Trial</a>
                                </li>
                            </c:when>
                        </c:choose>


                        <%}%>
                        <li class="nav-item">
                            <a class="nav-link" href="home">Home</a>
                        </li>
                        <c:if test="${not empty account}">
                            <li class="nav-item dropdown">
                                <a class="dropdown-item" href="home/upload">Publish</a>
                            </li>
                        </c:if>
                        <li class="nav-item">
                            <a class="nav-link" href="home/contact">Contact</a>
                        </li>
                        <!--                        <li class="nav-item dropdown">
                                                    <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Tags</a>
                        
                                                    <div class="dropdown-menu" aria-labelledby="dropdown01">
                                                        <a class="dropdown-item" href="/ProjectSWP/home/tag?tagid=1" >Action</a>
                                                        <a class="dropdown-item" href="/ProjectSWP/home/tag?tagid=2">Humor</a>
                                                        <a class="dropdown-item" href="/ProjectSWP/home/tag?tagid=3">Adventure</a>
                                                    </div>
                        
                                                </li>-->
                        <%
                            if (account == null) {
                        %>

                        <li class="nav-item">
                            <a class="nav-link highlight" href="home/login">Login</a>
                        </li>
                        <%} else {%>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Register Advertisement</a>
                            <div class="dropdown-menu" aria-labelledby="dropdown01">
                                <a class="dropdown-item" href="home/userRegisterAds">Register Advertisement</a>

                            </div>
                        </li>
                        <%ArrayList<Notification> notifications = (ArrayList<Notification>) request.getSession().getAttribute("notifications");%>
                        <li class="nav-item">
                            <a class="nav-link" href="home/messages"><i class="fa fa-envelope" style="font-size:24px"></i></a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bell" style="font-size:24px"></i></a>

                            <div class="dropdown-menu" aria-labelledby="dropdown01" style="background-color: lightblue">
                                <script type="text/javascript">
                                    var wsUrl;
                                    if (window.location.protocol == 'http:') {
                                        wsUrl = 'ws://';
                                    } else {
                                        wsUrl = 'wss://';
                                    }
                                    var ws = new WebSocket(wsUrl + window.location.host + "/ProjectSWP/notifications");
                                    ws.onmessage = function (event) {
                                        var data = JSON.parse(event.data);
                                        var notifDisp = document.getElementById("notifDisp");
                                        //alert(data.sendername + " and " + data.receivername + " and " + data.content + " and " + data.timesent)
                                        notifDisp.innerHTML = '<div style="background-color: lightblue; border:solid 1px" class="notifdiv">'
                                                + '<a class="dropdown-item" href="/ProjectSWP/home/work?workid=' + data.workid + '">' + data.sendername + ' ' + data.content + 'at ' + data.timesent + '</a>'
                                                + '</div>' + notifDisp.innerHTML;
                                        const directChildren = notifDisp.children.length;
                                        if (directChildren > 10) {
                                            let lastnotifVariable = document.querySelector(".notifdiv:last-of-type")
                                            notifDisp.removeChild(lastnotifVariable);
                                        }

                                    };

                                    ws.onerror = function (event) {
                                        console.log("Error ", event);
                                    }
                                </script>
                                <div id="notifDisp">
                                    <%for (Notification notif : notifications) {%>
                                    <div style="background-color: lightblue; border:solid 1px" class="notifdiv">
                                        <img class="img-fluid img-responsive rounded-circle mr-2" src="https://wompampsupport.azureedge.net/fetchimage?siteId=7575&v=2&jpgQuality=100&width=700&url=https%3A%2F%2Fi.kym-cdn.com%2Fphotos%2Fimages%2Fnewsfeed%2F002%2F111%2F316%2Fc57.gif" width="38">
                                        <a class="dropdown-item" href="/ProjectSWP/home/work?workid=<%=notif.getAp().getWorkid()%>"><%=notif.getSenderAcc().getUsername()%> <%=notif.getContent()%> at <%=notif.getTimesent().getHours()%> : <%=notif.getTimesent().getMinutes()%> | <%=notif.getTimesent().getDate()%>/<%=notif.getTimesent().getMonth() + 1%>/<%=notif.getTimesent().getYear() + 1900%></a> 
                                    </div>
                                    <%}%>
                                </div>
                                <a class="dropdown-item" href="/ProjectSWP/notifications" style="text-align: center"> View all notifications </a>
                            </div>

                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">



                                <img class="author-thumb" src="data:image/jpg;base64,${accPic.ava}"/> 


                                <c:if test="${not empty sessionScope.subcription}">
                                    <img src="view/affiliates/assets/images/logo-footer.png" style="width: 50px;length: 50px; color: #Fcbc19" title="Your Subcription Last Until ${sessionScope.subcription.todate}">
                                </c:if>

                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                <a class="dropdown-item" href="home/profile">View Profile</a>
                                <a class="dropdown-item" href="home/pin">Pinned Work</a>
                                <a class="dropdown-item" href ="home/delete">My Work</a> 
                                <a class="dropdown-item" href="followlist">Follow List</a>
                                <a class="dropdown-item" href="fanboxlist">Fanbox List</a>
                                <a class="dropdown-item" href="mute">Muted Work</a>
                                <a class="dropdown-item" href="history">View History</a>
                                <a class="dropdown-item" href="home/logout">Log Out</a>
                            </div>
                        </li>
                        <%}%>
                    </ul>
                    <!-- End Menu -->
                </div>
            </div>
        </header>
        <!-- End Menu Navigation
        ================================================== -->
        <div class="site-content">
            <!-- Home Jumbotron
        ================================================== -->
            <section class="intro">
            </section>
            <!-- Container
        ================================================== -->
            <div class="container">
                <div class="main-content">
                    <!-- Featured
        ================================================== -->
                    <!--                    <section class="featured-posts">
                                            <div class="section-title">
                                                <h2><span>Featured works</span></h2>
                                            </div>
                                            <div class="row listfeaturedtag">
                                                 begin post 
                                                <div class="col-sm-6">
                                                    <div class="card">
                                                        <div class="row">
                                                            <div class="col-md-5 wrapthumbnail">
                                                                <a href="single.html">
                                                                    <div class="thumbnail" style="background-image:url(assets/images/1.jpg);">
                                                                    </div>
                                                                </a>
                                                            </div>
                                                            <div class="col-md-7">
                                                                <div class="card-block">
                                                                    <h2 class="card-title"><a href="single.html">We all wait for summer</a></h2>
                                                                    <h4 class="card-text">This is changed. As I engage in the so-called “bull sessions” around and about the school, I too often find that most college men have...</h4>
                                                                    <div class="metafooter">
                                                                        <div class="wrapfooter">
                                                                            <span class="meta-footer-thumb">
                                                                                <img class="author-thumb" src="https://www.gravatar.com/avatar/b1cc14991db7a456fcd761680bbc8f81?s=250&d=mm&r=x" alt="John">
                                                                            </span>
                                                                            <span class="author-meta">
                                                                                <span class="post-name"><a target="_blank" href="#">John</a></span><br/>
                                                                                <span class="post-date">12 Jan 2018</span>
                                                                            </span>
                                                                            <span class="post-read-more"><a href="single.html" title="Read Story"><i class="fa fa-link"></i></a></span>
                                                                            <div class="clearfix">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                 end post 
                                                 begin post 
                                                <div class="col-sm-6">
                                                    <div class="card">
                                                        <div class="row">
                                                            <div class="col-md-5 wrapthumbnail">
                                                                <a href="single.html">
                                                                    <div class="thumbnail" style="background-image:url(assets/images/4.jpg);">
                                                                    </div>
                                                                </a>
                                                            </div>
                                                            <div class="col-md-7">
                                                                <div class="card-block">
                                                                    <h2 class="card-title"><a href="single.html">Powerful things you can do with the Markdown editor</a></h2>
                                                                    <h4 class="card-text">There are lots of powerful things you can do with the Markdown editor </h4>
                                                                    <div class="metafooter">
                                                                        <div class="wrapfooter">
                                                                            <span class="meta-footer-thumb">
                                                                                <img class="author-thumb" src="https://www.gravatar.com/avatar/e56154546cf4be74e393c62d1ae9f9d4?s=250&d=mm&r=x" alt="Sal">
                                                                            </span>
                                                                            <span class="author-meta">
                                                                                <span class="post-name"><a target="_blank" href="#">Sal</a></span><br/>
                                                                                <span class="post-date">12 Jan 2018</span>
                                                                            </span>
                                                                            <span class="post-read-more"><a href="single.html" title="Read Story"><i class="fa fa-link"></i></a></span>
                                                                            <div class="clearfix">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                 end post 
                                            </div>
                                        </section>-->
                    <!-- Posts Index
    ================================================== -->
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-8"></div>
                            <form name="loginBox" action="home/userSearchUser" method="POST" class="col-sm-4">
                                <div class="form-outline">
                                    <input type="search" id="form1" class="form-control" placeholder="Search Users" aria-label="Search" name="username" />
                                    <input type="submit" hidden />
                                </div>
                            </form>
                        </div>
                    </div>



                    <div class="d-flex bg-info text-dark">
                        <div class="p-2"><a class="text-white" href="/ProjectSWP/home?order=asc" >Oldest</a></div>
                        <div class="p-2"><a class="text-white" href="/ProjectSWP/home?order=desc">Newest</a></div>
                        <c:if test="${not empty sessionScope.subcription}">
                            <div class="p-2"><a href="/ProjectSWP/home?order=pop" class="text-white">By Popularity</a></div>
                        </c:if>
                        <div class="ml-auto p-2"><a class="text-white" data-toggle="modal" data-target="#default-example-modal-center" href="#">Search Options</a></div>
                    </div>

                    <div class="sidebar-section">


                        <div class="modal fade" id="default-example-modal-center" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <h2></h2>
                                <div class="modal-content">
                                    <div class="modal-header text-center ">
                                        <h4 class="modal-title w-100" style="text-align: center">
                                            Search Options
                                        </h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true"><i class="fal fa-times"></i></span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form action="home" method="POST" id="Searchfrm">
                                            <div class="form-group">
                                                <label>Keywords</label>
                                                <input type="text" name="work" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter keyword">
                                            </div>
                                            <div class="form-group">
                                                <label>Targets</label>
                                                <select class="custom-select my-1 mr-sm-2" id="inlineFormCustomSelectPref" name="tags">
                                                    <option selected value="0">Choose Tags...</option>
                                                    <% for (Tag t : tags) {%>
                                                    <option value="<%=t.getTagid()%>"><%=t.getTagname()%></option>
                                                    <% }%>
                                                </select>
                                            </div>
                                            <c:choose>
                                                <c:when test="${not empty sessionScope.subcription}">
                                                    <div class="form-group">
                                                        <label>Saveds</label>
                                                        <select class="custom-select my-1 mr-sm-2" id="inlineFormCustomSelectPref" name="savednum">
                                                            <option selected value="0">All Saved</option>
                                                            <option value="10000" >10000 +</option>
                                                            <option value="5000">5000 +</option>
                                                            <option value="1000">1000 +</option>
                                                            <option value="500" >500 +</option>
                                                            <option value="100">100 +</option>
                                                            <option value="50">50 +</option>
                                                        </select>
                                                    </div>
                                                </c:when>
                                                <c:otherwise>
                                                    <input type="hidden" name="savednum" value="0">
                                                </c:otherwise>
                                            </c:choose>

                                            <input type="hidden" name="search" value="search">
                                        </form>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="submit" form="Searchfrm" value="submit" class="btn btn-primary">Save changes</button>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>
                    <section class="recent-posts row">
                        <div class="col-sm-4">
                            <div class="sidebar">
                                <div class="sidebar-section">
                                    <h5><span>Recommended</span></h5>
                                    <ul style="list-none">
                                        <li><a target="_blank" href="https://m.do.co/c/84c9b45d0c47">Digital Ocean</a></li>
                                        <li><a target="_blank" href="https://www.cloudways.com/en/pricing.php?id=153986&a_bid=005da123">Cloudways</a></li>
                                        <li><a target="_blank" href="https://shareasale.com/r.cfm?b=875645&u=1087935&m=41388&urllink=&afftrack=">Page Speed Test</a></li>
                                        <li><a target="_blank" href="https://elementor.com/?ref=1556">Elementor Page Builder</a></li>
                                        <li><a target="_blank" href="https://www.wowthemes.net/category/freebies/">Our Freebies</a></li>
                                    </ul>
                                </div>
                                <div class="sidebar-section">

                                    <div class="container">
                                        <c:if test="${empty sessionScope.subcription}">    
                                            <h5><span>Advertisements</span></h5>
                                            <div id="myCarousel" class="carousel slide" >
                                                <div class="carousel-inner">
                                                    <div class="carousel-item active">
                                                        <img class="img-fluid" src="data:image/jpg;base64,${ads[0].base64Image}" style="width:100%"> 
                                                    </div>
                                                    <c:forEach items="${requestScope.ads}" var="a"> 
                                                        <div class="carousel-item">

                                                            <img class="img-fluid" src="data:image/jpg;base64,${a.base64Image}" style="width:100%"> 
                                                        </div>
                                                    </c:forEach>
                                                </div>
                                            </div>
                                        </c:if>
                                    </div> 
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <div class="section-title">
                                <h2><span>News feed</span></h2>
                            </div>
                            <%
                                PostDBContext pdb = new PostDBContext();
                                ArrayList<PostTag> PostTag = new ArrayList<PostTag>();
                                AccountDetail accPic = new AccountDetail();
                                AccountDBContext adb = new AccountDBContext();
                            %>
                            <div class="masonrygrid row listrecent">

                                <c:forEach items="${requestScope.post}" var="o">
                                    <c:set var = "muted" value = "0"/>
                                    <c:set var="workid" value="${o.ap.workid}"/>
                                    <c:set var="username" value="${o.ap.a.username}"/>
                                    <%
                                        PostTag = pdb.GetWorkTag(pageContext.getAttribute("workid").toString());
                                        request.setAttribute("posttag", PostTag);
                                        accPic.setAva(adb.getbase64Ava(pageContext.getAttribute("username").toString()));
                                        request.setAttribute("userpic", accPic);
                                    %>
                                    <c:if test="${o.privacy gt 0}">
                                        <c:if test="${not empty account}">
                                            <c:choose>
                                                <c:when test="${not empty mutedPost}">
                                                    <c:forEach items="${requestScope.mutedPost}" var="mp">
                                                        <c:if test="${o.ap.a.username eq mp.muteduser}">
                                                            <c:set var = "muted" value = "1"/>
                                                        </c:if>
                                                    </c:forEach>

                                                </c:when>
                                                <c:when test="${not empty requestScope.posttag}">
                                                    <c:forEach items="${requestScope.posttag}" var="pt">
                                                        <c:forEach items="${requestScope.mutedTag}" var="mt">
                                                            <c:if test="${pt.tag.tagid eq mt.t.tagid}">
                                                                <c:set var = "muted" value = "1"/>
                                                            </c:if>

                                                        </c:forEach> 
                                                    </c:forEach>
                                                </c:when>
                                            </c:choose>
                                        </c:if>
                                        <c:choose>

                                            <c:when test="${muted eq 1}">
                                                <div class="col-md-6 grid-item">
                                                    <div class="card">
                                                        <a href="home/work?workid=${o.ap.workid}">
                                                            <img style="filter: blur(8px);-webkit-filter: blur(8px);" class="img-fluid" src="data:image/jpg;base64,${o.base64Image}" style="height: 250px; width: 500px">
                                                        </a>
                                                        <div class="card-block">
                                                            <h2 class="card-title"><a style="color: transparent; text-shadow: 0 0 5px rgba(0,0,0,0.5);" href="?id=o.workid">${o.name}</a></h2>
                                                                <c:set var = "desc" value = "${o.description}"/>
                                                                <c:if test="${fn:length(desc) < 70 }">
                                                                <h4 style="color: transparent; text-shadow: 0 0 5px rgba(0,0,0,0.5);" class="card-text">${o.description}</h4>
                                                            </c:if>
                                                            <c:if test="${fn:length(desc) > 70 }">
                                                                <c:set var = "shortdesc" value = "${fn:substring(desc, 0, 69)}"/>
                                                                <h4 style="color: transparent; text-shadow: 0 0 5px rgba(0,0,0,0.5);" class="card-text">${shortdesc}...</h4>
                                                            </c:if>
                                                            <div class="metafooter">
                                                                <div class="wrapfooter">
                                                                    <span class="meta-footer-thumb">
                                                                        <img class="author-thumb" src="data:image/jpg;base64,${requestScope.userpic.ava}"   /> 
                                                                    </span>
                                                                    <span class="author-meta">
                                                                        <span style="color: transparent; text-shadow: 0 0 5px rgba(0,0,0,0.5);" class="post-name"><a target="_blank" href="home/profile?proname=${o.ap.a.username}">${o.ap.a.displayname}</a></span><br/>
                                                                        <span class="post-date">${o.date}</span>
                                                                    </span>
                                                                    <%
                                                                        if (account != null) {
                                                                    %>

                                                                    <span class="post-read-more"><a onclick="setPinColor('${o.ap.workid}P')"  title="Pin this work" class="pin" style="float: right" id="${o.ap.workid}P"><i class="bi bi-heart-fill"  id="${o.ap.workid}P"></i></a></span>



                                                                    <span style="" class="post-read-more"><a id="load"><i id="${o.ap.workid}Li">${o.like}</i></a></span>


                                                                    <span style="" class="post-read-more"><a onclick="setLikeColor('${o.ap.workid}L')"  title="Like this work" class="like" id="${o.ap.workid}L"><i class="fa fa-thumbs-up"  id="${o.ap.workid}L"></i></a></span>
                                                                            <%} else {%>
                                                                    <span style="" class="post-read-more"><a id="load"><i class="likecounter">${o.like}</i></a></span>
                                                                            <%}%>



                                                                    <div class="clearfix">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </c:when>
                                            <c:otherwise>
                                                <div class="col-md-6 grid-item">
                                                    <div class="card">
                                                        <a href="home/work?workid=${o.ap.workid}">
                                                            <img class="img-fluid" src="data:image/jpg;base64,${o.base64Image}" style="height: 250px; width: 500px">
                                                        </a>
                                                        <div class="card-block">
                                                            <h2 class="card-title"><a href="?id=o.workid">${o.name}</a></h2>
                                                                <c:set var = "desc" value = "${o.description}"/>
                                                                <c:if test="${fn:length(desc) < 70 }">
                                                                <h4 class="card-text">${o.description}</h4>
                                                            </c:if>
                                                            <c:if test="${fn:length(desc) > 70 }">
                                                                <c:set var = "shortdesc" value = "${fn:substring(desc, 0, 69)}"/>
                                                                <h4 class="card-text">${shortdesc}...</h4>
                                                            </c:if>
                                                            <div class="metafooter">
                                                                <div class="wrapfooter">

                                                                    <span class="meta-footer-thumb">
                                                                        <img class="author-thumb" src="data:image/jpg;base64,${requestScope.userpic.ava}"   /> 

                                                                    </span>
                                                                    <%
                                                                        if (account != null) {
                                                                    %>
                                                                    <span class="author-meta">
                                                                        <script>
                                                                            function sendNewMsg(sender, receiver) {
                                                                                let message = prompt("Send message to" + receiver);
                                                                                if (message != "" && message != null) {
                                                                                    location.href = "/ProjectSWP/messages/send?sendername=" + sender + "&receivername=" + receiver + "&content=" + message + "&timesent=" + Date.now();
                                                                                }
                                                                            }
                                                                        </script>
                                                                        <span class="post-name"><a target="_blank" href="home/profile?proname=${o.ap.a.username}">${o.ap.a.displayname}</a></span><br/>
                                                                        <span class="post-date">${o.date}</span>
                                                                    </span>


                                                                    <i class="fa fa-envelope" style="font-size:20px" onclick="sendNewMsg('<%=account.getUsername()%>', '${o.ap.a.username}')"></i>

                                                                    <span class="post-read-more"><a onclick="setPinColor('${o.ap.workid}P')"  title="Pin this work" class="pin" style="float: right" id="${o.ap.workid}P"><i class="bi bi-heart-fill"  id="${o.ap.workid}P"></i></a></span>



                                                                    <span style="" class="post-read-more"><a id="load"><i id="${o.ap.workid}Li">${o.like}</i></a></span>


                                                                    <span style="" class="post-read-more"><a onclick="setLikeColor('${o.ap.workid}L')"  title="Like this work" class="like" id="${o.ap.workid}L"><i class="fa fa-thumbs-up"  id="${o.ap.workid}L"></i></a></span>
                                                                            <%} else {%>
                                                                    <span class="post-name"><a target="_blank" href="home/profile?proname=${o.ap.a.username}">${o.ap.a.displayname}</a></span><br/>
                                                                    <span class="post-date">${o.date}</span>
                                                                    <span style="" class="post-read-more"><a id="load"><i class="likecounter">${o.like}</i></a></span>
                                                                            <%}%>



                                                                    <div class="clearfix">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </c:otherwise>
                                        </c:choose>
                                    </c:if>

                                </c:forEach>


                                <script>
                                    var color = "";
                                    <c:forEach items="${requestScope.post}" var="o">

                                        <c:forEach items="${requestScope.savedPost}" var="s">
                                            <c:if test="${s.ap.workid == o.ap.workid}">
                                    var property = document.getElementById('${o.ap.workid}P');
                                    color = property.style.color;
                                    property.style.color = "red";
                                            </c:if>
                                        </c:forEach>

                                        <c:forEach items="${requestScope.likedPost}" var="l">
                                            <c:if test="${l.ap.workid == o.ap.workid}">
                                    var property = document.getElementById('${o.ap.workid}L');
                                    color = property.style.color;
                                    property.style.color = "blue";
                                            </c:if>
                                        </c:forEach>
                                    </c:forEach>
                                </script>
                                <!--                                 begin post 
                                                                <div class="col-md-6 grid-item">
                                                                    <div class="card">
                                                                        <a href="single.html">
                                                                            <img class="img-fluid" src="view/affiliates/assets/images/2.jpg" alt="Tree of Codes">
                                                                        </a>
                                                                        <div class="card-block">
                                                                            <h2 class="card-title"><a href="single.html">Tree of Codes</a></h2>
                                                                            <h4 class="card-text">The first mass-produced book to deviate from a rectilinear format, at least in the United States, is thought to be this 1863 edition of Red Riding Hood, cut into the...</h4>
                                                                            <div class="metafooter">
                                                                                <div class="wrapfooter">
                                                                                    <span class="meta-footer-thumb">
                                                                                        <img class="author-thumb" src="https://www.gravatar.com/avatar/e56154546cf4be74e393c62d1ae9f9d4?s=250&d=mm&r=x" alt="Sal">
                                                                                    </span>
                                                                                    <span class="author-meta">
                                                                                        <span class="post-name"><a target="_blank" href="#">Sal</a></span><br/>
                                                                                        <span class="post-date">12 Jan 2018</span>
                                                                                    </span>
                                                                                    <span class="post-read-more"><a href="single.html" title="Read Story"><i class="fa fa-link"></i></a></span>
                                                                                    <div class="clearfix">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                 end post 
                                                                 begin post 
                                                                <div class="col-md-6 grid-item">
                                                                    <div class="card">
                                                                        <a href="single.html">
                                                                            <img class="img-fluid" src="view/affiliates/assets/images/3.jpg" alt="Red Riding Hood">
                                                                        </a>
                                                                        <div class="card-block">
                                                                            <h2 class="card-title"><a href="single.html">Red Riding Hood</a></h2>
                                                                            <h4 class="card-text">The first mass-produced book to deviate from a rectilinear format, at least in the United States, is thought to be this 1863 edition of Red Riding Hood, cut into the...</h4>
                                                                            <div class="metafooter">
                                                                                <div class="wrapfooter">
                                                                                    <span class="meta-footer-thumb">
                                                                                        <img class="author-thumb" src="https://www.gravatar.com/avatar/e56154546cf4be74e393c62d1ae9f9d4?s=250&d=mm&r=x" alt="Sal">
                                                                                    </span>
                                                                                    <span class="author-meta">
                                                                                        <span class="post-name"><a target="_blank" href="#">Sal</a></span><br/>
                                                                                        <span class="post-date">12 Jan 2018</span>
                                                                                    </span>
                                                                                    <span class="post-read-more"><a href="single.html" title="Read Story"><i class="fa fa-link"></i></a></span>
                                                                                    <div class="clearfix">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                 end post 
                                                                 begin post 
                                                                <div class="col-md-6 grid-item">
                                                                    <div class="card">
                                                                        <a href="single.html">
                                                                            <img class="img-fluid" src="view/affiliates/assets/images/5.jpg" alt="Is Intelligence Enough">
                                                                        </a>
                                                                        <div class="card-block">
                                                                            <h2 class="card-title"><a href="single.html">Is Intelligence Enough</a></h2>
                                                                            <h4 class="card-text">Education must also train one for quick, resolute and effective thinking. To think incisively and to think for one’s self is very difficult. </h4>
                                                                            <div class="metafooter">
                                                                                <div class="wrapfooter">
                                                                                    <span class="meta-footer-thumb">
                                                                                        <img class="author-thumb" src="https://www.gravatar.com/avatar/e56154546cf4be74e393c62d1ae9f9d4?s=250&d=mm&r=x" alt="Sal">
                                                                                    </span>
                                                                                    <span class="author-meta">
                                                                                        <span class="post-name"><a target="_blank" href="#">Sal</a></span><br/>
                                                                                        <span class="post-date">12 Jan 2018</span>
                                                                                    </span>
                                                                                    <span class="post-read-more"><a href="single.html" title="Read Story"><i class="fa fa-link"></i></a></span>
                                                                                    <div class="clearfix">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>-->
                                <!-- end post -->
                                <!-- begin post -->
                                <!--                                <div class="col-md-6 grid-item">
                                                                    <div class="card">
                                                                        <a href="single.html">
                                                                            <img class="img-fluid" src="view/affiliates/assets/images/6.jpg" alt="Markdown Example">
                                                                        </a>
                                                                        <div class="card-block">
                                                                            <h2 class="card-title"><a href="single.html">Markdown Example</a></h2>
                                                                            <h4 class="card-text">You’ll find this post in your _posts directory. Go ahead and edit it and re-build the site to see your changes. You can rebuild the site in many different ways,...</h4>
                                                                            <div class="metafooter">
                                                                                <div class="wrapfooter">
                                                                                    <span class="meta-footer-thumb">
                                                                                        <img class="author-thumb" src="https://www.gravatar.com/avatar/b1cc14991db7a456fcd761680bbc8f81?s=250&d=mm&r=x" alt="John">
                                                                                    </span>
                                                                                    <span class="author-meta">
                                                                                        <span class="post-name"><a target="_blank" href="#">John</a></span><br/>
                                                                                        <span class="post-date">11 Jan 2018</span>
                                                                                    </span>
                                                                                    <span class="post-read-more"><a href="single.html" title="Read Story"><i class="fa fa-link"></i></a></span>
                                                                                    <div class="clearfix">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>-->
                                <!-- end post -->
                            </div>
                            <!-- Pagination -->
                            <div class="bottompagination">
                                <div class="navigation">
                                    <nav class="pagination">
                                        <div id="container" class="paging"> </div>
                                        <% Integer totalPage = (Integer) request.getAttribute("onoOfPages");
                                            Integer pageIndex = (Integer) request.getAttribute("pageIndex");
                                        %>
                                        <script>
                                            paging("container",<%=pageIndex%>,<%=totalPage%>, 1);
                                        </script>
                                        <span class="page-number"> &nbsp; &nbsp; Page <%=pageIndex%> of <%=totalPage%> &nbsp; &nbsp; </span>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            <!-- /.container -->
            <!-- Before Footer
        ================================================== -->

            <!-- Begin Footer
        ================================================== -->
            <footer class="footer">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="footer-widget">
                                <a href="home/about"> 
                                    <img src="view/affiliates/assets/images/logo-footer.png" alt="logo footer" style="position: relative; width: 170px; left: 15px; background-size: contain;">
                                </a>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="footer-widget">
                                <h5 class="title">Author</h5>

                                <ul>
                                    <li><a href="#">About Us</a></li>
                                    <li><a target="_blank" href="https://www.wowthemes.net/affiliate-area/">Affiliates</a></li>
                                    <li><a href="https://www.wowthemes.net/terms-and-conditions/">License</a></li>
                                    <li><a href="https://www.wowthemes.net/blog/">Blog</a></li>
                                    <li><a href="contact.jsp">Contact</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="footer-widget textwidget">
                                <h5 class="title">Email us</h5>
                                <form action="email" method="POST">
                                    <input type="hidden" name="page" value="home">
                                    <input type="text" name="subject" placeholder="Subject">
                                    <input type="email" name="email" placeholder="E-mail Address You Want To Get Reply">
                                    <textarea rows="8" name="content" placeholder="Message"></textarea><br/>
                                    <input class="btn btn-success" type="submit" value="Send">
                                    ${requestScope.message}
                                </form>                             
                            </div>
                        </div>
                    </div>
                    <div class="copyright">
                        <p class="pull-left">
                            Copyright © 2018 Affiliates HTML Template
                        </p>
                        <p class="pull-right">
                            <!-- Leave credit to author unless you own a commercial license: https://www.wowthemes.net/freebies-license/ -->
                            <a target="_blank" href="https://www.wowthemes.net/affiliates-free-bootstrap-template/">"Affiliates Template"</a> - Design & Code by WowThemes.net
                        </p>
                        <div class="clearfix">
                        </div>
                    </div>
                </div>
            </footer>
            <!-- End Footer
        ================================================== -->
        </div>


        <!-- JavaScript
        ================================================== -->


        <script type='text/javascript' src="/bootstrap.js"></script>
        <script type="text/javascript" src="/functions.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.slim.min.js"></script>


        <script>
                                            $(document).ready(function () {

                                                $("#myCarousel").carousel({interval: 500});
                                            });
        </script>

        <script src="https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.slim.min.js"></script>

        <script src="view/affiliates/assets/js/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
        <script src="view/affiliates/assets/js/ie10-viewport-bug-workaround.js"></script>
        <script src="view/affiliates/assets/js/masonry.pkgd.min.js"></script>
        <script src="view/affiliates/assets/js/theme.js"></script>
        <script src="view/affiliates/assets/js/pageScript.js"></script>

        <script src="view/affiliates/assets/inteltemplate/js/vendors.bundle.js"></script>
        <script src="view/affiliates/assets/inteltemplate/js/app.bundle.js"></script>

    </body>
</html>
