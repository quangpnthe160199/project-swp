<%-- 
    Document   : fanboxsub.jsp
    Created on : Jul 23, 2022, 4:41:20 AM
    Author     : admin
--%>

<%@page import="model.Account"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Tạo mới đơn hàng</title>
        <!-- Bootstrap core CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
        <!-- Custom styles for this template -->
        <link href="../view/affiliates/assets/css/jumbotron-narrow.css" rel="stylesheet">      
        <%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
        <%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
        <%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
        <c:set var="balance" value="${requestScope.price}}}}" />
        <%
            Account account = (Account) request.getSession().getAttribute("account");
        %>
    </head>

    <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
            <link rel="icon" href="../view/affiliates/assets/images/logo-footer.png">
            <title>Beaucoup | An All-you-can-post page</title>
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
            <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
            <link href="https://fonts.googleapis.com/css?family=Rubik:400,400i,500,500i,700,700i" rel="stylesheet">
            <link href="../../view/affiliates/assets/css/theme.css" rel="stylesheet">
            <%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
            <%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
            <!-- Begin tracking codes here, including ShareThis/Analytics -->

            <!-- End tracking codes here, including ShareThis/Analytics -->
        </head>
        <body class="layout-default">
            <!-- Begin Menu Navigation
            ================================================== -->
            <header class="navbar navbar-toggleable-md navbar-light bg-white fixed-top mediumnavigation">
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarsWow" aria-controls="navbarsWow" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="container">
                    <!-- Begin Logo -->
                    <a class="navbar-brand" href="../../home">
                        <img src="../../view/affiliates/assets/images/logo.png" alt="Affiliates - Free Bootstrap Template" style="position: relative; width: 170px; left: 15px; background-size: contain;">
                    </a>
                    <!-- End Logo -->
                    <!-- Begin Menu -->
                    <div class="collapse navbar-collapse" id="navbarsWow">
                        <!-- Begin Menu -->
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item">
                                <a class="nav-link" href="../../home">Home</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="../../home/about">About</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="../../home/contact">Contact</a>
                            </li>
                            <%
                                 account = (Account) request.getSession().getAttribute("account");
                                if (account == null) {
                            %>
                            <li class="nav-item">
                                <a class="nav-link highlight" href="../../home/login">Login</a>
                            </li>
                            <%} else {%>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <img class="author-thumb" src="https://www.gravatar.com/avatar/b1cc14991db7a456fcd761680bbc8f81?s=250&amp;d=mm&amp;r=x" alt="John">
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                    <a class="dropdown-item" href="../../home">Dashboard</a>
                                    <a class="dropdown-item" href ="../../home/delete">Delete Work</a>
                                    <a class="dropdown-item" href="../../home/edit">Edit Profile</a>
                                    <a class="dropdown-item" href="../../home/logout">Log Out</a>
                                </div>
                            </li>
                            <%}%>
                        </ul>
                        <!-- End Menu -->
                    </div>
                </div>
            </header>
            <!-- End Menu Navigation
            ================================================== -->
            <div class="site-content">
                <!-- Home Jumbotron
            ================================================== -->
                <section class="intro">
                </section>
                <!-- Container
            ================================================== -->
                <div class="container">
                    <div class="header clearfix">

                        <h3 class="text-muted">BeauCoup Blog</h3>
                    </div>
                    <h3>Confirm Order</h3>
                    <div class="table-responsive">
                        <form action="subscribe" id="frmCreateOrder" method="post">        
                            <div class="form-group">
                                <label for="language"> Type of order </label>
                                <select name="ordertype" id="ordertype" style="pointer-events: none" class="form-control">
                                    <option value="billpayment" select="selected">Fanbox Subcription</option>
                                </select>
                            </div>
                            <input type="hidden" name="username" value="">
                            <div class="form-group">
                                <label for="amount">Amount</label>
                                <fmt:parseNumber var = "i" integerOnly = "true" 
                                                 type = "number" value = "${balance}" />
                                <input class="form-control" data-val="true" data-val-number="The field Amount must be a number." style="pointer-events: none" data-val-required="The Amount field is required." id="amount" max="100000000" min="1" name="amount" type="number" value="${i}" />
                            </div>
                            <div class="form-group">
                                <label for="OrderDescription" style="pointer-events: none">Description</label>
                                <textarea class="form-control" style="pointer-events: none"  cols="20" id="vnp_OrderInfo" name="vnp_OrderInfo" rows="2"><%=account.getUsername()%>_${requestScope.type}</textarea>
                            </div>
                            <div class="form-group">
                                <label >Email</label>
                                <input class="form-control" id="txt_inv_email"
                                       name="txt_inv_email" type="text" value="pholv@vnpay.vn"/>
                            </div>
                            <div class="form-group">
                                <label >Phone Number</label>
                                <input class="form-control" id="txt_inv_mobile"
                                       name="txt_inv_mobile" type="text" value="02437764668"/>
                            </div>
                            <div class="form-group">
                                <label for="bankcode">Bank</label>
                                <select name="bankcode" id="bankcode" class="form-control">
                                    <option value="">None </option>
                                    <option value="NCB">  	Ngan hang NCB</option>
                                    <option value="SACOMBANK">Ngan hang SacomBank  </option>
                                    <option value="EXIMBANK"> 	Ngan hang EximBank </option>
                                    <option value="MSBANK"> 	Ngan hang MSBANK </option>
                                    <option value="NAMABANK"> 	Ngan hang NamABank </option>
                                    <option value="VISA">  	Thanh toan qua VISA/MASTER</option>
                                    <option value="VNMART">  	Vi dien tu VnMart</option>
                                    <option value="VIETINBANK">Ngan hang Vietinbank  </option>
                                    <option value="VIETCOMBANK"> 	Ngan hang VCB </option>
                                    <option value="HDBANK">Ngan hang HDBank</option>
                                    <option value="DONGABANK">  	Ngan hang Dong A</option>
                                    <option value="TPBANK"> 	Ngân hàng TPBank </option>
                                    <option value="OJB">  	Ngân hàng OceanBank</option>
                                    <option value="BIDV"> Ngân hàng BIDV </option>
                                    <option value="TECHCOMBANK"> 	Ngân hàng Techcombank </option>
                                    <option value="VPBANK"> 	Ngan hang VPBank </option>
                                    <option value="AGRIBANK"> 	Ngan hang Agribank </option>
                                    <option value="MBBANK"> 	Ngan hang MBBank </option>
                                    <option value="ACB"> Ngan hang ACB </option>
                                    <option value="OCB"> Ngan hang OCB </option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="language">Lnaguage</label>
                                <select name="language" id="language" class="form-control">
                                    <option value="vn">Vietnamese</option>
                                    <option value="en">English</option>
                                </select>
                            </div>
                            <!--                    <div class="form-group">
                                                    <h3>Billing information</h3>
                                                </div>
                                                <div class="form-group">
                                                    <label >Full Name (*)</label>-->
                            <input class="form-control" id="txt_billing_fullname"
                                   name="txt_billing_fullname" type="hidden" value="NGUYEN VAN XO"/>             
                            <!--                    </div>
                                                <div class="form-group">
                                                    <label >Email (*)</label>-->
                            <input class="form-control" id="txt_billing_email"
                                   name="txt_billing_email" type="hidden" value="xonv@vnpay.vn"/>   
                            <!--                    </div>
                                                <div class="form-group">
                                                    <label >Phone Number (*)</label>-->
                            <input class="form-control" id="txt_billing_mobile"
                                   name="txt_billing_mobile" type="hidden" value="0934998386"/>   
                            <!--                    </div>
                                                <div class="form-group">
                                                    <label >Address (*)</label>-->
                            <input class="form-control" id="txt_billing_addr1"
                                   name="txt_billing_addr1" type="hidden" value="22 Lang Ha Dong Da Ha Noi"/>   
                            <!--                    </div>
                                                <div class="form-group">
                                                    <label >Postal Code (*)</label>-->
                            <input class="form-control" id="txt_postalcode"
                                   name="txt_postalcode" type="hidden" value="100000"/> 
                            <!--                    </div>
                                                <div class="form-group">
                                                    <label >City (*)</label>-->
                            <input class="form-control" id="txt_bill_city"
                                   name="txt_bill_city" type="hidden" value="Ha Noi"/> 
                            <!--                    </div>
                                                <div class="form-group">-->
                            <!--                        <label>State (Applicable for the USA, CA)</label>-->
                            <input class="form-control" id="txt_bill_state"
                                   name="txt_bill_state" type="hidden" value=""/>
                            <!--                    </div>
                                                <div class="form-group">
                                                    <label >Country (*)</label>-->
                            <input class="form-control" id="txt_bill_country"
                                   name="txt_bill_country" type="hidden" value="VN"/>
                            <!--                    </div>
                                                <div class="form-group">
                                                    <h3>Thông tin giao hàng (Shipping)</h3>
                                                </div>-->
                            <!--                    <div class="form-group">
                                                    <label >Name (*)</label>-->
                            <input class="form-control" id="txt_ship_fullname"
                                   name="txt_ship_fullname" type="hidden" value="Nguyen The Vinh"/>
                            <!--                    </div>
                                                <div class="form-group">
                                                    <label >Email (*)</label>-->
                            <input class="form-control" id="txt_ship_email"
                                   name="txt_ship_email" type="hidden" value="vinhnt@vnpay.vn"/>
                            <!--                    </div>
                                                <div class="form-group">
                                                    <label >Phone Number (*)</label>-->
                            <input class="form-control" id="txt_ship_mobile"
                                   name="txt_ship_mobile" type="hidden" value="0123456789"/>
                            <!--                    </div>
                                                <div class="form-group">
                                                    <label >Address (*)</label>-->
                            <input class="form-control" id="txt_ship_addr1"
                                   name="txt_ship_addr1" type="hidden" value="22 Lang Ha Dong Da Ha Noi"/>
                            <!--                    </div>
                                                <div class="form-group">
                                                    <label >Postal Code (*)</label>-->
                            <input class="form-control" id="txt_ship_postalcode"
                                   name="txt_ship_postalcode" type="hidden" value="1000000"/>
                            <!--                    </div>
                                                <div class="form-group">
                                                    <label >City (*)</label>-->
                            <input class="form-control" id="txt_ship_city"
                                   name="txt_ship_city" type="hidden" value="Ha Noi"/>
                            <!--                    </div>
                                                <div class="form-group">
                                                    <label>State (Applicable for US,CA)</label>-->
                            <input class="form-control" id="txt_ship_state"
                                   name="txt_ship_state" type="hidden" value=""/>
                            <!--                    </div>
                                                <div class="form-group">
                                                    <label >Country (*)</label>-->
                            <input class="form-control" id="txt_ship_country"
                                   name="txt_ship_country" type="hidden" value="VN"/>
                            <!--                    </div>
                                                <div class="form-group">
                                                    <h3>Invoice Information</h3>
                                                </div>
                                                <div class="form-group">
                                                    <label >Customer Name</label>-->
                            <input class="form-control" id="txt_inv_customer"
                                   name="txt_inv_customer" type="hidden" value="Nguyen Van A"/>
                            <!--                    </div>
                                                <div class="form-group">
                                                    <label >Company</label>-->
                            <input class="form-control" id="txt_inv_company"
                                   name="txt_inv_company" type="hidden" value="Cong Ty Co Phan Giai Phap Thanh Toan Viet Nam"/>
                            <!--                    </div>
                                                <div class="form-group">
                                                    <label >Address</label>-->
                            <input class="form-control" id="txt_inv_addr1"
                                   name="txt_inv_addr1" type="hidden" value="22 Lang Ha Dong Da Ha Noi"/>
                            <!--                    </div>
                                                <div class="form-group">
                                                    <label>Tax Number</label>-->
                            <input class="form-control" id="txt_inv_taxcode"
                                   name="txt_inv_taxcode" type="hidden" value="0102182292"/>
                            <!--                    </div>
                                                <div class="form-group">
                                                    <label >Type Of Inoice</label>-->
                            <select name="cbo_inv_type" id="cbo_inv_type" class="form-control" style="visibility: hidden">
                                <option value="I">Personal</option>
                                <option value="O">Company / Organization</option>
                            </select>
                            <!--                    </div>-->

                            <button type="submit" class="btn btn-success" style="align-content: center">Purchase</button>
                        </form>
                    </div>
                </div>
                <!-- /.container -->
                <!-- Before Footer
            ================================================== -->

                <!-- Begin Footer
            ================================================== -->
                <footer class="footer">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="footer-widget">
                                    <a href="../home/about"> 
                                        <img src="../view/affiliates/assets/images/logo-footer.png" alt="logo footer" style="position: relative; width: 170px; left: 15px; background-size: contain;">
                                    </a>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="footer-widget">
                                    <h5 class="title">Author</h5>
                                    <ul>
                                        <li><a href="#">About Us</a></li>
                                        <li><a target="_blank" href="https://www.wowthemes.net/affiliate-area/">Affiliates</a></li>
                                        <li><a href="https://www.wowthemes.net/terms-and-conditions/">License</a></li>
                                        <li><a href="https://www.wowthemes.net/blog/">Blog</a></li>
                                        <li><a href="contact.jsp">Contact</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="footer-widget textwidget">
                                    <h5 class="title">Email us</h5>
                                    <form action="https://formspree.io/wowthemesnet@gmail.com" method="POST">
                                        <input type="text" name="name" placeholder="Name">
                                        <input type="email" name="_replyto" placeholder="E-mail Address">
                                        <textarea rows="8" name="message" placeholder="Message"></textarea><br/>
                                        <input class="btn btn-success" type="submit" value="Send">
                                    </form>                             
                                </div>
                            </div>
                        </div>
                        <div class="copyright">
                            <p class="pull-left">
                                Copyright © 2018 Affiliates HTML Template
                            </p>
                            <p class="pull-right">
                                <!-- Leave credit to author unless you own a commercial license: https://www.wowthemes.net/freebies-license/ -->
                                <a target="_blank" href="https://www.wowthemes.net/affiliates-free-bootstrap-template/">"Affiliates Template"</a> - Design & Code by WowThemes.net
                            </p>
                            <div class="clearfix">
                            </div>
                        </div>
                    </div>
                </footer>
                <!-- End Footer
            ================================================== -->
            </div>


            <!-- JavaScript
            ================================================== -->
            <script src="../view/affiliates/assets/js/jquery.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
            <script src="../view/affiliates/assets/js/ie10-viewport-bug-workaround.js"></script>
            <script src="../view/affiliates/assets/js/masonry.pkgd.min.js"></script>
            <script src="../view/affiliates/assets/js/theme.js"></script>
            <script src="../view/affiliates/assets/js/pageScript.js"></script>
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
            <link href="https://pay.vnpay.vn/lib/vnpay/vnpay.css" rel="stylesheet" />
            <script src="https://pay.vnpay.vn/lib/vnpay/vnpay.min.js"></script>

            <script>
                $("#image").change(function ()
                {
                    var Data = document.getElementById('image');
                    var FileUploadPath = Data.value;

                    if (FileUploadPath != '') {
                        var Extension = FileUploadPath.substring(FileUploadPath.lastIndexOf('.') + 1).toLowerCase();
                        //The file uploaded is an image

                        if (Extension == "gif" || Extension == "png" || Extension == "bmp"
                                || Extension == "jpeg" || Extension == "jpg") {
                            return true;
                        } else if (Extension != 'gif' || Extension != 'png' || Extension != 'bmp' || Extension != 'jpeg' || Extension != 'jpg') {

                            alert("Photo only allows file types of GIF, PNG, JPG, JPEG and BMP. ");
                            Data.type = '';
                            Data.type = 'file';
                            return false;
                        }
                    }
                });

                $("#frmCreateOrder").submit(function () {
                    var postData = $("#frmCreateOrder").serialize();
                    var submitUrl = $("#frmCreateOrder").attr("action");
                    $.ajax({
                        type: "POST",
                        url: submitUrl,
                        data: postData,
                        dataType: 'JSON',
                        success: function (x) {
                            if (x.code === '00') {
                                if (window.vnpay) {
                                    vnpay.open({width: 768, height: 600, url: x.data});
                                } else {
                                    location.href = x.data;
                                }
                                return false;
                            } else {
                                alert(x.Message);
                            }
                        }
                    });
                    return false;
                });
            </script>
        </body>
    </html>
