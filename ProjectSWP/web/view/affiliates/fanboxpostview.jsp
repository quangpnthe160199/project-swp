
<%@page import="model.Notification"%>
<%@page import="java.util.UUID"%>
<%@page import="model.Reply"%>
<%@page import="model.Comment"%>
<%@page import="java.util.ArrayList"%>
<%@page import="model.Account"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="icon" href="../../view/affiliates/assets/images/logo-footer.png">
        <title>Beaucoup | An All-you-can-post page</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Rubik:400,400i,500,500i,700,700i" rel="stylesheet">
        <link href="../../view/affiliates/assets/css/theme2.css" rel="stylesheet">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.3/font/bootstrap-icons.css">
        <%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
        <%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
        <link rel="stylesheet" media="screen, print" href="../../view/affiliates/assets/inteltemplate/css/vendors.bundle.css">
        <link rel="stylesheet" media="screen, print" href="../../view/affiliates/assets/inteltemplate/css/app.bundle.css">
        <!-- base css -->
        <link rel="stylesheet" media="screen, print" href="../../view/affiliates/assets/inteltemplate/css/vendors.bundle.css">
        <link rel="stylesheet" media="screen, print" href="../../view/affiliates/assets/inteltemplate/css/app.bundle.css">
        <!-- Place favicon.ico in the root directory -->
        <link rel="apple-touch-icon" sizes="180x180" href="../../view/affiliates/assets/inteltemplate/img/favicon/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="../../view/affiliates/assets/inteltemplate/img/favicon/favicon-32x32.png">
    </head>
    <body class="layout-page">
        <!-- Begin Menu Navigation
        ================================================== -->
        <header class="navbar navbar-expand-lg navbar-light bg-light fixed-top mediumnavigation">   
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarsWow" aria-controls="navbarsWow" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="container">
                <!-- Begin Logo -->
                <a class="navbar-brand" href="../../home">
                    <img src="../../view/affiliates/assets/images/logo.png" alt="Affiliates - Free Bootstrap Template" style="position: relative; width: 170px; left: 15px; background-size: contain;">
                </a>
                <!-- End Logo -->
                <!-- Begin Menu -->
                <div class="collapse navbar-collapse" id="navbarsWow">
                    <!-- Begin Menu -->
                    <%
                        Account account = (Account) request.getSession().getAttribute("account");
                    %>
                    <ul class="navbar-nav ml-auto">
                        <%
                            if (account != null) {
                        %>
                        <c:choose>
                            <c:when test="${not empty sessionScope.subcription}">
                                <li class="nav-item">
                                    <a class="nav-link" style="color:#Fcbc19" href="../home/subcription">BeauCoup Blog Premium</a>
                                </li>
                            </c:when>
                            <c:when test="${empty sessionScope.subcription}">
                                <li class="nav-item">
                                    <a class="nav-link" style="color:#Fcbc19" href="../home/subcription">BeauCoup Blog Premium Trial</a>
                                </li>
                            </c:when>
                        </c:choose>


                        <%}%>
                        <li class="nav-item">
                            <a class="nav-link" href="../../home">Home</a>
                        </li>
                        <c:if test="${not empty account}">
                                <a class="dropdown-item" href="../../home/upload">Publish</a>
                        </c:if>
                        <li class="nav-item">
                            <a class="nav-link" href="../home/contact">Contact</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Tags</a>

                            <div class="dropdown-menu" aria-labelledby="dropdown01">
                                <a class="dropdown-item" href="/ProjectSWP/home/tag?tagid=1" >Action</a>
                                <a class="dropdown-item" href="/ProjectSWP/home/tag?tagid=2">Humor</a>
                                <a class="dropdown-item" href="/ProjectSWP/home/tag?tagid=3">Adventure</a>
                            </div>

                        </li>
                        <%
                            if (account == null) {
                        %>

                        <li class="nav-item">
                            <a class="nav-link highlight" href="../../home/login">Login</a>
                        </li>
                        <%} else {%>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Register Advertisement</a>
                            <div class="dropdown-menu" aria-labelledby="dropdown01">
                                <a class="dropdown-item" href="../../home/userRegisterAds">Register Advertisement</a>

                            </div>
                        </li>
                        <%ArrayList<Notification> notifications = (ArrayList<Notification>) request.getSession().getAttribute("notifications");%>
                        <li class="nav-item">
                            <a class="nav-link" href="../../home/messages"><i class="fa fa-envelope" style="font-size:24px"></i></a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bell" style="font-size:24px"></i></a>

                            <div class="dropdown-menu" aria-labelledby="dropdown01" style="background-color: lightblue" >
                                <%for (Notification notif : notifications) {%>
                                <div style="background-color: lightblue; border:solid 1px" id="notif<%=notif.getNid()%>" onclick="setNotifColor()">
                                    <img class="img-fluid img-responsive rounded-circle mr-2" src="https://wompampsupport.azureedge.net/fetchimage?siteId=7575&v=2&jpgQuality=100&width=700&url=https%3A%2F%2Fi.kym-cdn.com%2Fphotos%2Fimages%2Fnewsfeed%2F002%2F111%2F316%2Fc57.gif" width="38">
                                    <a class="dropdown-item" href="/ProjectSWP/home/work?workid=<%=notif.getAp().getWorkid()%>"><%=notif.getSenderAcc().getUsername()%> <%=notif.getContent()%> at <%=notif.getTimesent().getHours()%> : <%=notif.getTimesent().getMinutes()%> | <%=notif.getTimesent().getDate()%>/<%=notif.getTimesent().getMonth() + 1%>/<%=notif.getTimesent().getYear() + 1900%></a> 
                                </div>
                                <%}%>
                                <a class="dropdown-item" href="/ProjectSWP/notifications" style="text-align: center"> View all notifications </a>
                            </div>

                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              
                                <img class="author-thumb" src="data:image/jpg;base64,${sessionScope.accPic.ava}"   />
                                <c:if test="${not empty sessionScope.subcription}">
                                    <img src="../../view/affiliates/assets/images/logo-footer.png" style="width: 50px;length: 50px; color: #Fcbc19" title="Your Subcription Last Until ${sessionScope.subcription.todate}">
                                </c:if>

                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                <a class="dropdown-item" href="../../home/profile">View Profile</a>
                                <a class="dropdown-item" href="../../home/pin">Pinned Work</a>
                                <a class="dropdown-item" href ="../../home/delete">My Work</a> 
                                <a class="dropdown-item" href="../../mute">Muted Work</a>
                                <a class="dropdown-item" href="../../history">View History</a>
                                <a class="dropdown-item" href="../../home/logout">Log Out</a>
                            </div>
                        </li>
                        <%}%>
                    </ul>
                    <!-- End Menu -->
                </div>
            </div>
        </header>
        <!-- End Menu Navigation
        ================================================== -->
        <div class="site-content">
            <div class="container">
                <!-- Content
    ================================================== -->
                <div class="main-content">
                    <!-- Begin Article
        ================================================== -->
                    <div class="row">
                        <!-- Sidebar -->
                        <div class="col-sm-4">
                            <div class="sidebar">
                                <div class="sidebar-section">                    

                                    <script type='text/javascript'>(function ($) {
                                            window.fnames = new Array();
                                            window.ftypes = new Array();
                                            fnames[0] = 'EMAIL';
                                            ftypes[0] = 'email';
                                            fnames[3] = 'MMERGE3';
                                            ftypes[3] = 'text';
                                            fnames[1] = 'MMERGE1';
                                            ftypes[1] = 'text';
                                            fnames[2] = 'MMERGE2';
                                            ftypes[2] = 'text';
                                            fnames[4] = 'MMERGE4';
                                            ftypes[4] = 'text';
                                            fnames[5] = 'MMERGE5';
                                            ftypes[5] = 'text';
                                        }(jQuery));
                                        var $mcj = jQuery.noConflict(true);</script>
                                    <!--End mc_embed_signup-->
                                </div>
                                <div class="sidebar-section">
                                    <h5><span>Recommended</span></h5>
                                    <ul style="list-none">
                                        <li><a target="_blank" href="https://m.do.co/c/84c9b45d0c47">Digital Ocean</a></li>
                                        <li><a target="blank" href="https://www.cloudways.com/en/pricing.php?id=153986&amp;a_bid=005da123">Cloudways</a></li>
                                        <li><a target="blank" href="http://shareasale.com/r.cfm?b=875645&amp;u=1087935&amp;m=41388&amp;urllink=&amp;afftrack=">Page Speed Test</a></li>
                                        <li><a target="blank" href="https://elementor.com/?ref=1556">Elementor Page Builder</a></li>
                                        <li><a target="blank" href="https://www.wowthemes.net/category/jekyll-themes/">Our Jekyll Themes</a></li>
                                    </ul>
                                </div>
                                <div class="sidebar-section">
                                    <h5><span>Advertisements</span></h5>
                                    <a href="https://blogfb88.com" target="_blank" title="Nh ci chu u uy tn hng ??u Vi?t Nam"><img src="https://tylemacao.com/wp-content/uploads/2018/07/banner-fb88-tylemacao.gif"  alt="Nh ci chu u uy tn hng ??u Vi?t Nam"/></a>
                                    <a href="https://blogfb88.com" target="_blank" title="Nh ci chu u uy tn hng ??u Vi?t Nam"><img src="https://soicauxs888.com/wp-content/uploads/2021/09/fb88-banner-2-1.gif"  alt="Nh ci chu u uy tn hng ??u Vi?t Nam"/></a>
                                </div>
                            </div>
                        </div>
                        <!-- Post -->
                        <div class="col-sm-8">
                            <div class="mainheading">
                                <!-- Post Categories -->
                                <!--						<div class="after-post-tags">
                                                                                        <ul class="tags">
                                                                                                <li>
                                                                                                <a href="#">bootstrap</a>
                                                                                                </li>
                                                                                                <li>
                                                                                                <a href="#">tutorial</a>
                                                                                                </li>
                                                                                        </ul>
                                                                                </div>-->
                                <!-- End Categories -->
                                <!-- Post Title -->
                                
                                
                                <div class="mainheading">
                                            <h1 class="posttitle">${requestScope.post.name}</h1>
                                        </div>
                                        <div class="d-flex flex-row">
                                            <c:forEach items="${requestScope.tags}" var="t"> 
                                                <a class="p-2" href="../home?tag=${t.tagid}">#${t.tagname}</a>
                                            </c:forEach>
                                        </div>
                                        <!-- Post Featured Image -->
                                        <img class="featured-image img-fluid" src="data:image/jpg;base64,${requestScope.post.base64Image}" alt="">
                                        <!-- End Featured Image -->
                                        <!-- Post Content -->
                                        <div class="article-post">
                                            <p>${requestScope.post.description}</p>
                                            <div class="clearfix">
                                            </div>
                                        </div>
                            </div>

                            <!-- Post Date -->
                            <%
                                if (account != null) {
                            %>
                            <div class="d-flex flex-row-reverse">
                                <div class="dropdown">

                                    <i class="bi bi-three-dots fa-2x" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></i>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton" >
                                        <a class="dropdown-item" data-toggle="modal" data-target="#default-example-modal-center" href="/ProjectSWP/home?order=desc">Mute</a>
                                        <a class="dropdown-item" href="#">Report This Work</a>
                                    </div>
                                </div>
                               <div class="p-2"><a onclick="setLikeColor('${requestScope.post.pid}L')"  title="Like this work" class="flike" id="${requestScope.post.pid}L"><i class="fa fa-thumbs-up fa-2x"  id="${requestScope.post.pid}L"></i></a></div>

                            </div>
                            <%}
                            %>
                            <div class="d-flex flex-row">
                                <div class="p-2"><a title="Like" class="like"><i class="fa fa-thumbs-up"  id="${requestScope.post.pid}Li">     ${requestScope.post.like}</i></a></div>
                               <div class="p-2"><a title="View" class="view"><i class="bi bi-eye-fill"  id="${requestScope.post.pid}V">     ${requestScope.post.view}</i></a></div>
                            </div>
                            <p>
                                <small>
                                    <span class="post-date"><time class="post-date" datetime="2018-01-12">${requestScope.post.date}</time></span>
                                </small>
                            </p>

                            
                            <!-- Prev/Next -->
                            <!--					<div class="row PageNavigation mt-4 prevnextlinks">
                                                                            <div class="col-md-6 rightborder pl-0">
                                                                                    <a class="thepostlink" href="single.html"> Red Riding Hood</a>
                                                                            </div>
                                                                            <div class="col-md-6 text-right pr-0">
                                                                                    <a class="thepostlink" href="single-right-sidebar.html">We all wait for summer </a>
                                                                            </div>
                                                                    </div>-->
                            <!-- End Prev/Next -->
                            <!-- Author Box -->
                            <div class="row post-top-meta">
                                <div class="col-md-2">
                                    <img class="author-thumb" src="https://www.gravatar.com/avatar/b1cc14991db7a456fcd761680bbc8f81?s=250&amp;d=mm&amp;r=x" alt="John">
                                </div>
                                <div class="col-md-10">
                                    <a target="_blank" class="link-dark" href="#" id="authorname">${requestScope.post.fname}</a><br/>
                                    <span class="author-description">Among us</span>
                                </div>
                            </div>
                            <!-- Begin Comments================================================== -->
                            
                            <script>
                                var color = "";



                                <c:forEach items="${requestScope.likedPost}" var="l">
                                    <c:if test="${l.pid == requestScope.post.pid}">
                                var property = document.getElementById('${requestScope.post.pid}L');
                                color = property.style.color;
                                property.style.color = "blue";
                                    </c:if>
                                </c:forEach>
                            </script>


                            
                            
                            
                            

                            <!--End Comments ================================================== -->
                            <!-- End Post -->
                        </div>
                        <!-- End Article
            ================================================== -->
                    </div>
                </div>
                <!-- /.container -->
                <!-- Begin Footer
            ================================================== -->
                <footer class="footer">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="footer-widget">
                                    <a href="../../home/about"> 
                                        <img src="../../view/affiliates/assets/images/logo-footer.png" alt="logo footer" style="position: relative; width: 170px; left: 15px; background-size: contain;">
                                    </a>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="footer-widget">
                                    <h5 class="title">Resources</h5>
                                    <ul>
                                        <li><a target="_blank" href="https://m.do.co/c/84c9b45d0c47">Digital Ocean</a></li>
                                        <li><a target="blank" href="https://www.cloudways.com/en/pricing.php?id=153986&amp;a_bid=005da123">Cloudways</a></li>
                                        <li><a target="blank" href="https://shareasale.com/r.cfm?b=875645&amp;u=1087935&amp;m=41388&amp;urllink=&amp;afftrack=">Page Speed Test</a></li>
                                        <li><a target="blank" href="https://elementor.com/?ref=1556">Elementor Page Builder</a></li>
                                        <li><a target="blank" href="https://www.wowthemes.net/category/freebies/">Our Free Themes</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="footer-widget">
                                    <h5 class="title">Author</h5>
                                    <ul>
                                        <li><a href="https://www.wowthemes.net/premium-themes-templates/">About Us</a></li>
                                        <li><a target="_blank" href="https://www.wowthemes.net/affiliate-area/">Affiliates</a></li>
                                        <li><a href="https://www.wowthemes.net/terms-and-conditions/">License</a></li>
                                        <li><a href="https://www.wowthemes.net/blog/">Blog</a></li>
                                        <li><a href="https://www.wowthemes.net/support/">Contact</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="footer-widget textwidget">
                                    <h5 class="title">Download</h5>
                                    <p>
                                        Download "Affiliates" template and use it for your next project. If you have a question, a bug report, or if you simply want to say hi, <a href="https://www.wowthemes.net/support/">contact us here</a>.
                                    </p>
                                    <a href="https://gum.co/affiliates-html-template" target="_blank">Download</a>
                                </div>
                            </div>
                        </div>
                        <div class="copyright">
                            <p class="pull-left">
                                Copyright  2018 Affiliates HTMT Template
                            </p>
                            <p class="pull-right">
                                <!-- Leave credit to author unless you own a commercial license: https://www.wowthemes.net/freebies-license/ -->
                                <a target="_blank" href="https://www.wowthemes.net/affiliates-free-bootstrap-template/">"Affiliates Template"</a> - Design & Code by WowThemes.net
                            </p>
                            <div class="clearfix">
                            </div>
                        </div>
                    </div>
                </footer>
                <!-- End Footer
            ================================================== -->
            </div>
            <!-- JavaScript
            ================================================== -->
            <script src="https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.slim.min.js"></script>
            <script src="../../view/affiliates/assets/js/jquery.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
            <script src="../../view/affiliates/assets/js/ie10-viewport-bug-workaround.js"></script>
            <script src="../../view/affiliates/assets/js/masonry.pkgd.min.js"></script>
            <script src="../../view/affiliates/assets/js/theme.js"></script>
            <script src="../../view/affiliates/assets/js/pageScript.js"></script>
            <script src="../../view/affiliates/assets/inteltemplate/js/vendors.bundle.js"></script>
            <script src="../../view/affiliates/assets/inteltemplate/js/app.bundle.js"></script>
    </body>
</html>