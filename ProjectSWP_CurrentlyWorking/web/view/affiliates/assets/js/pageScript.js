/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function formSubmit() {

    $.ajax({
        url: 'localhost:8080/ProjectSWP/home/pinwork',
        data: $("#form1").serialize(),
        success: function (data) {
            $('#result').html(data);
        }
    });
}

function setPinColor(btn) {

    var property = document.getElementById(btn);

    const color = property.style.color;
    if (color === "red") {
        property.style.color = "#8F8F8F";
    } else {
        property.style.color = "red";
    }

}

function setLikeColor(btn) {

    var property = document.getElementById(btn);

    const color = property.style.color;
    if (color === "blue") {
        property.style.color = "#8F8F8F";
    } else {
        property.style.color = "blue";
    }
}



$(document).ready(function () {
    $('a.pin').on('click', function () {

        var that = $(this);
        url = "home/pinwork";
        method = "POST";
        workid = that.attr("id");
        var property = document.getElementById(workid);
        const color = property.style.color;
        if (color === "red") {
            $.ajax({
                url: url,
                type: method,
                data: { workid: workid, type : "insert"},
                success: function (response) {
                    console.log(response);
                }
            });
            //console.log("added");

        } else {
            $.ajax({
                url: url,
                type: method,
                data: { workid: workid, type : "delete"},
                success: function (response) {
                    console.log(response);
                }
            });
            //console.log("removed");
        }


        return false;
    });
});

$(document).ready(function () {
    $('a.like').on('click', function () {

        var that = $(this);
        url = "home/like";
        method = "POST";
        workid = that.attr("id");
        var property = document.getElementById(workid);
        const color = property.style.color;
        if (color === "blue") {
            $.ajax({
                url: url,
                type: method,
                data: { workid: workid, type : "insert"},
                success: function (response) {
                    var row = document.getElementById(workid + "i");
                    row.innerHTML = response;
                }
            });
            //console.log("added");

        } else {
            $.ajax({
                url: url,
                type: method,
                data: { workid: workid, type : "delete"},
                success: function (response) {
                    var row = document.getElementById(workid + "i");
                    row.innerHTML = response;
                }
            });
            //console.log("removed");
        }


        return false;
    });
});

function loadMore(){
        var amount = document.getElementsByClassName("post").length;
        var that = $(this);
        url = "profile";
        method = "POST";
        PostAmount = amount;
            $.ajax({
                url: url,
                type: method,
                data: { amount: PostAmount},
                success: function (response) {
                    console.log(response)
                    var row = document.getElementById("content");
                    row.innerHTML+=response;
                }
            });
        
        return false;
    }