<%-- 
    Document   : demohome
    Created on : May 13, 2022, 3:15:39 PM
    Author     : haiph
--%>

<%@page import="model.Account"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="icon" href="../view/affiliates/assets/images/logo-footer.png">
        <title>Beaucoup | An All-you-can-post page</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Rubik:400,400i,500,500i,700,700i" rel="stylesheet">
        <link href="../view/affiliates/assets/css/theme.css" rel="stylesheet">
        <%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
        <%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
        <%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
        <%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
        <%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
        <script src="../view/affiliates/assets/js/pinnedpostpaging.js" type="text/javascript"></script>
        <link href="../view/affiliates/assets/css/paging.css" rel="stylesheet" type="text/css"/>
        <!-- Begin tracking codes here, including ShareThis/Analytics -->

        <!-- End tracking codes here, including ShareThis/Analytics -->
    </head>
    <body class="layout-default">
        <!-- Begin Menu Navigation
        ================================================== -->
        <header class="navbar navbar-toggleable-md navbar-light bg-white fixed-top mediumnavigation">
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarsWow" aria-controls="navbarsWow" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="container">
                <!-- Begin Logo -->
                <a class="navbar-brand" href="../home">
                    <img src="../view/affiliates/assets/images/logo.png" alt="Affiliates - Free Bootstrap Template" style="position: relative; width: 170px; left: 15px; background-size: contain;">
                </a>
                <!-- End Logo -->
                <!-- Begin Menu -->
                <div class="collapse navbar-collapse" id="navbarsWow">
                    <!-- Begin Menu -->
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="../home">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="../home/about">About</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="../home/contact">Contact</a>
                        </li>
                        <%
                            Account account = (Account) request.getSession().getAttribute("account");
                            if (account == null) {
                        %>
                        <li class="nav-item">
                            <a class="nav-link highlight" href="home/login">Login</a>
                        </li>
                        <%} else {%>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img class="author-thumb" src="https://www.gravatar.com/avatar/b1cc14991db7a456fcd761680bbc8f81?s=250&amp;d=mm&amp;r=x" alt="John">
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                <a class="dropdown-item" href="../home">Dashboard</a>
                                <a class="dropdown-item" href ="../home/delete">Delete Work</a>
                                <a class="dropdown-item" href="../home/edit">Edit Profile</a>
                                <a class="dropdown-item" href="../home/logout">Log Out</a>
                            </div>
                        </li>
                        <%}%>
                    </ul>
                    <!-- End Menu -->
                </div>
            </div>
        </header>
        <!-- End Menu Navigation
        ================================================== -->
        <div class="site-content">
            <!-- Home Jumbotron
        ================================================== -->
            <section class="intro">
            </section>
            <!-- Container
        ================================================== -->
            <div class="container">
                <div class="main-content">
                    <div class="section-title">
                        <h2><span>Pinned Post</span></h2>
                    </div>
                    <div class="masonrygrid row listrecent">
                        <c:forEach items="${requestScope.post}" var="o">
                            <div class="col-md-6 grid-item">
                                <div class="card">
                                    <a href="../home/work?id=${o.ap.workid}">
                                        <img class="img-fluid" src="data:image/jpg;base64,${o.base64Image}" style="height: 250px; width: 500px">
                                    </a>
                                    <div class="card-block">
                                        <h2 class="card-title"><a>${o.name}</a></h2>
                                            <c:set var = "desc" value = "${o.description}"/>
                                            <c:if test="${fn:length(desc) < 70 }">
                                            <h4 class="card-text">${o.description}</h4>
                                        </c:if>
                                        <c:if test="${fn:length(desc) > 70 }">
                                            <c:set var = "shortdesc" value = "${fn:substring(desc, 0, 69)}"/>
                                            <h4 class="card-text">${shortdesc}...</h4>
                                        </c:if>
                                        <div class="metafooter">
                                            <div class="wrapfooter">
                                                <span class="meta-footer-thumb">
                                                    <img class="author-thumb" src="https://wompampsupport.azureedge.net/fetchimage?siteId=7575&v=2&jpgQuality=100&width=700&url=https%3A%2F%2Fi.kym-cdn.com%2Fphotos%2Fimages%2Fnewsfeed%2F002%2F111%2F316%2Fc57.gif" alt="Beaucoup admin">
                                                </span>
                                                <span class="author-meta">
                                                    <span class="post-name"><a target="_blank" href="#">${o.name}</a></span><br/>
                                                    <span class="post-date">${o.date}</span>
                                                </span>

                                                <span class="post-read-more">
                                                    <form action="pin" method="POST">
                                                        <input type="hidden" name="page" value="${requestScope.pageIndex}">
                                                        <input type="hidden" name="workid" value="${o.ap.workid}">
                                                        <button style="float:right" type="submit" class="btn btn-primary">Delete Pin</button></span>
                                                </form>


                                                    <div class="clearfix">
                                                    </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </c:forEach>
                        <!--                                 begin post 
                                                        <div class="col-md-6 grid-item">
                                                            <div class="card">
                                                                <a href="single.html">
                                                                    <img class="img-fluid" src="view/affiliates/assets/images/2.jpg" alt="Tree of Codes">
                                                                </a>
                                                                <div class="card-block">
                                                                    <h2 class="card-title"><a href="single.html">Tree of Codes</a></h2>
                                                                    <h4 class="card-text">The first mass-produced book to deviate from a rectilinear format, at least in the United States, is thought to be this 1863 edition of Red Riding Hood, cut into the...</h4>
                                                                    <div class="metafooter">
                                                                        <div class="wrapfooter">
                                                                            <span class="meta-footer-thumb">
                                                                                <img class="author-thumb" src="https://www.gravatar.com/avatar/e56154546cf4be74e393c62d1ae9f9d4?s=250&d=mm&r=x" alt="Sal">
                                                                            </span>
                                                                            <span class="author-meta">
                                                                                <span class="post-name"><a target="_blank" href="#">Sal</a></span><br/>
                                                                                <span class="post-date">12 Jan 2018</span>
                                                                            </span>
                                                                            <span class="post-read-more"><a href="single.html" title="Read Story"><i class="fa fa-link"></i></a></span>
                                                                            <div class="clearfix">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                         end post 
                                                         begin post 
                                                        <div class="col-md-6 grid-item">
                                                            <div class="card">
                                                                <a href="single.html">
                                                                    <img class="img-fluid" src="view/affiliates/assets/images/3.jpg" alt="Red Riding Hood">
                                                                </a>
                                                                <div class="card-block">
                                                                    <h2 class="card-title"><a href="single.html">Red Riding Hood</a></h2>
                                                                    <h4 class="card-text">The first mass-produced book to deviate from a rectilinear format, at least in the United States, is thought to be this 1863 edition of Red Riding Hood, cut into the...</h4>
                                                                    <div class="metafooter">
                                                                        <div class="wrapfooter">
                                                                            <span class="meta-footer-thumb">
                                                                                <img class="author-thumb" src="https://www.gravatar.com/avatar/e56154546cf4be74e393c62d1ae9f9d4?s=250&d=mm&r=x" alt="Sal">
                                                                            </span>
                                                                            <span class="author-meta">
                                                                                <span class="post-name"><a target="_blank" href="#">Sal</a></span><br/>
                                                                                <span class="post-date">12 Jan 2018</span>
                                                                            </span>
                                                                            <span class="post-read-more"><a href="single.html" title="Read Story"><i class="fa fa-link"></i></a></span>
                                                                            <div class="clearfix">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                         end post 
                                                         begin post 
                                                        <div class="col-md-6 grid-item">
                                                            <div class="card">
                                                                <a href="single.html">
                                                                    <img class="img-fluid" src="view/affiliates/assets/images/5.jpg" alt="Is Intelligence Enough">
                                                                </a>
                                                                <div class="card-block">
                                                                    <h2 class="card-title"><a href="single.html">Is Intelligence Enough</a></h2>
                                                                    <h4 class="card-text">Education must also train one for quick, resolute and effective thinking. To think incisively and to think for one’s self is very difficult. </h4>
                                                                    <div class="metafooter">
                                                                        <div class="wrapfooter">
                                                                            <span class="meta-footer-thumb">
                                                                                <img class="author-thumb" src="https://www.gravatar.com/avatar/e56154546cf4be74e393c62d1ae9f9d4?s=250&d=mm&r=x" alt="Sal">
                                                                            </span>
                                                                            <span class="author-meta">
                                                                                <span class="post-name"><a target="_blank" href="#">Sal</a></span><br/>
                                                                                <span class="post-date">12 Jan 2018</span>
                                                                            </span>
                                                                            <span class="post-read-more"><a href="single.html" title="Read Story"><i class="fa fa-link"></i></a></span>
                                                                            <div class="clearfix">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>-->
                        <!-- end post -->
                        <!-- begin post -->
                        <!--                                <div class="col-md-6 grid-item">
                                                            <div class="card">
                                                                <a href="single.html">
                                                                    <img class="img-fluid" src="view/affiliates/assets/images/6.jpg" alt="Markdown Example">
                                                                </a>
                                                                <div class="card-block">
                                                                    <h2 class="card-title"><a href="single.html">Markdown Example</a></h2>
                                                                    <h4 class="card-text">You’ll find this post in your _posts directory. Go ahead and edit it and re-build the site to see your changes. You can rebuild the site in many different ways,...</h4>
                                                                    <div class="metafooter">
                                                                        <div class="wrapfooter">
                                                                            <span class="meta-footer-thumb">
                                                                                <img class="author-thumb" src="https://www.gravatar.com/avatar/b1cc14991db7a456fcd761680bbc8f81?s=250&d=mm&r=x" alt="John">
                                                                            </span>
                                                                            <span class="author-meta">
                                                                                <span class="post-name"><a target="_blank" href="#">John</a></span><br/>
                                                                                <span class="post-date">11 Jan 2018</span>
                                                                            </span>
                                                                            <span class="post-read-more"><a href="single.html" title="Read Story"><i class="fa fa-link"></i></a></span>
                                                                            <div class="clearfix">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>-->
                        <!-- end post -->
                    </div>
                    <div class="bottompagination">
                                <div class="navigation">
                                    <nav class="pagination">
                                        <div id="container" class="paging"> </div>
                                        <% Integer totalPage = (Integer) request.getAttribute("onoOfPages");
                                           Integer pageIndex = (Integer) request.getAttribute("pageIndex");
                                        %>
                                        <script>
                                            paging("container",<%=pageIndex%>,<%=totalPage%>, 1);
                                        </script>
                                        <span class="page-number"> &nbsp; &nbsp; Page <%=pageIndex%> of <%=totalPage%> &nbsp; &nbsp; </span>
                                    </nav>
                                </div>
                            </div>
                </div>
            </div>
            <!-- /.container -->
            <!-- Before Footer
        ================================================== -->

            <!-- Begin Footer
        ================================================== -->
            <footer class="footer">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="footer-widget">
                                <a href="../home/about"> 
                                    <img src="../view/affiliates/assets/images/logo-footer.png" alt="logo footer" style="position: relative; width: 170px; left: 15px; background-size: contain;">
                                </a>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="footer-widget">
                                <h5 class="title">Author</h5>
                                <ul>
                                    <li><a href="#">About Us</a></li>
                                    <li><a target="_blank" href="https://www.wowthemes.net/affiliate-area/">Affiliates</a></li>
                                    <li><a href="https://www.wowthemes.net/terms-and-conditions/">License</a></li>
                                    <li><a href="https://www.wowthemes.net/blog/">Blog</a></li>
                                    <li><a href="contact.jsp">Contact</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="footer-widget textwidget">
                                <h5 class="title">Email us</h5>
                                <form action="https://formspree.io/wowthemesnet@gmail.com" method="POST">
                                    <input type="text" name="name" placeholder="Name">
                                    <input type="email" name="_replyto" placeholder="E-mail Address">
                                    <textarea rows="8" name="message" placeholder="Message"></textarea><br/>
                                    <input class="btn btn-success" type="submit" value="Send">
                                </form>                             
                            </div>
                        </div>
                    </div>
                    <div class="copyright">
                        <p class="pull-left">
                            Copyright © 2018 Affiliates HTML Template
                        </p>
                        <p class="pull-right">
                            <!-- Leave credit to author unless you own a commercial license: https://www.wowthemes.net/freebies-license/ -->
                            <a target="_blank" href="https://www.wowthemes.net/affiliates-free-bootstrap-template/">"Affiliates Template"</a> - Design & Code by WowThemes.net
                        </p>
                        <div class="clearfix">
                        </div>
                    </div>
                </div>
            </footer>
            <!-- End Footer
        ================================================== -->
        </div>


        <!-- JavaScript
        ================================================== -->
        <script src="../view/affiliates/assets/js/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
        <script src="../view/affiliates/assets/js/ie10-viewport-bug-workaround.js"></script>
        <script src="../view/affiliates/assets/js/masonry.pkgd.min.js"></script>
        <script src="../view/affiliates/assets/js/theme.js"></script>
        <script src="../view/affiliates/assets/js/pageScript.js"></script>
        <script>
                                    $("#image").change(function ()
                                    {
                                        var Data = document.getElementById('image');
                                        var FileUploadPath = Data.value;

                                        if (FileUploadPath != '') {
                                            var Extension = FileUploadPath.substring(FileUploadPath.lastIndexOf('.') + 1).toLowerCase();
                                            //The file uploaded is an image

                                            if (Extension == "gif" || Extension == "png" || Extension == "bmp"
                                                    || Extension == "jpeg" || Extension == "jpg") {
                                                return true;
                                            } else if (Extension != 'gif' || Extension != 'png' || Extension != 'bmp' || Extension != 'jpeg' || Extension != 'jpg') {

                                                alert("Photo only allows file types of GIF, PNG, JPG, JPEG and BMP. ");
                                                Data.type = '';
                                                Data.type = 'file';
                                                return false;
                                            }
                                        }
                                    });
        </script>
    </body>
</html>
