/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package validator;

import dal.AccountDBContext;

/**
 *
 * @author admin
 */
public class Validator {
    public String checkMobile(String mobile){
        String validMobile = "[0-9]+";
        if (!mobile.matches(validMobile)) return "Mobile format is incorrect!";
        else return null;
    }
    public String checkUsername(String username){
        AccountDBContext db = new AccountDBContext();
        if (db.checkUsernameExist(username)!=0) return "This username already exists!";
        else return null;
    }
    public String checkEmail(String email){
        String validEmail = "^(?=.{1,64}@)[A-Za-z0-9_-]+(\\\\.[A-Za-z0-9_-]+)*@[^-][A-Za-z0-9-]+(\\\\.[A-Za-z0-9-]+)*(\\\\.[A-Za-z]{2,})$";
        if (!email.matches(validEmail)) return "Email formant is incorrect!";
        else return null;
    }
    
    
    //Check for premium role
    public void PremiumRoleChecker(String username){
        AccountDBContext adb = new AccountDBContext();
        boolean IsValid = adb.IsPremium(username);
        if (IsValid == true){
            if (adb.getRole(username, "1") < 0){
            adb.AddAccountRole("1", username);
            }
        } else if (IsValid == false){
            adb.RemoveSubcription(username);
            adb.RemoveAccountRole("1", username);
        }
    }
    public boolean IsPremium(String username){
        AccountDBContext adb = new AccountDBContext();
        boolean IsValid = adb.IsPremium(username);
        return IsValid == true;
    }
}
