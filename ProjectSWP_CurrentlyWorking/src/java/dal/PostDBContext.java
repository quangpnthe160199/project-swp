/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dal;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.Part;
import model.AccountPost;
import model.Post;
import java.util.Base64;

import java.util.UUID;
import model.Account;
import model.Comment;
import model.PinPost;

/**
 *
 * @author BK
 */
public class PostDBContext extends DBContext {

    ResultSet rs = null;
    PreparedStatement stm = null;

    public ArrayList<Post> GetWorks() throws IOException {
        ArrayList<Post> pro = new ArrayList<>();
        try {
            String sql = "SELECT workid, [name], [description], img, publishdate, [like] FROM Work";

            PreparedStatement stm = connection.prepareStatement(sql);
            rs = stm.executeQuery();
            while (rs.next()) {
                Post p = new Post();
                AccountPost Ap = new AccountPost();
                Ap.setWorkid(rs.getString("workid"));
                p.setName(rs.getString("name"));
                p.setDescription(rs.getString("description"));

                Blob blob = rs.getBlob("img");

                InputStream inputStream = blob.getBinaryStream();
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                byte[] buffer = new byte[4096];
                int bytesRead = -1;

                while ((bytesRead = inputStream.read(buffer)) != -1) {
                    outputStream.write(buffer, 0, bytesRead);
                }

                byte[] imageBytes = outputStream.toByteArray();
                String base64Image = Base64.getEncoder().encodeToString(imageBytes);

                inputStream.close();
                outputStream.close();
                p.setLike(rs.getInt("like"));
                p.setBase64Image(base64Image);

                p.setDate(rs.getDate("publishdate"));
                p.setAp(Ap);
                pro.add(p);
            }
        } catch (SQLException ex) {
            Logger.getLogger(PostDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return pro;
    }

    public ArrayList<Post> GetWork(int pageIndex) throws IOException {
        ArrayList<Post> pro = new ArrayList<>();
        try {
            String sql = "SELECT workid, [name], [description], img, publishdate, [like] FROM Work\n"
                    + "ORDER BY publishdate DESC \n"
                    + "OFFSET 4*(? - 1) ROWS \n"
                    + "FETCH FIRST 4 ROWS ONLY;";

            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, pageIndex);
            ResultSet rs = stm.executeQuery();

            while (rs.next()) {
                Post p = new Post();
                AccountPost Ap = new AccountPost();
                Ap.setWorkid(rs.getString("workid"));
                p.setName(rs.getString("name"));
                p.setDescription(rs.getString("description"));

                Blob blob = rs.getBlob("img");

                InputStream inputStream = blob.getBinaryStream();
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                byte[] buffer = new byte[4096];
                int bytesRead = -1;

                while ((bytesRead = inputStream.read(buffer)) != -1) {
                    outputStream.write(buffer, 0, bytesRead);
                }

                byte[] imageBytes = outputStream.toByteArray();
                String base64Image = Base64.getEncoder().encodeToString(imageBytes);

                inputStream.close();
                outputStream.close();
                p.setLike(rs.getInt("like"));
                p.setBase64Image(base64Image);

                p.setDate(rs.getDate("publishdate"));
                p.setAp(Ap);
                pro.add(p);
            }
        } catch (SQLException ex) {
            Logger.getLogger(PostDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return pro;
    }

    public ArrayList<Post> GetUserPinned(int page, String username) throws IOException {
        ArrayList<Post> pro = new ArrayList<>();
        try {
            String sql = "SELECT c.workid 'workid', [name], [description], img, publishdate, [like] FROM Work A JOIN AccountWork B\n"
                    + "ON A.workid = B.workid\n"
                    + "JOIN SavedPost C\n"
                    + "ON A.workid = C.workid\n"
                    + "WHERE c.username = ?\n"
                    + "ORDER BY publishdate DESC\n"
                    + "OFFSET 4*(? - 1) ROWS \n"
                    + "FETCH FIRST 4 ROWS ONLY;";

            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, username);
            stm.setInt(2, page);
            rs = stm.executeQuery();
            while (rs.next()) {
                Post p = new Post();
                AccountPost Ap = new AccountPost();
                Ap.setWorkid(rs.getString("workid"));
                p.setName(rs.getString("name"));
                p.setDescription(rs.getString("description"));

                Blob blob = rs.getBlob("img");

                InputStream inputStream = blob.getBinaryStream();
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                byte[] buffer = new byte[4096];
                int bytesRead = -1;

                while ((bytesRead = inputStream.read(buffer)) != -1) {
                    outputStream.write(buffer, 0, bytesRead);
                }

                byte[] imageBytes = outputStream.toByteArray();
                String base64Image = Base64.getEncoder().encodeToString(imageBytes);

                inputStream.close();
                outputStream.close();
                p.setLike(rs.getInt("like"));
                p.setBase64Image(base64Image);

                p.setDate(rs.getDate("publishdate"));
                p.setAp(Ap);
                pro.add(p);
            }
        } catch (SQLException ex) {
            Logger.getLogger(PostDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return pro;
    }

    public ArrayList<Post> GetProfileWork(int pageIndex, String username) throws IOException {
        ArrayList<Post> pro = new ArrayList<>();
        try {
            String sql = "SELECT workid, [name], [description], img, publishdate, [like] FROM Work\n"
                    + "ORDER BY publishdate DESC \n"
                    + "OFFSET 4*(? - 1) ROWS \n"
                    + "FETCH FIRST 4 ROWS ONLY;";

            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, pageIndex);
            ResultSet rs = stm.executeQuery();

            while (rs.next()) {
                Post p = new Post();
                AccountPost Ap = new AccountPost();
                Ap.setWorkid(rs.getString("workid"));
                p.setName(rs.getString("name"));
                p.setDescription(rs.getString("description"));

                Blob blob = rs.getBlob("img");

                InputStream inputStream = blob.getBinaryStream();
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                byte[] buffer = new byte[4096];
                int bytesRead = -1;

                while ((bytesRead = inputStream.read(buffer)) != -1) {
                    outputStream.write(buffer, 0, bytesRead);
                }

                byte[] imageBytes = outputStream.toByteArray();
                String base64Image = Base64.getEncoder().encodeToString(imageBytes);

                inputStream.close();
                outputStream.close();
                p.setLike(rs.getInt("like"));
                p.setBase64Image(base64Image);

                p.setDate(rs.getDate("publishdate"));
                p.setAp(Ap);
                pro.add(p);
            }
        } catch (SQLException ex) {
            Logger.getLogger(PostDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return pro;
    }

    public ArrayList<Post> GetProfileWorkPage(int pageIndex, String username) throws IOException {
        ArrayList<Post> pro = new ArrayList<>();
        try {
            String sql = "SELECT A.workid, [name], [description], img, publishdate, [like] FROM Work A JOIN AccountWork B\n"
                    + "ON A.workid = B.workid\n"
                    + "WHERE username = ?\n"
                    + "ORDER BY publishdate DESC\n"
                    + "OFFSET 4*(? - 1) ROWS\n"
                    + "FETCH FIRST 4 ROWS ONLY";

            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, username);
            stm.setInt(2, pageIndex);
            ResultSet rs = stm.executeQuery();

            while (rs.next()) {
                Post p = new Post();
                AccountPost Ap = new AccountPost();
                Ap.setWorkid(rs.getString("workid"));
                p.setName(rs.getString("name"));
                p.setDescription(rs.getString("description"));

                Blob blob = rs.getBlob("img");

                InputStream inputStream = blob.getBinaryStream();
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                byte[] buffer = new byte[4096];
                int bytesRead = -1;

                while ((bytesRead = inputStream.read(buffer)) != -1) {
                    outputStream.write(buffer, 0, bytesRead);
                }

                byte[] imageBytes = outputStream.toByteArray();
                String base64Image = Base64.getEncoder().encodeToString(imageBytes);

                inputStream.close();
                outputStream.close();
                p.setLike(rs.getInt("like"));
                p.setBase64Image(base64Image);

                p.setDate(rs.getDate("publishdate"));
                p.setAp(Ap);
                pro.add(p);
            }
        } catch (SQLException ex) {
            Logger.getLogger(PostDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return pro;
    }

    public ArrayList<PinPost> GetPin(String username) {
        ArrayList<PinPost> pin = new ArrayList<>();
        try {
            String sql = "SELECT username, workid FROM SavedPost\n"
                    + "WHERE username = ?";

            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, username);
            rs = stm.executeQuery();
            while (rs.next()) {
                Account a = new Account();
                AccountPost AP = new AccountPost();
                PinPost SP = new PinPost();
                a.setUsername(rs.getString("username"));
                AP.setWorkid(rs.getString("workid"));
                SP.setA(a);
                SP.setAp(AP);
                pin.add(SP);
            }
        } catch (SQLException ex) {
            Logger.getLogger(PostDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return pin;
    }

    public Post AddPin(String username, String WorkID) {
        try {

            String sql = "INSERT INTO SavedPost (username, workid)\n"
                    + "VALUES (?, ?)";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, username);
            stm.setString(2, WorkID);
            stm.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(PostDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public ArrayList<PinPost> GetLike(String username) {
        ArrayList<PinPost> like = new ArrayList<>();
        try {
            String sql = "SELECT username, workid FROM PostLike\n"
                    + "WHERE username = ?";

            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, username);
            rs = stm.executeQuery();
            while (rs.next()) {
                Account a = new Account();
                AccountPost AP = new AccountPost();
                PinPost SP = new PinPost();
                a.setUsername(rs.getString("username"));
                AP.setWorkid(rs.getString("workid"));
                SP.setA(a);
                SP.setAp(AP);
                like.add(SP);
            }
        } catch (SQLException ex) {
            Logger.getLogger(PostDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return like;
    }

    public Post AddLike(String username, String WorkID) {
        try {

            String sql = "INSERT INTO PostLike (username, workid)\n"
                    + "VALUES (?, ?)";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, username);
            stm.setString(2, WorkID);
            stm.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(PostDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public Post DeleteLike(String username, String WorkID) {
        try {

            String sql = "DELETE FROM PostLike\n"
                    + "WHERE username = ? AND workid = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, username);
            stm.setString(2, WorkID);
            stm.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(PostDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public Post DeletePin(String username, String WorkID) {
        try {

            String sql = "DELETE FROM SavedPost\n"
                    + "WHERE username = ? AND workid = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, username);
            stm.setString(2, WorkID);
            stm.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(PostDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public ArrayList<Post> GetWorkWname(String workid) throws IOException {
        ArrayList<Post> pro = new ArrayList<>();
        try {
            String sql = "SELECT workid, [name], [description], img, publishdate, [like] FROM Work\n"
                    + "WHERE [name] LIKE ?";

            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, "%" + workid + "%");
            rs = stm.executeQuery();
            while (rs.next()) {
                Post p = new Post();
                AccountPost Ap = new AccountPost();
                Ap.setWorkid(rs.getString("workid"));
                p.setName(rs.getString("name"));
                p.setDescription(rs.getString("description"));

                Blob blob = rs.getBlob("img");

                InputStream inputStream = blob.getBinaryStream();
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                byte[] buffer = new byte[4096];
                int bytesRead = -1;

                while ((bytesRead = inputStream.read(buffer)) != -1) {
                    outputStream.write(buffer, 0, bytesRead);
                }

                byte[] imageBytes = outputStream.toByteArray();
                String base64Image = Base64.getEncoder().encodeToString(imageBytes);

                inputStream.close();
                outputStream.close();
                p.setLike(rs.getInt("like"));
                p.setBase64Image(base64Image);

                p.setDate(rs.getDate("publishdate"));
                p.setAp(Ap);
                pro.add(p);
            }
        } catch (SQLException ex) {
            Logger.getLogger(PostDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return pro;
    }

    public Post AddPost(String WorkID, String title, String description, InputStream img) {
        try {

            String sql = "INSERT INTO Work (workid, [name], [description], img, publishdate, [like])\n"
                    + "VALUES (?, ?, ?, ?, GETDATE(), 0)";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, WorkID);
            stm.setString(2, title);
            stm.setString(3, description);
            stm.setBlob(4, img);
            stm.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(PostDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public int GetNoOfRecord() {
        int total = 0;
        try {
            String sql = "SELECT COUNT(*) 'Total' FROM [Work]";
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();

            if (rs.next()) {
                total = rs.getInt("Total");
            }
        } catch (SQLException ex) {
            Logger.getLogger(PostDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return total;
    }

    public int GetNoOfUserRecord(String username) {
        int total = 0;
        try {
            String sql = "SELECT COUNT(*) 'Total' FROM [Work] A JOIN AccountWork B\n"
                    + "ON A.workid = B.workid\n"
                    + "WHERE username = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, username);
            ResultSet rs = stm.executeQuery();

            if (rs.next()) {
                total = rs.getInt("Total");
            }
        } catch (SQLException ex) {
            Logger.getLogger(PostDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return total;
    }

    public void deletePost(String WorkID) {
        String sql = "DELETE FROM Work WHERE workid = ?";
        PreparedStatement stm = null;
        try {
            stm = connection.prepareStatement(sql);
            stm.setString(1, WorkID);

            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(PostDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void deleteAccountPost(String WorkID) {
        String sql = "DELETE FROM AccountWork WHERE workid = ?";
        PreparedStatement stm = null;
        try {
            stm = connection.prepareStatement(sql);
            stm.setString(1, WorkID);

            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(PostDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public ArrayList<Post> GetWorks(String workid) throws IOException {
        ArrayList<Post> pro = new ArrayList<>();
        try {
            String sql = "SELECT workid, [name], [description], img, publishdate FROM Work\n"
                    + "WHERE workid = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, workid);

            ResultSet rs = stm.executeQuery();

            rs = stm.executeQuery();

            while (rs.next()) {
                Post p = new Post();
                AccountPost Ap = new AccountPost();
                Ap.setWorkid(rs.getString("workid"));
                p.setName(rs.getString("name"));
                p.setDescription(rs.getString("description"));

                Blob blob = rs.getBlob("img");

                InputStream inputStream = blob.getBinaryStream();
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                byte[] buffer = new byte[4096];
                int bytesRead = -1;

                while ((bytesRead = inputStream.read(buffer)) != -1) {
                    outputStream.write(buffer, 0, bytesRead);
                }

                byte[] imageBytes = outputStream.toByteArray();
                String base64Image = Base64.getEncoder().encodeToString(imageBytes);

                inputStream.close();
                outputStream.close();
                p.setLike(rs.getInt("like"));
                p.setBase64Image(base64Image);

                p.setDate(rs.getDate("publishdate"));
                p.setAp(Ap);
                pro.add(p);
            }
        } catch (SQLException ex) {
            Logger.getLogger(PostDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return pro;
    }

    public Post GetWork(String workid) throws IOException {
        try {

            String sql = "SELECT workid, [name], [description], img, publishdate, [like] FROM Work\n"
                    + "WHERE workid = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, workid);
            ResultSet rs = stm.executeQuery();
            if (rs.next()) {
                Post p = new Post();
                AccountPost Ap = new AccountPost();
                Ap.setWorkid(rs.getString("workid"));
                p.setName(rs.getString("name"));
                p.setDescription(rs.getString("description"));

                Blob blob = rs.getBlob("img");

                InputStream inputStream = blob.getBinaryStream();
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                byte[] buffer = new byte[4096];
                int bytesRead = -1;

                while ((bytesRead = inputStream.read(buffer)) != -1) {
                    outputStream.write(buffer, 0, bytesRead);
                }

                byte[] imageBytes = outputStream.toByteArray();
                String base64Image = Base64.getEncoder().encodeToString(imageBytes);

                inputStream.close();
                outputStream.close();
                p.setLike(rs.getInt("like"));
                p.setBase64Image(base64Image);

                p.setDate(rs.getDate("publishdate"));
                p.setAp(Ap);
                return p;
            }
        } catch (SQLException ex) {
            Logger.getLogger(PostDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public String getWorkUsername(String workid) {
        try {
            String sql = "select username from AccountWork \n"
                    + "where workid=?";
            PreparedStatement stm = connection.prepareCall(sql);
            stm.setString(1, workid);
            ResultSet rs = stm.executeQuery();

            while (rs.next()) {
                String username = rs.getString("username");
                return username;
            }

        } catch (SQLException ex) {
            Logger.getLogger(PostDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public ArrayList<Comment> getWorkComments(String workid) throws IOException {
        ArrayList<Comment> comments = new ArrayList<>();
        try {
            String sql = "SELECT cid, workid ,username, content FROM WorkComment \n"
                    + "WHERE workid = ?";

            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, workid);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Comment c = new Comment();
                AccountPost ap = new AccountPost();
                Account acc = new Account();
                ap.setWorkid(rs.getString("workid"));
                acc.setUsername(rs.getString("username"));
                c.setCid(rs.getString("cid"));
                c.setContent(rs.getString("content"));
                c.setAp(ap);
                c.setAcc(acc);
                comments.add(c);
            }
        } catch (SQLException ex) {
            Logger.getLogger(PostDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return comments;
    }

    public void addComment(String workid, String username, String content) throws IOException {
        try {
            String sql = "INSERT INTO WorkComment (cid, workid, [username], [content])\n"
                    + "VALUES (?, ?, ?, ?)";
            String uniqueID = UUID.randomUUID().toString();
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, uniqueID);
            stm.setString(2, workid);
            stm.setString(3, username);
            stm.setString(4, content);
            stm.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(PostDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
