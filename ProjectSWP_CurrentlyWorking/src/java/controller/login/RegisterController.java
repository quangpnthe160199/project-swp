/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.login;

import dal.AccountDBContext;
import dal.QuestionDBContext;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Account;
import model.Question;
import model.UserAnswer;
import validator.Validator;

/**
 *
 * @author admin
 */
public class RegisterController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        QuestionDBContext qdb = new QuestionDBContext();
        ArrayList<Question> questions = qdb.getQuestions();
        request.setAttribute("questions", questions);

        request.getRequestDispatcher("/view/affiliates/register.jsp").forward(request, response);

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        String username = request.getParameter("username");
        String displayname = request.getParameter("displayname");
        String password = request.getParameter("password");
        String fullname = request.getParameter("fullname");
        String mobile = request.getParameter("mobile");
        String email = request.getParameter("email");
        String address = request.getParameter("address");
        int gender = Integer.parseInt(request.getParameter("gender"));

        Validator v = new Validator();
        if (v.checkUsername(username) != null || v.checkMobile(mobile) != null
                || v.checkEmail(email) != null) {
            if (v.checkUsername(username) != null) {
                request.setAttribute("msg", v.checkUsername(username));
            } else if (v.checkMobile(mobile) != null) {
                request.setAttribute("msg", v.checkMobile(mobile));
            } else {
                request.setAttribute("msg", v.checkEmail(email));
            }
            request.setAttribute("username", username);
            request.setAttribute("displayname", displayname);
            request.setAttribute("password", password);
            request.setAttribute("fullname", fullname);
            request.setAttribute("mobile", mobile);
            request.setAttribute("email", email);
            request.setAttribute("address", address);
            request.setAttribute("gender", gender);
            doGet(request, response);
        } else {

            AccountDBContext db = new AccountDBContext();
            db.addAccount(username, password, displayname);
            db.addAccountDetail(username, fullname, gender, mobile, email, address);

            String answer = request.getParameter("answer");

            String qid = request.getParameter("q_list");

            QuestionDBContext adb = new QuestionDBContext();
            adb.addAnswer(username, qid, answer);

            request.setAttribute("qid", qid);

//        Account acc = new Account();
//        acc.setUsername(username);
//        acc.setPassword(password);
//        request.getSession().setAttribute("account", acc);
            response.sendRedirect(request.getContextPath() + "/home");
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
