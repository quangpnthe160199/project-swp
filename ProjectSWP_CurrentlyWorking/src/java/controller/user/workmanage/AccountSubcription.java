/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.user.workmanage;

import com.vnpay.common.Config;
import dal.AccountDBContext;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Account;

/**
 *
 * @author BK
 */
public class AccountSubcription extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Map fields = new HashMap();
        AccountDBContext adb = new AccountDBContext();
        for (Enumeration params = request.getParameterNames(); params.hasMoreElements();) {
            String fieldName = (String) params.nextElement();
            String fieldValue = request.getParameter(fieldName);
            if ((fieldValue != null) && (fieldValue.length() > 0)) {
                fields.put(fieldName, fieldValue);
            }
        }
        String username = request.getParameter("username");
        String vnp_SecureHash = request.getParameter("vnp_SecureHash");
        if (fields.containsKey("vnp_SecureHashType")) {
            fields.remove("vnp_SecureHashType");
        }
        if (fields.containsKey("vnp_SecureHash")) {
            fields.remove("vnp_SecureHash");
        }
        String signValue = Config.hashAllFields(fields);
        String PurchaseID = request.getParameter("vnp_TxnRef");
        int Amount = Integer.parseInt(request.getParameter("vnp_Amount"));
        String Info = request.getParameter("vnp_OrderInfo");
        String[] AccountOrder = Info.split("_", 5);
        username = AccountOrder[0];
        String type = AccountOrder[1];
        request.getParameter("vnp_ResponseCode");
        request.getParameter("vnp_TransactionNo");
        String bankcode = request.getParameter("vnp_BankCode");
        request.getParameter("vnp_PayDate");
        
        if (signValue.equals(vnp_SecureHash)) {
            if ("00".equals(request.getParameter("vnp_ResponseCode"))) {
                //response.getWriter().print("GD Thanh cong");
                //response.getWriter().write(PurchaseID + " " + username + " " + Amount + " " + type + " " + bankcode + " " + type.equalsIgnoreCase("1month") + " " + type.equalsIgnoreCase("6month") + " " + type.equalsIgnoreCase("12month"));
                adb.AccountPurchase(PurchaseID, username, Amount, type, bankcode);
                if (type.equalsIgnoreCase("1month")){
                    adb.AccountSubcription(PurchaseID, username, 1);
                } else if (type.equalsIgnoreCase("6month")){
                    adb.AccountSubcription(PurchaseID, username, 6);
                } else if (type.equalsIgnoreCase("12month")){
                    adb.AccountSubcription(PurchaseID, username, 12);
                }

            } else {
                response.getWriter().print("Purchase failed");
            }

        } else {
            response.getWriter().print("ReponseCode in unavailible");
        }

        response.sendRedirect( request.getContextPath() + "/home");
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
