/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.user.usermanage;

import dal.AccountDBContext;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Account;
import model.AccountDetail;

/**
 *
 * @author admin
 */
public class ProfileEditController extends HttpServlet {

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Account acc = (Account)request.getSession().getAttribute("account");
        String username = acc.getUsername();
        String password = acc.getPassword();
        String displayname = acc.getDisplayname();
        
        request.setAttribute("username", username);
        request.setAttribute("password", password);
        request.setAttribute("displayname", displayname);
        AccountDBContext db = new AccountDBContext();
        AccountDetail ad = db.getAccountDetail(username);
        request.setAttribute("address", ad.getAddress());
        request.setAttribute("email", ad.getEmail());
        request.setAttribute("fullname", ad.getFullname());
        request.setAttribute("gender", ad.getGender());
        request.setAttribute("mobile", ad.getMobile());
        
        
        request.getRequestDispatcher("/view/affiliates/editprofile/editprofile.jsp").forward(request, response);       
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String username = request.getParameter("username");
        String newPassword = request.getParameter("password");
        String newDisplayName = request.getParameter("displayname");
        String fullname = request.getParameter("fullname");
        String mobile = request.getParameter("mobile");
        String email = request.getParameter("email");
        String address = request.getParameter("address");
        int gender = Integer.parseInt(request.getParameter("gender"));
        
        AccountDBContext db = new AccountDBContext();
        db.updateAccount(username, newPassword, newDisplayName);
        AccountDBContext db2 = new AccountDBContext();
        db2.updateAccountDetail(username,fullname, mobile, email, address,gender);
        
        Account acc = new Account();
        acc.setUsername(username);
        acc.setPassword(newPassword);
        acc.setDisplayname(newDisplayName);
        request.getSession().setAttribute("account", acc);
        response.sendRedirect( request.getContextPath() + "/home");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
