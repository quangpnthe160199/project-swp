/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.homepage;

import dal.PostDBContext;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Account;
import model.Comment;
import model.Post;

/**
 *
 * @author BK
 */
@WebServlet(name = "ViewWork", urlPatterns = {"/home/works"})
public class ViewWork extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PostDBContext pdb = new PostDBContext();
        Post post = new Post();
        ArrayList<Comment> comments = new ArrayList<>();
        if (request.getParameter("id") != null) {
            String workid = request.getParameter("id");
            post = pdb.GetWork(workid);
            comments = pdb.getWorkComments(workid);
            request.setAttribute("workid", workid);
        }      
        request.setAttribute("comments", comments);
        request.setAttribute("post", post);
        request.getRequestDispatcher("/view/affiliates/viewwork.jsp").forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String workid = request.getParameter("workid");
        String content = request.getParameter("comment");
        Account account = (Account) request.getSession().getAttribute("account");
        if(account != null){
        String username = account.getUsername();
        PostDBContext pdb = new PostDBContext();
        pdb.addComment(workid, username, content);
        }
        
        response.sendRedirect("/ProjectSWP/home/work?id="+workid);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
