/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.admin.workmanage;

import controller.admin.BaseAuthController;
import dal.PostDBContext;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Post;

/**
 *
 * @author Dell
 */
public class AdminWorkManagementController extends BaseAuthController {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void processGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PostDBContext db = new PostDBContext();
        int pageIndex;
        try {
            pageIndex = Integer.parseInt(request.getParameter("page"));
        } catch (NumberFormatException e) {
            pageIndex = 1;
        }
        int recordsPerPage = 4;
        ArrayList<Post> works = db.GetWork(pageIndex);
        ArrayList<String> usernames = new ArrayList<>();
        for (Post w : works) {
            usernames.add(db.getWorkUsername(w.getAp().getWorkid()));
        }
        int onoOfRecords = db.GetNoOfRecord();
        int onoOfPages = (int) Math.ceil(onoOfRecords * 1.0 / recordsPerPage);
        request.setAttribute("pageIndex", pageIndex);
        request.setAttribute("onoOfRecords", onoOfRecords);
        request.setAttribute("onoOfPages", onoOfPages);
        request.setAttribute("usernames", usernames);
        request.setAttribute("works", works);
        request.getRequestDispatcher("/view/affiliates/adminmanagework.jsp").forward(request, response);

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void processPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
