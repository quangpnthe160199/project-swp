/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.admin;

import dal.AccountDBContext;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Account;

/**
 *
 * @author admin
 */
public abstract class BaseAuthController extends HttpServlet{
    private boolean isAuthenticated(HttpServletRequest request)
    {
        Account account = (Account) request.getSession().getAttribute("account");
        if(account == null)
            return false;
        else
        {
            String url = request.getServletPath();
            AccountDBContext db = new AccountDBContext();
            int permission = db.getAdminPermission(account.getUsername(), url);
            return permission >0;
        }
    }
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if(isAuthenticated(request))
        {
            processGet(request, response);
        }
        else
        {
            response.sendRedirect("/ProjectSWP/home");
        }
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if(isAuthenticated(request))
        {
            //business
            processPost(request, response);
        }
        else
        {
            response.sendRedirect("/ProjectSWP/home");
        }
    }

    protected abstract void processGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException;

    protected abstract void processPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException;
    //To change body of generated methods, choose Tools | Templates.

    
    
}
